Important Readme

Q.1# How to work on local or hosted environment for development?

1. To enable CSS, Jquery and other Assets on localhost move all below folders to "Public" Folder under webroot folder.

css
fonts
img
js
uploads

2. Make below varriables to below values.

$css_root_path = $root_path;		
$asset_root_path = $root_path;


Q.2# How to set Assets URL in Jquery file "root_path/public/js/script.js"?
	See for "window.asset_url" variable and replace values in second line in script.js.
	
	a. If hosted in AWS S3 bucket for Lamda.
		window.asset_url = 'https://S3_bucket_url/assets/';
	
	b. If hosted on local environment like Xampp/Wamp/cpanel etc.
		window.asset_url = '/public/';
		
		
		
Q.3# How to move development to hosted environment if using S3 bucket for AWS Lamda only?

1. To enable CSS, Jquery and other Assets on Lamda application create bucket named "assets" and move all folders to "assets" bucket.

css
fonts
img
js
uploads

2. Make below varriables to match below values.

$css_root_path = 'https://S3_bucket_url/assets/';
$asset_root_path = 'https://S3_bucket_url/assets/';