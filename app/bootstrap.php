<?php
	// Load config
	require_once 'config/config.php';

	require_once 'helpers/helper_functions.php';
	require_once 'helpers/session_helper.php';

	// Autoload classes
	spl_autoload_register(function($className){
		require_once 'libraries/' . $className . '.php';
	});