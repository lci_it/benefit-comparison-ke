<?php

/**
 * 
 */
class Timezone {
	private $db;

	public function __construct(){
			$this->db = new Database;
			$this->db->table = 'timezones';
	}
	/*--------------------------------------------------------------------*/

	public function TimeZoneList(){
		$output = '';
		$list = $this->db->selectAll();
		foreach($list as $record){
			$output .= "<option value='$record->id'>" . $record->name . "</option>";
		}
		return $output;
	}
	/*--------------------------------------------------------------------*/

	public static function UserTimeZone($date){
	 	$date_entered = new DateTime($date, new DateTimeZone('Asia/Muscat'));
	 	$date_entered->setTimezone(new DateTimeZone($_SESSION['user_time_zone']));
	 	$date_entered = $date_entered->format('d-m-Y h:i:s A');

	 	if(!empty($date) && isset($date)){
	 		return  $date_entered;
	 	} else {
	 		return null;
	 	}
	}
	/*--------------------------------------------------------------------*/






} // end of class