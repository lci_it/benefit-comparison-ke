<?php

class Upload {
	public static $image;
	public static $image_dir = 'img';

	public $module;
	public $last_insert_id;

	public static function saveImage3(){
		ob_get_contents();
		ob_end_clean();
		// ini_set('display_errors', 'Off');
		$upload_errors= array(

			UPLOAD_ERR_OK				=>	'There is no error',
			UPLOAD_ERR_INI_SIZE		=> 'Upload Failed - The uploaded file exceeds the MAXIMUM allowed size 2MB',
			UPLOAD_ERR_FORM_SIZE 	=> 'Upload Failed - The uploaded file exceeds the MAXIMUM SIZE 2MB',
			UPLOAD_ERR_PARTIAL		=>	'The uploaded file was on ly partially uploaded.',
			UPLOAD_ERR_NO_FILE		=>	'No file was uploaded',
			UPLOAD_ERR_NO_TMP_DIR	=>	'Missing a temporary folder',
			UPLOAD_ERR_CANT_WRITE	=>	'Failed to write file to disk',
			UPLOAD_ERR_EXTENSION		=>	'A PHP extension stopped the file upload'
		);

		$the_message = array();

		if($_SERVER['REQUEST_METHOD'] == 'POST' && empty($_POST) && empty($_FILES) && $_SERVER['CONTENT_LENGTH'] > 0 ){
			$_SESSION['info'] = $upload_errors[UPLOAD_ERR_INI_SIZE];
		}

		if(isset($_POST['submit']) ){
			$extensions = array("jpeg","jpg","png", "gif", "pjpeg");
			$directory = 'uploads';
			
			$temp_name =$_FILES['image']['tmp_name'];
			$file_name = strtolower(basename($_FILES['image']['name']));
			$file_type=$_FILES['image']['type'];
		 	$file_size =$_FILES['image']['size'];

		 	$the_error = $_FILES['image']['error'];

		 	if(!empty($temp_name) ){

		 		if($file_size > 2097152 ){
			 		$the_message[] = "Message size exceeds";
			 	}

			 	// check if extension is allowed
		     	$get_extension = explode('.', $file_name);
		     	$file_ext=end($get_extension);
		     	// $this->file_ext=strtolower(end(explode('.', $this->file_name)));

		     	if(!empty($file_name)){
		         if(is_array($extensions) && in_array($file_ext, $extensions ) === false){
		             // echo $file_ext;
		            // echo $this->file_name . "<br>";
		            $the_message[] = $file_name . " - extension not allowed";
		         }
		     	}

		     	// check correct MIME Type
		      $finfo = new finfo(FILEINFO_MIME_TYPE);
		      // $check_mime_type = $finfo->buffer(file_get_contents($this->file_tmp));
		      if (false === $extensions = array_search(
		         $finfo->file($temp_name),                     
		         array(
		             'jpg' => 'image/jpeg',
		             'jpeg' => 'image/jpeg',
		             'png' => 'image/png',
		             'gif' => 'image/gif'
		             ),
		         true
		         ))
		         {
		          	$the_message[] = $file_name . " - Not a valid file.";
		         }

			 	// check if file already exists
		      if(!empty($file_name)){
		         if(file_exists($directory.'/'.$file_name)) {
		             // echo $this->file_name . "<br>";
		             $the_message[] = $file_name . " - Sorry, file already exists.";                        
		         }
		     	}

		     	// check if file is image
		      if($file_ext == "jpg" || $file_ext == "png" || $file_ext == "gif"|| $file_ext == "pjpeg"){
		         $size = @getimagesize($temp_name);
		         if (empty($size) || ($size[0] === 0) || ($size[1] === 0)) {
		             // echo $this->file_name . "<br>";
		             $the_message[] = $file_name . " - Not a image.";
		         }
		     	}

		     	if(empty($the_message)){
		     		if(move_uploaded_file($temp_name, $directory . '/' . $file_name)){
		     			self::$image = $file_name;
		     			// $the_message[] = "File uploaded successfully";
		     			return true;
				 		
				 	}
		     	}

		 	} else {	 		
				$the_message[] = $upload_errors[$the_error];
			} 	
		}
		return $the_message;
	}
	/*------------------- end of saveImage3() ---------------------------*/

	public static function saveFiles($files_to_upload, $module_primary_id, $module){

		// if($_SERVER['REQUEST_METHOD'] == 'POST' && empty($_POST) && empty($_FILES) && $_SERVER['CONTENT_LENGTH'] > 0 ){
		//     echo "<span style='color: red;'>Upload failed, file size exceeds the limit.</span>";
		// }

		if(isset($_POST['submit'])){
			$files = $files_to_upload;
			$upload_dir = ATTACHMENT_DIR;
			$uploaded = array();
			$failed = array();
			$upload_messages = array();
			$allowed_ext = array("jpeg","jpg","png", "gif", "doc", "docx", "pdf", "pdfa", "xls", "xlsx", "ppt", "pptx");

			if(!empty($files['name'][0]) ){
				foreach($files['name'] as $position => $file_name){
					$file_tmp = $files['tmp_name'][$position];
					$file_size = $files['size'][$position];
					$file_type = $files['type'][$position];
					$file_error = $files['error'][$position];

					$file_ext = explode('.', $file_name);
					$file_ext = strtolower(end($file_ext));

					if(in_array($file_ext, $allowed_ext)){
						// check correct MIME Type
			         $finfo = new finfo(FILEINFO_MIME_TYPE);
			         // $check_mime_type = $finfo->buffer(file_get_contents($this->file_tmp));
			         if(false === $extensions = array_search(
			            $finfo->file($file_tmp),
			            array(
			               'jpg' => 'image/jpeg',
			               'jpeg' => 'image/jpeg',
			               'png' => 'image/png',
			               'gif' => 'image/gif',
			               'pdf' => 'application/pdf',
			               'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
			               'xlsx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
			               'pptx' => 'application/vnd.openxmlformats-officedocument.presentationml.presentation'
			               ),
			                 true))
			             	{
			             		$failed[$position] = "<span style='color: red;'> {$file_name} - Not a valid file.</span>";
			          		} else {
			          			if($file_error == 0){

										if($file_size <= 2097152 ){
											// check if file is image
			                        if($file_ext == "jpg" || $file_ext == "jpeg" || $file_ext == "png" || $file_ext == "gif"|| $file_ext == "pjpeg"){
			                            $size = @getimagesize($file_tmp);
			                            if (empty($size) || ($size[0] === 0) || ($size[1] === 0)) {
			                                $failed[$position] = "<span style='color: red;'> {$file_name} - Not a image, Upload failed.</span>";
			                            }
			                        }

			                        // check if file already exists
											if(file_exists($upload_dir.DS.$file_name)) {
				                        $failed[$position] = "<span style='color: red;'> {$file_name} - File already exists.</span>";
				                     } else {
				                     	// if no errors move file & upload
				                     	if(move_uploaded_file($file_tmp, $upload_dir . DS . $file_name)){
				                     		$upload_data = [
				                     			'module' => $module,
				                     			'module_primary_id' => $module_primary_id,
				                     			'file_name' => $file_name,
				                     			'file_type' => $file_type,
				                     			'file_size' => $file_size,
				                     			'assigned_to'	=> $_POST['assigned_to'],
												'date_entered' 	=> date('Y-m-d G:i:s'),
												'created_by' 	=> $_SESSION['user_id'],
												'deleted'		=> 0
				                     		];

		                                	$database = new Crud();
		                                	if($database->upload($upload_data, 'uploads')){
	                                			$uploaded[$position] = "<span style='color: green;'> {$file_name} - Files(s) Uploaded Successfully.</span>";
		                                	} else {
		                                		$failed[$position] = "<span style='color: red;'> {$file_name} - File(s) Upload failed.</span>";
		                                	}
		                                	// if($database->query($sql)){
		                                	// 	$uploaded[$position] = "<span style='color: green;'> {$file_name} - Uploaded Successfully.</span>";
		                                	// } else {
		                                	// 	$failed[$position] = "<span style='color: red;'> {$file_name} - Upload failed.</span>";
		                                	// }
													
												} else {
													
														// unlink($upload_dir.DS.$file_name);
														$failed[$position] = "<span style='color: red;'>{$file_name} - Failed to upload.</span>";													
													
												} // end move_uploaded

				                     } // end else file_exist
										} else {
											$failed[$position] = "<span style='color: red;'>{$file_name} is too large.</span>";
										}
									} else {
										$failed[$position] = "<span style='color: red;'>{$file_name} - Failed to upload {$file_error}.</span>";
									}
				            }
					} else {
						$failed[$position] = "<span style='color: red;'>{$file_name} file extension '{$file_ext}' is not allowed</span>";
					}

				} // end foreach

			} 
			// $upload_messages['success'] = $uploaded;
			$upload_messages['failed'] = $failed;

			return $upload_messages;

			// foreach ($upload_messages as $key=>$value) {
			// 	foreach($value as $msg){
			// 		echo $msg . '</br>';
			// 	}
			// }

		} // end isset($_POST['upload'])
	} // end function

	/*------------------- end of saveFiles() ---------------------------*/

	public static function saveImage(){
		// ob_get_contents();
		// ob_end_clean();

		if($_SERVER['REQUEST_METHOD'] == 'POST' && empty($_POST) && empty($_FILES) && $_SERVER['CONTENT_LENGTH'] > 0 ){
		    echo "<span style='color: red;'>Upload failed, file size exceeds the limit.</span>";
		}

		

			if(!empty($_FILES['image']['name'][0]) ){
				$files = $_FILES['image'];
				$upload_dir = 'img';
				$uploaded = array();
				$failed = array();
				$upload_messages = array();
				$allowed_ext = array("jpeg","jpg","png", "gif");

				$check_ext = array(
               'jpg' => 'image/jpeg',
               'jpeg' => 'image/jpeg',
               'png' => 'image/png',
               'gif' => 'image/gif'
            );	
				
				$file_name = $files['name'];
				$file_tmp = $files['tmp_name'];
				$file_size = $files['size'];
				$file_type = $files['type'];
				$file_error = $files['error'];

				if(!empty($file_tmp)){
					$file_ext = explode('.', $file_name);
					$file_ext = strtolower(end($file_ext));

					if(in_array($file_ext, $allowed_ext)){
						// check correct MIME Type
						$finfo = new finfo(FILEINFO_MIME_TYPE);
						// $check_mime_type = $finfo->buffer(file_get_contents($this->file_tmp));
						if(false === $allowed_ext = array_search($finfo->file($file_tmp), $check_ext, true)) {
							$failed[] = "<span style='color: red;'> {$file_name} - Not a valid file.</span>";
						} else {
							
							if($file_error == 0){

								if($file_size <= 2097152 ){

									// check if file is image
	                if($file_ext == "jpg" || $file_ext == "jpeg" || $file_ext == "png" || $file_ext == "gif"|| $file_ext == "pjpeg"){
	                    $size = @getimagesize($file_tmp);
	                    if (empty($size) || ($size[0] === 0) || ($size[1] === 0)) {
	                        $failed[] = "<span style='color: red;'> {$file_name} - Not a image, Upload failed.</span>";
	                    }
	                }

	                // check if file already exists
									if(file_exists($upload_dir.DS.$file_name)) {
	                  $failed[] = "<span style='color: red;'> {$file_name} - File already exists.</span>";
	                } else {

	                 	if(is_dir($upload_dir)==true){
	                    // mkdir("$desired_dir", 0700);
	                    // Create directory if it does not exist
	                    if(move_uploaded_file($file_tmp, $upload_dir . DS . $file_name)){
	                    	
	                    	self::$image = $file_name;
	                    	$uploaded[] = "<span style='color: green;'> {$file_name} - Uploaded Successfully.</span>";
	                    	return true;

	                    	// $sql="INSERT INTO upload_data1 (`FILE_NAME`,`FILE_SIZE`,`FILE_TYPE`) VALUES('$file_name','$file_size','$file_type')";
	                    	// $database = new Database();
	                    	// if($database->query($sql)){
	                    	// 	$uploaded[] = "<span style='color: green;'> {$file_name} - Uploaded Successfully.</span>";
	                    	// } else {
	                    	// 	$failed[] = "<span style='color: red;'> {$file_name} - DB failed.</span>";
	                    	// }
	                    } else {
	                    	if(file_exists($upload_dir.DS.$file_name)){
	                    		unlink($upload_dir.DS.$file_name);
	                    		$failed[] = "<span style='color: red;'>{$file_name} - Failed to upload.</span>";
	                    	}														
											} // end move_uploaded

	                  } else {
	                   	$failed[] = "<span style='color: red;'> Directory not found.</span>";
		                }
	                 	// if no errors move file & upload
					                     	

	                } // end else file_exist
								} else {
									$failed[] = "<span style='color: red;'>{$file_name} is too large.</span>";
								}
							} else {
								$failed[] = "<span style='color: red;'>{$file_name} - Failed to upload {$file_error}.</span>";
							}
            }
					} else {
						$failed[] = "<span style='color: red;'>{$file_name} file extension '{$file_ext}' is not allowed</span>";
					}

					$upload_messages[] = $uploaded;
					$upload_messages[] = $failed;

					return $upload_messages;


				} else {
					$_SESSION['danger'] = array(" $file_name Upload failed - file size exceeds the limit.");
				}

				

			} // end if(!empty($_FILES['files']['name'][0]))
		
	} // end saveImage function

	/*------------------- end of saveImage() ---------------------------*/

	public static function showImage($image){
		$image_placeholder = ASSETURL . static::$image_dir .'/dummy-img.png';
		return empty($image) || $image==NULL ? $image_placeholder : ASSETURL . static::$image_dir. '/'. $image;
	}
	/*------------------- end of showImage() ---------------------------*/

	public function picture_path(){
		return $this->upload_directory.DS.$this->image;

	} // end of picture_path() ---------------------------------------------------

	public static function importFile($files_to_upload){

		// if($_SERVER['REQUEST_METHOD'] == 'POST' && empty($_POST) && empty($_FILES) && $_SERVER['CONTENT_LENGTH'] > 0 ){
		//     echo "<span style='color: red;'>Upload failed, file size exceeds the limit.</span>";
		// }

		if(isset($_POST['submit'])){
			$files = $files_to_upload;
			// $upload_dir = ATTACHMENT_DIR;
			$uploaded = array();
			$failed = array();
			$upload_messages = array();
			$allowed_ext = array("csv");

			if(!empty($files['name'][0]) ){
				foreach(strtolower(basename($files['name'])) as $position => $file_name){
					$file_tmp = $files['tmp_name'][$position];
					$file_size = $files['size'][$position];
					$file_type = $files['type'][$position];
					$file_error = $files['error'][$position];

					$file_ext = explode('.', $file_name);
					$file_ext = strtolower(end($file_ext));

					if(in_array($file_ext, $allowed_ext)){
						// check correct MIME Type
			         $finfo = new finfo(FILEINFO_MIME_TYPE);
			         // $check_mime_type = $finfo->buffer(file_get_contents($this->file_tmp));
			         if(false === $extensions = array_search(
			            $finfo->file($file_tmp),
			            array(
			               'text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain'
			               ),
			                 true))
			             	{
			             		$failed[$position] = "<span style='color: red;'> {$file_name} - Not a valid file.</span>";
			          		} else {
			          			if($file_error == 0){

									if($file_size <= 2097152 ){

										//code goes here

										return true;

			                        
									} else {
										$failed[$position] = "<span style='color: red;'>{$file_name} is too large.</span>";
									}
								} else {
									$failed[$position] = "<span style='color: red;'>{$file_name} - Failed to Import {$file_error}.</span>";
								}
				            }
					} else {
						$failed[$position] = "<span style='color: red;'>{$file_name} file extension '{$file_ext}' is not allowed</span>";
					}

				} // end foreach

			} 
			// $upload_messages['success'] = $uploaded;
			$upload_messages['failed'] = $failed;

			return $upload_messages;

			// foreach ($upload_messages as $key=>$value) {
			// 	foreach($value as $msg){
			// 		echo $msg . '</br>';
			// 	}
			// }

		} // end isset($_POST['upload'])
	} // end function



} // end class