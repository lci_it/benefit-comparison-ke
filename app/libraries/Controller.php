<?php
  	/*
    	* Base Controller
       	* Loads the Models and Views
  	*/
	class Controller {

		// Load model
		public function loadModel($model){
			// Require model file
			if(file_exists('../app/models/' . $model . '.php')){
				require_once '../app/models/' . $model . '.php';

				// Instantiate model
				return new $model();
			} else {
				// Model does not exist
				die('Page dbclass' . $model . 'does not exist');
			}			
		}
		/*--------------------------------------------------------------------*/

		// Load view
		public function loadView($view_file, $data=[]){
			//Check for view file
			if(file_exists('../app/views/' . $view_file . '.php')){
				require_once '../app/views/' . $view_file . '.php';
			} else {
				// View does not exist
				die('Page does not exist');
			}
		}
		/*--------------------------------------------------------------------*/

		//Get the current id from url
	 	public function getUrlID(){
	 		return Core::getUrl()[2];
	 	}
	 	/*--------------------------------------------------------------------*/

	 	// Get current method
	 	public function getUrlMethod(){
	 		return Core::getUrl()[1];
	 	}
	 	/*--------------------------------------------------------------------*/

	 	public function action(){
			if($_SERVER['REQUEST_METHOD'] == 'POST'){
				if(isset($_POST['checkBoxArray'])){

					foreach($_POST['checkBoxArray'] as $checkBox_post_id){
						
						$bulk_options= $_POST['bulk_options'];

						switch($bulk_options){
							
							case '' :
								if($checkBox_post_id){
									// $_SESSION['warning'] = array("Select ID checkbox and Select Option");
									$this->errors = "Select Option";
									redirect(current_class($this) . '/list');
								}
							break;

							case 'Draft' :

							break;

							case 'Published' :

							break;

							case 'deleteall' :
								if($checkBox_post_id){
									if($this->Model->delete($checkBox_post_id)){
										// $_SESSION['success'] = array('Record Deleted - ' .$id);
										redirect(current_class($this) . '/list');
									} else {
										$this->errors = "$checkBox_post_id - Failed";
									}
								}

								break;

							case 'Clone' :
								$query = "SELECT * FROM posts WHERE post_id = {$checkBox_post_id}";
								$clone_post_ids_query = mysqli_query($connection, $query);

								while($row = mysqli_fetch_array($clone_post_ids_query)){
									$post_title = $row['post_title'];
									$post_category_id = $row['post_category_id'];
									$post_author = $row['post_author'];
									$post_status = $row['post_status'];
									$post_image = $row['post_image'];
									$post_tags = $row['post_tags'];
									$post_comment_count = $row['post_comment_count'];
									$post_date = $row['post_date'];
									$post_user = $row['post_user'];
									$post_content = $row['post_content'];
								}

								$query = "INSERT INTO `posts` (`post_title`, `post_category_id`, `post_author`, `post_status`, `post_image`, `post_tags`, `post_content`, `post_date`) VALUES ('$post_title', '$post_category_id', '$post_author', '$post_status', '$post_image', '$post_tags', '$post_content', now())";

								$clone_post_query = mysqli_query($connection, $query);
								confirmQueryresult($clone_post_query);
							break;
						}
					}
					if(empty($this->errors) ){
						$_SESSION['success'] = array("Success");
					} else {
						$_SESSION['warning'] = array($this->errors);
					}
				} else {
					$_SESSION['warning'] = array("Select ID checkbox");
					redirect(current_class($this) . '/list');
				}
			}
		}

	 	


	 	




	} // end of class