<?php

/**
 * CRUD class
 */
class Crud
{
	protected $db;
	
	public function __construct(){
		$this->db = new Database;
		// $this->db->table = strtolower('quotes');
	}

	/*--------------------------------------------------------------------------------*/

	// public function selectAll(){
	// 	$this->db->query("SELECT * FROM  " . static::$table . " WHERE deleted = 0");
	// 	return $this->db->resultSet();
	// }

	/*--------------------------------------------------------------------*/

	public function lastInsertId(){
		return $this->db->lastInsertId();
	}

	/*--------------------------------------------------------------------------------*/
	public function startTransaction() {
		$this->db->startTransaction();
	}
	/*--------------------------------------------------------------------------------*/
	public function commit() {
		$this->db->commit();
	}
	/*--------------------------------------------------------------------------------*/
	public function rollBack() {
		$this->db->rollBack();
	}
	/*--------------------------------------------------------------------------------*/

	public function getList(){
		$this->db->query("SELECT * FROM  " . static::$table . " WHERE deleted = 0");
		return $this->db->resultSet();
	}
	/*--------------------------------------------------------------------------------*/

	public function getByGroupId($created_by, $assigned_to){

		$created_by = (int) $created_by;
		$assigned_to = (int) $assigned_to;


		// $this->db->query("SELECT * FROM $table");
		// return $this->db->resultSet();
		$this->db->query("SELECT * FROM " . static::$table . " WHERE created_by = :created_by OR assigned_to = :assigned_to AND deleted = 0");
		$this->db->bind(':created_by', $created_by);
		$this->db->bind(':assigned_to', $assigned_to);
		return $this->db->resultSet();
	}
	/*--------------------------------------------------------------------------------*/

	public function getById($id){
		// $this->db->query("SELECT * FROM $table");
		// return $this->db->resultSet();
		$this->db->query("SELECT * FROM " . static::$table . " WHERE id = :id AND deleted = 0");
			$this->db->bind(':id', $id);
			return $this->db->singleRow();
	}
	/*--------------------------------------------------------------------------------*/

	// Get record row count
	public function rowCount(){
		return $this->stmt->rowCount();
	}
	/*--------------------------------------------------------------------------------*/

	public function SelectByTableID($table, $id){
		$this->db->query("SELECT * FROM $table WHERE id = :id AND deleted = 0");
		$this->db->bind(':id', $id);
		return $this->db->singleRow();	
	}
	/*--------------------------------------------------------------------------------*/

	public function SelectAllByTableColumn($table, $id, $column){
		$this->db->query("SELECT * FROM " . $table . " WHERE $column = :id");
		$this->db->bind(':id', $id);
		return $this->db->resultSet();	
	}
	/*--------------------------------------------------------------------------------*/

	public function SelectAllByColumn($id, $column){
		$this->db->query("SELECT * FROM " . static::$table . " WHERE $column = :id");
		$this->db->bind(':id', $id);
		return $this->db->resultSet();
	}
	/*--------------------------------------------------------------------------------*/
	
	public function selectAllFromTable($table){
		$this->db->query("SELECT * FROM " .$table . " WHERE deleted = 0");
		return $this->db->resultSet();
	}
	/*--------------------------------------------------------------------------------*/

	public function find_name($name){
		$this->db->query("SELECT * FROM " . static::$table . " WHERE name = :name");
		$this->db->bind(':name', $name);

		$row = $this->db->singleRow();

		// check row 
		if($row ){
			return true;
		} else {
				return false;
		}
	}
	/*--------------------------------------------------------------------------------*/

	public function create($data){
		// $this->isTableSet();

		$keys = implode(',', array_keys($data));
		$values = ":" . implode(', :', array_keys($data));

		$sql = "INSERT INTO  " . static::$table . " ($keys) ";
		$sql .= "VALUES ($values)";

		$this->db->query($sql);

		foreach($data as $key => $value){
			$this->db->bind(":$key", $value);			
		}

		if($this->db->execute()){
			return true;
		} else {
			return false;
		}
	}

	/*---------------------------// update data BIND Avoid SQL Injection ------------*/

	public function update($data){
		// $this->isTableSet();

		$updateKeys = NULL;

		foreach($data as $key => $value){
			$updateKeys .= "$key = :$key,";
		}

		$updateKeys = rtrim($updateKeys, ',');

		$sql = "UPDATE  " . static::$table . " SET $updateKeys WHERE id =:id";

		$this->db->query($sql);

		foreach($data as $key => $value){
			$this->db->bind(":$key", $value);			
		}

		if($this->db->execute()){
			return true;
		} else {
			return false;
		}			
	}
	/*--------------------------------------------------------------------------------*/

	public function delete($id){
		// $this->db->isTableSet();

		$this->db->query("UPDATE  " . static::$table . " SET deleted = 1 WHERE id =:id LIMIT 1");

		$this->db->bind(":id", $id);

		if($this->db->execute()){
			return true;
		} else {
			return false;
		}			
	}
	/*--------------------------------------------------------------------------------*/

	public function getDropdownList($column_name){
		$this->db->query("SELECT id, $column_name FROM dropdowns WHERE $column_name IS NOT NULL");
		$this->db->bind(':$column_name', $column_name);
		return $this->db->resultSet();
	}
	/*--------------------------------------------------------------------------------*/

	public function getDropdownValue($id, $column_name){
		$this->db->query("SELECT $id, $column_name FROM dropdowns WHERE id = $id");
		$this->db->bind(':$column_name', $column_name);
		$this->db->bind(':id', $id);
		return $this->db->singleRow();
	}
	/*--------------------------------------------------------------------------------*/

	public function countRows($table, $column, $id){
		if($id == ""){
			$id = 0;
		}
		$result_set = $this->db->query("SELECT COUNT(1) FROM " . $table . " WHERE " . $column ." = '$id'");
		return $result_set;
	}
	/*--------------------------------------------------------------------------------*/

	public function countByName($name){
		$this->db->query("SELECT COUNT(*) FROM " . static::$table . " WHERE  name = ?");
		return $this->db->countRecordsByName($name);
	}
	/*--------------------------------------------------------------------------------*/

	public function countColumnRows($column, $name){
		$this->db->query("SELECT COUNT(*) FROM " . static::$table . " WHERE  $column = ?");
		return $this->db->countRecordsByName($name);
	}
	/*--------------------------------------------------------------------------------*/

	public function upload($data, $table){
		// $this->isTableSet();

		$keys = implode(',', array_keys($data));
		$values = ":" . implode(', :', array_keys($data));

		$sql = "INSERT INTO  " . $table . " ($keys) ";
		$sql .= "VALUES ($values)";

		$this->db->query($sql);

		foreach($data as $key => $value){
			$this->db->bind(":$key", $value);
		}

		if($this->db->execute()){
			return true;
		} else {
			return false;
		}
	}
	/*--------------------------------------------------------------------------------*/

	public function getModuleFiles($module, $module_primary_id){
		$this->db->query("SELECT * FROM uploads WHERE module_primary_id = '$module_primary_id' AND module = '$module' AND deleted = 0");
		return $this->db->resultSet();
	}
	/*--------------------------------------------------------------------------------*/

	public function deleteFiles($file){

		$file_directory= ATTACHMENT_DIR;

		$this->db->query("SELECT * FROM uploads WHERE id='{$file}' LIMIT 1");
		$row = $this->db->singleRow();
		
		$this->db->query("DELETE FROM uploads WHERE id = '{$file}'");

		if($this->db->execute()){
			if(!unlink($file_directory . DS . $row->file_name)){
				throw new Exception(' File removal failed, either file moved or deleted previously.');
			}
		} else {
			throw new Exception(' Query Failed ');
		}


		// if($delete_file_query){
		// 	
		// }else {
		// 	
		// }
	}
	/*--------------------------------------------------------------------------------*/

	public function deleteProduct($id){
		// $this->db->isTableSet();

		$this->db->query("DELETE FROM " . static::$table . " WHERE id =:id LIMIT 1");

		$this->db->bind(":id", $id);

		if($this->db->execute()){
			return true;
		} else {
			return false;
		}			
	}
	/*--------------------------------------------------------------------------------*/

	public function ajaxCall($name, $type){
		$this->db->query("SELECT * FROM " . static::$table . " WHERE UPPER($type) LIKE '%".strtoupper($name)."%'");
			$row = $this->db->resultSet();
			return $row;
	}
	/*--------------------------------------------------------------------------------*/

	public function updateStock($data){
		// $this->isTableSet();

		$updateKeys = NULL;

		foreach($data as $key => $value){
			$updateKeys .= "$key = :$key,";
		}

		$updateKeys = rtrim($updateKeys, ',');

		$sql = "UPDATE  " . static::$table . " SET $updateKeys WHERE id =:id";

		$this->db->query($sql);

		foreach($data as $key => $value){
			$this->db->bind(":$key", $value);			
		}

		if($this->db->execute()){
			return true;
		} else {
			return false;
		}			
	}
	/*--------------------------------------------------------------------------------*/




} // end of class