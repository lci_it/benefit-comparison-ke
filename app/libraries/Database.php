<?php 
	/*
		* PDO Database Class
		* Connect to database
		* Create Prepared Statements
		* Bind Values
		*Return rows and results
	*/
	class Database {
		private $host = DB_HOST;
		private $user = DB_USER;
		private $pass = DB_PASS;
		private $dbname = DB_NAME;
		private $charset = 'utf8mb4';

		private $dbh;
		private $stmt;
		private $error;
		// public $table = NULL;
		/*--------------------------------------------------------------------------------*/

		public function __construct(){


			// Set DSN
			$dsn = 'mysql:host='.$this->host.';dbname='.$this->dbname;$this->charset;
			
			$options = array(
				PDO::ATTR_PERSISTENT => TRUE,
				PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
				// PDO::ATTR_EMULATE_PREPARES => false
			);

			// Create PDO instance
			try{
				$this->dbh = @new PDO($dsn, $this->user, $this->pass, $options );
				} catch (PDOException $e){
				// $this->error = $e->getMessage();
				// echo $this->error;
				die("Database Connection Failed");
			}
		}
		/*--------------------------------------------------------------------------------*/

		// Returns the last inserted ID
		public function lastInsertId(){
			return $this->dbh->lastInsertId();
		}
		/*--------------------------------------------------------------------------------*/

		//start transanction
		public function startTransaction(){
			// $this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->dbh->beginTransaction();
		}
		/*--------------------------------------------------------------------------------*/

		public function commit(){
			$this->dbh->commit();
			// $this->dbh->setAttribute(PDO::ATTR_AUTOCOMMIT,1);
		}
		/*--------------------------------------------------------------------------------*/

		public function rollBack(){
			$this->dbh->rollback();
		}
		/*--------------------------------------------------------------------------------*/

		// Prepare statement with query
		public function query($sql){
			$this->stmt = $this->dbh->prepare($sql);
		}
		/*--------------------------------------------------------------------------------*/

		public function countRecordsByName($name){
			
			// $row = $this->query($sql);
			$this->stmt->execute([$name]);
			return $count = $this->stmt->fetchColumn();
		}

		/*--------------------------------------------------------------------------------*/

		// Bind values
		public function bind($param, $value, $type = null){
			if(is_null($type)){
				switch(true){
					case is_int($value):
						$type = PDO::PARAM_INT;
						break;
					case is_bool($value):
						$type = PDO::PARAM_BOOL;
						break;
					case is_null($value):
						$type = PDO::PARAM_NULL;
						break;
					default:
						$type = PDO::PARAM_STR;
				}
			}

			$this->stmt->bindValue($param, $value, $type);
		}
		/*--------------------------------------------------------------------------------*/

		// Execute the prepared statement
		public function execute(){
			return $this->stmt->execute();
		}
		/*--------------------------------------------------------------------------------*/

		// Get result set of array of objects
		public function resultSet(){
			$this->execute();
			return $this->stmt->fetchAll(PDO::FETCH_OBJ);
		}
		/*--------------------------------------------------------------------------------*/

		// Get single record as object
		public function singleRow(){
			$this->execute();
			return $this->stmt->fetch(PDO::FETCH_OBJ);
		}
		/*--------------------------------------------------------------------------------*/

		private function isTableSet(){
			if ($this->table == Null){
				die("Set the Table");
			}
		}

		/*--------------------------------------------------------------------------------*/

		// Get record row count
		public function rowCount(){
			return $this->stmt->rowCount();
		}
		/*--------------------------------------------------------------------------------*/


		
	



	} // end of class