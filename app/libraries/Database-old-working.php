<?php 
	/*
		* PDO Database Class
		* Connect to database
		* Create Prepared Statements
		* Bind Values
		*Return rows and results
	*/
	class Database {
		private $host = DB_HOST;
		private $user = DB_USER;
		private $pass = DB_PASS;
		private $dbname = DB_NAME;

		private $dbh;
		private $stmt;
		private $error;
		public $table = NULL;

		public function __construct(){
			// Set DSN
			$dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->dbname;
			$options = array(
				// PDO::ATTR_PERSISTENT => true,
				PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
			);

			// Create PDO instance
			try{
				$this->dbh = new PDO($dsn, $this->user, $this->pass, $options);

			} catch (PDOException $e){
				$this->error = $e->getMessage();
				echo $this->error;
			}
		}

		//start transanction
		public function startTransaction(){
			$this->dbh->beginTransaction();
		}

		public function commit(){
			$this->dbh->commit();
		}

		public function rollBack(){
			$this->dbh->rollback();
		}


		// Prepare statement with query
		public function query($sql){
			$this->stmt = $this->dbh->prepare($sql);
		}

		// Bind values
		public function bind($param, $value, $type = null){
			if(is_null($type)){
				switch(true){
					case is_int($value):
						$type = PDO::PARAM_INT;
						break;
					case is_bool($value):
						$type = PDO::PARAM_BOOL;
						break;
					case is_null($value):
						$type = PDO::PARAM_NULL;
						break;
					default:
						$type = PDO::PARAM_STR;
				}
			}

			$this->stmt->bindValue($param, $value, $type);
		}

		// Execute the prepared statement
		public function execute(){
			return $this->stmt->execute();
		}

		// Get result set of array of objects
		public function resultSet(){
			$this->execute();
			return $this->stmt->fetchAll(PDO::FETCH_OBJ);
		}

		// Get single record as object
		public function singleRow(){
			$this->execute();
			return $this->stmt->fetch(PDO::FETCH_OBJ);
		}

		// Get record row count
		public function rowCount(){
			return $this->stmt->rowCount();
		}
	
		// Returns the last inserted ID
		public function lastInsertId(){
			return $this->dbh->lastInsertId();
		}

		private function isTableSet(){
			if ($this->table == Null){
				die("Set the Table");
			}
		}
	
		/*--------------------------------------------------------------------------------*/
	
		public function selectAll(){
			$this->query("SELECT * FROM  " . $this->table . " WHERE deleted = 0");
			return $this->resultSet();
		}

		/*--------------------------------------------------------------------------------*/
	
		public function selectAllFromTable($table){
			$this->query("SELECT * FROM  " . $table  . " WHERE deleted = 0");
			return $this->resultSet();
		}
		/*--------------------------------------------------------------------------------*/

		public function SelectAllByTableColumn($table, $id, $column){
			$this->query("SELECT * FROM " . $table . " WHERE $column = :id");
			$this->bind(':id', $id);
			return $this->resultSet();	
		}
		/*--------------------------------------------------------------------------------*/

		public function SelectByID($id){
			$this->query("SELECT * FROM " . $this->table . " WHERE id = :id AND deleted = 0");
			$this->bind(':id', $id);
			return $this->singleRow();	
		}
		/*--------------------------------------------------------------------------------*/

		public function SelectByColumnName($table, $column){
			$this->query("SELECT * FROM " . $table . " WHERE $column = :$column AND deleted = 0");
			$this->bind(':column', $column);
			return $this->singleRow();	
		}

		/*--------------------------------------------------------------------------------*/

		public function SelectByTableID($table, $id){
			$this->query("SELECT * FROM $table WHERE id = :id AND deleted = 0");
			$this->bind(':id', $id);
			return $this->singleRow();	
		}

		/*--------------------------------------------------------------------------------*/

		public function countByID($id){
				$row = $this->dbh->prepare("SELECT * FROM " . $this->table . " WHERE id = ? " . " AND deleted = 0");
				$row->execute([$id]);
				return $count = $row->rowCount();

			// $this->bind(':id', $id);
			// $this->resultSet();
			// return $this->rowCount();
		}
		/*--------------------------------------------------------------------------------*/

		public function countByColumnName($name){
				$row = $this->dbh->prepare("SELECT * FROM " . $this->table . " WHERE name = ?");
				$row->execute([$name]);
				return $count = $row->rowCount();

			// $this->bind(':id', $id);
			// $this->resultSet();
			// return $this->rowCount();
		}

		/*---------------------------// Insert data in table ------------------------------*/
	
		public function insert($data){
			$this->isTableSet();
			$keys = implode(',', array_keys($data));
			$values = ":" . implode(', :', array_keys($data));

			$this->dbh->beginTransaction();

			try{
				$sql = "INSERT INTO  " . $this->table . " ($keys) ";
				$sql .= "VALUES ($values)";
				$this->stmt = $this->dbh->prepare($sql);			

				foreach($data as $key => $value){
					$this->stmt->bindValue(":$key", $value);			
				}

				$this->stmt->execute();
				return $this->dbh->commit();

			} catch(Exception $e){
			    echo $e->getMessage();
			    $this->dbh->rollBack();
			}

		}

		/*---------------------------// Insert data BIND Avoid SQL Injection ------------*/
		public function insertBind($data){
			$this->isTableSet();

			$keys = implode(',', array_keys($data));
			$values = ":" . implode(', :', array_keys($data));

			$sql = "INSERT INTO  " . $this->table . " ($keys) ";
			$sql .= "VALUES ($values)";

			$this->query($sql);

			foreach($data as $key => $value){
				$this->bind(":$key", $value);			
			}

			if($this->execute()){
				return true;
			} else {
				return false;
			}
		}

		/*---------------------------// update data BIND Avoid SQL Injection ------------*/

		public function updateBind($data){
			$this->isTableSet();

			$updateKeys = NULL;

			foreach($data as $key => $value){
				$updateKeys .= "$key = :$key,";
			}

			$updateKeys = rtrim($updateKeys, ',');

			$sql = "UPDATE  " . $this->table . " SET $updateKeys WHERE id =:id";

			$this->query($sql);

			foreach($data as $key => $value){
				$this->bind(":$key", $value);			
			}

			if($this->execute()){
				return true;
			} else {
				return false;
			}			
		}
		/*---------------------------// update data BIND Avoid SQL Injection ------------*/

		public function deleteBind($id){
			$this->isTableSet();

			$sql = "UPDATE  " . $this->table . " SET deleted = 1 WHERE id =:id";

			$this->query($sql);

			$this->bind(":id", $id);

			if($this->execute()){
				return true;
			} else {
				return false;
			}			
		}

		/*--------------------------------------------------------------------------------*/
		public function getDropdownList($column_name){
			$this->query("SELECT id, $column_name FROM dropdowns WHERE $column_name IS NOT NULL");
			$this->bind(':$column_name', $column_name);
			return $this->resultSet();
		}
		/*--------------------------------------------------------------------------------*/

		public function getDropdownValue($id, $column_name){
			$this->query("SELECT $id, $column_name FROM dropdowns WHERE id = $id");
			$this->bind(':$column_name', $column_name);
			$this->bind(':id', $id);
			return $this->singleRow();
		}
		/*--------------------------------------------------------------------------------*/


		// public function update($table, $data){
		// 	$keys = implode(',', array_keys($data));
		// 	$values = ":" . implode(', :', array_keys($data));
		// 	$this->dbh->beginTransaction();
		// 	try{
		// 		$sql = "UPDATE $table SET ($keys) VALUES ($values)";
		// 		$this->stmt = $this->dbh->prepare($sql);			
		// 		foreach($data as $key => $value){
		// 			$this->stmt->bindValue(":$key", $value);			
		// 		}
		// 		$this->stmt->execute();
		// 		return $this->dbh->commit();
		// 	} catch(Exception $e){
		// 	    echo $e->getMessage();
		// 	    $this->dbh->rollBack();
		// 	}

		// }

		/*--------------------------------------------------------------------------------*/

	} // end of class