<?php
define( 'BASE_PATH', 'http://localhost/projects/ac/app/');
define('DB_HOST', 'localhost');
define('DB_NAME', 'pm21');
define('DB_USERNAME','root');
define('DB_PASSWORD','');

$con = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
if( mysqli_connect_error()) echo "Failed to connect to MySQL: " . mysqli_connect_error();


// require_once 'config.php';

//start of creditor details auto complete by name
if(isset($_POST['type']) && $_POST['type'] == 'creditors_t.name' ){
	$type = $_POST['type'];
	$name = $_POST['name_startsWith'];
	$query = "SELECT id, name, phone, currency, trn, CONCAT(street, ' ',city,' ', state,' ', country,' ', postal_code) AS address FROM creditors_t where UPPER($type) LIKE '%".strtoupper($name)."%'";
	$result = mysqli_query($con, $query);
	$data = array();
	while ($row = mysqli_fetch_assoc($result)) {
		$name = $row['id'].'|'.$row['name'].'|'.$row['phone'].'|'.$row['currency'].'|'.$row['trn'].'|'.$row['address'];
		array_push($data, $name);
	}
	mysqli_close($con);
	echo json_encode($data);exit;
}




//start of creditor details auto complete by id
if(isset($_POST['type']) && $_POST['type'] == 'creditors_t.id' ){
	$type = $_POST['type'];
	$name = $_POST['name_startsWith'];
	$query = "SELECT id, name, phone, currency, trn, CONCAT(street, ' ',city,' ', state,' ', country,' ', postal_code) AS address FROM creditors_t where UPPER($type) LIKE '%".strtoupper($name)."%'";
	$result = mysqli_query($con, $query);
	$data = array();
	while ($row = mysqli_fetch_assoc($result)) {
		$name = $row['id'].'|'.$row['name'].'|'.$row['phone'].'|'.$row['currency'].'|'.$row['trn'].'|'.$row['address'];
		array_push($data, $name);
	}
	mysqli_close($con);
	echo json_encode($data);exit;
}

//start of customer details auto complete by id
if(isset($_POST['type']) && $_POST['type'] == 'customers_t.id' ){
	$type = $_POST['type'];
	$name = $_POST['name_startsWith'];
	$query = "SELECT id, name, phone, currency, trn, CONCAT(street, ' ',city,' ', state,' ', country,' ', postal_code) AS address, mobile FROM customers_t where UPPER($type) LIKE '%".strtoupper($name)."%'";
	$result = mysqli_query($con, $query);
	$data = array();
	while ($row = mysqli_fetch_assoc($result)) {
		$name = $row['id'].'|'.$row['name'].'|'.$row['phone'].'|'.$row['currency'].'|'.$row['trn'].'|'.$row['address'].'|'.$row['mobile'];
		array_push($data, $name);
	}
	mysqli_close($con);
	echo json_encode($data);exit;
}


//start of customer details auto complete by name
if(isset($_POST['type']) && $_POST['type'] == 'customers.name' ){
	$type = $_POST['type'];
	$name = $_POST['name_startsWith'];
	$query = "SELECT id, name, phone, currency, trn, mobile, CONCAT_WS(' ', street, city, state, country, postal_code) AS address, mobile FROM customers_t where UPPER($type) LIKE '%".strtoupper($name)."%'";
	$result = mysqli_query($con, $query);
	$data = array();
	while ($row = mysqli_fetch_assoc($result)) {
		$name = $row['id'].'|'.$row['name'].'|'.$row['phone'].'|'.$row['currency'].'|'.$row['trn'].'|'.$row['address'].'|'.$row['mobile'];
		array_push($data, $name);
	}
	mysqli_close($con);
	echo json_encode($data);exit;
}






//start of product details auto complete by product name
if(isset($_POST['type']) && $_POST['type'] == 'products_t.name' ){
	$type = $_POST['type'];
	$name = $_POST['name_startsWith'];
	$query = "SELECT id, name, part_number, description, selling_price FROM products_t where UPPER($type) LIKE '%".strtoupper($name)."%'";
	$result = mysqli_query($con, $query);
	$data = array();
	while ($row = mysqli_fetch_assoc($result)) {
		$name = $row['id'].'|'.$row['name'].'|'.$row['part_number'].'|'.$row['description'].'|'.$row['selling_price'];
		array_push($data, $name);
	}
	mysqli_close($con);
	echo json_encode($data);exit;
}
//end of product details auto complete by product name


//start of product_details auto complete by part_number
if(isset($_POST['type']) && $_POST['type'] == 'products_t.part_number' ){
	$type = $_POST['type'];
	$name = $_POST['name_startsWith'];
	$query = "SELECT id, name, part_number, description, selling_price FROM products_t where UPPER($type) LIKE '%".strtoupper($name)."%'";
	$result = mysqli_query($con, $query);
	$data = array();
	while ($row = mysqli_fetch_assoc($result)) {
		$name = $row['id'].'|'.$row['name'].'|'.$row['part_number'].'|'.$row['description'].'|'.$row['selling_price'];
		array_push($data, $name);
	}
	mysqli_close($con);
	echo json_encode($data);exit;
}
//end of product_details auto complete by part_number


//start of project id auto complete by id
if(isset($_POST['type']) && $_POST['type'] == 'project_t.id' ){
	$type = $_POST['type'];
	$name = $_POST['name_startsWith'];
	$query = "SELECT id, name FROM project_t where UPPER($type) LIKE '%".strtoupper($name)."%'";
	$result = mysqli_query($con, $query);
	$data = array();
	while ($row = mysqli_fetch_assoc($result)) {
		$name = $row['id'].'|'.$row['name'];
		array_push($data, $name);
	}
	mysqli_close($con);
	echo json_encode($data);exit;
}


//start of owner details auto complete by name
if(isset($_POST['type']) && $_POST['type'] == 'owner_t.firstname' ){
	$type = $_POST['type'];
	$name = $_POST['name_startsWith'];
	$query = "SELECT id, firstname, lastname, mobile_phone, office_phone FROM owner_t where UPPER($type) LIKE '%".strtoupper($name)."%'";
	$result = mysqli_query($con, $query);
	$data = array();
	while ($row = mysqli_fetch_assoc($result)) {
		$name = $row['id'].'|'.$row['firstname'].'|'.$row['lastname'].'|'.$row['mobile_phone'].'|'.$row['office_phone'];
		array_push($data, $name);
	}
	mysqli_close($con);
	echo json_encode($data);exit;
}



//start of property details auto complete by name
if(isset($_POST['type']) && $_POST['type'] == 'property_t.property_name' ){
	$type = $_POST['type'];
	$name = $_POST['name_startsWith'];
	$query = "SELECT `property_t`.id, `property_t`.property_name, `property_t`.property_type, `property_t`.occupancy_status, `property_t`.owner_t_id, CONCAT(`owner_t`.firstname, ' ', `owner_t`.lastname) AS owner FROM property_t INNER JOIN owner_t ON `property_t`.owner_t_id = `owner_t`.id where UPPER($type) LIKE '%".strtoupper($name)."%'";
	$result = mysqli_query($con, $query);
	$data = array();
	while ($row = mysqli_fetch_assoc($result)) {
		$name = $row['id'].'|'.$row['property_name'].'|'.$row['property_type'].'|'.$row['occupancy_status'].'|'.$row['owner_t_id'].'|'.$row['owner'];
		array_push($data, $name);
	}
	mysqli_close($con);
	echo json_encode($data);
	exit;
}


//start of flat details auto complete by property_id/name
if(isset($_POST['depart']) ){

	$departid = $_POST['depart'];   // department id

	$sql = "SELECT id, flat_no, occupancy_status FROM flat_t WHERE property_t_id=".$departid;

	$result = mysqli_query($con,$sql);

	$data = array();

	while( $row = mysqli_fetch_array($result) ){
	    $id = $row['id'];
	    $name = $row['flat_no'];
	    $flat_status = $row['occupancy_status'];

	    $data[] = array("id" => $id, "name" => $name, "flat_status"=>$flat_status);
	}

	// encoding array to json format
	echo json_encode($data);

}

// ===============================================================================

//start of room  details auto complete by flat id/name
if(isset($_POST['getrooms']) ){

	$rooms = $_POST['getrooms'];   // flat id

	$sql = "SELECT id, room_name, flat_t_id, occupancy_status FROM room_t WHERE flat_t_id=".$rooms;

	$result = mysqli_query($con,$sql);

	$data = array();

	while( $row = mysqli_fetch_array($result) ){
		
	    $id = $row['id'];
	    $name = $row['room_name'];
	    
	    $flat_id = $row['flat_t_id'];
	    $room_status = $row['occupancy_status'];

	    $data[] = array("id" => $id, "name" => $name, "flat_id"=>$flat_id, "room_status"=>$room_status);
	}

	// encoding array to json format
	echo json_encode($data);

}

// ===============================================================================

// ===============================================================================

//start of bed details auto complete by room name
if(isset($_POST['room']) ){

	$room_id = $_POST['room'];   // room id
	$flat_id = $_POST['flat'];	// flat id
	$property_id = $_POST['property'];  //property id

	$sql = "SELECT id, bed_name, room_t_id, property_t_id, flat_t_id, occupancy_status FROM bed_t WHERE room_t_id=" . $room_id . " " AND " " . "flat_t_id =".$flat_id . " " AND " " . "property_t_id=".$property_id . "";

	$result = mysqli_query($con,$sql);

	$data = array();

	while( $row = mysqli_fetch_array($result) ){
		
	    $id = $row['id'];
	    $name = $row['bed_name'];
	    $room_id = $row['room_t_id'];
	    $property_id = $row['property_t_id'];
	    $flat_id = $row['flat_t_id'];
	    $bed_status = $row['occupancy_status'];

	    $data[] = array("id" => $id, "name" => $name, "$room_id"=> $room_id, "property_id"=>$property_id, "flat_id"=>$flat_id, "bed_status"=>$bed_status);
	}

	// encoding array to json format
	echo json_encode($data);

}

// ===============================================================================

//start of Rental id/ details auto complete by customer id
if(isset($_POST['type']) && $_POST['type'] == 'rental_t.customer_t_id' ){
	$type = $_POST['type'];
	$name = $_POST['name_startsWith'];
	$query = "SELECT rental_t.`id`, rental_t.`property_t_id`, rental_t.`flat_t_id`, rental_t.`room_t_id`, rental_t.`bed_t_id`, rental_t.`customer_t_id`, customers_t.`name`, property_t.`property_name`, customers_t.`mobile` FROM rental_t INNER JOIN customers_t ON rental_t.`customer_t_id` = customers_t.`id` INNER JOIN property_t ON property_t.`id` = rental_t.`property_t_id` where UPPER($type) LIKE '%".strtoupper($name)."%'";
	$result = mysqli_query($con, $query);
	$data = array();
	while ($row = mysqli_fetch_assoc($result)) {
		$name = $row['id'].'|'.$row['property_t_id'].'|'.$row['flat_t_id'].'|'.$row['room_t_id'].'|'.$row['bed_t_id'].'|'.$row['customer_t_id'].'|'.$row['name'].'|'.$row['property_name'].'|'.$row['mobile'];
		array_push($data, $name);
	}
	mysqli_close($con);
	echo json_encode($data);exit;
}

// ===============================================================================

//start of Rental id/ details auto complete by customer name
if(isset($_POST['type']) && $_POST['type'] == 'name' ){
	$type = $_POST['type'];
	$name = $_POST['name_startsWith'];
	$query = "SELECT customers_t.`name`, rental_t.`property_t_id`, rental_t.`flat_t_id`, rental_t.`room_t_id`, rental_t.`bed_t_id`, rental_t.`id`, rental_t.`customer_t_id`, property_t.`property_name`, customers_t.`mobile` FROM rental_t INNER JOIN customers_t ON rental_t.`customer_t_id` = customers_t.`id` INNER JOIN property_t ON property_t.`id` = rental_t.`property_t_id` where UPPER($type) LIKE '%".strtoupper($name)."%'";
	$result = mysqli_query($con, $query);
	$data = array();
	while ($row = mysqli_fetch_assoc($result)) {
		$name = $row['name'].'|'.$row['property_t_id'].'|'.$row['flat_t_id'].'|'.$row['room_t_id'].'|'.$row['bed_t_id'].'|'.$row['id'].'|'.$row['customer_t_id'].'|'.$row['property_name'].'|'.$row['mobile'];
		array_push($data, $name);
	}
	mysqli_close($con);
	echo json_encode($data);exit;
}
// ===============================================================================

//start of property_id/name auto complete by name
// if(isset($_POST['type']) && $_POST['type'] == 'property_t.name' ){
// 	$type = $_POST['type'];
// 	$name = $_POST['name_startsWith'];
// 	$query = "SELECT id, name FROM property_t where UPPER($type) LIKE '%".strtoupper($name)."%'";
// 	$result = mysqli_query($con, $query);
// 	$data = array();
// 	while ($row = mysqli_fetch_assoc($result)) {
// 		$name = $row['id'].'|'.$row['name'];
// 		array_push($data, $name);
// 	}
// 	mysqli_close($con);
// 	echo json_encode($data);exit;
// }
// ===============================================================================







// if(isset($_POST['type']) && $_POST['type'] == 'clientAddress' ){
// 	$id = $_POST['id'];
// 	$query = "SELECT id, addressLine1, addressLine2, city, state, country, phone FROM customers_t WHERE id =$id";
// 	$result = mysqli_query($con, $query);
// 	$data = mysqli_fetch_assoc($result);
// 	$address = '';

// 	if(!empty( $data )){
// 		$address = isset($data['addressLine1'])? $data['addressLine1'].', &#13;' : '';
// 		$address .= isset($data['addressLine2'])? $data['addressLine2'].', &#13;': '';
// 		$city = isset($data['city'])? $data['city']: '';
// 		$state = isset($data['state'])? $data['state']: '';
// 		$country = isset($data['country'])? $data['country']: '';
// 		$phone = isset($data['phone'])? $data['phone']: '';
		
// 		$address .=$city.','.$state.', &#13;';
// 		$address .=$country.', &#13; ';
// 		$address .=$phone.'.';

		
// 	}
// 	mysqli_close($con);
// 	echo $address;exit;
// }
// auto complete creditor details



// if($_POST['type'] == 'creditor_details'){

// 	$name = $_POST['name_startsWith'];
// 	$query = "SELECT name, street, phone, currency FROM creditors_t where name LIKE '".strtoupper($name)."%'";
// 	$result = mysqli_query($con, $query);
// 	$data = array();
// 	while ($row = mysqli_fetch_assoc($result)) {
// 		$name = $row['name'].'|'.$row['street'].'|'.$row['phone'].'|'.$row['currency'];
// 		array_push($data, $name);	
// 	}	
// 	echo json_encode($data);
// }


// $result = mysql_query("SELECT * FROM country");	

// while($row = mysql_fetch_array($result)){
// 	echo $row['name']. "<br>";
// }

// if($_GET['type'] == 'country'){
// 	$result = mysql_query("SELECT name FROM country where name LIKE '".strtoupper($_GET['name_startsWith'])."%'");	
// 	$data = array();
// 	while ($row = mysql_fetch_array($result)) {
// 		array_push($data, $row['name']);	
// 	}	
// 	echo json_encode($data);
// }



?>