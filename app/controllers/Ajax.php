<?php 
	class Ajax extends Controller {
		public function __construct() {
			checkLogin();
			$this->customer = $this->loadModel('Customer');
			$this->product = $this->loadModel('Product');
			$this->supplier = $this->loadModel('Supplier');
			$this->manufacturer = $this->loadModel('Manufacturer');
			$this->CompareDetail = $this->loadModel('CompareDetail');
			$this->QuoteCompareDetail = $this->loadModel('QuoteCompareDetail');
			$this->QuoteDetail =$this->loadModel('QuoteDetail');
			$this->Quote =$this->loadModel('Quote');
		}

		public function fillcustomerdetails(){
			//start of customer details auto complete by name
			if(isset($_POST['type']) && $_POST['type'] == 'customers.name' ){
				$type = $_POST['type'];
				$name = $_POST['name_startsWith'];

				$result = $this->customer->ajaxCall($name, $type);
				$data = array();
				foreach ($result as $row) {
					$name = $row->id.'|'.$row->name.'|'.$row->phone.'|'.$row->currency.'|'.$row->trn.'|'.$row->street.'|'.$row->mobile .'|'.$row->city .'|'.$row->state .'|'.$row->country .'|'.$row->postal_code;
					array_push($data, $name);
				}
				echo json_encode($data);
				exit;
			}
		}
		/*--------------------------------------------------------------------*/

		public function fillcustomerid(){
			//start of customer details auto complete by id
			if(isset($_POST['type']) && $_POST['type'] == 'customers.id' ){
				$type = $_POST['type'];
				$name = $_POST['name_startsWith'];
				
				$result = $this->customer->getCustomerDetails($name, $type);

				$data = array();
				foreach ($result as $row) {
					$name = $row->id.'|'.$row->name.'|'.$row->phone.'|'.$row->currency.'|'.$row->trn.'|'.$row->address.'|'.$row->mobile;
					array_push($data, $name);
				}
				echo json_encode($data);
				exit;
			}
		}
		/*--------------------------------------------------------------------*/

		public function fillsupplieretails(){
			//start of customer details auto complete by name
			if(isset($_POST['type']) && $_POST['type'] == 'suppliers.name' ){
				$type = $_POST['type'];
				$name = $_POST['name_startsWith'];

				$result = $this->supplier->ajaxCall($name, $type);
				$data = array();
				foreach ($result as $row) {
					$name = $row->id.'|'.$row->name.'|'.$row->phone.'|'.$row->currency.'|'.$row->trn.'|'.$row->street.'|'.$row->mobile .'|'.$row->city .'|'.$row->state .'|'.$row->country .'|'.$row->postal_code;
					array_push($data, $name);
				}
				echo json_encode($data);
				exit;
			}
		}	

		/*--------------------------------------------------------------------*/

		public function fillsupplierid(){
			//start of creditor details auto complete by id
			if(isset($_POST['type']) && $_POST['type'] == 'suppliers.id' ){
				$type = $_POST['type'];
				$name = $_POST['name_startsWith'];
				$query = "SELECT id, name, phone, currency, trn, CONCAT(street, ' ',city,' ', state,' ', country,' ', postal_code) AS address FROM suppliers where UPPER($type) LIKE '%".strtoupper($name)."%'";
				$result = mysqli_query($con, $query);
				$data = array();
				while ($row = mysqli_fetch_assoc($result)) {
					$name = $row['id'].'|'.$row['name'].'|'.$row['phone'].'|'.$row['currency'].'|'.$row['trn'].'|'.$row['address'];
					array_push($data, $name);
				}
				mysqli_close($con);
				echo json_encode($data);exit;
			}

		}
		/*--------------------------------------------------------------------*/

		public function fillproductdetails(){
			//start of product details auto complete by product name
			if(isset($_POST['type']) && $_POST['type'] == 'products.name' ){
				$type = $_POST['type'];
				$name = $_POST['name_startsWith'];
				
				$result = $this->product->getProductDetails($name, $type);

				$data = array();
				foreach ($result as $row) {
					$name = $row->id.'|'.$row->name.'|'.$row->part_number.'|'.$row->description.'|'.$row->selling_price;
					array_push($data, $name);
				}
				echo json_encode($data);exit;
			}
		}
		/*--------------------------------------------------------------------*/

		public function customerinvdetails(){
			//start of product details auto complete by product name

			// $id = (int) $data = $_REQUEST['id'];			 

			 ## Read value
			$draw = $_POST['draw'];
			$row = $_POST['start'];
			$rowperpage = $_POST['length']; // Rows display per page
			$columnIndex = $_POST['order'][0]['column']; // Column index
			$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
			$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
			$searchValue = $_POST['search']['value']; // Search value

			## Custom Field value
			$searchByName = $_POST['customer_id'];
			// $searchByGender = $_POST['searchByGender'];

			## Search 
			$searchQuery = $searchByName;

			## Fetch records
			$result = $this->invoice->SelectAllByColumn($searchQuery, 'customer_id');

			$data = array();

			foreach ($result as $row) {
			   $data[] = array(
			     "inv"=>$row->id,
			     "customer"=>$row->customer_id,
			     "due_date"=>$row->due_date,
			     "total"=>$row->total,
			     "balance"=>$row->total
			   );
			   // array_push($data, $name);
			}

			## Response
			$response = array(
			  "draw" => intval($draw),
			  // "iTotalRecords" => $totalRecords,
			  // "iTotalDisplayRecords" => $totalRecordwithFilter,
			  "aaData" => $data
			);

			echo json_encode($response);
			
		}
		/*--------------------------------------------------------------------*/

		public function customerinvoicedetails(){

			$searchByName = $_POST['customer_id'];

			$data = $this->invoice->SelectAllByColumn($searchByName, 'customer_id');

			// $data = array();

			// foreach ($result as $row) {
			//    $data[] = array(
			//      "inv"=>$row->id,
			//      "customer"=>$row->customer_id,
			//      "due_date"=>$row->due_date,
			//      "total"=>$row->total,
			//      "balance"=>$row->total
			//    );
			//    // array_push($data, $name);
			// }

			echo json_encode($data);

		}

		/*--------------------------------------------------------------------*/

		public function customerinvoicedetails2(){

			$searchByName = $_POST['customer_id'];

			$result = $this->invoice->SelectAllByColumn($searchByName, 'customer_id');

			$data = array();

			foreach ($result as $row) {
			   $data[] = array(
			     "inv"=>$row->id,
			     "due_date"=>$row->due_date,
			     "total"=>$row->total,
			     "paid"=>$row->paid,
			     "balance"=>$row->balance
			   );
			   // array_push($data, $name);
			}

			echo json_encode($data);

		}
		/*--------------------------------------------------------------------*/

		public function getproducts(){
			switch($_REQUEST['sAction']){
			default :
			getProductsTable();
			break;

			case'getPaginator';
			// getPaginator();
			break;

			}
		}
		/*--------------------------------------------------------------------*/

		public function addtocompare(){
			$product_id=$_POST['product_id'];
			$session_id = $_SESSION['user_id'];

			$check_duplicate = $this->CompareDetail->checkSessionProduct($session_id, $product_id);

			$count = $this->CompareDetail->countColumnRows('created_by', $session_id);			

			if($count >= 6 ){
				echo json_encode(array("statusCode"=>202));
			} else {

				if($check_duplicate){
					echo json_encode(array("statusCode"=>201));
				} else {
					$product = $this->product->getById($product_id);

					if($product){

						$data = [

							'session_id'	=> $session_id,
							'product_id' 	=> $product_id,
							'name'			=> $product->name,

							// 'active' 		=> $_POST['active'],
							// 'assigned_to'	=> $_POST['assigned_to'],
							'date_entered' 	=> date('Y-m-d G:i:s'),
							'created_by' 	=> $session_id,
							'deleted'		=> 0
						];

						// Add product in database
						if($this->CompareDetail->create($data)){
							echo json_encode(array("statusCode"=>200));
							// $this->last_insert_id = $this->Model->lastInsertId();
							// $class_name = URLROOT . current_class($this);
							// $detail_url = "<a href='$class_name/detail/$this->last_insert_id'>$this->last_insert_id</a>";
							// $_SESSION['success'] = array("Success - $detail_url");
							// $_SESSION['msg'] = $upload_msgs;
						}
					}
				}
				
			}
		}
		// end of addToCompare

		/*--------------------------------------------------------------------*/

		public function removeFromCompare(){
			$product_id=$_POST['product_id'];
			$session_id = $_SESSION['user_id'];
			
			$delete = $this->CompareDetail->removeProduct($session_id, $product_id);

			if($delete){
				echo json_encode(array("statusCode"=>200));
			} else {
				echo json_encode(array("statusCode"=>201));
			}
			
		}
		/*--------------------------------------------------------------------*/

		public function addToExisting(){
			$product_id=$_POST['product_id'];
			$session_id = $_SESSION['user_id'];
			$quote_id = $_POST['quote_id'];

			$check_duplicate = $this->QuoteDetail->checkQuoteProduct($quote_id, $product_id);

			$count = $this->QuoteDetail->countColumnRows('quote_id', $quote_id);

			// $count = $this->QuoteDetail->countQuoteProducts($quote_id, $session_id, $product_id);

			// $count = $count1 + $count2;

			if($count >= 9 ){
				echo json_encode(array("statusCode"=>202));
			} else {

				if($check_duplicate){
					echo json_encode(array("statusCode"=>201));
				} else {
					$product = $this->product->getById($product_id);

					if($product){

						$quotedetail = [
							'quote_id'	=> $quote_id,

							// $add_products->id = $purch_detail_current_id;
							//$add_products->sno = $_POST['sno_'.$i];

							'product_id'	=> $product_id,
							'product_name'	=> $product->name,

							'manufacturer_id'	=> $product->manufacturer_id,
							'plan_type'	=> $product->plan_type,
							'category_id'	=> $product->category_id,

							'annual_cover'	=> $product->annual_cover,
							'area_of_cover'	=> $product->area_of_cover,
							'emergency_treatment_outside_area_of_cover'	=> $product->emergency_treatment_outside_area_of_cover,
							'chronic_conditions'	=>$product->chronic_conditions,
							'preexisting_conditions'	=> $product->preexisting_conditions,
							'congenital_conditions'	=> $product->congenital_conditions,
							'network'	=> $product->network,
							'plan_compliance'	=> $product->plan_compliance,
							'underwriting_critera'	=> $product->underwriting_critera,
							'pricing_model'	=> $product->pricing_model,

							// In-Patient benefits
							'ip_deductible'	=> $product->ip_deductible,
							'ip_room_board'	=> $product->ip_room_board,
							'ip_specialist_fees'	=> $product->ip_specialist_fees,
							'ip_surgery_anesthesia'	=> $product->ip_surgery_anesthesia,
							'ip_diagnostic_tests'	=> $product->ip_diagnostic_tests,
							'ip_prescribed_medication'	=> $product->ip_prescribed_medication,
							'ip_cancer_treatment'	=> $product->ip_cancer_treatment,
							'ip_organ_transplant'	=> $product->ip_organ_transplant,
							'ip_prosthetic_device'	=> $product->ip_prosthetic_device,
							'ip_rehabilitation'	=> $product->ip_rehabilitation,
							'ip_home_nursing'	=> $product->ip_home_nursing,
							'ip_companian_accomodation'	=> $product->ip_companian_accomodation,
							'ip_cashbenenit'	=> $product->ip_cashbenenit,

							// Out-Patient benefits
							'op_annual_limit'	=> $product->op_annual_limit,
							'op_deductible'	=> $product->op_deductible,
							'op_gp_fees'	=> $product->op_gp_fees,
							'op_specialist_fees'	=> $product->op_specialist_fees,
							'op_diagnostic_tests'	=> $product->op_diagnostic_tests,
							'op_prescribed_medication'	=> $product->op_prescribed_medication,
							'op_physiotherapy'	=> $product->op_physiotherapy,
							'op_alternative_treatment'	=> $product->op_alternative_treatment,
							'op_cancer_treatment'	=> $product->op_cancer_treatment,
							'op_prosthetic_device'	=> $product->op_prosthetic_device,

							// Psychiatric benefits
							'psychiatric_inpatient'	=> $product->psychiatric_inpatient,
							'psychiatric_outpatient'	=> $product->psychiatric_outpatient,

							// Maternity benefits
							'maternity_outpatient'	=> $product->maternity_outpatient,
							'maternity_inpatient'	=> $product->maternity_inpatient,
							'maternity_c_section'	=> $product->maternity_c_section,
							'maternity_complication'	=> $product->maternity_complication,
							'maternity_new_born'	=> $product->maternity_new_born,
							
							// Additional benefits
							'dental'	=> $product->dental,
							'optical'	=> $product->optical,
							'wellness'	=> $product->wellness,
							'vaccinations_dha_moh_approved'	=> $product->vaccinations_dha_moh_approved,
							'vaccinations_others_travel'	=> $product->vaccinations_others_travel,
							'group_life_personal_cover'	=> $product->group_life_personal_cover,

							// Evacuation & Repatriation benefits
							'evacuation'	=> $product->evacuation,
							'repatriation'	=> $product->repatriation,

							// Co-insurance INSIDE UAE
							'co_insur_uae_inside_network'	=> $product->co_insur_uae_inside_network,
							'co_insur_uae_outside_network'	=> $product->co_insur_uae_outside_network,

							// Co-Insurance OUTSIDE UAE
							'co_insur_outsideuae_inside_network'	=> $product->co_insur_outsideuae_inside_network,
							'co_insur_outsideuae_outside_network'	=> $product->co_insur_outsideuae_outside_network,

							// Premium summary
							'total_members'	=> $product->total_members,
							'payment_mode'	=> $product->payment_mode,
							'expiring_premium'	=> $product->expiring_premium,
							'proposed_annual_renewal_premium'	=> $product->proposed_annual_renewal_premium,
							'increase'	=> $product->increase,
							
					 	];

						// Add product in database
						if($this->QuoteDetail->create($quotedetail)){

							$data = [
								'id' => $quote_id,
								// 'active' 		=> $_POST['active'],
								// 'assigned_to'	=> $_POST['assigned_to'],
								'date_modified' 	=> date('Y-m-d G:i:s'),
								'modified_by' 	=> $session_id,
								// 'deleted'		=> 0
							];

							if($this->Quote->update($data)){
								echo json_encode(array("statusCode"=>200));
							}

							// $this->last_insert_id = $this->Model->lastInsertId();
							// $class_name = URLROOT . current_class($this);
							// $detail_url = "<a href='$class_name/detail/$this->last_insert_id'>$this->last_insert_id</a>";
							// $_SESSION['success'] = array("Success - $detail_url");
							// $_SESSION['msg'] = $upload_msgs;
						}
					}
				}
				
			}
		}
		// end of addToCompare

		/*--------------------------------------------------------------------*/

		public function deleteFromQuote(){
			$product_id=$_POST['product_id'];
			// $session_id = $_SESSION['user_id'];
			$quote_id = $_POST['quote_id'];

			
			$delete = $this->QuoteDetail->removeQuoteProduct($quote_id, $product_id);

			if($delete){
				echo json_encode(array("statusCode"=>200));
			} else {
				echo json_encode(array("statusCode"=>201));
			}
			
		}
		/*--------------------------------------------------------------------*/


		






	



	} // end of class