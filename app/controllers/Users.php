<?php 
	class Users extends Controller {

		public $errors = [];
		public $module = "Users";

		public function __construct() {

			checkLogin();

			$this->userModel = $this->loadModel('User');
		}
		/*--------------------------------------------------------------------*/
		public function index(){
			if($_SESSION['group_id'] == 1) {
				$this->list();
			}			
		}
		/*--------------------------------------------------------------------*/
		public function list(){
			if($_SESSION['group_id'] == 1){
				$data['list'] = $this->userModel->getList();
				$this->loadView('users/list', $data);
			}			
		}
		/*--------------------------------------------------------------------*/
		public function add(){

			if($_SESSION['group_id'] == 1){
				// Check for $_POST
				if($_SERVER['REQUEST_METHOD'] == 'POST'){

					//Process form

					//Sanitize POST data
					$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

					
					$data = [
						'name' 			=> $_POST['name'],
						'password' 		=> $_POST['password'],
						'first_name' 	=> $_POST['first_name'],
						'last_name' 	=> $_POST['last_name'],
						'group_id' 	=> $_POST['group_id'],
						'email' 		=> $_POST['email'],
						'phone' 		=> $_POST['phone'],
						'mobile' 		=> $_POST['mobile'],
						'phone_extension' => $_POST['phone_extension'],
						'is_admin' 		=> $_POST['is_admin'],					
						'activation_code' => '',

						'street' 		=> $_POST['street'],
						'city' 			=> $_POST['city'],
						'state' 		=> $_POST['state'],
						'postal_code'	=> $_POST['postal_code'],
						'country'		=> $_POST['country'],

						'time_zone' 	=> $_POST['time_zone'],
						'reports_to' 	=> $_POST['reports_to'],

						'active' 		=> $_POST['active'],
						'assigned_to'	=> $_POST['assigned_to'],
						'date_entered' 	=> date('Y-m-d G:i:s'),
						'created_by' 	=> $_SESSION['user_id'],
						'deleted'		=> 0
					];

					ob_get_contents();
					ob_end_clean();

					if($_SERVER['REQUEST_METHOD'] == 'POST' && empty($_POST) && empty($_FILES) && $_SERVER['CONTENT_LENGTH'] > 0 ){
					    $_SESSION['danger'] = array("Upload failed, file size exceeds the limit.");
					}


					// Validate Username, password, confirm_password, email
					if(empty($data['name']) || empty($data['password']) || empty($_POST['confirm_password'] || empty($data['email']) || empty($data['group_id'])) ){
						$data['errors'][] = "Please fill required fields";
					}

					// Validate username and check for duplicate
					if(isset($data['name'])){
						if($this->userModel->find_user($data['name']) || $this->userModel->find_user($data['email'])){
							$data['errors'][] = "Username or Email address already registered";	
						}					
					}

					// Validate password length
					if(strlen($data['password']) < 6){
						$data['errors'][] = "Password must be at least 6 characters";
					}

					// Validate confirm password
					if($data['password'] != $_POST['confirm_password']){
						$data['errors'][] = "Password do not match";
					}

					// Make sure errors array is empty
					if(empty($data['errors']) ){
						
						// Validated

						// Hash password
						$data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);

						if($upload_msgs = Upload::saveImage() ){
							$data['image'] = Upload::$image;
						}						

						// Add user in database
						if($this->userModel->create($data)){
							$last_insert_id = $this->userModel->lastInsertId();
							$class_name = URLROOT . current_class($this);
							$detail_url = "<a href='$class_name/edit/$last_insert_id'>$last_insert_id</a>";
							$_SESSION['success'] = array("Success - $detail_url");
							$_SESSION['msg'] = $upload_msgs;
							redirect('users/list');

						} else {
							$_SESSION['danger'] = array('Failed');
							$this->loadView('users/add', $data);
						}
						
					} else {
						$this->loadView('users/add', $data);					
					}

				} else {
					// Init data
					$data = [
						'name' 			=> '',
						'password' 		=> '',
						'confirm_password' 	=> '',
						'first_name' 	=> '',
						'last_name' 	=> '',
						'group_id' 		=> '',
						'email' 		=> '',
						'phone' 		=> '',
						'mobile' 		=> '',
						'phone_extension' => '',
						'is_admin' 		=> '',					
						'activation_code' => '',

						'street' 		=> '',
						'city' 			=> '',
						'state' 		=> '',
						'postal_code'	=> '',
						'country'		=> '',
					];

					// Load view
					$this->loadView('users/add', $data);

				}
			}			
		}
		/*--------------------------------------------------------------------*/
		public function detail($id){

			if($_SESSION['group_id'] == 1) {
				// $detail = $this->userModel->getById($id);

				$data['detail'] = $this->userModel->getById($id);
				$data['count'] = $this->userModel->countByName($data['detail']->name);
				$this->loadView('users/detail', $data);
			}			
		}

		/*--------------------------------------------------------------------*/
		public function edit($id){
			if($_SESSION['group_id'] == 1){
				// Check for $_POST
				if($_SERVER['REQUEST_METHOD'] == 'POST'){

					//Process form

					//Sanitize POST data
					$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
					
					$data = [
						'id'			=> $id,
						'name' 			=> $_POST['name'],
						// 'password' 		=> $_POST['password'],
						'first_name' 	=> $_POST['first_name'],
						'last_name' 	=> $_POST['last_name'],
						'group_id' 	=> $_POST['group_id'],
						'email' 		=> $_POST['email'],
						'phone' 		=> $_POST['phone'],
						'mobile' 		=> $_POST['mobile'],
						'phone_extension' => $_POST['phone_extension'],
						'is_admin' 		=> $_POST['is_admin'],					
						'activation_code' => '',

						'street' 		=> $_POST['street'],
						'city' 			=> $_POST['city'],
						'state' 		=> $_POST['state'],
						'postal_code'	=> $_POST['postal_code'],
						'country'		=> $_POST['country'],

						'time_zone' 	=> $_POST['time_zone'],
						'reports_to' 	=> $_POST['reports_to'],

						'active' 		=> $_POST['active'],
						'assigned_to'	=> $_POST['assigned_to'],
						'date_modified' 	=> date('Y-m-d G:i:s'),
						'modified_by' 	=> $_SESSION['user_id'],
						'deleted'		=> 0
					];



					ob_get_contents();
					ob_end_clean();

					if($_SERVER['REQUEST_METHOD'] == 'POST' && empty($_POST) && empty($_FILES) && $_SERVER['CONTENT_LENGTH'] > 0 ){
					    $_SESSION['danger'] = array("Upload failed, file size exceeds the limit.");
					}


					// Validate Username, password, confirm_password, email
					if(empty($data['name']) || empty($data['email']) || empty($data['group_id']) ){
						$data['errors'][] = "Please fill required fields";
					}

					// Validate username and check for duplicate
					if(isset($data['name'])){
						if($this->userModel->countByName($data['name']) > 1 || $this->userModel->countByName($data['name']) > 1){
							$data['errors'][] = "Username or Email address already registered";	
						}
					}



					// Validate password length
					// if(strlen($data['password']) < 6){
					// 	$data['errors'][] = "Password must be at least 6 characters";
					// }

					// Validate confirm password
					// if($data['password'] != $_POST['confirm_password']){
					// 	$data['errors'][] = "Password do not match";
					// }

					// Make sure errors array is empty
					if(empty($data['errors']) ){
						
						// Validated

						// Hash password
						// $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);

						if($upload_msgs = Upload::saveImage() ){
							$data['image'] = Upload::$image;
						}						

						// Add user in database
						if($this->userModel->update($data)){
							$_SESSION['user_time_zone'] = findName('timezones', $data['time_zone']);
							$class_name = URLROOT . current_class($this);
							$detail_url = "<a href='$class_name/edit/$id'>$id</a>";
							$_SESSION['success'] = array('Success ' . $detail_url);
							$_SESSION['msg'] = $upload_msgs;
							redirect('users/list');

						} else {
							$_SESSION['danger'] = array('Failed');
							$this->loadView('users/edit', $data);
						}
						
					} else {
						$this->loadView('users/edit', $data);					
					}

				} else {
					// Get existing user record from model
					$user = $this->userModel->getById($id);

					// Init data
					$data = [
						'id'			=> $id,
						'name' 			=> $user->name,
						// 'password' 		=> $user->password,
						'first_name' 	=> $user->first_name,
						'last_name' 	=> $user->last_name,
						'email' 		=> $user->email,
						'group_id' 	=> $user->group_id,
						'phone' 		=> $user->phone,
						'mobile' 		=> $user->mobile,
						'phone_extension' => $user->phone_extension,
						'is_admin' 		=> $user->is_admin,				
						'activation_code' => '',
						'image'			=> $user->image,

						'street' 		=> $user->street,
						'city' 			=> $user->city,
						'state' 		=> $user->state,
						'postal_code'	=> $user->postal_code,
						'country'		=> $user->country,

						'time_zone' 	=> $user->time_zone,
						'reports_to' 	=> $user->reports_to,

						'active' 		=> $user->active,
						'assigned_to'	=> $user->assigned_to,
						'date_modified' 	=> $user->date_modified,
						'modified_by' 	=> $user->modified_by,
						'deleted'		=> $user->deleted
					];

					$data['user'] = $this->userModel->getList();

					// Load view
					$this->loadView('users/edit', $data);

				}
			}
		}
		/*--------------------------------------------------------------------*/
		public function delete($id){
			if($_SESSION['group_id'] == 1){
				if($_SERVER['REQUEST_METHOD'] == 'POST'){

					if($_SESSION['user_id'] != 1){
						redirect('users/list');
					}

					if($this->userModel->delete($id) ){
						$_SESSION['success'] = array('Record Deleted - ' .$id);
						redirect('users/list');
					} else {
						die('Something went wrong');
					}
				} else {
					redirect('users/list');
				}
			}
			
		}
		/*--------------------------------------------------------------------*/
		public function reset($id){
			if($_SESSION['group_id'] == 1){
				// Check for $_POST
				if($_SERVER['REQUEST_METHOD'] == 'POST'){

					//Process form

					//Sanitize POST data
					$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
					
					$data = [
						'id'			=> $id,
						'name'			=> $_POST['name'],
						'password' 		=> $_POST['password'],
						'date_modified' 	=> date('Y-m-d G:i:s'),
						'modified_by' 	=> $_SESSION['user_id'],

					];


					// Validate Username, password, confirm_password, email
					if(empty($data['name']) || empty($data['password']) || empty($_POST['confirm_password'] ) ){
						$data['errors'][] = "Please fill required fields";
					}

					// Validate username and check for duplicate
					if(isset($data['name'])){
						if($this->userModel->countByName($data['name']) > 1){
							$data['errors'][] = "Username already registered";	
						}					
					}

					// Validate password length
					if(strlen($data['password']) < 6){
						$data['errors'][] = "Password must be at least 6 characters";
					}

					// Validate confirm password
					if($data['password'] != $_POST['confirm_password']){
						$data['errors'][] = "Password do not match";
					}

					// Make sure errors array is empty
					if(empty($data['errors']) ){
						
						// Validated

						// Hash password
						$data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);

						// Update user in database
						if($this->userModel->update($data)){
							$_SESSION['success'] = array('Success');
							redirect('users/list');

						} else {
							$_SESSION['danger'] = array('Failed');
							$this->loadView('users/reset', $data);
						}
						
					} else {
						$this->loadView('users/reset', $data);					
					}

				} else {
					// Get existing user record from model
					$user = $this->userModel->getById($id);

					// Init data
					$data = [
						'id'			=> $id,
						'name'			=> $user->name,
						'password' 		=> '',
						'confirm_password' 	=> '',
					];

					// Load view
					$this->loadView('users/reset', $data);

				}
			}			
		}
		/*--------------------------------------------------------------------*/
		public function mypassword($session_userid){
			if($_SESSION['user_id'] == $session_userid){

				$user = $this->userModel->getById($session_userid);

				// Check for $_POST
				if($_SERVER['REQUEST_METHOD'] == 'POST'){

					//Process form

					//Sanitize POST data
					$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
					
					$data = [
						'id'			=> $user->id,
						'name'			=> $user->name,
						'password' 		=> $_POST['password'],
						'date_modified' 	=> date('Y-m-d G:i:s'),
						'modified_by' 	=> $_SESSION['user_id'],

					];


					// Validate Username, password, confirm_password, email
					if(empty($data['name']) || empty($data['password']) || empty($_POST['confirm_password'] ) ){
						$data['errors'][] = "Please fill required fields";
					}

					// Validate username and check for duplicate
					if(isset($data['name'])){
						if($this->userModel->countByName($data['name']) > 1){
							$data['errors'][] = "Username already registered";	
						}					
					}

					// Validate password length
					if(strlen($data['password']) < 6){
						$data['errors'][] = "Password must be at least 6 characters";
					}

					// Validate confirm password
					if($data['password'] != $_POST['confirm_password']){
						$data['errors'][] = "Password do not match";
					}

					// Make sure errors array is empty
					if(empty($data['errors']) ){
						
						// Validated

						// Hash password
						$data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);

						// Update user in database
						if($this->userModel->update($data)){
							$_SESSION['success'] = array('Success');
							$this->loadView('users/mypassword', $data);

						} else {
							$_SESSION['danger'] = array('Failed');
							$this->loadView('users/mypassword', $data);
						}
						
					} else {
						$this->loadView('users/mypassword', $data);					
					}

				} else {

					// Get existing user record from model
					$user = $this->userModel->getById($session_userid);

					// Init data
					$data = [
						'id'			=> $user->id,
						'name'			=> $user->name,
						'password' 		=> '',
						'confirm_password' 	=> '',
					];

					// Load view
					$this->loadView('users/mypassword', $data);

				}
			}			
		}












		// public function login(){

		// 	// Check for $_POST
		// 	if($_SERVER['REQUEST_METHOD'] == 'POST'){

		// 		//Process form

		// 		//Sanitize POST data
		// 		$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
				
		// 		$data = [
		// 			'username' => trim($_POST['username']),
		// 			'password' => trim($_POST['password']),
		// 		];

		// 		// Validate Username, password, confirm_password, email
		// 		if(empty($data['username']) || empty($data['password']) ){
		// 			$data['errors'][] = "Please fill required fields";
		// 		}

		// 		// find username
		// 		if($this->userModel->find_user($data['username'])){
		// 			// user found


		// 		} else {
		// 			$data['errors'][] = "User not found";
		// 		}


		// 		// Make sure errors are empty
		// 		if(empty($data['errors']) ){
		// 			//Validated
		// 			// Verify username & password, set login
		// 			$loggedInUser = $this->userModel->verify_login($data['username'], $data['password']);

		// 			if($loggedInUser){
		// 				// Create Session
		// 				$this->createUserSession($loggedInUser);

		// 			} else {
		// 				$data['errors'][] = "Password incorrect";
		// 				$this->loadView('login', $data);
		// 			}
		// 		} else {
		// 			$this->loadView('login', $data);
		// 		}

				
		// 	} else {
		// 		// Init data
		// 		$data = [					
		// 			'username' => '',
		// 			'password' => ''
		// 		];

		// 		// Load view
		// 		$this->loadView('login', $data);
		// 	}

		// }
		/*--------------------------------------------------------------------*/

		// public function createUserSession($user){
		// 	$_SESSION['user_id'] = $user->id;
		// 	$_SESSION['user_email'] = $user->email;
		// 	$_SESSION['username'] = $user->username;
		// 	$_SESSION['user_fullname'] = $user->first_name . ' ' . $user->last_name;

		// 	redirect('pages/index');
		// }
		/*--------------------------------------------------------------------*/

		// public function logout(){
		// 	unset ($_SESSION['user_id']);
		// 	unset ($_SESSION['user_email']);
		// 	unset ($_SESSION['username']);
		// 	unset ($_SESSION['user_fullname']);
		// 	session_destroy();

		// 	redirect('login');
		// }
		/*--------------------------------------------------------------------*/

		// public function isLoggedIn(){
		// 	if(isset($_SESSION['user_id'])){
		// 		return true;
		// 	} else {
		// 		false;
		// 	}
		// }
		/*--------------------------------------------------------------------*/



	} // end of class
