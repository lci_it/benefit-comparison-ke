<?php 
	class Logout extends Controller{

		public function index(){
			unset ($_SESSION['user_id']);
			unset ($_SESSION['user_email']);
			unset ($_SESSION['username']);
			unset ($_SESSION['user_fullname']);
			session_destroy();

			redirect('login');
		}
		/*--------------------------------------------------------------------*/



	}