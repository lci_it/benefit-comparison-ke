<?php 
	class Settings extends Controller {

		public $errors = [];
		public $module = "Settings";

		public function __construct() {

			checkLogin();

			$this->Model = $this->loadModel('Setting');
		}
		/*--------------------------------------------------------------------*/
		public function index(){
			$this->list();
		}
		/*--------------------------------------------------------------------*/
		public function list(){
			$data['list'] = $this->Model->getList();

			$this->loadView(current_class($this) . '/list', $data);
		}

		/*--------------------------------------------------------------------*/






	} // end of class