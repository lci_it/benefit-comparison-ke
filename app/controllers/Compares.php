<?php 
	class Compares extends Controller {

		public $errors = [];
		public $module = "Comparisons";


		public function __construct() {
			checkLogin();
			$this->Model = $this->loadModel('Compare');
			$this->Product = $this->loadModel('Product');
			$this->CompareDetail = $this->loadModel('CompareDetail');
			$this->Manufacturer = $this->loadModel('Manufacturer');
		}
		/*--------------------------------------------------------------------*/
		public function index(){
			$this->list();
		}
		/*--------------------------------------------------------------------*/
		public function list(){
			$data['list'] = $this->Model->getList();
			$this->loadView(current_class($this) . '/list', $data);
		}
		/*--------------------------------------------------------------------*/
		public function add(){
			// Check for $_POST
			if($_SERVER['REQUEST_METHOD'] == 'POST'){

			//Process form

			//Sanitize POST data
				$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

				ob_get_contents();
				ob_end_clean();

				if($_SERVER['REQUEST_METHOD'] == 'POST' && empty($_POST) && empty($_FILES) && $_SERVER['CONTENT_LENGTH'] > 0 ){
				    $_SESSION['danger'] = array("Upload failed, file size exceeds the limit.");
				}

				try {
					$this->Model->startTransaction();
					if(!empty($_POST['customer_id']) && !empty($_POST['customer_name'])){
						
						$totalsum = $_POST['subtotal'] - $_POST['discount'];
						$vat_amount = $_POST['vat_amount'];

						$data = [
							'name' 		=> $_POST['name'],
							'customer_id' 		=> $_POST['customer_id'],
							'customer_name' 	=> $_POST['customer_name'],
							'street' => $_POST['street'],
							'city' 		=> $_POST['city'],
							'state' 	=> $_POST['state'],
							'country' 		=> $_POST['country'],
							'postal_code'		=> $_POST['postal_code'],
							'phone' 	=> $_POST['phone'],
							// 'mobile' 		=> $_POST['mobile'],
							// 'email'	=> $_POST['email'],

							// 'opportunity'	=> $_POST['opportunity'],
							'currency'	=> $_POST['currency'],
							// 'payment_mode'	=> $_POST['payment_mode'],
							// 'contact_id'	=> $_POST['contact_id'],
							'contact_name'	=> $_POST['contact_name'],
							'customer_trn'	=> $_POST['customer_trn'],
							'quote_stage' => 2,

							'salesperson'	=> $_POST['salesperson'],

							// 'invoice_no'	=> $_POST['invoice_no'],
							'date'	=> $_POST['date'],
							// 'quote_lpo_ref'	=> $_POST['quote_lpo_ref'],
							// 'delivery_to'	=> $_POST['delivery_to'],
							'payment_terms'	=> $_POST['payment_terms'],
							
							'subtotal'	=> $_POST['subtotal'],
							'discount'	=> $_POST['discount'],
							'vat_rate'	=> $_POST['vat_rate'],

							
							'vat_amount' => $_POST['vat_amount'],
							'total' => $totalsum + $vat_amount,

							// 'notes' 	=> $_POST['notes'],

							// 'active' 		=> $_POST['active'],
							'assigned_to'	=> $_POST['assigned_to'],
							'date_entered' 	=> date('Y-m-d G:i:s'),
							'created_by' 	=> $_SESSION['user_id'],
							'deleted'		=> 0
						];

						// if(isset($_POST['product_name-']) ) {
						// 	echo $line_total_sum = array_sum($_POST['line_total-']);
						// 	echo $purchase->purchase_invoice_total = $line_total_sum;
						// }

						// Add product in database
						if($this->Model->create($data)){
							$this->last_insert_id = $this->Model->lastInsertId();
							$class_name = URLROOT . current_class($this);
							$detail_url = "<a href='$class_name/detail/$this->last_insert_id'>$this->last_insert_id</a>";
							// $_SESSION['success'] = array("Success - $detail_url");
							// $_SESSION['msg'] = $upload_msgs;
						} else {
							throw new Exception(' Failed');
						}

						// $purchase->upload_all_files($_FILES['files']);
						// echo "<pre>";
						// print_r($_FILES['files']);

					} else {
						throw new Exception(' Supplier fields required');
					} // end of if statement check supplier details

					// $totalrow = count($_POST['product_name-']);
					// echo $totalrow;
					//$result_chk = '0';

					$totalrow = $_POST['totalrows'];

					// print_r(count($_POST['product_name-']));

					for($i = 0; $i < $totalrow; $i++){

						$deleted = $_POST['delete-'][$i];

						// echo $_POST['product_name-'][$i];

						if( $this->last_insert_id && $deleted == '0' ){

							if( isset($_POST['product_id-'][$i]) && isset($_POST['product_name-'][$i]) && !empty($_POST['unit_price-'][$i]) && $_POST['unit_price-'][$i] != 0 ) {

								$temp_product_id = $_POST['product_id-'][$i];
								// $check_product = Product::count_rows("id", $temp_product_id);

								if($this->Product->getByID( $temp_product_id ) ) {

									if( isset($_POST['product_qty-'][$i]) && $_POST['product_qty-'][$i] != 0 && $_POST['unit_price-'][$i] * $_POST['product_qty-'][$i] == $_POST['line_total-'][$i] ){

										// 	$sql = "SELECT COALESCE(MAX(CAST(id AS UNSIGNED)) +1, 1) as id FROM purchase_product_t_project_t_join";
										// $row = $add_products->find_by_query($sql);
										// $purch_detail_current_id = $row[0]->id;

										$product = [
											'quote_id'	=> $this->last_insert_id,

											// $add_products->id = $purch_detail_current_id;
											//$add_products->sno = $_POST['sno_'.$i];

											'product_id'	=> $_POST['product_id-'][$i],
											'product_name'	=> $_POST['product_name-'][$i],

											// $add_products->products_t_id = $_POST['product_id-'][$i];
											// $add_products->product_name = $_POST['product_name-'][$i];
											'part_number' => $_POST['part_number-'][$i],
					 	
										 	'product_description' => $_POST['product_desc-'][$i],
										 	// 'project_id' => $_POST['project_id-'][$i],
										 	// 'property_t_id' = $_POST['property_id-'][$i];
										 	// 'flat_t_id' = $_POST['flat_id-'][$i];
										 	'unit_price' => $_POST['unit_price-'][$i],
										 	'quantity' => $_POST['product_qty-'][$i],
										 	'line_total' => $_POST['line_total-'][$i],

									 	];

									 	if(!$this->QuoteDetail->create($product)) {
									 		throw new Exception(' Query failed');
								 		}
							 		} else {
							 			throw new Exception(' Correct Product line item values ');
						 			}
									
								} else {
									throw new Exception(' Product not found');									
						 		}

					 		} else {
					 			throw new Exception('Product Fields Required');
					 		}
					 	}
				 	} // end of for loop

				 	$upload_msgs = Upload::saveFiles($_FILES['files'], $this->last_insert_id, $this->module);

					$this->Model->commit();
					$_SESSION['success'] = array("Record added successfully <a href='$class_name/detail/$this->last_insert_id'>$this->last_insert_id</a>" );

					$_SESSION['msg'] = $upload_msgs;
					redirect(current_class($this) . '/list');

				} catch (Exception $e) {
					$_SESSION['danger'] = array("Transaction failed:  " . $e->getMessage() );
					$this->Model->rollback();
					$this->loadView(current_class($this) . '/add', $data);
				}

				// Validate required fields are filled
				// if(empty($data['name']) ){
				// 	$data['errors'][] = "Please fill required fields";
				// }

				// Validate product and check for duplicate
				// if(isset($data['name'])){
				// 	if($this->Model->find_name($data['name'])){
				// 		$data['errors'][] = "Name already registered";	
				// 	}					
				// }

				// Make sure errors array is empty

			} else {
				// Init data
				$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
				
				$data = [
					'name' 		=> '',
					'customer_id' 		=> '',
					'customer_name' 	=> '',
					'street' => '',
					'city' 		=> '',
					'state' 	=> '',
					'country' 		=> '',
					'postal_code'		=> '',
					'phone' 	=> '',
					'mobile' 		=> '',
					'email'	=> '',

					'opportunity'	=> '',
					'currency'	=> '',
					'payment_mode'	=> '',
					'contact_id'	=> '',
					'contact_name'	=> '',
					'customer_trn'	=> '',

					'salesperson'	=> '',

					'invoice_no'	=> '',
					'date'	=> '',
					'quote_lpo_ref'	=> '',
					'delivery_to'	=> '',
					'payment_terms'	=> '',
					
					'subtotal'	=> '',
					'discount'	=> '',
					'vat_rate'	=> '',

					'vat_amount' => '',
					'total' => '',




					'notes' 	=> '',

					// 'active' 		=> $_POST['active'],
					'assigned_to'	=> $_POST['assigned_to'],
					'date_entered' 	=> date('Y-m-d G:i:s'),
					'created_by' 	=> $_SESSION['user_id'],
					'deleted'		=> 0
				];

				$data['manufacturer'] = $this->Manufacturer->getList();
				$data['products'] = $this->Product->getList();

				// Load view
				$this->loadView(current_class($this) . '/add', $data);

			}
		}
		/*--------------------------------------------------------------------*/
		public function detail($id){

			$data['detail'] = $this->Model->getById($id);
			$data['products'] = $this->Model->getQuoteProducts($id);
			$data['files'] = $this->Model->getModuleFiles($this->module, $data['detail']->id);
			
			$this->loadView(current_class($this).'/detail', $data);
		}

		/*--------------------------------------------------------------------*/

		public function edit($id){
			// Check for $_POST
			if($_SERVER['REQUEST_METHOD'] == 'POST'){

				//check for $id in database, if found process
				if($quote = $this->Model->getById($id) ){

					if($quote->invoice_status == 1){
						$_SESSION['danger'] = array('Already Invoiced, Editing not possible ' . "<a href='detail/$id'>" . $id ."</a>");
						redirect(current_class($this) . '/list');						
					} else {
						//Process form //Sanitize POST data
						$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

						ob_get_contents();
						ob_end_clean();

						if($_SERVER['REQUEST_METHOD'] == 'POST' && empty($_POST) && empty($_FILES) && $_SERVER['CONTENT_LENGTH'] > 0 ){
						    $_SESSION['danger'] = array("Upload failed, file size exceeds the limit.");
						}

						try {
							$this->Model->startTransaction();
							if(!empty($_POST['customer_id']) && !empty($_POST['customer_name']) ){

								$totalsum = $_POST['subtotal'] - $_POST['discount'];
								$vat_amount = $_POST['vat_amount'];

								$data = [
									'id' => $id,
									'name' 		=> $_POST['name'],
									'customer_id' 		=> $_POST['customer_id'],
									'customer_name' 	=> $_POST['customer_name'],
									'street' => $_POST['street'],
									'city' 		=> $_POST['city'],
									'state' 	=> $_POST['state'],
									'country' 		=> $_POST['country'],
									'postal_code'		=> $_POST['postal_code'],
									'phone' 	=> $_POST['phone'],
									// 'mobile' 		=> $_POST['mobile'],
									// 'email'	=> $_POST['email'],

									// 'opportunity'	=> $_POST['opportunity'],
									'currency'	=> $_POST['currency'],
									// 'payment_mode'	=> $_POST['payment_mode'],
									// 'contact_id'	=> $_POST['contact_id'],
									'contact_name'	=> $_POST['contact_name'],
									'customer_trn'	=> $_POST['customer_trn'],
									'quote_stage' => $_POST['quote_stage'],

									'salesperson'	=> $_POST['salesperson'],

									// 'invoice_no'	=> $_POST['invoice_no'],
									'date'	=> $_POST['date'],
									// 'quote_lpo_ref'	=> $_POST['quote_lpo_ref'],
									// 'delivery_to'	=> $_POST['delivery_to'],
									'payment_terms'	=> $_POST['payment_terms'],
									
									'subtotal'	=> $_POST['subtotal'],
									'discount'	=> $_POST['discount'],
									'vat_rate'	=> $_POST['vat_rate'],

									
									'vat_amount' => $_POST['vat_amount'],
									'total' => $totalsum + $vat_amount,

									// 'notes' 	=> $_POST['notes'],

									// 'active' 		=> $_POST['active'],
									'assigned_to'	=> $_POST['assigned_to'],
									'date_entered' 	=> date('Y-m-d G:i:s'),
									'created_by' 	=> $_SESSION['user_id'],
									'deleted'		=> 0
								];

								// if(isset($_POST['product_name-']) ) {
								// 	echo $line_total_sum = array_sum($_POST['line_total-']);
								// 	echo $purchase->purchase_invoice_total = $line_total_sum;
								// }

								// Add product in database
								if($this->Model->update($data)){
									// $last_insert_id = $this->Model->lastInsertId();
									$class_name = URLROOT . current_class($this);
									$detail_url = "<a href='$class_name/detail/$id'>$id</a>";
									// $_SESSION['success'] = array("Success - $detail_url");
									// $_SESSION['msg'] = $upload_msgs;
								} else {
									throw new Exception(' Failed');
								}

								// $purchase->upload_all_files($_FILES['files']);
								// echo "<pre>";
								// print_r($_FILES['files']);

							} else {
								throw new Exception(' Supplier fields required');
							} // end of if statement check supplier details

							// $totalrow = count($_POST['product_name-']);
							// echo $totalrow;
							//$result_chk = '0';

							$totalrow = $_POST['totalrows'];

							// print_r(count($_POST['product_name-']));

							for($i = 0; $i < $totalrow; $i++){

								$deleted = $_POST['delete-'][$i];
								$table_id = $_POST['j_table_id-'][$i];

								// delete items
								if(isset($table_id) && !empty($table_id) && $deleted == 1 ){
									$delete_product = $this->QuoteDetail->deleteProduct($table_id);
								}

								// echo $_POST['product_name-'][$i];

								//update products
								if( $id && $deleted == '0' ){

									if( isset($_POST['product_id-'][$i]) && isset($_POST['product_name-'][$i]) && !empty($_POST['unit_price-'][$i]) && $_POST['unit_price-'][$i] != 0 ) {

										$temp_product_id = $_POST['product_id-'][$i];
										// $check_product = Product::count_rows("id", $temp_product_id);

										if($this->Product->getByID( $temp_product_id ) ) {

											if( isset($_POST['product_qty-'][$i]) && $_POST['product_qty-'][$i] != 0 && $_POST['unit_price-'][$i] * $_POST['product_qty-'][$i] == $_POST['line_total-'][$i] ){

												// 	$sql = "SELECT COALESCE(MAX(CAST(id AS UNSIGNED)) +1, 1) as id FROM purchase_product_t_project_t_join";
												// $row = $add_products->find_by_query($sql);
												// $purch_detail_current_id = $row[0]->id;

												$updateproduct = [
													'id' => $table_id,
													'quote_id'	=> $id,

													// $add_products->id = $purch_detail_current_id;
													//$add_products->sno = $_POST['sno_'.$i];

													'product_id'	=> $_POST['product_id-'][$i],
													'product_name'	=> $_POST['product_name-'][$i],

													// $add_products->products_t_id = $_POST['product_id-'][$i];
													// $add_products->product_name = $_POST['product_name-'][$i];
													'part_number' => $_POST['part_number-'][$i],
							 	
												 	'product_description' => $_POST['product_desc-'][$i],
												 	// 'project_id' => $_POST['project_id-'][$i],
												 	// 'property_t_id' = $_POST['property_id-'][$i];
												 	// 'flat_t_id' = $_POST['flat_id-'][$i];
												 	'unit_price' => $_POST['unit_price-'][$i],
												 	'quantity' => $_POST['product_qty-'][$i],
												 	'line_total' => $_POST['line_total-'][$i],

											 	];

											 	if(!$this->QuoteDetail->update($updateproduct)) {
											 		throw new Exception(' Query failed');
										 		}
									 		} else {
									 			throw new Exception(' Correct Product line item values ');
								 			}
											
										} else {
											throw new Exception(' Product not found');									
								 		}

							 		} else {
							 			throw new Exception('Product Fields Required');
							 		}
							 	}

							 	//add products
							 	if(empty($table_id) && isset($_POST['product_id-'][$i]) && isset($_POST['product_name-'][$i]) && isset($_POST['unit_price-'][$i]) && isset($_POST['product_qty-'][$i]) && $deleted == 0 ){

									// $add_product = new Purchase_Details_J();

									$addproduct = [
										'quote_id'	=> $id,

										// $add_products->id = $purch_detail_current_id;
										//$add_products->sno = $_POST['sno_'.$i];

										'product_id'	=> $_POST['product_id-'][$i],
										'product_name'	=> $_POST['product_name-'][$i],

										// $add_products->products_t_id = $_POST['product_id-'][$i];
										// $add_products->product_name = $_POST['product_name-'][$i];
										'part_number' => $_POST['part_number-'][$i],
				 	
									 	'product_description' => $_POST['product_desc-'][$i],
									 	// 'project_id' => $_POST['project_id-'][$i],
									 	// 'property_t_id' = $_POST['property_id-'][$i];
									 	// 'flat_t_id' = $_POST['flat_id-'][$i];
									 	'unit_price' => $_POST['unit_price-'][$i],
									 	'quantity' => $_POST['product_qty-'][$i],
									 	'line_total' => $_POST['line_total-'][$i],

								 	];

								 	if(!$this->QuoteDetail->create($addproduct)) {
								 		throw new Exception(' Query failed');
							 		}
								}
						 	} // end of for loop

							$totalfiles = $_POST['filerows'];

							for($i = 0; $i < $totalfiles; $i++){
								$delete = $_POST['delete_file-'][$i];
								$file = $_POST['file_id-'][$i];

								if($delete == 1){
									$this->Model->deleteFiles($file);
								}
							}

							$upload_msgs = Upload::saveFiles($_FILES['files'], $id, $this->module);

							$this->Model->commit();
							$_SESSION['success'] = array("Record added successfully <a href='$class_name/detail/$id'>$id</a>" );
							$_SESSION['msg'] = $upload_msgs;
							redirect(current_class($this) . '/list');

						} catch (Exception $e) {
							$_SESSION['danger'] = array("Transaction failed:  " . $e->getMessage() );
							$this->Model->rollback();
							$this->loadView(current_class($this) . '/edit', $data);
						}
					}
				} else {
					throw new Exception('ID Not Found');
				}
			
			} else {
				// Init data
				$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

				// Get existing user record from model
				if($this->Model->getById($id) ){
					$data['record'] = $this->Model->getById($id);
					$id = $data['record']->id;
					$data['product'] = $this->Model->SelectAllByTableColumn('quotes_products', $id, 'quote_id');
					$data['files'] = $this->Model->getModuleFiles($this->module, $id);

					// Load view
					$this->loadView(current_class($this) . '/edit', $data);
				} else {
					redirect(current_class($this) . '/list');
					$_SESSION['danger'] = array("ID Not Found" );
				}
				

			}
		}
		/*--------------------------------------------------------------------*/
		public function delete($id){

			if($_SESSION['user_id'] != 1){
				redirect(current_class($this) . '/list');
			}

			if($this->Model->delete($id) ){
				$_SESSION['success'] = array('Record Deleted - ' .$id);
				redirect(current_class($this) . '/list');
			} else {
				$_SESSION['danger'] = array("Something went wrong");
			}			
		}
		/*--------------------------------------------------------------------*/

		public function action(){
			if($_SERVER['REQUEST_METHOD'] == 'POST'){
				if(isset($_POST['checkBoxArray'])){

					foreach($_POST['checkBoxArray'] as $checkBox_post_id){
						
						$bulk_options= $_POST['bulk_options'];

						switch($bulk_options){
							
							case '' :
								if($checkBox_post_id){
									// $_SESSION['warning'] = array("Select ID checkbox and Select Option");
									$this->errors = "Select Option";
									redirect(current_class($this) . '/list');
								}
							break;

							case 'Draft' :

							break;

							case 'Published' :

							break;

							case 'deleteall' :
								if($checkBox_post_id){
									if($this->Model->delete($checkBox_post_id)){
										// $_SESSION['success'] = array('Record Deleted - ' .$id);
										redirect(current_class($this) . '/list');
									} else {
										$this->errors = "$checkBox_post_id - Failed";
									}
								}

								break;

							case 'Clone' :
								$query = "SELECT * FROM posts WHERE post_id = {$checkBox_post_id}";
								$clone_post_ids_query = mysqli_query($connection, $query);

								while($row = mysqli_fetch_array($clone_post_ids_query)){
									$post_title = $row['post_title'];
									$post_category_id = $row['post_category_id'];
									$post_author = $row['post_author'];
									$post_status = $row['post_status'];
									$post_image = $row['post_image'];
									$post_tags = $row['post_tags'];
									$post_comment_count = $row['post_comment_count'];
									$post_date = $row['post_date'];
									$post_user = $row['post_user'];
									$post_content = $row['post_content'];
								}

								$query = "INSERT INTO `posts` (`post_title`, `post_category_id`, `post_author`, `post_status`, `post_image`, `post_tags`, `post_content`, `post_date`) VALUES ('$post_title', '$post_category_id', '$post_author', '$post_status', '$post_image', '$post_tags', '$post_content', now())";

								$clone_post_query = mysqli_query($connection, $query);
								confirmQueryresult($clone_post_query);
							break;
						}
					}
					if(empty($this->errors) ){
						$_SESSION['success'] = array("Success");
					} else {
						$_SESSION['warning'] = array($this->errors);
					}
				} else {
					$_SESSION['warning'] = array("Select ID checkbox");
					redirect(current_class($this) . '/list');
				}
			}
		}
		/*--------------------------------------------------------------------*/



	} // end of class