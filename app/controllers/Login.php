<?php 
require_once(APPROOT . DS . 'helpers' .DS . 'session_helper.php');

	class Login extends Controller{

		public function __construct() {

			$this->userModel = $this->loadModel('User');
		}
		/*--------------------------------------------------------------------*/

		public function index(){
			$this->verify();
		}
		/*--------------------------------------------------------------------*/

		public function verify(){

			// Check for $_POST
			if($_SERVER['REQUEST_METHOD'] == 'POST'){

				//Process form

				//Sanitize POST data
				$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
				
				$data = [
					'username' => trim($_POST['username']),
					'password' => trim($_POST['password']),
				];

				// Validate Username, password, confirm_password, email
				if(empty($data['username']) || empty($data['password']) ){
					$data['errors'][] = "Please fill required fields";
				}

				// find username
				if($this->userModel->find_user($data['username'])){
					// user found

				} else {
					$data['errors'][] = "User not found";
				}

				// Make sure errors are empty
				if(empty($data['errors']) ){
					//Validated
					// Verify username & password, set login
					$loggedInUser = $this->userModel->verify_login($data['username'], $data['password']);

					if($loggedInUser){
						// Create Session
						$this->createUserSession($loggedInUser);

					} else {
						$data['errors'][] = "Password incorrect";
						$this->loadView('login', $data);
					}
				} else {
					$this->loadView('login', $data);
				}

			} else {
				// Init data
				$data = [					
					'username' => '',
					'password' => ''
				];

				// Load view
				$this->loadView('login', $data);
			}

		}

		/*--------------------------------------------------------------------*/

		protected function createUserSession($user){
			$_SESSION['user_id'] = $user->id;
			$_SESSION['user_email'] = $user->email;
			$_SESSION['username'] = $user->name;
			$_SESSION['user_fullname'] = $user->first_name . ' ' . $user->last_name;
			$_SESSION['user_time_zone'] = timeZoneDesc('timezones', $user->time_zone);
			$_SESSION['user_image'] = $user->image;
			$_SESSION['is_admin'] = $user->is_admin;
			$_SESSION['group_id'] = $user->group_id;
			$_SESSION['reports_to'] = $user->reports_to;
			$_SESSION['email'] = $user->email;

			if($_POST['password'] == 'Lci@123#'){
				redirect('users/mypassword/'.$_SESSION['user_id']);
			} else {
				redirect('quotes/list');
			}
			
		}
		/*--------------------------------------------------------------------*/






	} // end of class