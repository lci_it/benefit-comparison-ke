<?php 
	class Customers extends Controller {

		public $errors = [];
		public $module = "Customers";

		public function __construct() {
			checkLogin();
			$this->Model = $this->loadModel('Customer');
		}
		/*--------------------------------------------------------------------*/
		public function index(){
			$this->list();
		}
		/*--------------------------------------------------------------------*/
		public function list(){
			$data['list'] = $this->Model->getList();
			$this->loadView(current_class($this) . '/list', $data);
		}
		/*--------------------------------------------------------------------*/
		public function add(){
			// Check for $_POST
			if($_SERVER['REQUEST_METHOD'] == 'POST'){

				//Process form

				//Sanitize POST data
				$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
				
				$data = [
					'name' 			=> $_POST['name'],
					'trn' 		=> $_POST['trn'],
					'currency' 	=> $_POST['currency'],
					'phone' 	=> $_POST['phone'],
					'mobile' 		=> $_POST['mobile'],
					'email'	=> $_POST['email'],
					'street' => $_POST['street'],
					'city' 		=> $_POST['city'],

					'state' 	=> $_POST['state'],
					'country' 		=> $_POST['country'],
					'postal_code'		=> $_POST['postal_code'],
					'notes' 	=> $_POST['notes'],

					'active' 		=> $_POST['active'],
					'assigned_to'	=> $_POST['assigned_to'],
					'date_entered' 	=> date('Y-m-d G:i:s'),
					'created_by' 	=> $_SESSION['user_id'],
					'deleted'		=> 0
				];

				ob_get_contents();
				ob_end_clean();

				if($_SERVER['REQUEST_METHOD'] == 'POST' && empty($_POST) && empty($_FILES) && $_SERVER['CONTENT_LENGTH'] > 0 ){
				    $_SESSION['danger'] = array("Upload failed, file size exceeds the limit.");
				}


				// Validate required fields are filled
				if(empty($data['name']) ){
					$data['errors'][] = "Please fill required fields";
				}

				// Validate product and check for duplicate
				if(isset($data['name'])){
					if($this->Model->find_name($data['name'])){
						$data['errors'][] = "Name already registered";	
					}					
				}

				// Make sure errors array is empty
				if(empty($data['errors']) ){
					
					// Validated

					if($upload_msgs = Upload::saveImage() ){
						$data['image'] = Upload::$image;
					}

					// Add product in database
					if($this->Model->create($data)){
						$last_insert_id = $this->Model->lastInsertId();
						$class_name = URLROOT . current_class($this);
						$detail_url = "<a href='$class_name/detail/$last_insert_id'>$last_insert_id</a>";
						$_SESSION['success'] = array("Success - $detail_url");
						$_SESSION['msg'] = $upload_msgs;
						redirect(current_class($this) . '/list');

					} else {
						$_SESSION['danger'] = array('Failed');
						$this->loadView(current_class($this) . '/add', $data);
					}
					
				} else {
					$this->loadView(current_class($this) . '/add', $data);					
				}

			} else {
				// Init data
				$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
				
				$data = [
					'name'	=> '',
					'trn' 		=> '',
					'currency' 	=> '',
					'phone' 	=> '',
					'mobile' 		=> '',
					'email'	=> '',
					'street' => '',
					'city' 		=> '',

					'state' 	=> '',
					'country' 		=> '',
					'postal_code'		=> '',
					'notes' 	=> '',

					'active' 		=> '',
					'assigned_to'	=> '',
					'date_entered' 	=> '',
					'created_by' 	=> '',
					'deleted'		=> '',
				];

				// Load view
				$this->loadView(current_class($this) . '/add', $data);

			}
		}
		/*--------------------------------------------------------------------*/
		public function detail($id){

			$data['detail'] = $this->Model->getById($id);
			$this->loadView(current_class($this).'/detail', $data);
		}

		/*--------------------------------------------------------------------*/
		public function edit($id){
			// Check for $_POST
			if($_SERVER['REQUEST_METHOD'] == 'POST'){

				//Process form

				//Sanitize POST data
				$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
				
				$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
				
				$data = [
					'id'		=> $id,
					'name' 	=> $_POST['name'],
					'trn' 	=> $_POST['trn'],
					'currency' 	=> $_POST['currency'],
					'phone' 	=> $_POST['phone'],
					'mobile' 		=> $_POST['mobile'],
					'email'	=> $_POST['email'],
					'street' => $_POST['street'],
					'city' 		=> $_POST['city'],

					'state' 	=> $_POST['state'],
					'country' 		=> $_POST['country'],
					'postal_code'		=> $_POST['postal_code'],
					'notes' 	=> $_POST['notes'],

					// 'active' 		=> $_POST['active'],
					'assigned_to'	=> $_POST['assigned_to'],
					'date_modified' 	=> date('Y-m-d G:i:s'),
					'modified_by' 	=> $_SESSION['user_id']
				];

				ob_get_contents();
				ob_end_clean();

				if($_SERVER['REQUEST_METHOD'] == 'POST' && empty($_POST) && empty($_FILES) && $_SERVER['CONTENT_LENGTH'] > 0 ){
				    $_SESSION['danger'] = array("Upload failed, file size exceeds the limit.");
				}

				// Validate required fields are filled
				if(empty($data['name']) ){
					$data['errors'][] = "Please fill required fields";
				}

				// Validate product and check for duplicate
				if(isset($data['name'])){
					if($this->Model->find_name($data['name']) > 1){
						$data['errors'][] = "Name already registered";	
					}					
				}

				// Make sure errors array is empty
				if(empty($data['errors']) ){

					if($upload_msgs = Upload::saveImage() ){
						$data['image'] = Upload::$image;
					}					

					// Add user in database
					if($this->Model->update($data)){
						$class_name = URLROOT . current_class($this);
						$detail_url = "<a href='$class_name/detail/$id'>$id</a>";
						$_SESSION['success'] = array("Success - $detail_url");
						$_SESSION['msg'] = $upload_msgs;
						redirect(current_class($this) . '/list');

					} else {
						$_SESSION['danger'] = array('Failed');
						$this->loadView(current_class($this) . '/edit', $data);
					}
					
				} else {
					$this->loadView(current_class($this) . '/edit', $data);					
				}

			} else {
				// Get existing record from model
				$record = $this->Model->getById($id);

				// Init data
				$data = [
					'id'		=> $id,
					'name' 	=> $record->name,
					'trn' 	=> $record->trn,
					'currency' 	=> $record->currency,
					'phone' 	=> $record->phone,
					'mobile' => $record->mobile,
					'email'	=> $record->email,
					'street' => $record->street,
					'city' 	=> $record->city,

					'state' 	=> $record->state,
					'country' => $record->country,
					'postal_code'	=> $record->postal_code,
					'notes' 	=> $record->notes,

					// 'active' => $record->active,
					'assigned_to'	=> $record->assigned_to,
				];

				// $data['product'] = $this->productModel->getList();

				// Load view
				$this->loadView(current_class($this) . '/edit', $data);

			}
		}
		/*--------------------------------------------------------------------*/
		public function delete($id){
			if($_SERVER['REQUEST_METHOD'] == 'POST'){

				if($_SESSION['user_id'] != 1){
					redirect(current_class($this) . '/list');
				}

				if($this->Model->delete($id) ){
					$_SESSION['success'] = array('Record Deleted - ' .$id);
					redirect(current_class($this) . '/list');
				} else {
					$_SESSION['danger'] = array("Something went wrong");
				}
			} else {
				redirect(current_class($this) . '/list');
			}
			
		}
		/*--------------------------------------------------------------------*/

		
		/*--------------------------------------------------------------------*/




	} // end of class