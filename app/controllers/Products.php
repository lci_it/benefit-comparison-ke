<?php 
	class Products extends Controller {

		public $errors = [];
		public $module = "Products";

		public function __construct() {

			// checkLogin();

			$this->Model = $this->loadModel('Product');
			$this->importproduct = $this->loadModel('ImportProduct');
		}
		/*--------------------------------------------------------------------*/
		public function index(){
			$this->list();
		}
		/*--------------------------------------------------------------------*/
		public function list(){
			$data['list'] = $this->Model->getList();

			$this->loadView(current_class($this) . '/list', $data);
		}
		/*--------------------------------------------------------------------*/
		public function add(){
			// Check for $_POST
			if($_SERVER['REQUEST_METHOD'] == 'POST'){

				//Process form

				//Sanitize POST data
				$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

				$data = [
					
					'name'	=> $_POST['name'],
					'currency_id'	=> $_POST['currency_id'],
					'manufacturer_id'	=> $_POST['manufacturer_id'],
					'plan_type'	=> $_POST['product_type'],
					'category_id'	=> $_POST['category_id'],

					'annual_cover'	=> $_POST['annual_cover'],
					'area_of_cover'	=> $_POST['area_of_cover'],
					'emergency_treatment_outside_area_of_cover'	=> $_POST['emergency_treatment_outside_area_of_cover'],
					'preexisting_conditions'	=> $_POST['preexisting_conditions'],
					'congenital_conditions'	=> $_POST['congenital_conditions'],
					'network'	=> $_POST['network'],
					'plan_compliance'	=> $_POST['plan_compliance'],
					'underwriting_critera'	=> $_POST['underwriting_critera'],
					'pricing_model'	=> $_POST['pricing_model'],

					// In-Patient benefits
					'ip_deductible'	=> $_POST['ip_deductible'],
					'ip_room_board'	=> $_POST['ip_room_board'],
					'ip_specialist_fees'	=> $_POST['ip_specialist_fees'],
					'ip_surgery_anesthesia'	=> $_POST['ip_surgery_anesthesia'],
					'ip_diagnostic_tests'	=> $_POST['ip_diagnostic_tests'],
					'ip_prescribed_medication'	=> $_POST['ip_prescribed_medication'],
					'ip_cancer_treatment'	=> $_POST['ip_cancer_treatment'],
					'ip_organ_transplant'	=> $_POST['ip_organ_transplant'],
					'ip_prosthetic_device'	=> $_POST['ip_prosthetic_device'],
					'ip_rehabilitation'	=> $_POST['ip_rehabilitation'],
					'ip_home_nursing'	=> $_POST['ip_home_nursing'],
					'ip_companian_accomodation'	=> $_POST['ip_companian_accomodation'],
					'ip_cashbenenit'	=> $_POST['ip_cashbenenit'],

					// Out-Patient benefits
					'op_annual_limit'	=> $_POST['op_annual_limit'],
					'op_deductible'	=> $_POST['op_deductible'],
					'op_gp_fees'	=> $_POST['op_gp_fees'],
					'op_specialist_fees'	=> $_POST['op_specialist_fees'],
					'op_diagnostic_tests'	=> $_POST['op_diagnostic_tests'],
					'op_prescribed_medication'	=> $_POST['op_prescribed_medication'],
					'op_physiotherapy'	=> $_POST['op_physiotherapy'],
					'op_alternative_treatment'	=> $_POST['op_alternative_treatment'],
					'op_cancer_treatment'	=> $_POST['op_cancer_treatment'],
					'op_prosthetic_device'	=> $_POST['op_prosthetic_device'],

					// Psychiatric benefits
					'psychiatric_inpatient'	=> $_POST['psychiatric_inpatient'],
					'psychiatric_outpatient'	=> $_POST['psychiatric_outpatient'],

					// Maternity benefits
					'maternity_outpatient'	=> $_POST['maternity_outpatient'],
					'maternity_inpatient'	=> $_POST['maternity_inpatient'],
					'maternity_c_section'	=> $_POST['maternity_c_section'],
					'maternity_complication'	=> $_POST['maternity_complication'],
					'maternity_new_born'	=> $_POST['maternity_new_born'],
					
					// Additional benefits
					'dental'	=> $_POST['dental'],
					'optical'	=> $_POST['optical'],
					'wellness'	=> $_POST['wellness'],
					'vaccinations_dha_moh_approved'	=> $_POST['vaccinations_dha_moh_approved'],
					'vaccinations_others_travel'	=> $_POST['vaccinations_others_travel'],
					'group_life_personal_cover'	=> $_POST['group_life_personal_cover'],

					// Evacuation & Repatriation benefits
					'evacuation'	=> $_POST['evacuation'],
					'repatriation'	=> $_POST['repatriation'],

					// Co-insurance INSIDE UAE
					'co_insur_uae_inside_network'	=> $_POST['co_insur_uae_inside_network'],
					'co_insur_uae_outside_network'	=> $_POST['co_insur_uae_outside_network'],

					// Co-Insurance OUTSIDE UAE
					'co_insur_outsideuae_inside_network'	=> $_POST['co_insur_outsideuae_inside_network'],
					'co_insur_outsideuae_outside_network'	=> $_POST['co_insur_outsideuae_outside_network'],

					// Premium summary
					'total_members'	=> $_POST['total_members'],
					'payment_mode'	=> $_POST['payment_mode'],
					'expiring_premium'	=> $_POST['expiring_premium'],
					'proposed_annual_renewal_premium'	=> $_POST['proposed_annual_renewal_premium'],
					'increase'	=> $_POST['increase'],

					'comment'		=> $_POST['comment'],
					'active' 		=> $_POST['active'],
					'assigned_to'	=> $_POST['assigned_to'],
					'date_entered' 	=> date('Y-m-d G:i:s'),
					'created_by' 	=> $_SESSION['user_id'],
					'deleted'		=> 0
					
				];

				ob_get_contents();
				ob_end_clean();

				if($_SERVER['REQUEST_METHOD'] == 'POST' && empty($_POST) && empty($_FILES) && $_SERVER['CONTENT_LENGTH'] > 0 ){
				    $_SESSION['danger'] = array("Upload failed, file size exceeds the limit.");
				}


				// Validate required fields are filled
				if(empty($data['name']) || empty($_POST['category_id']) || empty($data['manufacturer_id']) || empty($data['plan_type']) ){
					$data['errors'][] = "Please fill required fields";
				}

				// Validate product and check for duplicate
				if(isset($data['name'])){
					if($this->Model->find_name($data['name'])){
						$data['errors'][] = "Name already registered";
					}
				}

				// Make sure errors array is empty
				if(empty($data['errors']) ){
					
					// Validated

					if($upload_msgs = Upload::saveImage() ){
						$data['image'] = Upload::$image;
					}

					// Add product in database
					if($this->Model->create($data)){
						$last_insert_id = $this->Model->lastInsertId();
						$class_name = URLROOT . current_class($this);
						$detail_url = "<a href='$class_name/detail/$last_insert_id'>$last_insert_id</a>";
						$_SESSION['success'] = array("Success - $detail_url");
						$_SESSION['msg'] = $upload_msgs;
						redirect(current_class($this) . '/list');

					} else {
						$_SESSION['danger'] = array('Failed');
						$this->loadView(current_class($this) . '/add', $data);
					}
					
				} else {
					$this->loadView(current_class($this) . '/add', $data);					
				}

			} else {
				// Init data
				$data = [
					'name' 			=> '',
					'currency_id'	=> '',
					'plan_type' => '',
					'model' 		=> '',
					'part_number' 	=> '',
					'description' 	=> '',
					'cost_price' 		=> '',
					'selling_price'	=> '',
					'manufacturer_id' 		=> '',
					'category_id' 		=> '',

					'product_accountset_t_id' 	=> '',
					'vat_code' 		=> '',
					'comment'		=> '',

					'active' 		=> '',
					'assigned_to'	=> '',
					'date_entered' 	=> '',
					'created_by' 	=> '',
					'deleted'		=> 0
				];

				// Load view
				$this->loadView(current_class($this) . '/add', $data);

			}
		}
		/*--------------------------------------------------------------------*/
		public function detail($id){

			$data['detail'] = $this->Model->getById($id);
			$this->loadView('products/detail', $data);
		}

		/*--------------------------------------------------------------------*/
		public function edit($id){
			// Check for $_POST
			if($_SERVER['REQUEST_METHOD'] == 'POST'){

				//Process form

				//Sanitize POST data
				$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
				
				$data = [

					'id'	=> (int)$id,					
					'name'	=> $_POST['name'],
					'manufacturer_id'	=> $_POST['manufacturer_id'],
					'plan_type'	=> $_POST['product_type'],
					'category_id'	=> $_POST['category_id'],
					
					'currency_id'	=> $_POST['currency_id'],
					'annual_cover'	=> $_POST['annual_cover'],
					'area_of_cover'	=> $_POST['area_of_cover'],
					'emergency_treatment_outside_area_of_cover'	=> $_POST['emergency_treatment_outside_area_of_cover'],
					'preexisting_conditions'	=> $_POST['preexisting_conditions'],
					'congenital_conditions'	=> $_POST['congenital_conditions'],
					'network'	=> $_POST['network'],
					'plan_compliance'	=> $_POST['plan_compliance'],
					'underwriting_critera'	=> $_POST['underwriting_critera'],
					'pricing_model'	=> $_POST['pricing_model'],

					// In-Patient benefits
					'ip_deductible'	=> $_POST['ip_deductible'],
					'ip_room_board'	=> $_POST['ip_room_board'],
					'ip_specialist_fees'	=> $_POST['ip_specialist_fees'],
					'ip_surgery_anesthesia'	=> $_POST['ip_surgery_anesthesia'],
					'ip_diagnostic_tests'	=> $_POST['ip_diagnostic_tests'],
					'ip_prescribed_medication'	=> $_POST['ip_prescribed_medication'],
					'ip_cancer_treatment'	=> $_POST['ip_cancer_treatment'],
					'ip_organ_transplant'	=> $_POST['ip_organ_transplant'],
					'ip_prosthetic_device'	=> $_POST['ip_prosthetic_device'],
					'ip_rehabilitation'	=> $_POST['ip_rehabilitation'],
					'ip_home_nursing'	=> $_POST['ip_home_nursing'],
					'ip_companian_accomodation'	=> $_POST['ip_companian_accomodation'],
					'ip_cashbenenit'	=> $_POST['ip_cashbenenit'],

					// Out-Patient benefits
					'op_annual_limit'	=> $_POST['op_annual_limit'],
					'op_deductible'	=> $_POST['op_deductible'],
					'op_gp_fees'	=> $_POST['op_gp_fees'],
					'op_specialist_fees'	=> $_POST['op_specialist_fees'],
					'op_diagnostic_tests'	=> $_POST['op_diagnostic_tests'],
					'op_prescribed_medication'	=> $_POST['op_prescribed_medication'],
					'op_physiotherapy'	=> $_POST['op_physiotherapy'],
					'op_alternative_treatment'	=> $_POST['op_alternative_treatment'],
					'op_cancer_treatment'	=> $_POST['op_cancer_treatment'],
					'op_prosthetic_device'	=> $_POST['op_prosthetic_device'],

					// Psychiatric benefits
					'psychiatric_inpatient'	=> $_POST['psychiatric_inpatient'],
					'psychiatric_outpatient'	=> $_POST['psychiatric_outpatient'],

					// Maternity benefits
					'maternity_outpatient'	=> $_POST['maternity_outpatient'],
					'maternity_inpatient'	=> $_POST['maternity_inpatient'],
					'maternity_c_section'	=> $_POST['maternity_c_section'],
					'maternity_complication'	=> $_POST['maternity_complication'],
					'maternity_new_born'	=> $_POST['maternity_new_born'],
					
					// Additional benefits
					'dental'	=> $_POST['dental'],
					'optical'	=> $_POST['optical'],
					'wellness'	=> $_POST['wellness'],
					'vaccinations_dha_moh_approved'	=> $_POST['vaccinations_dha_moh_approved'],
					'vaccinations_others_travel'	=> $_POST['vaccinations_others_travel'],
					'group_life_personal_cover'	=> $_POST['group_life_personal_cover'],

					// Evacuation & Repatriation benefits
					'evacuation'	=> $_POST['evacuation'],
					'repatriation'	=> $_POST['repatriation'],

					// Co-insurance INSIDE UAE
					'co_insur_uae_inside_network'	=> $_POST['co_insur_uae_inside_network'],
					'co_insur_uae_outside_network'	=> $_POST['co_insur_uae_outside_network'],

					// Co-Insurance OUTSIDE UAE
					'co_insur_outsideuae_inside_network'	=> $_POST['co_insur_outsideuae_inside_network'],
					'co_insur_outsideuae_outside_network'	=> $_POST['co_insur_outsideuae_outside_network'],

					// Premium summary
					'total_members'	=> $_POST['total_members'],
					'payment_mode'	=> $_POST['payment_mode'],
					'expiring_premium'	=> $_POST['expiring_premium'],
					'proposed_annual_renewal_premium'	=> $_POST['proposed_annual_renewal_premium'],
					'increase'	=> $_POST['increase'],

					'comment'		=> $_POST['comment'],
					'active' 		=> $_POST['active'],
					'assigned_to'	=> $_POST['assigned_to'],
					'date_modified' 	=> date('Y-m-d G:i:s'),
					'modified_by' 	=> $_SESSION['user_id'],

					
				];

				ob_get_contents();
				ob_end_clean();

				if($_SERVER['REQUEST_METHOD'] == 'POST' && empty($_POST) && empty($_FILES) && $_SERVER['CONTENT_LENGTH'] > 0 ){
				    $_SESSION['danger'] = array("Upload failed, file size exceeds the limit.");
				}

				try {
					$this->Model->startTransaction();

					// Validate required fields are filled
					if(!empty($data['name']) || !empty($_POST['category_id']) || !empty($data['manufacturer_id']) || !empty($data['plan_type']) || !empty($data['comment'])){

						// Validate product and check for duplicate
						if(isset($data['name'])){
							if($this->Model->find_name($data['name']) > 1){
								throw new Exception(' Name already registered');
							}
						}

						if(empty($data['comment'])){
							throw new Exception(' Comment required');
						}

						// Make sure errors array is empty

						if($upload_msgs = Upload::saveImage() ){
							$data['image'] = Upload::$image;
						}

						// Edit product in database
						if($this->Model->update($data)){
							$class_name = URLROOT . current_class($this);
							$detail_url = "<a href='$class_name/detail/$id'>$id</a>";
							$_SESSION['success'] = array("Success - $detail_url");
							$_SESSION['msg'] = $upload_msgs;
							redirect(current_class($this) . '/detail/' . $id);

						} else {
							throw new Exception(' Query failed');
						}
				
					} else {
						throw new Exception(' Please fill required fields');
					}

					$this->Model->commit();
					$_SESSION['success'] = array("Record Updated successfully <a href='$class_name/detail/$id'>$id</a>" );
					$_SESSION['msg'] = $upload_msgs;
					redirect(current_class($this) . '/detail/' . $id);
					
				} catch (Exception $e) {
					$_SESSION['danger'] = array("Transaction failed:  " . $e->getMessage() );
					$this->Model->rollback();
					redirect(current_class($this) . '/edit/' . $id);
				}

			} else {
				// Get existing record from model
				$data['product'] = $this->Model->getById($id);

				// Load view
				$this->loadView('products/edit', $data);

			}
		}
		/*--------------------------------------------------------------------*/
		public function delete($id){
			if($_SERVER['REQUEST_METHOD'] == 'POST'){

				if($_SESSION['user_id'] != 1){
					redirect(current_class($this) . '/list');
				}

				if($this->Model->delete($id) ){
					$_SESSION['success'] = array('Record Deleted - ' .$id);
					redirect(current_class($this) . '/list');
				} else {
					$_SESSION['danger'] = array("Something went wrong");
				}
			} else {
				redirect(current_class($this) . '/list');
			}
			
		}
		/*--------------------------------------------------------------------*/

		public function action(){
			if($_SERVER['REQUEST_METHOD'] == 'POST'){
				if(isset($_POST['checkBoxArray'])){

					foreach($_POST['checkBoxArray'] as $checkBox_post_id){
						
						$bulk_options= $_POST['bulk_options'];

						switch($bulk_options){
							case 'Draft' :

							break;

							case 'Published' :

							break;

							case 'deleteall' :
								if($checkBox_post_id){
									if($this->Model->delete($checkBox_post_id)){
										// $_SESSION['success'] = array('Record Deleted - ' .$id);
										redirect(current_class($this) . '/list');
									} else {
										$this->errors[] = "$checkBox_post_id - Failed";
									}
								}

								break;

							case 'Clone' :
								$query = "SELECT * FROM posts WHERE post_id = {$checkBox_post_id}";
								$clone_post_ids_query = mysqli_query($connection, $query);

								while($row = mysqli_fetch_array($clone_post_ids_query)){
									$post_title = $row['post_title'];
									$post_category_id = $row['post_category_id'];
									$post_author = $row['post_author'];
									$post_status = $row['post_status'];
									$post_image = $row['post_image'];
									$post_tags = $row['post_tags'];
									$post_comment_count = $row['post_comment_count'];
									$post_date = $row['post_date'];
									$post_user = $row['post_user'];
									$post_content = $row['post_content'];
								}

								$query = "INSERT INTO `posts` (`post_title`, `post_category_id`, `post_author`, `post_status`, `post_image`, `post_tags`, `post_content`, `post_date`) VALUES ('$post_title', '$post_category_id', '$post_author', '$post_status', '$post_image', '$post_tags', '$post_content', now())";

								$clone_post_query = mysqli_query($connection, $query);
								confirmQueryresult($clone_post_query);
							break;
						}
					}
					if(empty($this->errors) ){
						$_SESSION['success'] = array("Success");
					} else {
						$_SESSION['danger'] = array($this->errors);
					}
				} else {
					$_SESSION['warning'] = array("Select ID checkbox");
					redirect(current_class($this) . '/list');
				}
			}
		}
		/*--------------------------------------------------------------------*/

		public function import(){
			// Check for $_POST
			if($_SERVER['REQUEST_METHOD'] == 'POST'){

				//Process form

				//Sanitize POST data
				$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

				ob_get_contents();
				ob_end_clean();

				if($_SERVER['REQUEST_METHOD'] == 'POST' && empty($_POST) && empty($_FILES) && $_SERVER['CONTENT_LENGTH'] > 0 ){
				    $_SESSION['danger'] = array("Upload failed, file size exceeds the limit.");
				    $this->loadView(current_class($this) . '/import', $data);
				}

				try {
					$this->Model->startTransaction();

					if( (isset($_FILES['file']) && is_array( $_FILES['file'])) ){

						$data = [];

						// check if group membership is correct
						if(!$_SESSION['group_id'] = 1 || !$_SESSION['group_id'] == 3){
							throw new Exception(' Not athorized to upload file');
						}

						// Validate required fields are filled
						if(empty($_POST['comment']) ){
							throw new Exception(' Please fill comment ');
						}

						if($_FILES["file"]["size"] <= 0){
							throw new Exception(' Select the file to import');
						}

						// // Validate product and check for duplicate
						// if(isset($data['name'])){
						// 	if($this->Model->find_name($data['name'])){
						// 		$data['errors'][] = "Name already registered";
						// 	}
						// }

						//upload files
					 	// $upload_msgs = Upload::importFile($_FILES['file']);

						// Make sure errors array is empty
							
						// Validated

						$csv = $_FILES['file'];

				        if(isset($csv['tmp_name']) && !empty($csv['tmp_name'])) {

				            $finfo = finfo_open(FILEINFO_MIME_TYPE);
				            $mime = finfo_file($finfo, $csv['tmp_name']);
				            finfo_close($finfo);

				            $allowed_mime = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');

				            if(in_array($mime, $allowed_mime) && is_uploaded_file($csv['tmp_name']) ) {

				                // $f = fopen($csv['tmp_name'], 'r');

				                // fgetcsv($f);

				               	$filename= $csv["tmp_name"];

								if($csv["size"] > 0 ){

								  	$file = fopen($filename, "r");

								  	// fgets($file);

							        while (($getData = fgetcsv($file, 100000, ",")) !== FALSE){

							        	$data = [
							
											'manufacturer_id'	=> $getData[0],
											'name'	=> $getData[1],
											'plan_type'	=> $getData[2],
											'category_id'	=> $getData[3],

											'annual_cover'	=> $getData[4],
											'area_of_cover'	=> $getData[5],
											'emergency_treatment_outside_area_of_cover'	=> $getData[6],
											'preexisting_conditions'	=> $getData[7],
											'congenital_conditions'	=> $getData[8],
											'network'	=> $getData[9],
											'plan_compliance'	=> $getData[10],
											'underwriting_critera'	=> $getData[11],
											'pricing_model'	=> $getData[12],

											// In-Patient benefits
											'ip_deductible'	=> $getData[13],
											'ip_room_board'	=> $getData[14],
											'ip_specialist_fees'	=> $getData[15],
											'ip_surgery_anesthesia'	=> $getData[16],
											'ip_diagnostic_tests'	=> $getData[17],
											'ip_prescribed_medication'	=> $getData[18],
											'ip_cancer_treatment'	=> $getData[19],
											'ip_organ_transplant'	=> $getData[20],
											'ip_prosthetic_device'	=> $getData[21],
											'ip_rehabilitation'	=> $getData[22],
											'ip_home_nursing'	=> $getData[23],
											'ip_companian_accomodation'	=> $getData[24],
											'ip_cashbenenit'	=> $getData[25],

											// Out-Patient benefits
											'op_annual_limit'	=> $getData[26],
											'op_deductible'	=> $getData[27],
											'op_gp_fees'	=> $getData[28],
											'op_specialist_fees'	=> $getData[29],
											'op_diagnostic_tests'	=> $getData[30],
											'op_prescribed_medication'	=> $getData[31],
											'op_physiotherapy'	=> $getData[32],
											'op_alternative_treatment'	=> $getData[33],
											'op_cancer_treatment'	=> $getData[34],

											// Psychiatric benefits
											'psychiatric_inpatient'	=> $getData[35],
											'psychiatric_outpatient'	=> $getData[36],

											// Maternity benefits
											'maternity_outpatient'	=> $getData[37],
											'maternity_inpatient'	=> $getData[38],
											'maternity_c_section'	=> $getData[39],
											'maternity_complication'	=> $getData[40],
											'maternity_new_born'	=> $getData[41],
											
											// Additional benefits
											'dental'	=> $getData[42],
											'optical'	=> $getData[43],
											'wellness'	=> $getData[44],
											'vaccinations_dha_moh_approved'	=> $getData[45],
											'vaccinations_others_travel'	=> $getData[46],

											// Evacuation & Repatriation benefits
											'evacuation'	=> $getData[47],
											'repatriation'	=> $getData[48],

											// Co-insurance INSIDE UAE
											'co_insur_uae_inside_network'	=> $getData[49],
											'co_insur_uae_outside_network'	=> $getData[50],

											// Co-Insurance OUTSIDE UAE
											'co_insur_outsideuae_inside_network'	=> $getData[51],
											'co_insur_outsideuae_outside_network'	=> $getData[52],

											// Premium summary
											'total_members'	=> $getData[53],
											'payment_mode'	=> $getData[54],
											'expiring_premium'	=> $getData[55],
											'proposed_annual_renewal_premium'	=> $getData[56],
											'increase'	=> $getData[57],
											'currency_id'	=> $getData[58],

											'comment'		=> '',
											'active' 		=> 1,
											'assigned_to'	=> $_POST['assigned_to'],
											'date_entered' 	=> date('Y-m-d G:i:s'),
											'created_by' 	=> $_SESSION['user_id'],
											'deleted'		=> 0
										
										];

							        	if(isset($getData[1])){
											if($this->Model->find_name($getData[1]) && $this->Model->find_name($getData[10])){
												throw new Exception(' Name already exists - '. $getData[1] );
											}
										}									
										

							           // $sql = "INSERT into employeeinfo (emp_id,firstname,lastname,email,reg_date) 
							           //     values ('".$getData[0]."','".$getData[1]."','".$getData[2]."','".$getData[3]."','".$getData[4]."')";

						               // $result = mysqli_query($con, $sql);



						               // Add product in database
										if(!$this->Model->create($data)){												
											throw new Exception(' Query Failed');
										}

										// if(!isset($result))
										// {
										// 	echo "<script type=\"text/javascript\">
										// 			alert(\"Invalid File:Please Upload CSV File.\");
										// 			window.location = \"index.php\"
										// 		  </script>";		
										// }
										// else {
										// 	  echo "<script type=\"text/javascript\">
										// 		alert(\"CSV File has been successfully Imported.\");
										// 		window.location = \"index.php\"
										// 	</script>";
										// }
								        

									}



							         	
									
							        fclose($file);

							        $data['comment'] = [

										'comment'		=> $_POST['comment'],
										'assigned_to'	=> $_POST['assigned_to'],
										'date_entered' 	=> date('Y-m-d G:i:s'),
										'created_by' 	=> $_SESSION['user_id'],
										'deleted'		=> 0
									];

									if(!$this->importproduct->create($data['comment'])){
										throw new Exception(' Comment update failed');
									}
								}
				            } else {
				            	throw new Exception(' Please upload CSV file');
				            	// $this->loadView(current_class($this) . '/import', $data);
				            }
				        }

						
						 // end of import function

						// if($upload_msgs = Upload::saveImage() ){
						// 	$data['image'] = Upload::$image;
						// }

						$this->Model->commit();
						$_SESSION['success'] = array("Import Successfull" );
						// $_SESSION['msg'] = $upload_msgs;
						redirect(current_class($this) . '/import');
							
						
					}

					
				} catch (Exception $e) {
					$_SESSION['danger'] = array("Transaction failed:  " . $e->getMessage() );
					$this->Model->rollback();
					$this->loadView(current_class($this) . '/import', $data);
					
				}

				

			} else {
				// Init data
				$data = [
				];

				// Load view
				$this->loadView(current_class($this) . '/import', $data);

			}

		}
		// import end





	} // end of class