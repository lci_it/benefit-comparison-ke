<?php 
 class Pages extends Controller {
 	public function __construct() {

			if(!isLoggedIn()){
				redirect('login');
				exit;
			}
		}

 	public function index(){

 		$data = [
 			'title' => 'Welcome to LINKERP',
 			'description' => 'Demo Description'

 		];
 		
 		// $this->loadView('pages/index', $data);
 		redirect('quotes/list');

 	}

 	public function about(){
 		$data = [
 			'title' => 'About Us'
 		];
 		$this->loadView('pages/about');
 		
 	}


 }

