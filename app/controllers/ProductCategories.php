<?php 
	class ProductCategories extends Controller {

		public $errors = [];
		public $module = "Categories";

		public function __construct() {

			checkLogin();

			$this->Model = $this->loadModel('ProductCategory');
		}
		/*--------------------------------------------------------------------*/
		public function index(){
			$this->list();
		}
		/*--------------------------------------------------------------------*/
		public function list(){
			$data['list'] = $this->Model->getList();
			$this->loadView(current_class($this) . '/list', $data);
		}
		/*--------------------------------------------------------------------*/
		public function add(){
			// Check for $_POST
			if($_SERVER['REQUEST_METHOD'] == 'POST'){

				//Process form

				//Sanitize POST data
				$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
				
				$data = [
					'name' 			=> $_POST['name'],
					'parent_category_id' 		=> $_POST['parent_category_id'],
					'description' 	=> $_POST['description'],
					'image' 	=> $_POST['image'],
					
					'notes' 		=> $_POST['notes'],

					'active' 		=> $_POST['active'],
					'assigned_to'	=> $_SESSION['user_id'],
					'date_entered' 	=> date('Y-m-d G:i:s'),
					'created_by' 	=> $_SESSION['user_id'],
					'deleted'		=> 0
				];

				ob_get_contents();
				ob_end_clean();

				if($_SERVER['REQUEST_METHOD'] == 'POST' && empty($_POST) && empty($_FILES) && $_SERVER['CONTENT_LENGTH'] > 0 ){
				    $_SESSION['danger'] = array("Upload failed, file size exceeds the limit.");
				}

				// Validate Username, password, confirm_password, email
				if(empty($data['name']) || empty($data['parent_category_id']) ){
					$data['errors'][] = "Please fill required fields";
				}

				// Validate username and check for duplicate
				if(isset($data['name'])){
					if($this->Model->find_user($data['name'])){
						$data['errors'][] = "Category already registered";	
					}					
				}

				// Make sure errors array is empty
				if(empty($data['errors']) ){
					
					// Validated
					if($upload_msgs = Upload::saveImage() ){
						$data['image'] = Upload::$image;
					}

					// Add user in database
					if($this->Model->create($data)){
						$last_insert_id = $this->Model->lastInsertId();
						$class_name = URLROOT . current_class($this);
						$detail_url = "<a href='$class_name/edit/$last_insert_id'>$last_insert_id</a>";
						$_SESSION['success'] = array("Success - $detail_url");
						$_SESSION['msg'] = $upload_msgs;
						redirect(current_class($this) . '/list');

					} else {
						$_SESSION['danger'] = array('Failed');
						$this->loadView(current_class($this) . '/add', $data);
					}
					
				} else {
					$this->loadView(current_class($this) . '/add', $data);					
				}

			} else {
				// Init data
				$data = [
					
				];

				// Load view
				$this->loadView(current_class($this) . '/add', $data);

			}
		}
		/*--------------------------------------------------------------------*/
		public function detail($id){
			// $detail = $this->userModel->getById($id);

			$data['detail'] = $this->Model->getById($id);
			$this->loadView(current_class($this) . '/detail', $data);
		}

		/*--------------------------------------------------------------------*/
		public function edit($id){
			// Check for $_POST
			if($_SERVER['REQUEST_METHOD'] == 'POST'){

				//Process form

				//Sanitize POST data
				$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
				
				$data = [
					'name' 			=> $_POST['name'],
					'parent_category_id' 		=> $_POST['parent_category_id'],
					'description' 	=> $_POST['description'],
					'image' 	=> $_POST['image'],
					'sort_order' 		=> $_POST['sort_order'],
					'sales_revenue_in' 		=> $_POST['sales_revenue_in'],
					'cost_of_sales_in' 		=> $_POST['cost_of_sales_in'],
					'notes' 		=> $_POST['notes'],

					'active' 		=> $_POST['active'],
					'assigned_to'	=> $_SESSION['user_id'],
					'date_modified' 	=> date('Y-m-d G:i:s'),
					'modified_by' 	=> $_SESSION['user_id']
				];

				ob_get_contents();
				ob_end_clean();

				if($_SERVER['REQUEST_METHOD'] == 'POST' && empty($_POST) && empty($_FILES) && $_SERVER['CONTENT_LENGTH'] > 0 ){
				    $_SESSION['danger'] = array("Upload failed, file size exceeds the limit.");
				}


				// Validate Username, password, confirm_password, email
				// if(empty($data['name']) || empty($data['password']) || empty($_POST['confirm_password'] || empty($data['email'])) ){
				// 	$data['errors'][] = "Please fill required fields";
				// }

				// Validate username and check for duplicate
				if(isset($data['name'])){
					if($this->userModel->countByName($data['name']) > 1){
						$data['errors'][] = "Username already registered";	
					}
				}

				// Validate password length
				// if(strlen($data['password']) < 6){
				// 	$data['errors'][] = "Password must be at least 6 characters";
				// }

				// Validate confirm password
				// if($data['password'] != $_POST['confirm_password']){
				// 	$data['errors'][] = "Password do not match";
				// }

				// Make sure errors array is empty
				if(empty($data['errors']) ){
					
					// Validated

					// Hash password
					// $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);

					if($upload_msgs = Upload::saveImage() ){
						$data['image'] = Upload::$image;
					}						

					// Add user in database
					if($this->Model->update($data)){
						$_SESSION['user_time_zone'] = findName('timezones', $data['time_zone']);
						$_SESSION['success'] = array('Success');
						$_SESSION['msg'] = $upload_msgs;
						redirect('users/list');

					} else {
						$_SESSION['danger'] = array('Failed');
						$this->loadView('users/edit', $data);
					}
					
				} else {
					$this->loadView('users/edit', $data);					
				}

			} else {
				// Get existing user record from model
				$user = $this->userModel->getById($id);

				// Init data
				$data = [
					
				];

				$data['record'] = $this->Model->getList();

				// Load view
				$this->loadView(current_class($this) . '/edit', $data);

			}
		}
		/*--------------------------------------------------------------------*/
		public function delete($id){
			if($_SERVER['REQUEST_METHOD'] == 'POST'){

				if($_SESSION['user_id'] != 1){
					redirect('productcategories/list');
				}

				if($this->Model->delete($id) ){
					$_SESSION['success'] = array('Record Deleted - ' .$id);
					redirect('productcategories/list');
				} else {
					die('Something went wrong');
				}
			} else {
				redirect('productcategories/list');
			}
			
		}
		/*--------------------------------------------------------------------*/

		public function find_parent_name($id){
		  if($this->parent_category_id == 0 ){
		    return $this->name;
		  }
		  
		  $parent_name = $this->find_by_query("SELECT * FROM " . static::$table . " WHERE id = $id");
		  foreach($parent_name as $parent){
		    return $parent->name;
		  }
		}











	} //end of class