<?php include (APPROOT . "/views/inc/admin_header.php"); ?>
	
	<!-- widget grid -->
	<section id="widget-grid" class="col-">
	
		<!-- <div class="well">
			
		</div> -->
	
		<!-- row -->
		<div class="row">
	
			<!-- NEW WIDGET START -->
			<article class="col-md-12">
	
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false">
					<!-- widget options:
					usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
	
					data-widget-colorbutton="false"
					data-widget-editbutton="false"
					data-widget-togglebutton="false"
					data-widget-deletebutton="false"
					data-widget-fullscreenbutton="false"
					data-widget-custombutton="false"
					data-widget-collapsed="true"
					data-widget-sortable="false"
	
					-->
					<header>
						<span class="widget-icon"> <i class="fa fa-eye"></i> </span>
						<h2>Add User Form</h2>
	
					</header>
	
					<!-- widget div-->
					<div>
	
						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
	
						</div>
						<!-- end widget edit box -->
	
						<!-- widget content -->
						<div class="widget-body">
	

						<form class="form-horizontal" method="post" action="<?php echo htmlspecialchars( URLROOT . current_class($this) . '/add'); ?>" enctype="multipart/form-data">

								<fieldset>
									<!-- <legend>User Details </legend> -->
									

								<div class="form-group"> <!-- START OF FORM GROUP USER DETAILS-->
									
									<div class="col-xs-12 col-sm-6 col-lg-2">
										<label class="control-label">Username</label>
										<div class="">
											<input class="form-control input-xs" type="text" name="name" required>
										</div>
									</div>
									
									<div class="col-xs-12 col-sm-6 col-lg-2">
										<label class="control-label">Password</label>
										<div class="">
											<input class="form-control input-xs" type="password" name="password" required>
										</div>
									</div>

									<div class="col-xs-12 col-sm-6 col-lg-2">
										<label class="control-label">Confirm Password</label>
										<div class="">
											<input class="form-control input-xs" type="password" name="confirm_password" required>
										</div>
									</div>

									<div class="col-xs-12 col-sm-6 col-lg-2">
										<label class="control-label">Firstname</label>
										<div class="">
											<input class="form-control input-xs" type="text" name="first_name">
										</div>
									</div>

									<div class="col-xs-12 col-sm-6 col-lg-2">
										<label class="control-label">Lastname</label>
										<div class="">
											<input class="form-control input-xs" type="text" name="last_name">
										</div>
									</div>

									<div class="col-xs-8 col-sm-6 col-lg-2">
										<label class="control-label">User Role</label>
										<div class="">
											<select class="form-control input-xs" id="group_id" name="group_id" required>
												<option value="" selected>Select</option>
												<?php echo getListFromTable('groupmember'); ?>
											</select>
										</div>
									</div>

									<div class="col-xs-12 col-sm-6 col-lg-2">
										<label class="control-label">Phone</label>
										<div class="">
											<input class="form-control  input-xs" type="text" name="phone">
										</div>
									</div>
									
									<div class="col-xs-12 col-sm-6 col-lg-2">
										<label class="control-label">Ext No</label>
										<div class="">
											<input class="form-control  input-xs" type="text" name="phone_extension">
										</div>
									</div>


									<div class="col-xs-12 col-sm-6 col-lg-2">
										<label class="control-label">Mobile</label>
										<div class="">
											<input class="form-control  input-xs" type="text" name="mobile">
										</div>
									</div>

									<div class="col-xs-12 col-sm-6 col-lg-2">
										<label class="control-label">Email</label>
										<div class="">
											<input class="form-control input-xs" type="email" name="email" required>
										</div>
									</div>

									<div class="col-xs-12 col-sm-6 col-lg-2">
										<label class="control-label">is Admin</label>
										<div class="">
											<select class="form-control input-xs" name="is_admin">											
												<option value="1">Yes</option>
												<option selected value="0">No</option>											
											</select> 											
										</div>
										
									</div><!-- END OF select active -->


									<div class="col-xs-12 col-sm-6 col-lg-2">
										<label class="control-label">Active</label>
										<div class="">
											<select class="form-control input-xs" name="active">											
												<option selected value="1">Yes</option>
												<option value="0">No</option>										
											</select>										
										</div>
										
									</div><!-- END OF select active -->

							

								</div> <!-- END OF FORM GROUP USER DETAILS-->

								</fieldset>




								<fieldset>
								<legend>Address Details</legend>
									<div class="form-group">
									
									<div class="col-xs-12 col-sm-6 col-lg-2">
										<label class="control-label">Street</label>
										<div class="">
										<input class="form-control input-xs" type="text" name="street">
										</div>
									</div>

									<div class="col-xs-12 col-sm-6 col-lg-2">
										<label class="control-label">City</label>
										<div class="">
										<input class="form-control input-xs"  type="text" name="city">
										</div>
									</div>

									<div class="col-xs-12 col-sm-6 col-lg-2">
										<label class="control-label">State</label>
										<div class="">
										<input class="form-control input-xs" type="text" name="state">
										</div>
									</div>

									<div class="col-xs-12 col-sm-6 col-lg-2">
										<label class="control-label">Postal Code</label>
										<div class="">
										<input class="form-control input-xs" type="text" name="postal_code">
										</div>
									</div>

									<div class="col-xs-12 col-sm-6 col-lg-2">
										<label class="control-label">Country</label>
										<div class="">
										<input class="form-control input-xs" type="text" name="country">
										</div>
									</div>
								</div>

								</fieldset>


								<fieldset>
									<legend>Other</legend>
								<div class="form-group">

									<div class="col-xs-8 col-sm-6 col-lg-3">
										<label class="control-label">Assigned to</label>
										<div class="">
											<select class="form-control input-xs" id="assigned_to" name="assigned_to">
												<option value="<?php echo $_SESSION['user_id']; ?>"><?php echo findName('users', $_SESSION['user_id']); ?></option>
													
												<?php echo getListFromTable('users'); ?>
											</select>											
										
											<p class="note"><strong>Note:</strong> Default is logged in user.</p>
										</div>
									</div>

									<div class="col-xs-8 col-sm-6 col-lg-3">
										<label class="control-label">Time Zone</label>
										<div class="">
											<select class="form-control input-xs" id="user_time_zone" name="time_zone">
												<option selected value="79">(GMT+04:00) Abu Dhabi </option>
												<?php echo TimeZoneList(); ?>
											</select>						
										</div>
									</div>


									<div class="col-xs-8 col-sm-6 col-lg-2">
										<label class="control-label">Reports To</label>
										<div class="">
											<select class="form-control input-xs" id="reports_to" name="reports_to" required>
												<option value="">Select Option</option>
												<?php echo getListFromTable('users'); ?>											
											</select>
						
										</div>
									</div>


									<div class="col-xs-8 col-sm-6 col-lg-3">
										<label class="control-label">Upload Image</label>
										<div class="">
											<input name="MAX_FILE_SIZE" value="2097152" type="hidden"/>
											<input type="file" name="image">
										</div>
									</div>



								</div>
		
								</fieldset>




								<div class="form-actions">
									<div class="row">
										<div class="col-md-12">
										 <a href="<?php echo htmlspecialchars( URLROOT . current_class($this) ); ?>" class="btn btn-default" role="button">Cancel</a>
											
								<button class="btn btn-primary" type="submit" name="submit">
												<i class="fa fa-save"></i>
												Submit
											</button>
										</div>
									</div>
								</div>
	
							</form>
	
						</div>
						<!-- end widget content -->
	
					</div>
					<!-- end widget div -->
	
				</div>
				<!-- end widget -->
	
				<!-- Widget ID (each widget will need unique ID)-->

				<!-- end widget -->
	

	
			</article>

	
		</div>
	
		<!-- end row -->
	
		<!-- row -->
		<div class="row">
	
			<!-- NEW WIDGET START -->
			
			<!-- WIDGET END -->
	
		</div>
		<!-- end row -->
	
		<!-- row -->
		<div class="row">
	
			
					
		</div>
		<!-- end row -->		
	
	
	</section>
	<!-- end widget grid -->

<!-- PAGE FOOTER -->
<?php include (APPROOT . "/views/inc/admin_footer.php"); ?>
<!-- END PAGE FOOTER -->