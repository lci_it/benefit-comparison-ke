<?php include (APPROOT . "/views/inc/admin_header.php"); ?>

<div id="content">

	<div class="row">
		
		
	</div>
	
	<!-- widget grid -->
	<section id="widget-grid" class="col-">
	
		<!-- <div class="well">
			
		</div> -->
	
		<!-- row -->
		<div class="row">
	
			<!-- NEW WIDGET START -->
			<article class="col-md-12">
	
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false">
					<!-- widget options:
					usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
	
					data-widget-colorbutton="false"
					data-widget-editbutton="false"
					data-widget-togglebutton="false"
					data-widget-deletebutton="false"
					data-widget-fullscreenbutton="false"
					data-widget-custombutton="false"
					data-widget-collapsed="true"
					data-widget-sortable="false"
	
					-->
					<header>
						<span class="widget-icon"> <i class="fa fa-eye"></i> </span>
						<h2>Edit User Form</h2>
	
					</header>
	
					<!-- widget div-->
					<div>
	
						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
	
						</div>
						<!-- end widget edit box -->
	
						<!-- widget content -->
						<div class="widget-body">
							<form class='form-horizontal' method='post' action='<?php echo htmlspecialchars( URLROOT . current_class($this) . "/edit/" . $data['id']); ?>' enctype='multipart/form-data'>

								<fieldset>
										<legend>User Details</legend>

									<div class="form-group"> <!-- START OF FORM GROUP USER DETAILS-->
										
										<div class="col-xs-8 col-sm-6 col-lg-3 hidden">
											<label class="control-label">ID</label>
											<div class="">
												<input class="form-control input-xs" type="number" name="id" value="<?php echo $data['id'] ?>">
											</div>
										</div>

										<div class="col-xs-8 col-sm-6 col-lg-3">
											<label class="control-label">Username</label>
											<div class="">
												<input class="form-control input-xs" type="text" name="name" value="<?php echo $data['name'] ?>">
											</div>
										</div>
										
										<!-- <div class="col-xs-12 col-sm-6 col-lg-3">
											<label class="control-label">Password</label>
											<div class="">
												<input class="form-control input-xs" type="password" name="password" value="<?php  ?>">
											</div>
										</div> -->

										<div class="col-xs-12 col-sm-6 col-lg-3">
											<label class="control-label">Firstname</label>
											<div class="">
												<input class="form-control input-xs" type="text" name="first_name" value="<?php echo $data['first_name']; ?>">
											</div>
										</div>

										<div class="col-xs-12 col-sm-6 col-lg-3">
											<label class="control-label">Lastname</label>
											<div class="">
												<input class="form-control input-xs" type="text" name="last_name" value="<?php echo $data['last_name']; ?>">
											</div>
										</div>

										<div class="col-xs-8 col-sm-6 col-lg-3">
											<label class="control-label">User Role</label>
											<div class="">
												<select class="form-control input-xs" id="group_id" name="group_id">
													<option value="<?php echo $data['group_id']; ?>" selected><?php echo findName('groupmember', $data['group_id']); ?></option>
														
													<?php echo getListFromTable('groupmember'); ?>
												</select>											
											</div>
										</div>

										<div class="col-xs-12 col-sm-6 col-lg-2">
											<label class="control-label">Phone</label>
											<div class="">
												<input class="form-control  input-xs" type="text" name="phone" value="<?php echo $data['phone']; ?>">
											</div>
										</div>

										<div class="col-xs-12 col-sm-6 col-lg-1">
											<label class="control-label">Ext No</label>
											<div class="">
												<input class="form-control  input-xs" type="text" name="phone_extension" value="<?php echo $data['phone_extension']; ?>">
											</div>
										</div>
										
										<div class="col-xs-12 col-sm-6 col-lg-2">
											<label class="control-label">Mobile</label>
											<div class="">
												<input class="form-control  input-xs" type="text" name="mobile" value="<?php echo $data['mobile']; ?>">
											</div>
										</div>

										<div class="col-xs-12 col-sm-6 col-lg-3">
											<label class="control-label">Email</label>
											<div class="">
												<input class="form-control input-xs" type="email" name="email" value="<?php echo $data['email']; ?>" required>
											</div>
										</div>

										<div class="col-xs-12 col-sm-6 col-lg-2">
											<label class="control-label">is Admin</label>
											<div class="">

											<select class="form-control input-xs" name="is_admin">
												<option value="<?php echo $data['is_admin']; ?>">
													<?php show_yes_no($data['is_admin']); ?>						
												</option>
												<option value="1">Yes</option>
												<option value="0">No</option>
											
											</select> 
												
										</div>
											
										</div><!-- END OF select active -->


										<div class="col-xs-12 col-sm-6 col-lg-2">
											<label class="control-label">Active</label>
											<div class="">

												<select class="form-control input-xs" name="active">
													<option value="<?php echo $data['active']; ?>">
														<?php show_yes_no($data['active']); ?>
													</option>
													<option value="1">Yes</option>
													<option value="0">No</option>
												
												</select> 
													
											</div>
											
										</div><!-- END OF select active -->

								

										</div> <!-- END OF FORM GROUP USER DETAILS-->

								</fieldset>

								<fieldset>
									<legend>Address Details</legend>
									<div class="form-group">
										
										<div class="col-xs-12 col-sm-6 col-lg-3">
											<label class="control-label">Street</label>
											<div class="">
											<input class="form-control input-xs" type="text" name="street" value="<?php echo $data['street']; ?>">
											</div>
										</div>

										<div class="col-xs-12 col-sm-6 col-lg-2">
											<label class="control-label">City</label>
											<div class="">
											<input class="form-control input-xs"  type="text" name="city" value="<?php echo $data['city']; ?>">
											</div>
										</div>

										<div class="col-xs-12 col-sm-6 col-lg-2">
											<label class="control-label">State</label>
											<div class="">
											<input class="form-control input-xs" type="text" name="state" value="<?php echo $data['state']; ?>">
											</div>
										</div>

										<div class="col-xs-12 col-sm-6 col-lg-2">
											<label class="control-label">Postal Code</label>
											<div class="">
											<input class="form-control input-xs" type="text" name="postal_code" value="<?php echo $data['postal_code']; ?>">
											</div>
										</div>

										<div class="col-xs-12 col-sm-6 col-lg-3">
											<label class="control-label">Country</label>
											<div class="">
											<input class="form-control input-xs" type="text" name="country" value="<?php echo $data['country']; ?>">
											</div>
										</div>

									</div>
								</fieldset>


								<fieldset>
									<legend>Other</legend>
									<div class="form-group">

										<div class="col-xs-8 col-sm-6 col-lg-2">
											<label class="control-label">Assigned to</label>
											<div class="">
												<select class="form-control input-xs" id="assigned_to" name="assigned_to">
													<option selected value="<?php echo $data['assigned_to']; ?>"><?php echo findName('users', $data['assigned_to']); ?>
													</option>
													<?php echo getListFromTable('users'); ?>
												</select>
											</div>
										</div>

										<div class="col-xs-8 col-sm-6 col-lg-2">
											<label class="control-label">Time Zone</label>
											<div class="">
												<select class="form-control input-xs" id="user_time_zone" name="time_zone">
													<option selected value="<?php echo $data['time_zone']; ?>">
														<?php echo findName('timezones', $data['time_zone']); ?>
													</option>
													<?php echo TimeZoneList(); ?>												
												</select>						
											</div>
										</div>

										<div class="col-xs-8 col-sm-6 col-lg-2">
											<label class="control-label">Reports To</label>
											<div class="">
												<select class="form-control input-xs" id="reports_to_id" name="reports_to">
													<option value="<?php echo $data['reports_to']; ?>"><?php echo findName('users', $data['reports_to']); ?>
													</option>													
													<?php echo getListFromTable('users'); ?>
												</select>						
											</div>
										</div>

										<div class="col-xs-8 col-sm-6 col-lg-2 col-lg-offset-1">
											<a href=""><img class="img-responsive edit_profile_image" src="<?php echo Upload::showImage($data['image']); ?>">
											</a>
										</div>							

										<div class="col-xs-8 col-sm-6 col-lg-2">
											<label class="control-label">Upload Image</label>
											<div class="">
												<input name="MAX_FILE_SIZE" value="2097152" type="hidden"/>
												<input type="file" name="image">
											</div>
										</div>

									</div>
								</fieldset>

								<div class="form-actions">
									<div class="row">
										<div class="col-md-12">
											<a href="<?php echo htmlspecialchars(URLROOT . current_class($this) ); ?>" class="btn btn-default" role="button">Cancel</a>
											<button class="btn btn-primary" type="submit" name="update">
												<i class="fa fa-save"></i>
												Update
											</button>
										</div>
									</div>
								</div>
							
							</form>
	
						</div>
						<!-- end widget content -->
	
					</div>
					<!-- end widget div -->
	
				</div>
				<!-- end widget -->
	
				<!-- Widget ID (each widget will need unique ID)-->

				<!-- end widget -->
	

	
			</article>

	
		</div>
	
		<!-- end row -->
	
		<!-- row -->
		<div class="row">
	
			<!-- NEW WIDGET START -->
			
			<!-- WIDGET END -->
	
		</div>
		<!-- end row -->
	
		<!-- row -->
		<div class="row">
	
			
					
		</div>
		<!-- end row -->		
	
	
	</section>
	<!-- end widget grid -->

</div>

<!-- PAGE FOOTER -->
<?php include (APPROOT . "/views/inc/admin_footer.php"); ?>
<!-- END PAGE FOOTER -->