<?php include (APPROOT . "/views/inc/admin_header.php"); ?>

<div id="content">

	<div class="row">
		
		
	</div>
	
	<!-- widget grid -->
	<section id="widget-grid" class="col-">
	
		<!-- <div class="well">
			
		</div> -->
	
		<!-- row -->
		<div class="row">
	
			<!-- NEW WIDGET START -->
			<article class="col-md-12">
	
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false">
					<!-- widget options:
					usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
	
					data-widget-colorbutton="false"
					data-widget-editbutton="false"
					data-widget-togglebutton="false"
					data-widget-deletebutton="false"
					data-widget-fullscreenbutton="false"
					data-widget-custombutton="false"
					data-widget-collapsed="true"
					data-widget-sortable="false"
	
					-->
					<header>
						<span class="widget-icon"> <i class="fa fa-eye"></i> </span>
						<h2>Change Password Form</h2>
	
					</header>
	
					<!-- widget div-->
					<div>
	
						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
	
						</div>
						<!-- end widget edit box -->
	
						<!-- widget content -->
						<div class="widget-body">
							<form class='form-horizontal' method='post' action='<?php echo htmlspecialchars( URLROOT . "users/mypassword/" . $data['id']); ?>' enctype='multipart/form-data'>

								<fieldset>
										<legend>Type New Password</legend>

									<div class="form-group"> <!-- START OF FORM GROUP USER DETAILS-->
										
										<div class="col-xs-8 col-sm-6 col-lg-3 hidden">
											<label class="control-label">ID</label>
											<div class="">
												<input class="form-control input-xs" type="number" name="id" value="<?php echo $data['id'] ?>">
											</div>
										</div>
										
										<div class="col-xs-12 col-sm-6 col-lg-3">
											<label class="control-label">Password</label>
											<div class="">
												<input class="form-control input-xs" type="password" name="password" value="">
											</div>
										</div>

										<div class="col-xs-12 col-sm-6 col-lg-3">
											<label class="control-label">Confirm Password</label>
											<div class="">
												<input class="form-control input-xs" type="password" name="confirm_password" value="">
											</div>
										</div>
								

										</div> <!-- END OF FORM GROUP USER DETAILS-->

								</fieldset>

								<div class="form-actions">
									<div class="row">
										<div class="col-md-12">
											<a href="<?php echo URLROOT; ?>" class="btn btn-default" role="button">Cancel</a>
											<button class="btn btn-primary" type="submit" name="update">
												<i class="fa fa-save"></i>
												Update
											</button>
										</div>
									</div>
								</div>
							
							</form>
	
						</div>
						<!-- end widget content -->
	
					</div>
					<!-- end widget div -->
	
				</div>
				<!-- end widget -->
	
				<!-- Widget ID (each widget will need unique ID)-->

				<!-- end widget -->
	

	
			</article>

	
		</div>
	
		<!-- end row -->
	
		<!-- row -->
		<div class="row">
	
			<!-- NEW WIDGET START -->
			
			<!-- WIDGET END -->
	
		</div>
		<!-- end row -->
	
		<!-- row -->
		<div class="row">
	
			
					
		</div>
		<!-- end row -->		
	
	
	</section>
	<!-- end widget grid -->

</div>

<!-- PAGE FOOTER -->
<?php include (APPROOT . "/views/inc/admin_footer.php"); ?>
<!-- END PAGE FOOTER -->