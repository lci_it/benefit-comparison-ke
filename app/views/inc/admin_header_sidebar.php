<?php include (APPROOT . "/views/inc/admin_header.php"); ?>

	
<!-- HEADER AND TOP NAVIGATION-->
<?php include (APPROOT . "/views/inc/admin_top_nav.php"); ?>
<!-- END HEADER AND TOP NAVIGATION-->


<!-- Left panel : Navigation area -->
<!-- Note: This width of the aside area can be adjusted through LESS variables -->

<?php include (APPROOT . "/views/inc/admin_sidebar_nav.php"); ?>
<!-- END NAVIGATION -->

<!-- RIBBON -->
<?php include (APPROOT . "/views/inc/admin_breadcrumb.php"); ?>
<!-- END RIBBON -->