<!-- MAIN PANEL -->
<div id="main" role="main">

	<!-- RIBBON -->
	<div class="row">
		<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
			<h1 class="page-title txt-color-blueDark custom_margin_page_title">
				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon custom_refresh_margin" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span>
				</span>
				<i class="fa-fw fa fa-home"></i> <span style="custom_page_title">Dashboard</span> <span>> My Dashboard</span> 
			</h1>

		</div>

		<!-- <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
			<ul id="sparks" class="">
				<li class="sparks-info">
					<h5> Comparisons <span class="txt-color-blue"><a href="reports.php?action=units_profit"><?php echo "Comparison 25K"; ?></a></span></h5>
					<div class="sparkline txt-color-blue hidden-mobile hidden-md hidden-sm">
						1300, 1877, 2500, 2577, 2000, 2100, 3000, 2700, 3631, 2471, 2700, 3631, 2471
					</div>
				</li>
				<li class="sparks-info">
					<h5><a href='reports.php?action=vacant_units'> Quotes Lost </a><span class="txt-color-red"><i class="fa fa-arrow-circle-down"></i>&nbsp;<a href='reports.php?action=vacant_units'><?php echo "Lost 35%"; ?></a></span></h5>
					<div class="sparkline txt-color-purple hidden-mobile hidden-md hidden-sm">
						110,150,300,130,400,240,220,310,220,300, 270, 210
					</div>
				</li>
				<li class="sparks-info">
					<h5> Site Orders <span class="txt-color-greenDark"><i class="fa fa-shopping-cart"></i>&nbsp;2447</span></h5>
					<div class="sparkline txt-color-greenDark hidden-mobile hidden-md hidden-sm">
						110,150,300,130,400,240,220,310,220,300, 270, 210
					</div>
				</li>
			</ul>
		</div> -->

	</div>
	<!-- END RIBBON -->

	<!-- START MAIN CONTENT -->
	<div id="content">
		
		<?php print_messages(); ?>
		<?php print_upload_msgs('msg'); ?>
		<?php
			if(!empty($data['errors'])){
				echo "<div id='print_messages' class='alert alert-danger' style='margin-left: 7px; margin-right: 7px; margin-bottom: 2px;'><div style='float:left;'>";
				foreach ($data['errors'] as $errors) {
					echo " * " . $errors . "<br>";
				}
				echo "</div><div class='alert-close text-right'><button style='background: none; padding-bottom: 10px;'> x </button></div></div>";
			}
		?>			

	<!-- MAIN PANEL END IN FOOTER-->