<aside id="left-panel">

			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as it --> 
					
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
						<img src="<?php echo Upload::showImage($_SESSION['user_image']); ?>" alt="me" class="online" /> 
						<span>
							<?php echo $_SESSION['username']; ?> 
						</span>
						<i class="fa fa-angle-down"></i>
					</a> 
					
				</span>
			</div>
			<!-- end user info -->

			<!-- NAVIGATION : This navigation is also responsive-->
			<nav>
				<!-- 
				NOTE: Notice the gaps after each icon usage <i></i>..
				Please note that these links work a bit different than
				traditional href="" links. See documentation for details.
				-->

				<ul>
					<li class="active">
						<a href="#" title="Dashboard"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">Dashboard</span></a>
						<ul>
							<li class='active'>
								<a href="index.php" title="Dashboard"><span class="menu-item-parent">Analytics Dashboard</span></a>
							</li>
							<li class=''>
								<a href="dashboard-social.html" title="Dashboard"><span class="menu-item-parent">Social Wall</span></a>
							</li>
						</ul>	
					</li>



					<li>
						<a href="inbox.html"><i class="fa fa-lg fa-fw fa-inbox"></i> <span class="menu-item-parent">Outlook</span> <span class="badge pull-right inbox-badge margin-right-13">14</span></a>
					</li>
					<li>
						<a href="#"><i class="fa fa-lg fa-fw fa-bar-chart-o"></i> <span class="menu-item-parent">Reports</span></a>
						<ul>

							<li>
								<a href="reports.php?action=vacant_units">view Vacant Units</a>
							</li>

							<li>
								<a href="reports.php?action=units_profit">view Units Profit</a>
							</li>

							<li>
								<a href="flot.html">Flot Chart</a>
							</li>
							<li>
								<a href="morris.html">Morris Charts</a>
							</li>
							<li>
								<a href="sparkline-charts.html">Sparklines</a>
							</li>
							<li>
								<a href="easypie-charts.html">EasyPieCharts</a>
							</li>
							<li>
								<a href="dygraphs.html">Dygraphs</a>
							</li>
							<li>
								<a href="chartjs.html">Chart.js</a>
							</li>
							<li>
								<a href="hchartable.html">HighchartTable <span class="badge pull-right inbox-badge bg-color-yellow">new</span></a>
							</li>
						</ul>
					</li>

					<li class="">
						<a href="#"><i class="fa fa-lg fa-fw fa-table"></i> <span class="menu-item-parent">Customers</span></a>
						<ul>
														
							<li class="">
								<a href="<?php echo URLROOT; ?>customers">List Customers</a>
							</li>

							<li>
								<a href="<?php echo URLROOT; ?>customers/add">Add Customer</a>
							</li>
							

							<li>
								<a href="<?php echo URLROOT; ?>quotes"><i class="fa fa-lg fa-fw fa fa-file-text-o"></i> List Quotes</a>
							</li>

							<li>
								<a href="<?php echo URLROOT; ?>quotes/add">Add Quote</a>
							</li>
							

							<li>
								<a href="<?php echo URLROOT; ?>proformainvoices">List Proforma Invoices</a>
							</li>

							<li>
								<a href="<?php echo URLROOT; ?>proformainvoices/add">Add Proforma Invoice</a>
							</li>

							
						</ul>
					</li>

					<li class="">
						<a href="#"><i class="fa fa-lg fa-fw fa fa-file-text-o"></i> <span class="menu-item-parent">Quotes</span></a>
						<ul>
														
							<li>
								<a href="<?php echo URLROOT; ?>quotes/add">Add Quote</a>
							</li>

							<li>
								<a href="<?php echo URLROOT; ?>quotes">List Quotes</a>
							</li>
						</ul>
					</li>

					<li class="">
						<a href="#"><i class="fa fa-lg fa-fw fa fa-file-text-o"></i> <span class="menu-item-parent">Proforma Invoices</span></a>
						<ul>														
							<li>
								<a href="<?php echo URLROOT; ?>proformainvoices/add">Add Proforma Invoice</a>
							</li>

							<li>
								<a href="<?php echo URLROOT; ?>proformainvoices">List Proforma Invoices</a>
							</li>
						</ul>
					</li>

					<li class="">
						<a href="#"><i class="fa fa-lg fa-fw fa fa-file-text-o"></i> <span class="menu-item-parent">Sale Invoices</span></a>
						<ul>
														
							<li>
								<a href="<?php echo URLROOT; ?>saleinvoices/add">Add Sale Invoice</a>
							</li>

							<li>
								<a href="<?php echo URLROOT; ?>saleinvoices">List Sale Invoices</a>
							</li>
						</ul>
					</li>


					<li class="">
						<a href="#"><i class="fa fa-lg fa-fw fa-list-alt"></i> <span class="menu-item-parent">Suppliers</span></a>
						<ul>
														
							<li class="">
								<a href="<?php echo URLROOT; ?>suppliers">List Suppliers</a>
							</li>

							<li>
								<a href="<?php echo URLROOT; ?>suppliers/add">Add Supplier</a>
							</li>
						</ul>
					</li>

					<li class="">
						<a href="#"><i class="fa fa-lg fa-fw fa fa-file-text-o"></i> <span class="menu-item-parent">Purchases</span></a>
						<ul>
														
							<li>
								<a href="<?php echo URLROOT; ?>purchaseinvoices/add">Add Purchase</a>
							</li>

							<li>
								<a href="<?php echo URLROOT; ?>purchaseinvoices">List Purchase Invoices</a>
							</li>
						</ul>
					</li>
					

					<li class="">
						<a href="#"><i class="fa fa-lg fa-fw fa fa-file-text-o"></i> <span class="menu-item-parent">Expenses</span></a>
						<ul>
														
							<li>
								<a href="<?php echo URLROOT; ?>expenses/add">Add Expense</a>
							</li>

							<li>
								<a href="<?php echo URLROOT; ?>expenses">List Expenses</a>
							</li>

							<li>
								<a href="<?php echo URLROOT; ?>expensecategories">Expense Category</a>
							</li>
						</ul>
					</li>

					<!-- <li class="">
						<a href="#"><i class="fa fa-lg fa-fw fa-building"></i> <span class="menu-item-parent">Leases</span></a>
						<ul>
														
							<li>
								<a href="lease.php?action=add_lease">Add Lease</a>
							</li>

							<li>
								<a href="lease.php?action=view_all">List Lease</a>
							</li>
						</ul>
					</li> -->

					<!-- <li class="">
						<a href="#"><i class="fa fa-lg fa-fw fa-building"></i> <span class="menu-item-parent">Rentals</span></a>
						<ul>
														
							<li>
								<a href="rentals.php?action=rent_flat">Add Rental</a>
							</li>

							<li>
								<a href="rentals.php?action=view_all">List Rentals</a>
							</li>
						</ul>
					</li>
 -->
					<!-- <li class="">
						<a href="#"><i class="fa fa-lg fa-fw fa-cubes"></i> <span class="menu-item-parent">Properties</span></a>
						<ul>
														
							<li class="">
								<a href="properties.php?action=view_all">List Properties</a>
							</li>

							<li>
								<a href="properties.php?action=add">Add Property</a>
							</li>

							<li>
								<a href="properties.php?action=add_unit">Add Unit</a>
							</li>							

						</ul>
					</li> -->


					<li class="">
						<a href="#"><i class="fa fa-lg fa-fw fa-money"></i> <span class="menu-item-parent">Payment Receipts</span></a>
						<ul>
														
							<li class="">
								<a href="receipt_vouchers.php?action=view_all">List Receipts</a>
							</li>

							<li>
								<a href="receipt_vouchers.php?action=add">Add Payment Receipts</a>
							</li>
						</ul>
					</li>


					<li class="">
						<a href="#"><i class="fa fa-lg fa-fw fa-suitcase"></i> <span class="menu-item-parent">COA Setup</span></a>
						<ul>
														
							<li class="">
								<a href="coa.php?action=view_all">List COAs</a>
							</li>

							<li>
								<a href="coa.php?action=add">Add COA</a>
							</li>


						</ul>
					</li>
					<li class="">
						<a href="#"><i class="fa fa-lg fa-fw fa-user"></i> <span class="menu-item-parent">Products</span></a>
						<ul>
							<li class="">
								<a href="<?php echo URLROOT; ?>products">List Products</a>
							</li>
							<li>
								<a href="<?php echo URLROOT; ?>products/add">Add Product</a>
							</li>
						</ul>
					</li>

					<li class="">
						<a href="#"><i class="fa fa-lg fa-fw fa-user"></i> <span class="menu-item-parent">Users</span></a>
						<ul>
														
							<li class="">
								<a href="<?php echo URLROOT; ?>users">List Users</a>
							</li>

							<li>
								<a href="<?php echo URLROOT; ?>users/add">Add User</a>
							</li>
						</ul>
					</li>


					<li class="chat-users top-menu-invisible hidden">
						<a href="#"><i class="fa fa-lg fa-fw fa-comment-o"><em class="bg-color-pink flash animated">!</em></i> <span class="menu-item-parent">Smart Chat API <sup>beta</sup></span></a>
						<ul>
							<li>
								<!-- DISPLAY USERS -->
								<div class="display-users">

									<input class="form-control chat-user-filter" placeholder="Filter" type="text">
									
								  	<a href="#" class="usr" 
									  	data-chat-id="cha1" 
									  	data-chat-fname="Sadi" 
									  	data-chat-lname="Orlaf" 
									  	data-chat-status="busy" 
									  	data-chat-alertmsg="Sadi Orlaf is in a meeting. Please do not disturb!" 
									  	data-chat-alertshow="true" 
									  	data-rel="popover-hover" 
									  	data-placement="right" 
									  	data-html="true" 
									  	data-content="
											<div class='usr-card'>
												<img src='img/avatars/5.png' alt='Sadi Orlaf'>
												<div class='usr-card-content'>
													<h3>Sadi Orlaf</h3>
													<p>Marketing Executive</p>
												</div>
											</div>
										"> 
									  	<i></i>Sadi Orlaf
								  	</a>
								  
									<a href="#" class="usr" 
										data-chat-id="cha2" 
									  	data-chat-fname="Jessica" 
									  	data-chat-lname="Dolof" 
									  	data-chat-status="online" 
									  	data-chat-alertmsg="" 
									  	data-chat-alertshow="false" 
									  	data-rel="popover-hover" 
									  	data-placement="right" 
									  	data-html="true" 
									  	data-content="
											<div class='usr-card'>
												<img src='img/avatars/1.png' alt='Jessica Dolof'>
												<div class='usr-card-content'>
													<h3>Jessica Dolof</h3>
													<p>Sales Administrator</p>
												</div>
											</div>
										"> 
									  	<i></i>Jessica Dolof
									</a>
								  
									<a href="#" class="usr" 
									  	data-chat-id="cha3" 
									  	data-chat-fname="Zekarburg" 
									  	data-chat-lname="Almandalie" 
									  	data-chat-status="online" 
									  	data-rel="popover-hover" 
									  	data-placement="right" 
									  	data-html="true" 
									  	data-content="
											<div class='usr-card'>
												<img src='img/avatars/3.png' alt='Zekarburg Almandalie'>
												<div class='usr-card-content'>
													<h3>Zekarburg Almandalie</h3>
													<p>Sales Admin</p>
												</div>
											</div>
										"> 
									  	<i></i>Zekarburg Almandalie
									</a>
								 
									<a href="#" class="usr" 
									  	data-chat-id="cha4" 
									  	data-chat-fname="Barley" 
									  	data-chat-lname="Krazurkth" 
									  	data-chat-status="away" 
									  	data-rel="popover-hover" 
									  	data-placement="right" 
									  	data-html="true" 
									  	data-content="
											<div class='usr-card'>
												<img src='img/avatars/4.png' alt='Barley Krazurkth'>
												<div class='usr-card-content'>
													<h3>Barley Krazurkth</h3>
													<p>Sales Director</p>
												</div>
											</div>
										"> 
									  	<i></i>Barley Krazurkth
									</a>
								  
									<a href="#" class="usr offline" 
									  	data-chat-id="cha5" 
									  	data-chat-fname="Farhana" 
									  	data-chat-lname="Amrin" 
									  	data-chat-status="incognito" 
									  	data-rel="popover-hover" 
									  	data-placement="right" 
									  	data-html="true" 
									  	data-content="
											<div class='usr-card'>
												<img src='img/avatars/female.png' alt='Farhana Amrin'>
												<div class='usr-card-content'>
													<h3>Farhana Amrin</h3>
													<p>Support Admin <small><i class='fa fa-music'></i> Playing Beethoven Classics</small></p>
												</div>
											</div>
										"> 
									  	<i></i>Farhana Amrin (offline)
									</a>
								  
									<a href="#" class="usr offline" 
										data-chat-id="cha6" 
									  	data-chat-fname="Lezley" 
									  	data-chat-lname="Jacob" 
									  	data-chat-status="incognito" 
									  	data-rel="popover-hover" 
									  	data-placement="right" 
									  	data-html="true" 
									  	data-content="
											<div class='usr-card'>
												<img src='img/avatars/male.png' alt='Lezley Jacob'>
												<div class='usr-card-content'>
													<h3>Lezley Jacob</h3>
													<p>Sales Director</p>
												</div>
											</div>
										"> 
									  	<i></i>Lezley Jacob (offline)
									</a>
									
									<a href="ajax/chat.html" class="btn btn-xs btn-default btn-block sa-chat-learnmore-btn">About the API</a>

								</div>
								<!-- END DISPLAY USERS -->
							</li>
						</ul>	
					</li>


				</ul>
			</nav>
			

			<span class="minifyme" data-action="minifyMenu"> 
				<i class="fa fa-arrow-circle-left hit"></i> 
			</span>

</aside>