<?php include (APPROOT . "/views/inc/admin_header.php"); ?>

<!-- widget grid -->
	<section id="widget-grid" class="col-">
	
		<!-- <div class="well">
			
		</div> -->
	
		<!-- row -->
		<div class="row">
	
			<!-- NEW WIDGET START -->
			<article class="col-md-12">
	
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false">
					<!-- widget options:
					usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
	
					data-widget-colorbutton="false"
					data-widget-editbutton="false"
					data-widget-togglebutton="false"
					data-widget-deletebutton="false"
					data-widget-fullscreenbutton="false"
					data-widget-custombutton="false"
					data-widget-collapsed="true"
					data-widget-sortable="false"
	
					-->
					<header>
						<span class="widget-icon"> <i class="fa fa-eye"></i> </span>
						<h2>Edit Comparison</h2>	
					</header>
	
					<!-- widget div-->
					<div>
						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->	
						</div>
						<!-- end widget edit box -->
	
						<!-- widget content -->
						<div class="widget-body">
							<form class="form-horizontal" method="post" action="<?php echo htmlspecialchars( URLROOT . current_class($this) . '/edit/' . $data['record']->id); ?>" enctype="multipart/form-data">
								<button class="btn btn-info txt-color-white" onclick="goBack()"> <span style="font-weight: bold; text-transform: none;"> Back </span> </button>

								<fieldset>
									<legend> <span class="label bg-color-purple pull-right">ID <?php echo $data['record']->id; ?></span></legend><br>
									<!-- <legend>Customer Details <?php; ?></legend> -->
									<!-- START OF pull-left -->
										<div class="pull-left">
											<div class="form-group">
												<label class="col-md-5 control-label text-left">Customer <sup>*</sup></label>
												<div class="col-md-7">
													<input class="form-control input-xs txt-auto ui-widget" name="customer" id="customer" value= "<?php echo $data['record']->customer_name; ?>" pattern="^[a-zA-Z0-9 ]+$" title="Alphabets, Numbers & White Spaces are allowed" required>
												</div>
											</div>
										</div>
										<!-- END OF pull-left -->

										<!-- START OF pull-right -->
											<div class="pull-right">
												<div class="form-group">
													<label class="col-md-5 control-label text-left">Valid Until</label>
													<div class="col-md-7">
														<input class="form-control input-xs" type="date" name="valid_until" id="valid_until" value="<?php echo $data['record']->valid_until; ?>">
													</div>
												</div>
											</div>
											<!-- END OF pull-right -->
								</fieldset>

								<fieldset>
									<div>
										<a style="color: #fff;" class="btn btn-info compare" href="<?php echo htmlspecialchars( URLROOT . current_class($this) . '/addplan/' . $data['record']->id); ?>">Add More Plans to Comparison #<?php echo $data['record']->id; ?> </a>
									</div>
								</fieldset>

								<fieldset>
									<div class="table-responsive">
										<table class="order-details table-hover" id="items_table">
											<tbody>
												<?php
													$quote = $data['record']->id;
													$result = $data['products'];
													if(!(bool)$result){
														$i = 1;
														$html = "";
														echo $html;

													} else {
														$html = '';

														$i = 1;

														echo "
														<table class='table table-hover table-striped table-bordered custom-height' id='comparison_table'>
															<thead style='background: #542989 !important; color: #fff;'>
																<tr style='background: #542989 !important; color: #fff; text-transform: uppercase;'>
																	<th style='text-align: left; font-weight: bold; vertical-align: middle; min-width: 220px; width:220px !important; height:45px;'>Service Provider</th>";

																	foreach($result as $record) {
																		$manufacturer = findName('manufacturer', $record->manufacturer_id);
																		echo "<th style='background: none; font-weight: bold; vertical-align: middle; text-align: center;'>$manufacturer</th>";
																	}

																	echo "</tr>
																	<tr style='background: #542989 !important; color: #fff; text-transform: uppercase;'>
																	<th style='text-align: left; font-weight: bold; vertical-align: middle; height:50px;'>Plan</th>";

																	foreach($result as $record) {
																		echo "<th style='font-weight: bold; vertical-align: middle; text-align: center;'><p>$record->product_name</p>
																		<div class='alert alert-success alert-dismissible success' id='success' style='display:none;''>
																		  <a href='#'' class='close' data-dismiss='alert' aria-label='close'>×</a>
																		</div>
																		<form id='fupForm' name='form1' method='post'>
																			<input type='hidden' name='rem_product_id' value='$record->product_id' id='rem_product_id' class='rem_product_id'/>
																			<input type='hidden' name='quote_id' value='$quote' id='quote_id' class='quote_id'/>
																			<input type='button' class='btn btn-warning btn-xs deleteFromQuote' name='remove' value='Remove' id='remove'/>
																		</form>
																		</th>";
																	}

																	echo "</tr>
															</thead>
																<tbody>
																
																<tr>
																		<td style='text-align: left; font-weight: bold;'>Currency</td>";
																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='currency_name[]' rows='1' readonly>" . dropdownValue($record->currency_id, 'currency') . "</textarea></td>";
																		}
																		
																echo "</tr>
																
																<tr class='hidden'>
																		<td style='text-align: left; font-weight: bold;'>Currency ID</td>";
																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='currency_id[]' rows='1' readonly>$record->currency_id</textarea></td>";
																		}
																		
																echo "</tr>
																		
																		
																		
																	<tr>
																		<td style='text-align: left; font-weight: bold;'>Annual limit AED</td>";

																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><input type='hidden' name='product_id[]' value='$record->product_id' id='product_id' class='product_id'/>
																			<input type='hidden' name='product_name[]' value='$record->product_name' id='product_name' class='product_name'/><textarea style='text-align: center; padding-top:5px;' name='annual_cover[]' rows='1'>$record->annual_cover</textarea></td>";
																		}

																	echo "</tr>

																	<tr class='hidden'>
																		<td style='text-align: left; font-weight: bold;'>j_table_id</td>";
																		foreach($result as $record) {
																			
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='j_table_id[]' rows='1'>$record->id</textarea></td>"; 
																			
																		}
																		
																	echo "</tr>

																	<tr>
																		<td style='text-align: left; font-weight: bold;'>Area of Cover</td>";
																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='area_of_cover[]' rows='1'>$record->area_of_cover</textarea></td>";
																		}
																		
																		echo "</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold;'>Emergency Treatment Outside Area of Cover</td>";

																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='emergency_treatment_outside_area_of_cover[]' rows='1'>$record->emergency_treatment_outside_area_of_cover</textarea></td>";
																		}

																		echo "</tr>
																		<tr>
																		<td style='text-align: left; font-weight: bold;'>Pre-existing Conditions(2) including Pre-existing Chronic Conditions</td>";

																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='preexisting_conditions[]' rows='1'>$record->preexisting_conditions</textarea></td>";
																		}

																		echo "</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold;'>Congenital Conditions</td>";

																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='congenital_conditions[]' rows='1'>$record->congenital_conditions</textarea></td>";
																		}

																		echo "</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold;'>Network (Hospitals / Clinics / Diagnostic Centers)</td>";

																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='network[]' rows='1'>$record->network</textarea></td>";
																		}																		

																		echo "</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold;'>Plan Compliance (DHA/ HAAD/ CCHI)</td>";

																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='plan_compliance[]' rows='1'>$record->plan_compliance</textarea></td>";
																		}

																		echo "</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold;'>Underwriting Criteria (FMU / MHD)</td>";

																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='underwriting_critera[]' rows='1'>$record->underwriting_critera</textarea></td>";
																		}

																		echo "</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold;'>Pricing Model (Experience Rated / Pool Rated / Blended)</td>";

																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='pricing_model[]' rows='1'>$record->pricing_model</textarea></td>";
																		}

																		echo "</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold; color: #fff; background:#00aedb;' colspan='100%'>IN PATIENT TREATMENT</td>
																		</tr>
																		<tr>
																		<td style='text-align: left; font-weight: bold;'>Deductible / Co-insurance / Excess</td>";

																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='ip_deductible[]' rows='1'>$record->ip_deductible</textarea></td>";
																		}

																		echo "</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold;'>Room & Board</td>";

																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='ip_room_board[]' rows='1'>$record->ip_room_board</textarea></td>";
																		}

																		echo "</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold;'>Specialist Fees</td>";

																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='ip_specialist_fees[]' rows='1'>$record->ip_specialist_fees</textarea></td>";
																		}

																		echo "</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold;'>Surgery and Anesthesia</td>";

																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='ip_surgery_anesthesia[]' rows='1'>$record->ip_surgery_anesthesia</textarea></td>";
																		}

																		echo "</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold;'>Diagnostic Tests</td>";

																		foreach($result as $product) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='ip_diagnostic_tests[]' rows='1'>$record->ip_diagnostic_tests</textarea></td>";
																		}

																		echo "</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold;'>Prescribed Medication</td>";

																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='ip_prescribed_medication[]' rows='1'>$record->ip_prescribed_medication</textarea></td>";
																		}

																		echo "</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold;'>Cancer Treatment </td>";

																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='ip_cancer_treatment[]' rows='1'>$record->ip_cancer_treatment</textarea></td>";
																		}

																		echo "</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold;'>Organ Transplant</td>";

																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='ip_organ_transplant[]' rows='1'>$record->ip_organ_transplant</textarea></td>";
																		}

																		echo "</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold;'>Prosthetic Device</td>";

																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='ip_prosthetic_device[]' rows='1'>$record->ip_prosthetic_device</textarea></td>";
																		}

																		echo "</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold;'>Rehabilitation</td>";

																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='ip_rehabilitation[]' rows='1'>$record->ip_rehabilitation</textarea></td>";
																		}

																		echo "</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold;'>Home Nursing (immediately after or instead of hospitalization)</td>";

																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='ip_home_nursing[]' rows='1'>$record->ip_home_nursing</textarea></td>";
																		}

																		echo "</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold;'>Parent / Companion Accomodation</td>";

																		foreach($result as $product) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='ip_companian_accomodation[]' rows='1'>$record->ip_companian_accomodation</textarea></td>";
																		}

																		echo "</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold;'>In-Patient Cash Benefit</td>";

																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='ip_cashbenenit[]' rows='1'>$record->ip_cashbenenit</textarea></td>";
																		}

																		echo "</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold; color: #fff; background:#00aedb;' colspan='100%'>OUT PATIENT TREATMENT</td>
																		</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold;'>Annual Limit</td>";

																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='op_annual_limit[]' rows='1'>$record->op_annual_limit</textarea></td>";
																		}

																		echo "</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold;'>Deductible / Co-insurance / Excess</td>";
																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='op_deductible[]' rows='1'>$record->op_deductible</textarea></td>";
																		}
																		echo "</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold;'>General Practioner Fees</td>";

																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='op_gp_fees[]' rows='1'>$record->op_gp_fees</textarea></td>";
																		}

																		echo "</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold;'>Specialist Fees</td>";

																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='op_specialist_fees[]' rows='1'>$record->op_specialist_fees</textarea></td>";
																		}
																		echo "</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold;'>Diagnostic Tests</td>";

																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='op_diagnostic_tests[]' rows='1'>$record->op_diagnostic_tests</textarea></td>";
																		}

																		echo "</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold;'>Prescribed Medication</td>";

																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='op_prescribed_medication[]' rows='1'>$record->op_prescribed_medication</textarea></td>";
																		}

																		echo "</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold;'>Physiotherapy</td>";
																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='op_physiotherapy[]' rows='1'>$record->op_physiotherapy</textarea></td>";
																		}
																		echo "</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold;'>Alternative Treatment</td>";
																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='op_alternative_treatment[]' rows='1'>$record->op_alternative_treatment</textarea></td>";
																		}
																		echo "</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold;'>Cancer Treatment </td>";
																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='op_cancer_treatment[]' rows='1'>$record->op_cancer_treatment</textarea></td>";
																		}
																		echo "</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold; color: #fff; background:#00aedb;' colspan='100%'>PSYCHIATRIC COVER</td>
																		</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold;'>Inpatient</td>";
																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='psychiatric_inpatient[]' rows='1'>$record->psychiatric_inpatient</textarea></td>";
																		}
																		echo "</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold;'>Outpatient</td>";
																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='psychiatric_outpatient[]' rows='1'>$record->psychiatric_outpatient</textarea></td>";
																		}
																		echo "</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold; color: #fff; background:#00aedb;' colspan='100%'>MATERNITY COVER</td>
																		</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold;'>Outpatient</td>";
																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='maternity_outpatient[]' rows='1'>$record->maternity_outpatient</textarea></td>";
																		}
																		echo "</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold;'>Inpatient</td>";
																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='maternity_inpatient[]' rows='1'>$record->maternity_inpatient</textarea></td>";
																		}
																		echo "</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold;'>C-Section</td>";
																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='maternity_c_section[]' rows='1'>$record->maternity_c_section</textarea></td>";
																		}
																		echo "</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold;'>Complication</td>";
																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='maternity_complication[]' rows='1'>$record->maternity_complication</textarea></td>";
																		}

																		echo "</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold;'>New Born Cover</td>";
																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='maternity_new_born[]' rows='1'>$record->maternity_new_born</textarea></td>";
																		}

																		echo "</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold; color: #fff; background:#00aedb;' colspan='100%'>ADDITIONAL BENEFITS</td>
																		</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold;'>Dental</td>";
																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='dental[]' rows='1'>$record->dental</textarea></td>";
																		}

																		echo "</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold;'>Optical</td>";
																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='optical[]' rows='1'>$record->optical</textarea></td>";
																		}

																		echo "</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold;'>Annual Health Check-up / Wellness Screening</td>";
																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='wellness[]' rows='1'>$record->wellness</textarea></td>";
																		}

																		echo "</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold;'>Vaccinations (DHA / MOH approved)</td>";
																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='vaccinations_dha_moh_approved[]' rows='1'>$record->vaccinations_dha_moh_approved</textarea></td>";
																		}
																		echo "</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold;'>Vaccinations (other including Travel Vaccinations)</td>";
																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='vaccinations_others_travel[]' rows='1'>$record->vaccinations_others_travel</textarea></td>";
																		}
																		echo "</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold; color: #fff; background:#00aedb;' colspan='100%'>EVACUATION  & REPATRIATION</td>
																		</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold;'>Evacuation</td>";
																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='evacuation[]' rows='1'>$record->evacuation</textarea></td>";
																		}
																		echo "</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold;'>Repatriation</td>";
																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='repatriation[]' rows='1'>$record->repatriation</textarea></td>";
																		}
																		echo "</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold; color: #fff; background:#00aedb;' colspan='100%'>CO-INSURANCE (Inside UAE)</td>
																		</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold;'>Inside the Network</td>";
																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='co_insur_uae_inside_network[]' rows='1'>$record->co_insur_uae_inside_network</textarea></td>";
																		}
																		
																		echo "</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold;'>Outside the Network</td>";
																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='co_insur_uae_outside_network[]' rows='1'>$record->co_insur_uae_outside_network</textarea></td>";
																		}
																		echo "</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold; color: #fff; background:#00aedb;' colspan='100%'>CO-INSURANCE (Outside UAE)</td>
																		</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold;'>Inside the Network</td>";
																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='co_insur_outsideuae_inside_network[]' rows='1'>$record->co_insur_outsideuae_inside_network</textarea></td>";
																		}
																		echo "</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold;'>Outside the Network</td>";
																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='co_insur_outsideuae_outside_network[]' rows='1'>$record->co_insur_outsideuae_outside_network</textarea></td>";
																		}
																		echo "</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold; color: #fff; background:#00aedb;' colspan='100%'>PREMIUM SUMMARY</td>
																		</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold;'>Total No of Members (Emp + Dependents)</td>";
																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='total_members[]' rows='1'>$record->total_members</textarea></td>";
																		}
																		echo "</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold;'>Premium Payment Mode</td>";
																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='payment_mode[]' rows='1'>$record->payment_mode</textarea></td>";
																		}
																		echo "</tr>

																		<tr>
																		<td style='text-align: left; font-weight: bold;'>Expiring Premium</td>";
																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='expiring_premium[]' rows='1'>$record->expiring_premium</textarea></td>";
																		}
																		echo "</tr>

																		<tr class='bg-color-blueLight'>

																		<td style='text-align: left; font-weight: bold;'>Proposed Annual / Renewal Premium (excluding VAT)</td>";
																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='proposed_annual_renewal_premium[]' rows='1'>$record->proposed_annual_renewal_premium</textarea></td>";
																		}
																	echo "</tr>

																	<tr>
																		<td style='text-align: left; font-weight: bold;'>% Increase</td>";
																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='increase[]' rows='1'>$record->increase</textarea></td>";
																		}
																	echo "</tr>
																	
																	<tr>
																		<td style='text-align: left; font-weight: bold;'>5% Withhod Tax</td>";
																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='withhold_tax[]' rows='1'>$record->withhold_tax</textarea></td>";
																		}
																	echo "</tr>
																	
																	<tr>
																		<td style='text-align: left; font-weight: bold;'>IPT Tax</td>";
																		foreach($result as $record) {
																			echo "<td style='text-align: center;'><textarea style='text-align: center; padding-top:5px;' name='ipt_tax[]' rows='1'>$record->ipt_tax</textarea></td>";
																		}
																	echo "</tr>

																</tbody>
														</table>";

														

														echo $html;
													}
												?>
													
											</tbody>
										</table>
										
										<br>

										<div><input type="hidden" name="totalrows" id="totalrows" value=""></div>

										<!-- <div><button type="button" name="add" id="add" class="btn btn-success">Add More</button></div> -->

										<!-- <div style="display: none;">Total Row: <input type="number" name="totalrow" id="totalrow"></div> -->
									</div>
								</fieldset>
								<br>

								<fieldset>
									
									

									<div class="form-group col-md-6">
										<label class="col-md-4 control-label text-left">Comments</label>
										<div class="col-md-7">
											<textarea class="form-control input-xs" rows="2" name="comment"><?php echo $data['record']->comment; ?></textarea>
										</div>
									</div>

									<?php if($_SESSION['group_id']== 1 || $_SESSION['group_id']==2 ){
										$hide = '';
									} else {
										$hide = 'hidden';
									}

									?>

									<div class="form-group col-md-6 <?php echo $hide; ?>">											
										<label class="control-label text-left col-md-4 col-md-offset-1">RM Name</label>
										<div class="col-md-7">
											<select class="form-control input-xs" id="salesperson" name="salesperson">
													<option selected value="<?php echo $data['record']->salesperson; ?>"><?php echo findName('users', $data['record']->salesperson); ?>
													</option>
													<?php echo getListFromTable('users'); ?>
												</select>
											<p class="note"><strong>Note:</strong> Default is logged in user.</p>
										</div>
									</div>

									<div class="form-group col-md-6 <?php echo $hide; ?>">
										<label class="control-label text-left col-md-4 col-md-offset-1">Assigned to</label>
										<div class="col-md-7">
											<select class="form-control input-xs" id="assigned_to" name="assigned_to">
												<option selected value="<?php echo $data['record']->assigned_to; ?>; ?>"><?php echo findName('users', $data['record']->assigned_to); ?>
												</option>
												<?php echo getListFromTable('users'); ?>
											</select>
										</div>
									</div>

									

								</fieldset>
									
								<br>

								<fieldset class="hidden">
									<legend>Attached Docuemnts</legend>
									<div class="col-xs-8 col-sm-6 col-lg-8">														
										<table id='attachments' class='table-hover table-responsive'>
										<?php
											foreach ($data['files'] as $file){
												$file_directory= ATTACHMENT_DIR;	
												echo "<tr>
													<td>
													<a href='/{$file_directory}/{$file->file_name}' target='_blank'>$file->file_name</a>
													</td>
													<td style='padding: 3px;'>
														<input type='hidden' name='file_id-[]' value='{$file->id}'><input type='hidden' id='delete_file-' name='delete_file-[]' value='0' class='input-xs delete_file '>
														&nbsp &nbsp
														<a class='fa fa-trash-o btn btn-warning delete-icon btn-sm' href='#'> x</a>
													</td>
												</tr>";
											}
										?>
										</table>
										
									</div>

									<div><input type="hidden" name="filerows" id="filerows" value=""></div>
								</fieldset>

								<fieldset class="hidden">									
								<legend>Upload Docuemnts</legend>
									<div class="form-group">
										<div class="col-md-7">
											<input class="" type="file" name="files[]" multiple="multiple">
										</div>
									</div>
								</fieldset>

								<div class="form-actions">
									<div class="row">
										<div class="col-md-12">
											<a href="<?php echo htmlspecialchars( URLROOT . current_class($this) ); ?>" class="btn btn-default" role="button">Cancel</a>
											<button class="btn btn-primary" id="savecomparison" type="submit" name="submit">
												<i class="fa fa-save"></i>Submit
											</button>
										</div>
									</div>
								</div>
		
							</form>
						</div>
						<!-- end widget content -->
	
					</div>
					<!-- end widget div -->
	
				</div>
				<!-- end widget -->
	
				<!-- Widget ID (each widget will need unique ID)-->

				<!-- end widget -->
			</article>
		</div>
	
		<!-- end row -->
	
		<!-- row -->
		<div class="row">
	
			<!-- NEW WIDGET START -->
			
			<!-- WIDGET END -->
	
		</div>
		<!-- end row -->
	
		<!-- row -->
		<div class="row">
			
		</div>
		<!-- end row -->
	</section>
	<!-- end widget grid -->

<!-- PAGE FOOTER -->
<?php include (APPROOT . "/views/inc/admin_footer.php"); ?>
<!-- END PAGE FOOTER -->