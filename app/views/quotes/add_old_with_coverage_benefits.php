<?php 

$aMan  = array();
$aPCat = array();
$aWorldwide = array();
$aWorldwideExUSA = array();
$aUae = array();

/// Manufacturers Code Starts ///

if(isset($_REQUEST['man'])&&is_array($_REQUEST['man'])){
	foreach($_REQUEST['man'] as $sKey=>$sVal){
		if((int)$sVal!=0){
			$aMan[(int)$sVal] = (int)$sVal;
		}
	}
}
/// Manufacturers Code Ends ///

/// Dental Code Starts ///
if(isset($_REQUEST['p_cat'])&&is_array($_REQUEST['p_cat'])){
	foreach($_REQUEST['p_cat'] as $sKey=>$sVal){
		if($sVal!=''){
			$aPCat[$sVal] = $sVal;
		}
	}
}
/// Dental Code Ends ///

// Worldwide Code Starts ///
if(isset($_REQUEST['worldwide'])&&is_array($_REQUEST['worldwide'])){
	foreach($_REQUEST['worldwide'] as $sKey=>$sVal){
		if($sVal!=''){
			$aWorldwide[$sVal] = $sVal;
		}
	}
}
/// Worldwide Code Ends ///

// Worldwide Code Starts ///
if(isset($_REQUEST['WorldwideExUSA'])&&is_array($_REQUEST['WorldwideExUSA'])){
	foreach($_REQUEST['WorldwideExUSA'] as $sKey=>$sVal){
		if($sVal!=''){
			$aWorldwideExUSA[$sVal] = $sVal;
		}
	}
}
/// Worldwide Code Ends ///

// Worldwide Code Starts ///
if(isset($_REQUEST['uae'])&&is_array($_REQUEST['uae'])){
	foreach($_REQUEST['uae'] as $sKey=>$sVal){
		if($sVal!=''){
			$aUae[$sVal] = $sVal;
		}
	}
}
/// Worldwide Code Ends ///

?>

<?php include (APPROOT . "/views/inc/admin_header.php"); ?>
	
	<!-- widget grid -->
	<section id="widget-grid" class="col-">
	
		<!-- <div class="well">
			
		</div> -->
	
		<!-- row -->
		<div class="row">
	
			<!-- NEW WIDGET START -->
			<article class="col-md-12">
	
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false">
					<!-- widget options:
					usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
	
					data-widget-colorbutton="false"
					data-widget-editbutton="false"
					data-widget-togglebutton="false"
					data-widget-deletebutton="false"
					data-widget-fullscreenbutton="false"
					data-widget-custombutton="false"
					data-widget-collapsed="true"
					data-widget-sortable="false"
	
					-->
					<header>
						<span class="widget-icon"> <i class="fa fa-eye"></i> </span>
						<h2>Add <?php echo $this->module; ?></h2>	
					</header>
	
					<!-- widget div-->
					<div>
						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->	
						</div>
						<!-- end widget edit box -->
	
						<!-- widget content -->
						<div class="widget-body">
							<form class="form-horizontal" method="post" action="<?php echo htmlspecialchars( URLROOT . current_class($this) . '/add'); ?>" enctype="multipart/form-data">

								<fieldset>
									<!-- <legend>Customer Details <?php; ?></legend> -->
									<!-- START OF pull-left -->
										<div class="pull-left">
											<div class="form-group">
												<label class="col-md-5 control-label text-left">Title</label>
												<div class="col-md-7">
													<input class="form-control input-xs" type="text" name="name" id="name">
												</div>
											</div>

											<div class="form-group hidden">
												<label class="col-md-5 control-label text-left">Customer Code <sup>*</sup></label>
												<div class="col-md-7">
													<input class="form-control input-xs ui-widget" type="text" name="customer_id" id="customer_id" required>
												</div>
											</div>

											<div class="form-group">
												<label class="col-md-5 control-label text-left">Customer <sup>*</sup></label>
												<div class="col-md-7">
													<input class="form-control input-xs txt-auto ui-widget" name="customer_name" id="customer_name" required>
												</div>
											</div>

											<div class="form-group">
												<label class="col-md-5 control-label text-left">Contact</label>
												<div class="col-md-7">
													<input class="form-control input-xs txt-auto ui-widget" name="contact_name" id="contact_name">
												</div>
											</div>

											<!-- <br><br> -->

											<!-- <div class="form-group">
												<label class="col-md-5 control-label text-left">street</label>
												<div class="col-md-7">
													<input class="form-control input-xs" type="text" name="street" id="street">
												</div>
											</div>

											<div class="form-group">
												<label class="col-md-5 control-label text-left">City</label>
												<div class="col-md-7">
													<input class="form-control input-xs" type="text" name="city" id="city">
												</div>
											</div>

											<div class="form-group">
												<label class="col-md-5 control-label text-left">State</label>
												<div class="col-md-7">
													<input class="form-control input-xs" type="text" name="state" id="state">
												</div>
											</div>

											<div class="form-group">
												<label class="col-md-5 control-label text-left">Country</label>
												<div class="col-md-7">
													<input class="form-control input-xs" type="text" name="country" id="country">
												</div>
											</div> -->

											

											<!-- <div class="form-group">
												<label class="col-md-5 control-label text-left">Customer TRN</label>
												<div class="col-md-7">
													<input class="form-control input-xs" type="text" name="customer_trn" id="customer_trn">
												</div>
											</div> -->

										</div>
										<!-- END OF pull-left -->

										<!-- START OF pull-right -->
											<div class="pull-right">

												<div class="form-group">
													<label class="col-md-5 control-label text-left">Opportunity</label>
													<div class="col-md-7">
														<input class="form-control input-xs txt-auto ui-widget" name="opportunity" id="opportunity">
													</div>
												</div>

												<div class="form-group">
													<label class="col-md-5 control-label text-left">Quote Date</label>
													<div class="col-md-7">
														<input class="form-control input-xs" type="date" name="date" value="<?php echo date('Y-m-d'); ?>">
													</div>
												</div>

												<!-- <div class="form-group">
													<label class="col-md-5 control-label text-left">Invoice No.</label>
													<div class="col-md-7">
														<input class="form-control input-xs" type="text" name="invoice_no">
													</div>
												</div> -->

												

												<div class="form-group">
													<label class="col-md-5 control-label text-left">Valid Until</label>
													<div class="col-md-7">
														<input class="form-control input-xs" type="date" name="valid_until" value="<?php echo validityDate('7'); ?>">
													</div>
												</div>

												<!-- <div class="form-group">
													<label class="col-md-5 control-label text-left">Quote/LPO Ref</label>
													<div class="col-md-7">
														<input class="form-control input-xs" type="text" name="quote_lpo_ref">
													</div>
												</div> -->

												<!-- <div class="form-group">
													<label class="col-md-5 control-label text-left">Deliver To</label>
													<div class="col-md-7">
														<textarea class="form-control input-xs" rows="1" name="delivery_to"></textarea>
													</div>
												</div> -->

												<!-- <br><br> -->

												<!-- <div class="form-group">
													<label class="col-md-5 control-label text-left">Currency</label>
													<div class="col-md-7">
														<select class="form-control input-xs" name="currency" id="currency">
															<?php echo dropDownList("currency"); ?>
														</select>
													</div>
												</div>

												<div class="form-group">
													<label class="col-md-5 control-label text-left">Phone</label>
													<div class="col-md-7">
														<input class="form-control input-xs" type="text" name="phone" id="phone" >
													</div>
												</div>

												<div class="form-group">
													<label class="col-md-5 control-label text-left">Postal Code</label>
													<div class="col-md-7">
														<input class="form-control input-xs" type="text" name="postal_code" id="postal_code" >
													</div>
												</div> -->

											</div>
											<!-- END OF pull-right -->				
								</fieldset>

								<fieldset>
									<div class="pull-left">
										
									</div>

									<div class="pull-right">
										
										
									</div>
									
								</fieldset>

								<fieldset>
									<div class="col-md-3" style="border-right: 1px solid #ccc;">
										<h6> Coverage </h6>

					                    <div class="panel-collapse collapse-data">

											<div class="">
												<ul class="nav nav-pills nav-stacked" id="dev-p-cats">

													<li style='display: inline-block; width: 85px;'>
													    <input <?php if(isset($aWorldwide['worldwide'])){ echo "checked='checked'";} ?>
																			 type='checkbox' value='area_of_cover' name='worldwide' class='get_worldwide onoffswitch-checkbox' id='worldwide'>
													    <label class="onoffswitch-label" for="worldwide">
													        <span class="onoffswitch-inner-worldwide onoffswitch-inner"></span>
													        <span class=""></span>
													    </label>
												    </li>

												    <li style='display: inline-block; width: 120px;'>
													    <input <?php if(isset($aWorldwideExUSA['WorldwideExUSA'])){ echo "checked='checked'";} ?>
																			 type='checkbox' value='area_of_cover' name='WorldwideExUSA' class='get_WorldwideExUSA onoffswitch-checkbox' id='WorldwideExUSA'>
													    <label class="onoffswitch-label" for="WorldwideExUSA">
													        <span class="onoffswitch-inner-WorldwideExUSA onoffswitch-inner"></span>
													        <span class=""></span>
													    </label>
												    </li>

												    <li style='display: inline-block; width: 85px;'>
													    <input <?php if(isset($aUae['uae'])){ echo "checked='checked'";} ?>
																			 type='checkbox' value='area_of_cover' name='uae' class='get_uae onoffswitch-checkbox' id='uae'>
													    <label class="onoffswitch-label" for="uae">
													        <span class="onoffswitch-inner-uae onoffswitch-inner"></span>
													        <span class=""></span>
													    </label>
												    </li>
												</ul>

											</div>

										</div>
									</div>

									<!-- New Item manufacturers Start -->									
									<div class="col-md-5" style="border-right: 1px solid #ccc;">
										<h6> Insurance Providers</h6>
										
										<div class="panel-collapse collapse-data">

											<div class="panel-body scroll-menu">
												<ul class="nav nav-pills nav-stacked category-menu" id="dev-manufacturer">

													<?php foreach($data['manufacturer'] as $record) : ?>

														<li style='display: inline-block;' class='checkbox checkbox-primary'>
															<a>
																<label>
																	<input <?php if(isset($aMan[$record->id])){ echo "checked='checked'";} ?>
																	 type='checkbox' value='<?php echo $record->id; ?>' name='manufacturer' class='get_manufacturer'>
																		<span>							
																			<?php echo $record->name; ?>
																		</span>

																</label>
															</a>
														</li>

													<?php endforeach; ?>
													
												</ul>				
											</div>
											
										</div>	
									</div>
									<!-- New Item manufacturers End -->

									<!-- New Item Benefits Start -->									
									
									<!-- New Item Benefits End -->

									<div class="col-md-4" style="border-right: 1px solid #ccc;">
										<h6> Benefits </h6>

					                    <div class="panel-collapse collapse-data">

											<div class="">
												<ul class="nav nav-pills nav-stacked" id="dev-p-cats">

													<li style='display: inline-block; width: 105px;'>
													    <input <?php if(isset($aPcat['maternity'])){ echo "checked='checked'";} ?>
																			 type='checkbox' value='maternity' name='p_cat' class='get_p_cat onoffswitch-checkbox' id='p_cat'>
													    <label class="onoffswitch-label" for="p_cat">
													        <span class="onoffswitch-inner-maternity onoffswitch-inner"></span>
													        <span class="onoffswitch-switch"></span>
													    </label>
												    </li>

												    <li style='display: inline-block; width: 105px;'>										    
													    <input <?php if(isset($aPcat['inpatient'])){ echo "checked='checked'";} ?>
																			 type='checkbox' value='inpatient' name='p_cat' class='get_p_cat onoffswitch-checkbox' id='p_cat1'>
													    <label class="onoffswitch-label" for="p_cat1">
													        <span class="onoffswitch-inner-inpatient onoffswitch-inner"></span>
													        <span class="onoffswitch-switch"></span>
													    </label>
												    </li>

												    <li style='display: inline-block; width: 105px;'>
													    <input <?php if(isset($aPcat['outpatient'])){ echo "checked='checked'";} ?>
																			 type='checkbox' value='outpatient' name='p_cat' class='get_p_cat onoffswitch-checkbox' id='p_cat2'>
													    <label class="onoffswitch-label" for="p_cat2">
													        <span class="onoffswitch-inner-outpatient onoffswitch-inner"></span>
													        <span class="onoffswitch-switch"></span>
													    </label>
												    </li>

												    <li style='display: inline-block; width: 105px;'>
													    <input <?php if(isset($aPcat['dental'])){ echo "checked='checked'";} ?>
																			 type='checkbox' value='dental' name='p_cat' class='get_p_cat onoffswitch-checkbox' id='p_cat3'>
													    <label class="onoffswitch-label" for="p_cat3">
													        <span class="onoffswitch-inner-dental onoffswitch-inner"></span>
													        <span class="onoffswitch-switch"></span>
													    </label>
												    </li>

												    <li style='display: inline-block; width: 105px;'>
													    <input <?php if(isset($aPcat['optical'])){ echo "checked='checked'";} ?>
																			 type='checkbox' value='optical' name='p_cat' class='get_p_cat onoffswitch-checkbox' id='p_cat4'>
													    <label class="onoffswitch-label" for="p_cat4">
													        <span class="onoffswitch-inner-optical onoffswitch-inner"></span>
													        <span class="onoffswitch-switch"></span>
													    </label>
												    </li>

												    <li style='display: inline-block; width: 105px;'>
													    <input <?php if(isset($aPcat['wellness'])){ echo "checked='checked'";} ?>
																			 type='checkbox' value='wellness' name='p_cat' class='get_p_cat onoffswitch-checkbox' id='p_cat5'>
													    <label class="onoffswitch-label" for="p_cat5">
													        <span class="onoffswitch-inner-wellness onoffswitch-inner"></span>
													        <span class="onoffswitch-switch"></span>
													    </label>
												    </li>



												</ul>

											</div>

										</div>
									</div>
								</fieldset>

								<button style="color: #fff;" class="btn btn-info compare" rel="<?php ; ?>">Compare Plans</button><br><br>

								<!-- <fieldset>
									<div class="col-lg-12">

										<div class="section-heading">
											<h2>Deal for Individual Plans</h2>

											<div class="table-responsive">
												<table class="table table-hover table-striped">
													<thead style="background: #496949 !important; color: #fff;">
														<tr style="background: #496949 !important; color: #fff;">
															<td style="text-align: left; font-weight: bold; vertical-align: middle;">Benefits</td>
															<?php foreach($data['products'] as $record) : ?>
																<td style="font-weight: bold; vertical-align: middle;"><?php echo $record->name; ?></td>
															<?php endforeach; ?>
														</tr>
													</thead>
														<tbody>
															<tr>
																<td style="text-align: left; font-weight: bold;">Annual limit</td>

																<?php foreach($data['products'] as $record) : ?>
																	<td><?php echo $record->annual_cover; ?></td>        
																<?php endforeach; ?>
															</tr>

															<tr>
																<td style="text-align: left; font-weight: bold;">Area of Cover</td>

																<?php foreach($data['products'] as $record) : ?>
																	<td><?php echo $record->selling_price; ?></td>        
																<?php endforeach; ?>
															</tr>

															<tr>
																<td style="text-align: left; font-weight: bold;">Treatment Oustside Area of Cover</td>

																<?php foreach($data['products'] as $record) : ?>
																	<td><?php echo $record->area_of_cover; ?></td>        
																<?php endforeach; ?>
															</tr>

															<tr>
																<td style="text-align: left; font-weight: bold;">Chronic conditions</td>

																<?php foreach($data['products'] as $record) : ?>

																<td><?php echo $record->chronic_conditions; ?></td>        
																<?php endforeach; ?>
															</tr>
														</tbody>
												</table>
														
											</div>
										</div>

									</div>
								</fieldset> -->

								<fieldset>

								<div class="col-md-12" style="overflow-x: auto;">
									<div class="row" id="Products">
										<div class="">
											
											
										</div>
										
									</div>
									
								</div>								

									
								</fieldset>

								<!-- <div class="text-center">
									<button class="btn btn-success wei-add-service-button" type="button">
										<i class="fa fa-plus-square"></i>
										Add Row
									</button>

									<button class="btn btn-success wei-add-compare-button" type="button">
										<i class="fa fa-plus-square"></i>
										Get Comparision
									</button>

									<button class="btn btn-success" type="button" >
										<i class="fa fa-codepen"></i>
										Add Product
									</button>
								</div> -->
								<br>

								<!-- Old working Products filtering -->

								<!-- div class="row" id="Productsold">
									
								</div> -->

								<fieldset>
									<div class="table-responsive">
										<table class="order-details table-hover" id="items_table">
											<tbody>
												
											</tbody>
										</table>
										<br>

										<!-- <div class="text-left">
											<button class="btn btn-success wei-add-service-button" id="addbutton" type="button">
												<i class="fa fa-plus-square"></i>Add Row
											</button>
											<button class="btn btn-success" type="button">
												<i class="fa fa-codepen"></i>Add Product
											</button>
										</div> -->
										<!-- <div class="wei-add-service"><a href="#" class="button-secondary wei-add-service-button">Add Item</a></div> -->
										<br>

										

										<div><input type="hidden" name="totalrows" id="totalrows" value=""></div>

										<!-- <div><button type="button" name="add" id="add" class="btn btn-success">Add More</button></div> -->

										<!-- <div style="display: none;">Total Row: <input type="number" name="totalrow" id="totalrow"></div> -->
									</div>
								</fieldset>
								<br>

								<fieldset>

									
									
								</fieldset>

								<fieldset>
									<div class="form-group col-md-6">
										<label class="col-md-4 control-label text-left">Payment Terms</label>
										<div class="col-md-7">
										<textarea class="form-control input-xs" rows="1" name="payment_terms"></textarea>
										</div>
									</div>

									<div class="form-group col-md-6">											
										<label class="control-label text-left col-md-4 col-md-offset-1">Sales Person</label>
										<div class="col-md-7">
											<select class="form-control input-xs" id="salesperson" name="salesperson">
													<option selected value="<?php echo $_SESSION['user_id']; ?>"><?php echo findName('users', $_SESSION['user_id']); ?>
													</option>
													<?php echo getListFromTable('users'); ?>
												</select>
											<p class="note"><strong>Note:</strong> Default is logged in user.</p>
										</div>
									</div>

									<div class="form-group col-md-6">
										<label class="col-md-4 control-label text-left">Comments</label>
										<div class="col-md-7">
											<textarea class="form-control input-xs" rows="2" name="comments"></textarea>
										</div>
									</div>

									<div class="form-group col-md-6">
										<label class="control-label text-left col-md-4 col-md-offset-1">Assigned to</label>
										<div class="col-md-7">
											<select class="form-control input-xs" id="assigned_to" name="assigned_to">
												<option selected value="<?php echo $_SESSION['user_id']; ?>"><?php echo findName('users', $_SESSION['user_id']); ?>
												</option>
												<?php echo getListFromTable('users'); ?>
											</select>
										</div>
									</div>

								</fieldset>
									
								<br>

								<fieldset>									
								<legend>Upload Docuemnts</legend>
									<div class="form-group">
										<div class="col-md-7">
											<input class="" type="file" name="files[]" multiple="multiple">
										</div>
									</div>
								</fieldset>

								<div class="form-actions">
									<div class="row">
										<div class="col-md-12">
											<a href="<?php echo htmlspecialchars( URLROOT . current_class($this) ); ?>" class="btn btn-default" role="button">Cancel</a>
											<button class="btn btn-primary" id="submit" type="submit" name="submit">
												<i class="fa fa-save"></i> Save & Send
											</button>
										</div>
									</div>
								</div>
		
							</form>
						</div>
						<!-- end widget content -->
	
					</div>
					<!-- end widget div -->
	
				</div>
				<!-- end widget -->
	
				<!-- Widget ID (each widget will need unique ID)-->

				<!-- end widget -->
			</article>
		</div>
	
		<!-- end row -->
	
		<!-- row -->
		<div class="row">
	
			<!-- NEW WIDGET START -->
			
			<!-- WIDGET END -->
	
		</div>
		<!-- end row -->
	
		<!-- row -->
		<div class="row">
			
		</div>
		<!-- end row -->
	</section>
	<!-- end widget grid -->

<!-- PAGE FOOTER -->
<?php include (APPROOT . "/views/inc/admin_footer.php"); ?>
<!-- END PAGE FOOTER -->