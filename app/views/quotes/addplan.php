<?php 

$aMan  = array();
$aPCat = array();
$aWorldwide = array();
$aWorldwideExUSA = array();
$aUae = array();
$_SESSION['plan'] = "addplan";
$_SESSION['quote_id'] = "";

/// Manufacturers Code Starts ///

if(isset($_REQUEST['man'])&&is_array($_REQUEST['man'])){
	foreach($_REQUEST['man'] as $sKey=>$sVal){
		if((int)$sVal!=0){
			$aMan[(int)$sVal] = (int)$sVal;
		}
	}
}
/// Manufacturers Code Ends ///

/// Dental Code Starts ///
if(isset($_REQUEST['p_cat'])&&is_array($_REQUEST['p_cat'])){
	foreach($_REQUEST['p_cat'] as $sKey=>$sVal){
		if($sVal!=''){
			$aPCat[$sVal] = $sVal;
		}
	}
}
/// Dental Code Ends ///

// Worldwide Code Starts ///
if(isset($_REQUEST['worldwide'])&&is_array($_REQUEST['worldwide'])){
	foreach($_REQUEST['worldwide'] as $sKey=>$sVal){
		if($sVal!=''){
			$aWorldwide[$sVal] = $sVal;
		}
	}
}
/// Worldwide Code Ends ///

// Worldwide Code Starts ///
if(isset($_REQUEST['WorldwideExUSA'])&&is_array($_REQUEST['WorldwideExUSA'])){
	foreach($_REQUEST['WorldwideExUSA'] as $sKey=>$sVal){
		if($sVal!=''){
			$aWorldwideExUSA[$sVal] = $sVal;
		}
	}
}
/// Worldwide Code Ends ///

// Worldwide Code Starts ///
if(isset($_REQUEST['uae'])&&is_array($_REQUEST['uae'])){
	foreach($_REQUEST['uae'] as $sKey=>$sVal){
		if($sVal!=''){
			$aUae[$sVal] = $sVal;
		}
	}
}
/// Worldwide Code Ends ///

?>

<?php include (APPROOT . "/views/inc/admin_header.php"); ?>
	
	<!-- widget grid -->
	<section id="widget-grid" class="col-">
	
		<!-- <div class="well">
			
		</div> -->
	
		<!-- row -->
		<div class="row">
	
			<!-- NEW WIDGET START -->
			<article class="col-md-12">
	
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false">
					<!-- widget options:
					usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
	
					data-widget-colorbutton="false"
					data-widget-editbutton="false"
					data-widget-togglebutton="false"
					data-widget-deletebutton="false"
					data-widget-fullscreenbutton="false"
					data-widget-custombutton="false"
					data-widget-collapsed="true"
					data-widget-sortable="false"
	
					-->
					<header>
						<span class="widget-icon"> <i class="fa fa-eye"></i> </span>
						<h2>Add to Existing Comparison</h2>	
					</header>
	
					<!-- widget div-->
					<div>
						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->	
						</div>
						<!-- end widget edit box -->
	
						<!-- widget content -->
						<div class="widget-body">
							<a style="color: #fff;" class="btn btn-info txt-color-white compare" href="<?php echo URLROOT . current_class($this); ?>/edit/<?php echo $data['record']->id; ?>"><span style="font-weight: bold; text-transform: none;">Back to Edit Plans in Ref# <?php echo $data['record']->id; ?></span></a>

								<fieldset>
									<div class="">
										<div class="form-group">
											<label class="col-md-2 control-label text-left"><strong>Comparison Ref# </strong></label>
											<div class="col-md-3">
												<strong><a href="<?php echo URLROOT . current_class($this); ?>/edit/<?php echo $data['record']->id; ?>"><?php echo $_SESSION['quote_id']=$data['record']->id; ?></a></strong>
											</div>


										</div>
									</div>
									<br><br>

									<!-- New Item manufacturers Start -->									
									<div class="col-md-12" style="border-right: 1px solid #ccc;">
										<h6> Insurance Providers</h6>
										
										<div class="panel-collapse collapse-data">

											<div class="panel-body scroll-menu">
												<ul class="nav nav-pills nav-stacked category-menu" id="dev-manufacturer">

													<?php foreach($data['manufacturer'] as $record) : ?>

														<li style='display: inline-block;' class='checkbox checkbox-primary'>
															<a style="color: #043777 !important;">
																<label>
																	<input <?php if(isset($aMan[$record->id])){ echo "checked='checked'";} ?>
																	 type='checkbox' value='<?php echo $record->id; ?>' name='manufacturer' class='get_manufacturer'>
																		<span>							
																			<?php echo $record->name; ?>
																		</span>

																</label>
															</a>
														</li>

													<?php endforeach; ?>
													
												</ul>				
											</div>
											
										</div>

										<div class="col-md-12">
											<!-- <h6> Coverage </h6> -->

						                    <div class="panel-collapse collapse-data">

												<div class="">
													<ul class="nav nav-pills nav-stacked" id="dev-p-cats">
														
														<li style='display: inline-block; width: 85px;'>
														    <input <?php if(isset($aPlantypeGrp['plantypeGrp'])){ echo "checked='checked'";} ?>
																				 type='checkbox' value='category_id' name='plantypeGrp' class='get_plantypeGrp onoffswitch-checkbox' id='plantypeGrp'>
														    <label class="onoffswitch-label" for="plantypeGrp">
														        <span class="onoffswitch-inner-plantypeGrp onoffswitch-inner"></span>
														        <span class=""></span>
														    </label>
													    </li>

														<li style='display: inline-block; width: 85px;'>
														    <input <?php if(isset($aPlantypeInd['plantypeind'])){ echo "checked='checked'";} ?>
																				 type='checkbox' value='category_id' name='plantypeInd' class='get_plantypeInd onoffswitch-checkbox' id='plantypeInd'>
														    <label class="onoffswitch-label" for="plantypeInd">
														        <span class="onoffswitch-inner-plantypeInd onoffswitch-inner"></span>
														        <span class=""></span>
														    </label>
													    </li>
														
														 <span style="padding-right: 50px;"></span>

													   <!--
													  

													    <li style='display: inline-block; width: 85px;'>
														    <input <?php if(isset($aDha['dha'])){ echo "checked='checked'";} ?>
																				 type='checkbox' value='plan_compliance' name='dha' class='get_dha onoffswitch-checkbox' id='dha'>
														    <label class="onoffswitch-label" for="dha">
														        <span class="onoffswitch-inner-dha onoffswitch-inner"></span>
														        <span class=""></span>
														    </label>
													    </li>

														<li style='display: inline-block; width: 85px;'>
														    <input <?php if(isset($aHaad['haad'])){ echo "checked='checked'";} ?>
																				 type='checkbox' value='plan_compliance' name='haad' class='get_haad onoffswitch-checkbox' id='haad'>
														    <label class="onoffswitch-label" for="haad">
														        <span class="onoffswitch-inner-haad onoffswitch-inner"></span>
														        <span class=""></span>
														    </label>
													    </li>
														-->
														
														<li style='display: inline-block; width: 85px;'>
														    <input <?php if(isset($aEuro['euro'])){ echo "checked='checked'";} ?>
																				 type='checkbox' value='currency_id' name='euro' class='get_euro onoffswitch-checkbox' id='euro'>
														    <label class="onoffswitch-label" for="euro">
														        <span class="onoffswitch-inner-euro onoffswitch-inner"></span>
														        <span class=""></span>
														    </label>
													    </li>
														
														
														<li style='display: inline-block; width: 85px;'>
														    <input <?php if(isset($aGBP['gbp'])){ echo "checked='checked'";} ?>
																				 type='checkbox' value='currency_id' name='gbp' class='get_gbp onoffswitch-checkbox' id='gbp'>
														    <label class="onoffswitch-label" for="gbp">
														        <span class="onoffswitch-inner-gbp onoffswitch-inner"></span>
														        <span class=""></span>
														    </label>
													    </li>
														
														
														<li style='display: inline-block; width: 85px;'>
														    <input <?php if(isset($aUSD['usd'])){ echo "checked='checked'";} ?>
																				 type='checkbox' value='currency_id' name='usd' class='get_usd onoffswitch-checkbox' id='usd'>
														    <label class="onoffswitch-label" for="usd">
														        <span class="onoffswitch-inner-usd onoffswitch-inner"></span>
														        <span class=""></span>
														    </label>
													    </li>
													    
														
														
													    
													</ul>

													<a style="color: #fff; margin-top: 7px;" class="btn btn-info compare hidden" href="<?php echo URLROOT . current_class($this). '/compare'; ?>">Compare Plans</a>	

												</div>

											</div>
										</div>
									</div>
									<!-- New Item manufacturers End -->

									<!-- New Item Benefits Start -->									
									
									<!-- New Item Benefits End -->									
								</fieldset>

								<br><br>

								<fieldset>

								<div class="col-md-12" style="overflow-x: auto;">
									<div class="row" id="Products">
										<div class="">
											
											
										</div>
										
									</div>
									
								</div>								

									
								</fieldset>

								<!-- <div class="text-center">
									<button class="btn btn-success wei-add-service-button" type="button">
										<i class="fa fa-plus-square"></i>
										Add Row
									</button>

									<button class="btn btn-success wei-add-compare-button" type="button">
										<i class="fa fa-plus-square"></i>
										Get Comparision
									</button>

									<button class="btn btn-success" type="button" >
										<i class="fa fa-codepen"></i>
										Add Product
									</button>
								</div> -->
								<br>

								<!-- Old working Products filtering -->

								<!-- div class="row" id="Productsold">
									
								</div> -->

								<fieldset>
									<div class="table-responsive">
										<table class="order-details table-hover" id="items_table">
											<tbody>
												
											</tbody>
										</table>
										<br>

										<!-- <div class="text-left">
											<button class="btn btn-success wei-add-service-button" id="addbutton" type="button">
												<i class="fa fa-plus-square"></i>Add Row
											</button>
											<button class="btn btn-success" type="button">
												<i class="fa fa-codepen"></i>Add Product
											</button>
										</div> -->
										<!-- <div class="wei-add-service"><a href="#" class="button-secondary wei-add-service-button">Add Item</a></div> -->
										<br>

										<div><input type="hidden" name="totalrows" id="totalrows" value=""></div>

										<!-- <div><button type="button" name="add" id="add" class="btn btn-success">Add More</button></div> -->

										<!-- <div style="display: none;">Total Row: <input type="number" name="totalrow" id="totalrow"></div> -->
									</div>
								</fieldset>
								<br>

								<fieldset>

									
									
								</fieldset>

								<fieldset>

									<div class="form-group col-md-6">
										<label class="col-md-4 control-label text-left">Comments</label>
										<div class="col-md-7">
											<textarea class="form-control input-xs" rows="2" name="comment"></textarea>
										</div>
									</div>

									<?php if($_SESSION['group_id']== 1 || $_SESSION['group_id']==2 ){
										$hide = '';
									} else {
										$hide = 'hidden';
									}

									?>			

									<div class="form-group col-md-6 <?php echo $hide; ?>">											
										<label class="control-label text-left col-md-4 col-md-offset-1">RM Name</label>
										<div class="col-md-7">
											<select class="form-control input-xs" id="salesperson" name="salesperson">
													<option selected value="<?php echo $_SESSION['user_id']; ?>"><?php echo findName('users', $_SESSION['user_id']); ?>
													</option>
													<?php echo getListFromTable('users'); ?>
												</select>
											<p class="note"><strong>Note:</strong> Default is logged in user.</p>
										</div>
									</div>									

									<div class="form-group col-md-6 <?php echo $hide; ?>">
										<label class="control-label text-left col-md-4 col-md-offset-1">Assigned to</label>
										<div class="col-md-7">
											<select class="form-control input-xs" id="assigned_to" name="assigned_to">
												<option selected value="<?php echo $_SESSION['user_id']; ?>"><?php echo findName('users', $_SESSION['user_id']); ?>
												</option>
												<?php echo getListFromTable('users'); ?>
											</select>
										</div>
									</div>

								

								</fieldset>
									

								<div class="form-actions hidden">
									<div class="row">
										<div class="col-md-12">
											<a href="#" class="btn btn-default" role="button">Cancel</a>
											<button class="btn btn-primary" id="submit" type="submit" name="submit">
												<i class="fa fa-save"></i> Save & Send
											</button>
										</div>
									</div>
								</div>
		
							
						</div>
						<!-- end widget content -->
	
					</div>
					<!-- end widget div -->
	
				</div>
				<!-- end widget -->
	
				<!-- Widget ID (each widget will need unique ID)-->

				<!-- end widget -->
			</article>
		</div>
	
		<!-- end row -->
	
		<!-- row -->
		<div class="row">
	
			<!-- NEW WIDGET START -->
			
			<!-- WIDGET END -->
	
		</div>
		<!-- end row -->
	
		<!-- row -->
		<div class="row">
			
		</div>
		<!-- end row -->
	</section>
	<!-- end widget grid -->

<!-- PAGE FOOTER -->
<?php include (APPROOT . "/views/inc/admin_footer.php"); ?>
<!-- END PAGE FOOTER -->