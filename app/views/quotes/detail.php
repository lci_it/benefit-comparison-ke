<?php include (APPROOT . "/views/inc/admin_header.php"); ?>
<div class="module">

	<?php include (APPROOT . "/views/inc/admin_detail_toolbar.php"); ?>
</div>

	<!-- widget grid -->
<section id="widget-grid" class="">
				
	<!-- row -->
	<div class="row">

		<!-- NEW WIDGET START -->
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget well jarviswidget-color-darken" id="wid-id-0" data-widget-sortable="false" data-widget-deletebutton="false" data-widget-editbutton="false" data-widget-colorbutton="false">
				<!-- widget options:
				usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

				data-widget-colorbutton="false"
				data-widget-editbutton="false"
				data-widget-togglebutton="false"
				data-widget-deletebutton="false"
				data-widget-fullscreenbutton="false"
				data-widget-custombutton="false"
				data-widget-collapsed="true"
				data-widget-sortable="false"

				-->
				<header>
					<span class="widget-icon"> <i class="fa fa-barcode"></i> </span>
					<h2>Item #44761 </h2>

				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->

					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body no-padding">

						

						<div class="padding-10" id="invoice">
							
							<div class="pull-left">
								<!-- <img src="img/logoold.jpg" width="150" height="32" alt="invoice icon">	 -->
								<h1 class="hidden"><strong>Benefit Comparison</strong></h1>
								
							</div>
							<div class="pull-right">
								<!-- <h1 class="font-500">Benefit Comparison</h1> -->
							</div>
							<div class="clearfix"></div>
							<!-- <br> -->

							<?php 
								$result = $data['products'];

								$count = count($result)+1;
								
							?>

							<fieldset>
								<div class="table-responsive">

									<table class='table table-hover table-striped table-bordered custom-height' id='comparison_table'>
										<thead style='color: #fff; text-transform: uppercase;'>

											<tr>
												<th style='background: #fff !important; color: #09276f; text-align: right; padding-top: 1em; padding-bottom: 1em;' colspan="<?php echo $count; ?>" width="100%">

													<input class="hidden" type="text" id="customer_name" name="customer_name" value="<?php echo $data['detail']->customer_name; ?>" readonly>

													<input class="hidden" type="text" id="rm_name" name="rm_name" value="<?php echo $_SESSION['user_fullname']; ?>" readonly>

													<input class="hidden" type="text" id="email_address" name="email_address" value="<?php echo $_SESSION['email']; ?>" readonly>

													
												</th>
											</tr>

											<tr>
												<th style='background: #542989 !important; font-weight: bold; vertical-align: middle; text-align: left; padding: 5px 10px !important;'>Service Provider</th>

												<?php
													foreach($result as $record) {
														$manufacturer = findName('manufacturer', $record->manufacturer_id);
														echo "<th style='background: #542989; font-weight: bold; vertical-align: middle; text-align: center; width: 200px; padding: 5px 10px !important;'>$manufacturer</th>";
													}
												?>

											</tr>

											<tr style='color: #fff; text-transform: uppercase;'>
												<th style='background: #542989 !important; text-align: left; font-weight: bold; vertical-align: middle; width:200px; height:50px; padding: 5px 10px !important;'>Plan</th>

												<?php foreach($result as $record) {
													echo "<th style='background: #542989 !important; font-weight: bold; vertical-align: middle; text-align: center; padding: 5px 10px !important;'><p>$record->product_name</p>
													<div class='alert alert-success alert-dismissible success' id='success' style='display:none;'>
													  <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
													</div>
													</th>";
												}
												?>
											</tr>
										</thead>
											<tbody>
												<tr>
													<td style='text-align: left; font-weight: bold;'>Currency</td>

													<?php foreach($result as $record) {
														echo "<td style='text-align: center;'>" . dropdownValue($record->currency_id, 'currency') . "</td>";      
													}
													?>						

												</tr>
												
												<tr>
													<td style='text-align: left; font-weight: bold;'>Annual limit</td>

													<?php foreach($result as $record) {
														echo "<td style='text-align: center;'>" . $record->annual_cover . "</td>";      
													}
													?>						

												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Area of Cover</td>

													<?php foreach($result as $record) {
														echo "<td style='text-align: center;'>$record->area_of_cover</td>";       
													}
													?>
													
												</tr><tr>
													<td style='text-align: left; font-weight: bold;'>Emergency Treatment Outside Area of Cover</td>

													<?php foreach($result as $record) {
														echo "<td style='text-align: center;'>$record->emergency_treatment_outside_area_of_cover</td>";
													}
													?>

													</tr>


													<tr>
													<td style='text-align: left; font-weight: bold;'>Congenital Conditions</td>

													<?php foreach($result as $record) {
														echo "<td style='text-align: center;'>$record->congenital_conditions </td>";
													}
													?>

													</tr>

													<tr>
													<td style='text-align: left; font-weight: bold;'>Direct Settlement</td>

													<?php foreach($result as $record) {
														echo "<td style='text-align: center;'>$record->network </td>";
													}
													?>

													</tr>


													<tr>
													<td style='text-align: left; font-weight: bold;'>Underwriting Criteria (FMU / MHD)</td>

													<?php foreach($result as $record) {
														echo "<td style='text-align: center;'>$record->underwriting_critera </td>";
													}
													?>

													</tr>

													<tr>
													<td style='text-align: left; font-weight: bold;'>Pricing Model (Community Rated / Individual Rated)</td>

													<?php foreach($result as $record) {
														echo "<td style='text-align: center;'>$record->pricing_model </td>";
													}
													?>

													</tr>

											</tbody>
									</table>

									<table class='table table-hover table-striped table-bordered custom-height' id='inpatient_benefits'>
										<thead style='color: #fff; text-transform: uppercase;'>

											<tr>
												<th style='background: #fff !important; color: #09276f; text-align: right; padding-top: 1em; padding-bottom: 1em;' colspan="<?php echo $count; ?>" width="100%">

													<input class="hidden" type="text" id="customer_name" name="customer_name" value="<?php echo $data['detail']->customer_name; ?>" readonly>

													<input class="hidden" type="text" id="rm_name" name="rm_name" value="<?php echo $_SESSION['user_fullname']; ?>" readonly>

													<input class="hidden" type="text" id="email_address" name="email_address" value="<?php echo $_SESSION['email']; ?>" readonly>
												</th>
											</tr>

											<tr>
												<th style='background: #542989 !important; font-weight: bold; vertical-align: middle; text-align: left; padding: 5px 10px !important;'>Service Provider</th>

												<?php
													foreach($result as $record) {
														$manufacturer = findName('manufacturer', $record->manufacturer_id);
														echo "<th style='background: #542989; font-weight: bold; vertical-align: middle; text-align: center; width: 200px; padding: 5px 10px !important;'>$manufacturer</th>";
													}
												?>

											</tr>

											<tr style='color: #fff; text-transform: uppercase;'>
												<th style='background: #542989 !important; text-align: left; font-weight: bold; vertical-align: middle; width:200px; height:50px; padding: 5px 10px !important;'>Plan</th>

												<?php foreach($result as $record) {
													echo "<th style='background: #542989 !important; font-weight: bold; vertical-align: middle; text-align: center; padding: 5px 10px !important;'><p>$record->product_name</p>
													<div class='alert alert-success alert-dismissible success' id='success' style='display:none;'>
													  <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
													</div>
													</th>";
												}
												?>
											</tr>
										</thead>
											<tbody>												

													<tr>
													<td style='text-align: left; font-weight: bold; color: #fff; background:#00aedb; padding: 5px 10px !important;' colspan='<?php echo $count; ?>' width="100%">IN PATIENT TREATMENT</td>
													</tr>

													<tr>
													<td style='text-align: left; font-weight: bold;'>Deductible / Excess / Co-Insurance</td>

													<?php foreach($result as $record) {
														echo "<td style='text-align: center;'>$record->ip_deductible </td>";
													}
													?>

													</tr>

													<tr>
													<td style='text-align: left; font-weight: bold;'>Room & Board</td>

													<?php foreach($result as $record) {
														echo "<td style='text-align: center;'>$record->ip_room_board </td>";
													}
													?>

													</tr>

													

													<tr>
													<td style='text-align: left; font-weight: bold;'>Specialist Fees</td>

													<?php foreach($result as $record) {
														echo "<td style='text-align: center;'>$record->ip_specialist_fees </td>";
													}
													?>

													</tr>

													<tr>
													<td style='text-align: left; font-weight: bold;'>Surgery and Anesthesia</td>

													<?php foreach($result as $record) {
														echo "<td style='text-align: center;'>$record->ip_surgery_anesthesia </td>";
													}
													?>

													</tr>

													<tr>
													<td style='text-align: left; font-weight: bold;'>Diagnostic Tests</td>

													<?php foreach($result as $record) {
														echo "<td style='text-align: center;'>$record->ip_diagnostic_tests </td>";
													}
													?>

													</tr>

													<tr>
													<td style='text-align: left; font-weight: bold;'>Prescribed Medication</td>

													<?php foreach($result as $record) {
														echo "<td style='text-align: center;'>$record->ip_prescribed_medication </td>";
													}
													?>

													</tr>

													<tr>
													<td style='text-align: left; font-weight: bold;'>Cancer Treatment </td>

													<?php foreach($result as $record) {
														echo "<td style='text-align: center;'>$record->ip_cancer_treatment </td>";
													}
													?>

													</tr>

													<tr>
													<td style='text-align: left; font-weight: bold;'>Organ Transplant</td>

													<?php foreach($result as $record) {
														echo "<td style='text-align: center;'>$record->ip_organ_transplant </td>";
													}
													?>

													</tr>

													<tr>
													<td style='text-align: left; font-weight: bold;'>Prosthetic Device</td>

													<?php foreach($result as $record) {
														echo "<td style='text-align: center;'>$record->ip_prosthetic_device </td>";
													}
													?>

													</tr>

													<tr>
													<td style='text-align: left; font-weight: bold;'>Rehabilitation</td>

													<?php foreach($result as $record) {
														echo "<td style='text-align: center;'>$record->ip_rehabilitation </td>";
													}
													?>

													</tr>

													<tr>
													<td style='text-align: left; font-weight: bold;'>Home Nursing (immediately after or instead of hospitalization)</td>

													<?php foreach($result as $record) {
														echo "<td style='text-align: center;'>$record->ip_home_nursing </td>";
													}
													?>

													</tr>

													<tr>
													<td style='text-align: left; font-weight: bold;'>Parent / Companion Accomodation</td>

													<?php foreach($result as $record) {
														echo "<td style='text-align: center;'>$record->ip_companian_accomodation </td>";
													}
													?>

													</tr>

													<tr>
													<td style='text-align: left; font-weight: bold;'>In-Patient Cash Benefit</td>

													<?php foreach($result as $record) {
														echo "<td style='text-align: center;'>$record->ip_cashbenenit </td>";
													}
													?>

													</tr>													

											</tbody>
									</table>

									<table class='table table-hover table-striped table-bordered custom-height' id='outpatient_benefits'>
										<thead style='color: #fff; text-transform: uppercase;'>

											<tr>
												<th style='background: #fff !important; color: #09276f; text-align: right; padding-top: 1em; padding-bottom: 1em;' colspan="<?php echo $count; ?>" width="100%">

													<input class="hidden" type="text" id="customer_name" name="customer_name" value="<?php echo $data['detail']->customer_name; ?>" readonly>

													<input class="hidden" type="text" id="rm_name" name="rm_name" value="<?php echo $_SESSION['user_fullname']; ?>" readonly>

													<input class="hidden" type="text" id="email_address" name="email_address" value="<?php echo $_SESSION['email']; ?>" readonly>
												</th>
											</tr>

											<tr>
												<th style='background: #542989 !important; font-weight: bold; vertical-align: middle; text-align: left; padding: 5px 10px !important;'>Service Provider</th>

												<?php
													foreach($result as $record) {
														$manufacturer = findName('manufacturer', $record->manufacturer_id);
														echo "<th style='background: #542989; font-weight: bold; vertical-align: middle; text-align: center; width: 200px; padding: 5px 10px !important;'>$manufacturer</th>";
													}
												?>

											</tr>

											<tr style='color: #fff; text-transform: uppercase;'>
												<th style='background: #542989 !important; text-align: left; font-weight: bold; vertical-align: middle; width:200px; height:50px; padding: 5px 10px !important;'>Plan</th>

												<?php foreach($result as $record) {
													echo "<th style='background: #542989 !important; font-weight: bold; vertical-align: middle; text-align: center; padding: 5px 10px !important;'><p>$record->product_name</p>
													<div class='alert alert-success alert-dismissible success' id='success' style='display:none;'>
													  <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
													</div>
													</th>";
												}
												?>
											</tr>
										</thead>
											<tbody>												
													<tr>
													<td style='text-align: left; font-weight: bold; color: #fff; background:#00aedb; padding: 5px 10px !important;' colspan='<?php echo $count; ?>' width="100%">OUT PATIENT TREATMENT</td>
													</tr>

													<tr>
													<td style='text-align: left; font-weight: bold;'>Annual Limit</td>
													<?php foreach($result as $record) {
														echo "<td style='text-align: center;'>$record->op_annual_limit </td>";
													}
													?>
													</tr>

													<tr>
													<td style='text-align: left; font-weight: bold;'>Deductible / Excess / Co-insurance </td>
													<?php foreach($result as $record) {
														echo "<td style='text-align: center;'>$record->op_deductible </td>";
													}
													?>
													</tr>

													<tr>
													<td style='text-align: left; font-weight: bold;'>General Practioner Fees</td>
													<?php foreach($result as $record) {
														echo "<td style='text-align: center;'>$record->op_gp_fees </td>";
													}
													?>
													</tr>

													<tr>
													<td style='text-align: left; font-weight: bold;'>Specialist Fees</td>
													<?php foreach($result as $record) {
														echo "<td style='text-align: center;'>$record->op_specialist_fees </td>";
													}
													?>
													</tr>

													<tr>
													<td style='text-align: left; font-weight: bold;'>Diagnostic Tests</td>
													<?php foreach($result as $record) {
														echo "<td style='text-align: center;'>$record->op_diagnostic_tests </td>";
													}
													?>
													</tr>

													<tr>
													<td style='text-align: left; font-weight: bold;'>Prescribed Medication</td>
													<?php foreach($result as $record) {
														echo "<td style='text-align: center;'>$record->op_prescribed_medication </td>";
													}
													?>
													</tr>

													<tr>
													<td style='text-align: left; font-weight: bold;'>Physiotherapy</td>
													<?php foreach($result as $record) {
														echo "<td style='text-align: center;'>$record->op_physiotherapy </td>";
													}
													?>
													</tr>

													<tr>
													<td style='text-align: left; font-weight: bold;'>Alternative Treatment</td>
													<?php foreach($result as $record) {
														echo "<td style='text-align: center;'>$record->op_alternative_treatment </td>";
													}
													?>
													</tr>

													<tr>
													<td style='text-align: left; font-weight: bold;'>Cancer Treatment </td>
													<?php foreach($result as $record) {
														echo "<td style='text-align: center;'>$record->op_cancer_treatment </td>";
													}
													?>
													</tr>													

											</tbody>
									</table>

									<table class='table table-hover table-striped table-bordered custom-height' id='additional_benefits'>
										<thead style='color: #fff; text-transform: uppercase;'>

											<tr>
												<th style='background: #fff !important; color: #09276f; text-align: right; padding-top: 1em; padding-bottom: 1em;' colspan="<?php echo $count; ?>" width="100%">

													<input class="hidden" type="text" id="customer_name" name="customer_name" value="<?php echo $data['detail']->customer_name; ?>" readonly>

													<input class="hidden" type="text" id="rm_name" name="rm_name" value="<?php echo $_SESSION['user_fullname']; ?>" readonly>

													<input class="hidden" type="text" id="email_address" name="email_address" value="<?php echo $_SESSION['email']; ?>" readonly>
												</th>
											</tr>

											<tr>
												<th style='background: #542989 !important; font-weight: bold; vertical-align: middle; text-align: left; padding: 5px 10px !important;'>Service Provider</th>

												<?php
													foreach($result as $record) {
														$manufacturer = findName('manufacturer', $record->manufacturer_id);
														echo "<th style='background: #542989; font-weight: bold; vertical-align: middle; text-align: center; width: 200px; padding: 5px 10px !important;'>$manufacturer</th>";
													}
												?>

											</tr>

											<tr style='color: #fff; text-transform: uppercase;'>
												<th style='background: #542989 !important; text-align: left; font-weight: bold; vertical-align: middle; width:200px; height:50px; padding: 5px 10px !important;'>Plan</th>

												<?php foreach($result as $record) {
													echo "<th style='background: #542989 !important; font-weight: bold; vertical-align: middle; text-align: center; padding: 5px 10px !important;'><p>$record->product_name</p>
													<div class='alert alert-success alert-dismissible success' id='success' style='display:none;'>
													  <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
													</div>
													</th>";
												}
												?>
											</tr>
										</thead>
											<tbody>

													
													<tr>
													<td style='text-align: left; font-weight: bold; color: #fff; background:#00aedb; padding: 5px 10px !important;' colspan='<?php echo $count; ?>' width="100%">MATERNITY COVER</td>
													</tr>

													

													<tr>
													<td style='text-align: left; font-weight: bold;'>C-Section</td>
													<?php foreach($result as $record) {
														echo "<td style='text-align: center;'>$record->maternity_c_section </td>";
													}
													?>
													</tr>

													<tr>
													<td style='text-align: left; font-weight: bold;'>Complication</td>
													<?php foreach($result as $record) {
														echo "<td style='text-align: center;'>$record->maternity_complication </td>";
													}
													?>
													</tr>

													<tr>
													<td style='text-align: left; font-weight: bold;'>New Born Cover</td>
													<?php foreach($result as $record) {
														echo "<td style='text-align: center;'>$record->maternity_new_born </td>";
													}
													?>
													</tr>

													<tr>
													<td style='text-align: left; font-weight: bold; color: #fff; background:#00aedb; padding: 5px 10px !important;' colspan='<?php echo $count; ?>' width="100%">ADDITIONAL BENEFITS</td>
													</tr>

													<tr>
													<td style='text-align: left; font-weight: bold;'>Dental</td>
													<?php foreach($result as $record) {
														echo "<td style='text-align: center;'>$record->dental </td>";
													}
													?>
													</tr>

													<tr>
													<td style='text-align: left; font-weight: bold;'>Optical</td>
													<?php foreach($result as $record) {
														echo "<td style='text-align: center;'>$record->optical </td>";
													}
													?>
													</tr>

													<tr>
													<td style='text-align: left; font-weight: bold;'>Annual Health Check-up / Wellness Screening</td>
													<?php foreach($result as $record) {
														echo "<td style='text-align: center;'>$record->wellness </td>";
													}
													?>
													</tr>

													

													<tr>
													<td style='text-align: left; font-weight: bold;'>Vaccinations (other including Travel Vaccinations)</td>
													<?php foreach($result as $record) {
														echo "<td style='text-align: center;'>$record->vaccinations_others_travel </td>";
													}
													?>
													</tr>

													<tr>
													<td style='text-align: left; font-weight: bold; color: #fff; background:#00aedb; padding: 5px 10px !important;' colspan='<?php echo $count; ?>' width="100%">EVACUATION  & REPATRIATION</td>
													</tr>

													<tr>
													<td style='text-align: left; font-weight: bold;'>Evacuation</td>
													<?php foreach($result as $record) {
														echo "<td style='text-align: center;'>$record->evacuation </td>";
													}
													?>
													</tr>

													<tr>
													<td style='text-align: left; font-weight: bold;'>Repatriation</td>
													<?php foreach($result as $record) {
														echo "<td style='text-align: center;'>$record->repatriation </td>";
													}
													?>
													</tr>																	

											</tbody>
									</table>

									<table class='table table-hover table-striped table-bordered custom-height' id='premium_summary'>
										<thead style='color: #fff; text-transform: uppercase;'>

											<tr>
												<th style='background: #fff !important; color: #09276f; text-align: right; padding-top: 1em; padding-bottom: 1em;' colspan="<?php echo $count; ?>" width="100%">

													<input class="hidden" type="text" id="customer_name" name="customer_name" value="<?php echo $data['detail']->customer_name; ?>" readonly>

													<input class="hidden" type="text" id="rm_name" name="rm_name" value="<?php echo $_SESSION['user_fullname']; ?>" readonly>

													<input class="hidden" type="text" id="email_address" name="email_address" value="<?php echo $_SESSION['email']; ?>" readonly>
												</th>
											</tr>

											<tr>
												<th style='background: #542989 !important; font-weight: bold; vertical-align: middle; text-align: left; padding: 5px 10px !important;'>Service Provider</th>

												<?php
													foreach($result as $record) {
														$manufacturer = findName('manufacturer', $record->manufacturer_id);
														echo "<th style='background: #542989; font-weight: bold; vertical-align: middle; text-align: center; width: 200px; padding: 5px 10px !important;'>$manufacturer</th>";
													}
												?>

											</tr>

											<tr style='color: #fff; text-transform: uppercase;'>
												<th style='background: #542989 !important; text-align: left; font-weight: bold; vertical-align: middle; width:200px; height:50px; padding: 5px 10px !important;'>Plan</th>

												<?php foreach($result as $record) {
													echo "<th style='background: #542989 !important; font-weight: bold; vertical-align: middle; text-align: center; padding: 5px 10px !important;'><p>$record->product_name</p>
													<div class='alert alert-success alert-dismissible success' id='success' style='display:none;'>
													  <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
													</div>
													</th>";
												}
												?>
											</tr>
										</thead>
											<tbody>

												<tr>
													<td style='text-align: left; font-weight: bold; color: #fff; background:#00aedb; padding: 5px 10px !important;' colspan='<?php echo $count; ?>' width="100%">PREMIUM SUMMARY</td>
												</tr>

													<tr>
													<td style='text-align: left; font-weight: bold;'>Total No of Members (Emp + Dependents)</td>
													<?php foreach($result as $record) {
														echo "<td style='text-align: center;'>$record->total_members </td>";
													}
													?>
													</tr>

													<tr>
													<td style='text-align: left; font-weight: bold;'>Premium Payment Mode</td>
													<?php foreach($result as $record) {							
														echo "<td style='text-align: center;'>$record->payment_mode</td>";
													}
													?>
													</tr>

													<tr>
													<td style='text-align: left; font-weight: bold;'>Expiring Premium</td>
													<?php foreach($result as $record) {
														echo "<td style='text-align: center;'>$record->expiring_premium </td>";
													}
													?>
													</tr>

													<tr class='' style="background: #d2ebf4 !important;">

													<td style='text-align: left; font-weight: bold;'>Proposed Annual / Renewal Premium (excluding VAT) <span style="color: purple;">AED</span></td>
													<?php foreach($result as $record) {
														echo "<td style='text-align: center;'>$record->proposed_annual_renewal_premium</td>";
													}
													?>
												</tr>

												<tr class=''>
													<td style='text-align: left; font-weight: bold;'>Proposed Annual / Renewal Premium (excluding VAT) <span style="color: red;">USD</span> </td>
													<?php foreach($result as $record) {
														
														echo "<td style='text-align: center;'>$record->usd_proposed_annual_renewal_premium</td>";
													}
													?>
												</tr>

												<tr style="background: #d2ebf4 !important;">
													<td style='text-align: left; font-weight: bold;'>% Increase</td>
													<?php foreach($result as $record) {							
														echo "<td style='text-align: center;'>$record->increase</td>";
													}
													?>
												</tr>
												
												<tr style="background: #d2ebf4 !important;">
													<td style='text-align: left; font-weight: bold;'>Withhod Tax 5%</td>
													<?php foreach($result as $record) {							
														echo "<td style='text-align: center;'>$record->withhold_tax</td>";
													}
													?>
												</tr>
												
												<tr style="background: #d2ebf4 !important;">
													<td style='text-align: left; font-weight: bold;'>IPT Tax</td>
													<?php foreach($result as $record) {							
														echo "<td style='text-align: center;'>$record->ipt_tax</td>";
													}
													?>
												</tr>


												<tr>
													<td style='text-align: left;' colspan="<?php echo $count; ?>" width="100%">
														<br> <span style="font-weight: bold;">Notes: </span><br><br>

														(1) This quote is valid for a period of 30 days.<br>					
														(2) Withholding tax is to be paid by the member directly to KRA. The member then needs to share the withholding tax certificate with us.<br>
														(3) This quotation is on Corporate Terms.<br>
														(4) The above rates are subject to correct date of birth and start date of cover.<br>
														(5) This is only a quotation and is subject to completion of form, underwriting, KYC Clearance and acceptance by Insurance Provider.<br>
														(6) Certain benefits are available only on a reimbursement basis.<br>
														(7) Valid until <?php echo $data['detail']->valid_until; ?>.<br><br>

														This is only a  summary of  benefits excluding errors and omissions. For further details, please refer to the table of benefits of each provider available upon request.

													</td>
													
												</tr>

											</tbody>
									</table>
								</div>

							</fieldset>



							<div class="invoice-footer">
								<br>

								<fieldset class="hidden">
									<legend>Attached Docuemnts</legend>
									<div class="col-xs-8 col-sm-6 col-lg-8">
										
										<table id='attachments' class='table-hover table-responsive'>
										<?php
										$file_directory= ATTACHMENT_DIR . DS;
											foreach ($data['files'] as $file){
												echo "<tr>
													<td>
													<a href='/{$file_directory}{$file->file_name}' target='_blank'>$file->file_name</a>
													</td>
												</tr>";
											}
										?>
										</table>
										
									</div>

									<div>
										<input type="hidden" name="filerows" id="filerows" value="">
									</div>

								</fieldset>

								<div class="row">

									<div class="col-sm-7">
										
									</div>
									<!-- <div class="col-sm-4">
										<div class="invoice-sum-total pull-right">
											<h3><strong>Total: <span class="text-success"><?php ?> AED</span></strong></h3>
										</div>
									</div> -->

								</div>
								
								<div class="row">
									<div class="col-sm-12">
										<p class="note"></p>
									</div>
								</div>

							</div>
						</div>

					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->

			</div>
			<!-- end widget -->

		</article>
		<!-- WIDGET END -->

	</div>

	<!-- end row -->

</section>
<!-- end widget grid -->



<!-- row -->
<div class="row">

	<!-- <h2 class="row-seperator-header"><i class="fa fa-plus"></i> Customized Tabs </h2> -->





</div>
<!-- end row -->


<!-- PAGE FOOTER -->
<?php include (APPROOT . "/views/inc/admin_footer.php"); ?>
<!-- END PAGE FOOTER -->
