<?php include (APPROOT . "/views/inc/admin_header.php"); ?>

	<!-- START CONTENT  -->
	<section id="widget-grid" class="">

		<!-- row -->
		<div class="row">

			<!-- NEW WIDGET START -->
			<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
					<!-- widget options:
					usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

					data-widget-colorbutton="false"
					data-widget-editbutton="false"
					data-widget-togglebutton="false"
					data-widget-deletebutton="false"
					data-widget-fullscreenbutton="false"
					data-widget-custombutton="false"
					data-widget-collapsed="true"
					data-widget-sortable="false"

					-->

					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2>Suppliers List </h2>

					</header>

					<!-- widget div-->
					<div>

						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->

						</div>
						<!-- end widget edit box -->

						<!-- widget content -->
						<div class="widget-body no-padding">
						
							<table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
		
						        <thead>
									
						            <tr>
						            	<th data-hide="phone,tablet"><input type="checkbox" name="select_all"></th>
					                    <th data-hide="phone,tablet">ID</th>
					                    <th data-class="expand">Name</th>
					                    <th data-hide="phone,tablet">Phone</th>
					                    <th data-hide="phone,tablet">Mobile</th>
					                    <th data-hide="phone,tablet">Email</th>
					                    <th data-hide="phone,tablet">Address</th>
					                    <th data-hide="phone,tablet">Created By</th>
					                    <th data-hide="phone,tablet">Image</th>
					                    <th data-hide="phone,tablet">Actions</th>
					                  

						            </tr>
						        </thead>

								<tbody>
								
				

						<tr>
							<td><span><input type='checkbox' name='current_row_id'></span></td>

							<td>1</td>
							<td><a href="suppliers.php?action=detail&id=1">name</a></td>
							<td>phone</td>
							<td>mobile</td>
							<td>supplier</td>
							<td>street</td>
							<td>Admin</td>
							<td><img class="profile_image" src="img/avatar-img.png" alt=""></td>
							<td>
								<a href="suppliers.php?action=edit&id=1; ?>"><i class="fa fa-lg fa-fw fa-pencil-square-o"></i></a><a href="suppliers.php?action=delete&id=1"><i class="fa fa-lg fa-fw fa-trash-o"></i>
								</a>
							</td>
						</tr>

								
									
								</tbody>
													
							</table>

						</div>
						<!-- end widget content -->

					</div>
					<!-- end widget div -->

				</div>
				<!-- end widget -->

			</article>
			<!-- WIDGET END -->
			
		</div>

		<!-- end row -->

		<!-- row -->

		<div class="row">

			<!-- a blank row to get started -->
			<div class="col-sm-12">
				<!-- your contents here -->
			</div>
				
		</div>

		<!-- end row -->

	</section>
	<!-- END CONTENT -->	


<!-- PAGE FOOTER -->
<?php include (APPROOT . "/views/inc/admin_footer.php"); ?>
<!-- END PAGE FOOTER -->

		
