<?php include (APPROOT . "/views/inc/admin_header.php"); ?>
<div class="module">
	<div class="col-xs-9 col-sm-4 col-md-4 col-lg-4">
		<h4><?php echo $data['detail']->name; ?>  - ID <?php echo $data['detail']->id; ?></h4>
	</div>

	<?php include (APPROOT . "/views/inc/admin_detail_toolbar.php"); ?>
</div>

	<!-- widget grid -->
	<section id="widget-grid-1" class="">



		<!-- row -->
		<div class="row">
			
			<!-- NEW WIDGET START -->
			<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

					<!-- widget options:
						usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
						
						data-widget-colorbutton="false"	
						data-widget-editbutton="false"
						data-widget-togglebutton="false"
						data-widget-deletebutton="false"
						data-widget-fullscreenbutton="false"
						data-widget-custombutton="false"
						data-widget-collapsed="true" 
						data-widget-sortable="false"
						
					-->
					<header>
						<span class="widget-icon"> <i class="fa"></i> </span>
						<h2 class="custom-h2"><?php echo $this->module;?> Details </h2>				
						
					</header>

					<!-- widget div-->
					<div>
						
						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
							<input class="form-control" type="text">	
						</div>
						<!-- end widget edit box -->
						
						<!-- widget content -->
						<div class="widget-body no-padding detail508">

							<table cellpadding="5" cellspacing="0" border="0" class="table table-condensed">
						        <tr>
						            <td scope="col" style="width:12.5%">Name</td>
						            <td style="width: 35.5%"><?php echo $data['detail']->name; ?></td>
						             <td scope="col" style="width:12.5%">Insurance Provider Type</td>
						             <td style="width: 35.5%"><?php echo dropdownValue($data['detail']->provider_type, 'insurance_provider_type' ); ?></td>
						        </tr>

						        <tr>
						            <td scope="col" style="width:12.5%">Top</td>
						            <td style="width: 35.5%"><?php echo show_yes_no($data['detail']->top); ?></td>
						            <td scope="col" style="width:12.5%">Phone</td>
						            <td style="width: 35.5%"><?php echo $data['detail']->phone; ?></td>
						        </tr>

						        <tr>
						            <td scope="col" style="width:12.5%">Mobile</td>
						            <td style="width: 35.5%"><?php echo $data['detail']->mobile; ?></td>
						            <td scope="col" style="width:12.5%">Email</td>
						            <td style="width: 35.5%"><?php echo $data['detail']->email; ?></td>
						        </tr>
						         
						        <tr>
						            <td scope="col" style="width:12.5%">Address</td>
						            <td style="width: 35.5%"><?php echo $data['detail']->street . ' ' . $data['detail']->city . ' ' . $data['detail']->state . ' ' . $data['detail']->country; ?></td>
						            <td scope="col" style="width:12.5%">Postal Code</td>
						            <td style="width: 35.5%"><?php echo $data['detail']->postal_code; ?></td>
						        </tr>

						         <tr>
						            <td scope="col" style="width:12.5%">Assigned To</td>
						            <td style="width: 35.5%"><?php echo findName('users', $data['detail']->assigned_to); ?></td>
						            <td scope="col" style="width:12.5%">Date Entered</td>
						            <td style="width: 35.5%">
						            	<?php echo UserTimeZone($data['detail']->date_entered); ?>
					            	</td>
						        </tr>

						        <tr>
						            <td scope="col" style="width:12.5%">Created By</td>
						            <td style="width: 35.5%">
						            	<?php echo findName('users', $data['detail']->created_by); ?>	            		
					            	</td>
						            <td scope="col" style="width:12.5%">Date Modified</td>
						            <td style="width: 35.5%">
						            	<?php echo UserTimeZone($data['detail']->date_modified); ?>
					            	</td>
						        </tr>

						        <tr>
						            <td scope="col" style="width:12.5%">Modified By</td>
						            <td style="width: 35.5%">
						            	<?php echo findName('users', $data['detail']->modified_by); ?>
					            	</td>
						            <td scope="col" style="width:12.5%">Currency</td>
						            <td style="width: 35.5%"><?php echo dropdownValue($data['detail']->currency, 'currency' ); ?></td>

						            <td scope="col" style="width:12.5%">TRN#</td>
						            <td style="width: 35.5%"><?php echo $data['detail']->trn; ?></td>
						        </tr>

						        <tr>
							        	<td scope="col" style="width:12.5%">Comment</td>
						            <td style="width: 35.5%"><?php $data['detail']->comment; ?></td>
						            <td scope="col" style="width:12.5%">Image</td>
						            <td style="width: 35.5%">
						            	<img class="img-responsive edit_profile_image" src="<?php echo Upload::showImage($data['detail']->image); ?>">
					            	</td>
						            
						        </tr>
						       
					    </table>

							
						

						</div>
						<!-- end widget content -->
						
					</div>
					<!-- end widget div -->
					
				</div>
				<!-- end widget -->

			</article>
			<!-- WIDGET END -->
			
		</div>

		<!-- end row -->

		<!-- row -->

		<div class="row">

			<!-- a blank row to get started -->
			<div class="col-sm-12">
				<!-- your contents here -->
			</div>
				
		</div>

		<!-- end row -->

	</section>
	<!-- end widget grid -->



	<!-- row -->
	<div class="row">

		<!-- <h2 class="row-seperator-header"><i class="fa fa-plus"></i> Customized Tabs </h2> -->

		<!-- NEW WIDGET START -->
		<article class="col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget" id="wid-id-3" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-custombutton="false" data-widget-sortable="false">
				<!-- widget options:
				usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

				data-widget-colorbutton="false"
				data-widget-editbutton="false"
				data-widget-togglebutton="false"
				data-widget-deletebutton="false"
				data-widget-fullscreenbutton="false"
				data-widget-custombutton="false"
				data-widget-collapsed="true"
				data-widget-sortable="false"

				-->
				<!-- <header>
					<span class="widget-icon"> <i class="fa fa-comments"></i> </span>
					<h2>Default Tabs with border </h2>

				</header> -->

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->

					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body">
						<!-- 				
						<p>
							Tabs inside
							<code>
								.jarviswidget .well
							</code>
							(Bordered Tabs)
						</p> -->
						<!-- <hr class="simple"> -->
						<ul id="myTab1" class="nav nav-tabs bordered tabs-noborder">
							<li class="active">
								<a href="#s1" data-toggle="tab"> Purchases </a>
							</li>
							<li>
								<a href="#s2" data-toggle="tab"> More Details </a>
							</li>

							<!-- <li>
								<a href="#s3" data-toggle="tab"> History </a>
							</li> -->

						
							<li class="pull-right">
								<a href="javascript:void(0);">
								<div class="sparkline txt-color-pinkDark text-align-right" data-sparkline-height="18px" data-sparkline-width="90px" data-sparkline-barwidth="7">
									5,10,6,7,4,3
								</div> </a>
							</li>
						</ul>

						<div id="myTabContent1" class="tab-content padding-10">

							<div class="tab-pane fade in active" id="s1">

								<?php echo "include('supplier_purchases.php')"; ?>

							</div>

							<div class="tab-pane fade" id="s2">
								

							</div>

							<div class="tab-pane fade" id="s3">

								<!-- widget grid -->
								<section id="widget-grid-1" class="">



									<!-- row -->
									<div class="row">
										
										<!-- NEW WIDGET START -->
										<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

											<!-- Widget ID (each widget will need unique ID)-->
											<div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

												<!-- widget options:
													usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
													
													data-widget-colorbutton="false"	
													data-widget-editbutton="false"
													data-widget-togglebutton="false"
													data-widget-deletebutton="false"
													data-widget-fullscreenbutton="false"
													data-widget-custombutton="false"
													data-widget-collapsed="true" 
													data-widget-sortable="false"
													
												-->
												<header>
													<span class="widget-icon"> <i class="fa"></i> </span>
													<h2 class="custom-h2">Address Details </h2>				
													
												</header>

												<!-- widget div-->
												<div>
													
													<!-- widget edit box -->
													<div class="jarviswidget-editbox">
														<!-- This area used as dropdown edit box -->
														<input class="form-control" type="text">	
													</div>
													<!-- end widget edit box -->
													
													<!-- widget content -->
													<div class="widget-body no-padding detail508">

														<table cellpadding="5" cellspacing="0" border="0" class="table table-condensed">
													       <!--  <tr>
													            <td scope="col" style="width:12.5%">Area</td>
													            <td style="width: 35.5%">value</td>
													            <td scope="col" style="width:12.5%">Address</td>
													            <td style="width: 35.5%">value</td>

													        </tr>
													         
													        <tr>
													            <td scope="col" style="width:12.5%">City</td>
													            <td style="width: 35.5%">value</td>
													            <td scope="col" style="width:12.5%">State</td>
													            <td style="width: 35.5%">value</td>

													        </tr>

													        <tr>
													            <td scope="col" style="width:12.5%">PO Box</td>
													            <td style="width: 35.5%">value</td>
													            <td scope="col" style="width:12.5%">Country</td>
													            <td style="width: 35.5%">value</td>

													        </tr> -->

													       
												    </table>
							
														
													

													</div>
													<!-- end widget content -->
													
												</div>
												<!-- end widget div -->
												
											</div>
											<!-- end widget -->

										</article>
										<!-- WIDGET END -->
										
									</div>

									<!-- end row -->

									<!-- row -->

									<div class="row">

										<!-- a blank row to get started -->
										<div class="col-sm-12">
											<!-- your contents here -->
										</div>
											
									</div>

									<!-- end row -->

								</section>
								<!-- end widget grid -->
								
								<!-- widget grid -->
								<section id="widget-grid-1" class="">



									<!-- row -->
									<div class="row">
										
										<!-- NEW WIDGET START -->
										<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

											<!-- Widget ID (each widget will need unique ID)-->
											<div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

												<!-- widget options:
													usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
													
													data-widget-colorbutton="false"	
													data-widget-editbutton="false"
													data-widget-togglebutton="false"
													data-widget-deletebutton="false"
													data-widget-fullscreenbutton="false"
													data-widget-custombutton="false"
													data-widget-collapsed="true" 
													data-widget-sortable="false"
													
												-->
												<header>
													<span class="widget-icon"> <i class="fa"></i> </span>
													<h2 class="custom-h2">More Details </h2>				
													
												</header>

												<!-- widget div-->
												<div>
													
													<!-- widget edit box -->
													<div class="jarviswidget-editbox">
														<!-- This area used as dropdown edit box -->
														<input class="form-control" type="text">	
													</div>
													<!-- end widget edit box -->
													
													<!-- widget content -->
													<div class="widget-body no-padding detail508">

														<table cellpadding="5" cellspacing="0" border="0" class="table table-condensed">
													        <!-- <tr>
													            <td scope="col" style="width:12.5%">Description</td>
													            <td style="width: 35.5%">value</td>
													            <td scope="col" style="width:12.5%">Assigned To</td>
													            <td style="width: 35.5%">value</td>

													        </tr>
													         
													        <tr>
													            <td scope="col" style="width:12.5%">Date Entered</td>
													            <td style="width: 35.5%">value</td>
													            <td scope="col" style="width:12.5%">Created By</td>
													            <td style="width: 35.5%">value</td>

													        </tr>

													        <tr>
													            <td scope="col" style="width:12.5%">Modified By</td>
													            <td style="width: 35.5%">value</td>
													            <td scope="col" style="width:12.5%">Date Modified</td>
													            <td style="width: 35.5%">value</td>

													        </tr>

													        <tr>
													            <td scope="col" style="width:12.5%">Image</td>
													            <td style="width: 35.5%">value</td>
													            <td scope="col" style="width:12.5%"></td>
													            <td style="width: 35.5%">value</td>

													        </tr> -->

													       
												    </table>
							
														
													

													</div>
													<!-- end widget content -->
													
												</div>
												<!-- end widget div -->
												
											</div>
											<!-- end widget -->

										</article>
										<!-- WIDGET END -->
										
									</div>

									<!-- end row -->

									<!-- row -->

									<div class="row">

										<!-- a blank row to get started -->
										<div class="col-sm-12">
											<!-- your contents here -->
										</div>
											
									</div>

									<!-- end row -->

								</section>
								<!-- end widget grid -->

							</div>
							
						</div>

					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->

			</div>
			<!-- end widget -->


		</article>
		<!-- WIDGET END -->



	</div>
	<!-- end row -->


<!-- PAGE FOOTER -->
<?php include (APPROOT . "/views/inc/admin_footer.php"); ?>
<!-- END PAGE FOOTER -->
