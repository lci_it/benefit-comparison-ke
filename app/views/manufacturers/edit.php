<?php include (APPROOT . "/views/inc/admin_header.php"); ?>

<div id="content">

	<div class="row">
		
		
	</div>
	
	<!-- widget grid -->
	<section id="widget-grid" class="col-">
	
		<!-- <div class="well">
			
		</div> -->
	
		<!-- row -->
		<div class="row">
	
			<!-- NEW WIDGET START -->
			<article class="col-md-12">
	
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false">
					<!-- widget options:
					usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
	
					data-widget-colorbutton="false"
					data-widget-editbutton="false"
					data-widget-togglebutton="false"
					data-widget-deletebutton="false"
					data-widget-fullscreenbutton="false"
					data-widget-custombutton="false"
					data-widget-collapsed="true"
					data-widget-sortable="false"
	
					-->
					<header>
						<span class="widget-icon"> <i class="fa fa-eye"></i> </span>
						<h2>Edit <?php echo $this->module;?></h2>
	
					</header>
	
					<!-- widget div-->
					<div>
	
						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
	
						</div>
						<!-- end widget edit box -->
	
						<!-- widget content -->
						<div class="widget-body">
	
						<form class="form-horizontal" method="post" action='<?php echo htmlspecialchars( URLROOT . current_class($this) . "/edit/" . $data['id']); ?>' enctype="multipart/form-data">

								<fieldset>
									<!-- <legend>Customer Details <?php; ?></legend> -->
									<div class="form-group">
										<div class="col-xs-9 col-sm-6 col-lg-3">
											<label class="control-label">Name</label>
											<div class="">
												<input class="form-control input-xs" type="text" name="name" value="<?php echo $data['name']; ?>" required>
											</div>
										</div>

										<div class="col-xs-9 col-sm-6 col-lg-3">
											<label class="control-label">Insurance Provider Type</label>
											<div class="">
												<select class="form-control input-xs" name="provider_type" id="provider_type" required>
													<option selected value="<?php echo $data['provider_type']; ?>"><?php echo dropdownValue($data['provider_type'], 'insurance_provider_type' ); ?></option>
													<?php echo dropDownList("insurance_provider_type"); ?>
												</select>
											</div>
										</div>

										<div class="col-xs-12 col-sm-6 col-lg-2">
											<label class="control-label">Top</label>
											<div class="">
											<select class="form-control input-xs" name="top">
												<option value="<?php echo $data['top']; ?>">
													<?php show_yes_no($data['top']); ?>						
												</option>
												<option value="1">Yes</option>
												<option value="0">No</option>											
											</select>												
											</div>											
										</div><!-- END OF select active -->

										<div class="col-xs-9 col-sm-6 col-lg-3">
											<label class="control-label">Phone</label>
											<div class="">
												<input class="form-control input-xs" type="text" name="phone" value="<?php echo $data['phone']; ?>">
											</div>
										</div>
										
										<div class="col-xs-9 col-sm-6 col-lg-3">
											<label class="control-label">Mobile</label>
											<div class="">
												<input class="form-control  input-xs" type="text" name="mobile" value="<?php echo $data['mobile']; ?>">
											</div>
										</div>

										<div class="col-xs-9 col-sm-6 col-lg-3">
											<label class="control-label">Email</label>
											<div class="">
												<input class="form-control input-xs" type="email" name="email" value="<?php echo $data['email']; ?>">
											</div>
										</div>
									</div>
								</fieldset>

								<fieldset>
									<div class="form-group">
										<div class="col-xs-9 col-sm-6 col-lg-3">
											<label class="control-label">Street</label>
											<div class="">
													<input class="form-control input-xs" type="text" name="street" value="<?php echo $data['street']; ?>">
											</div>
										</div>

										<div class="col-xs-9 col-sm-6 col-lg-3">
											<label class="control-label">City</label>
											<div class="">
												<input class="form-control input-xs" type="text" name="city" value="<?php echo $data['city']; ?>">
											</div>
										</div>

										<div class="col-xs-9 col-sm-6 col-lg-3">
											<label class="control-label">State</label>
											<div class="">
												<input class="form-control input-xs" type="text" name="state" value="<?php echo $data['state']; ?>">
											</div>
										</div>

										<div class="col-xs-9 col-sm-6 col-lg-3">
											<label class="control-label">Postal Code</label>
											<div class="">
												<input class="form-control input-xs" type="text" name="postal_code" value="<?php echo $data['postal_code']; ?>">
											</div>
										</div>

										<div class="col-xs-9 col-sm-6 col-lg-3">
											<label class="control-label">Country</label>
											<div class="">
												<input class="form-control input-xs" type="text" name="country" value="<?php echo $data['country']; ?>">
											</div>
										</div>

										<div class="col-xs-9 col-sm-6 col-lg-3">
											<label class="control-label">Currency</label>
											<div class="">
												<select class="form-control input-xs" name="currency" id="currency">
													<option value="<?php echo $data['currency']; ?>"><?php echo dropdownValue($data['currency'], 'currency'); ?></option>
													<?php echo dropDownList("currency"); ?>
												</select>
											</div>
										</div>
										<div class="col-xs-9 col-sm-6 col-lg-3">
											<label class="control-label">TRN#</label>
											<div class="">
												<input class="form-control input-xs" type="text" name="trn" value="<?php echo $data['trn']; ?>">
											</div>
										</div>
										<div class="col-xs-9 col-sm-6 col-lg-3">
											<label class="control-label">Comment</label>
											<div class="">
												<input class="form-control input-xs" type="text" name="comment" value="<?php echo $data['comment']; ?>">
											</div>
										</div>
									</div>
								</fieldset>

								<fieldset>
									<legend>Other</legend>
									<div class="form-group">
										<div class="col-xs-9 col-sm-6 col-lg-4">
											<label class="control-label">Assigned to</label>
											<div class="">											
												<select class="form-control input-xs" id="assigned_to" name="assigned_to">
													<option selected value="<?php echo $_SESSION['user_id']; ?>"><?php echo findName('users', $_SESSION['user_id']); ?>
													</option>
													<?php echo getListFromTable('users'); ?>
												</select>										
												<p class="note"><strong>Note:</strong> Default is logged in user.</p>
											</div>
										</div>

										<div class="col-xs-9 col-sm-6 col-lg-3 col-lg-offset-1">
											<label class="control-label">Upload Image</label>
											<div class="">
												<input type="file" name="image">
											</div>
										</div>
									</div>
								</fieldset>
								

								<div class="form-actions">
									<div class="row">
										<div class="col-md-12">
										 <a href="<?php echo htmlspecialchars(URLROOT . current_class($this) ); ?>" class="btn btn-default" role="button">Cancel</a>
											
								<button class="btn btn-primary" type="submit" name="update">
												<i class="fa fa-save"></i>
												Update
											</button>
										</div>
									</div>
								</div>
	
							</form>
	
						</div>
						<!-- end widget content -->
	
					</div>
					<!-- end widget div -->
	
				</div>
				<!-- end widget -->
	
				<!-- Widget ID (each widget will need unique ID)-->

				<!-- end widget -->
	

	
			</article>

	
		</div>
	
		<!-- end row -->
	
		<!-- row -->
		<div class="row">
	
			<!-- NEW WIDGET START -->
			
			<!-- WIDGET END -->
	
		</div>
		<!-- end row -->
	
		<!-- row -->
		<div class="row">
	
			
					
		</div>
		<!-- end row -->		
	
	
	</section>
	<!-- end widget grid -->

</div>

<!-- PAGE FOOTER -->
<?php include (APPROOT . "/views/inc/admin_footer.php"); ?>
<!-- END PAGE FOOTER -->