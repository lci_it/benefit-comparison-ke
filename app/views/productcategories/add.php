<?php include (APPROOT . "/views/inc/admin_header.php"); ?>
	
	<!-- widget grid -->
	<section id="widget-grid" class="col-">
	
		<!-- <div class="well">
			
		</div> -->
	
		<!-- row -->
		<div class="row">
	
			<!-- NEW WIDGET START -->
			<article class="col-md-12">
	
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false">
					<!-- widget options:
					usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
	
					data-widget-colorbutton="false"
					data-widget-editbutton="false"
					data-widget-togglebutton="false"
					data-widget-deletebutton="false"
					data-widget-fullscreenbutton="false"
					data-widget-custombutton="false"
					data-widget-collapsed="true"
					data-widget-sortable="false"
	
					-->
					<header>
						<span class="widget-icon"> <i class="fa fa-eye"></i> </span>
						<h2>Add Product Category</h2>
	
					</header>
	
					<!-- widget div-->
					<div>
	
						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
	
						</div>
						<!-- end widget edit box -->
	
						<!-- widget content -->
						<div class="widget-body">
	

						<form class="form-horizontal" method="post" action="<?php echo htmlspecialchars( URLROOT . current_class($this) . '/add'); ?>" enctype="multipart/form-data">

								<fieldset>
									<!-- <legend>User Details </legend> -->
									

								<div class="form-group"> <!-- START OF FORM GROUP USER DETAILS-->
									
									<div class="col-xs-12 col-sm-6 col-lg-2">
										<label class="control-label">Name</label>
										<div class="">
											<input class="form-control input-xs" type="text" name="name" required>
										</div>
									</div>

									<!-- <div class="col-xs-12 col-sm-6 col-lg-2">
										<label class="checkbox">Is Parent</label>
										<div class="">
											<input type="checkbox" name="checkbox-inline">
										</div>										
									</div> -->
									
									<div class="col-xs-9 col-sm-6 col-lg-2">
										<label class="control-label">Parent</label>
										<div class="">
											<select id="category_id-" name="parent_category_id" class="form-control itemsel input-xs wei-add-field category" required>
												<option value="">Select Option</option>
												<?php echo getCategoryDropdown();?>
										</select>
										</div>
									</div>
									
									<div class="col-xs-9 col-sm-6 col-lg-3">
										<label class="control-label">Description</label>
										<div class="">
											<textarea class="form-control input-xs" name="description"></textarea>
										</div>
									</div>								

									<div class="col-xs-12 col-sm-6 col-lg-2">
										<label class="control-label">Active</label>
										<div class="">
											<select class="form-control input-xs" name="active">
												<option selected value="1">Yes</option>
												<option value="0">No</option>
											</select>
										</div>
									</div>




								</div> <!-- END OF FORM GROUP USER DETAILS-->

								</fieldset>


								<fieldset>
									<legend>Other</legend>
								<div class="form-group">
									<div class="col-xs-8 col-sm-6 col-lg-3">
										<label class="control-label">Upload Image</label>
										<div class="">
											<input name="MAX_FILE_SIZE" value="2097152" type="hidden"/>
											<input type="file" name="image">
										</div>
									</div>
								</div>		
								</fieldset>

								<div class="form-actions">
									<div class="row">
										<div class="col-md-12">
										 <a href="/users" class="btn btn-default" role="button">Cancel</a>
											
											<button class="btn btn-primary" type="submit" name="submit">
												<i class="fa fa-save"></i>
												Submit
											</button>
										</div>
									</div>
								</div>
	
							</form>
	
						</div>
						<!-- end widget content -->
	
					</div>
					<!-- end widget div -->
	
				</div>
				<!-- end widget -->
	
				<!-- Widget ID (each widget will need unique ID)-->

				<!-- end widget -->
	

	
			</article>

	
		</div>
	
		<!-- end row -->
	
		<!-- row -->
		<div class="row">
	
			<!-- NEW WIDGET START -->
			
			<!-- WIDGET END -->
	
		</div>
		<!-- end row -->
	
		<!-- row -->
		<div class="row">
	
			
					
		</div>
		<!-- end row -->		
	
	
	</section>
	<!-- end widget grid -->

<!-- PAGE FOOTER -->
<?php include (APPROOT . "/views/inc/admin_footer.php"); ?>
<!-- END PAGE FOOTER -->