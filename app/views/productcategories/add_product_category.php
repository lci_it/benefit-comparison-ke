<?php $message = ""; ?>

<?php

	if( isset($_POST['add_coa']) ) {

		try {

		$database->connection->begin_transaction();

		$add_coa = new COA();
		if($add_coa && !empty($_POST['name']) ) {

		$timestamp = date('Y-m-d G:i:s');

		$add_coa->name = $_POST['name'];
		$add_coa->parent_category_id = $_POST['category_id'];

		$type_category = $add_coa->coa_account_type_category($add_coa->parent_category_id);

		$add_coa->account_type = $type_category[0]->account_type;
		$add_coa->bs_account_category = $type_category[0]->bs_account_category;
		// $add_coa->parent_category_id = $type_category[0]->parent_category_id;

		$add_coa->business_unit = $_POST['business_unit'];
		$add_coa->active = $_POST['active'];

		$add_coa->notes = $_POST['description'];

		
		$add_coa->assigned_to = $_POST['assigned_to'];
		
		$add_coa->date_entered = $timestamp;
		$add_coa->created_by = $session->user_id;

		$add_coa->create();

		// redirect("products.php");

			} else {
				 throw new Exception(' Fill required product details');
			} // end of if statement check $product class

///////////////////////////////////////////////////////////////////////////////////////


		$database->connection->commit();
		$message = "<span class='label bg-color-green pull-middle'>Record created successfully</span>";	

		} catch (Exception $e) {
 
		  $message = "<span class='label bg-color-red pull-middle'>Transaction failed: " . $e->getMessage();
		  $database->connection->rollback();
		}

		
	} // end of if add_product


?>


<div id="content">

	<div class="row">
		
		
	</div>
	
	<!-- widget grid -->
	<section id="widget-grid" class="col-">
	
		<!-- <div class="well">
			
		</div> -->
	
		<!-- row -->
		<div class="row">
	
			<!-- NEW WIDGET START -->
			<article class="col-md-12">
	
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false">
					<!-- widget options:
					usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
	
					data-widget-colorbutton="false"
					data-widget-editbutton="false"
					data-widget-togglebutton="false"
					data-widget-deletebutton="false"
					data-widget-fullscreenbutton="false"
					data-widget-custombutton="false"
					data-widget-collapsed="true"
					data-widget-sortable="false"
	
					-->
					<header>
						<span class="widget-icon"> <i class="fa fa-eye"></i> </span>
						<h2>Add Product Category</h2>
	
					</header>
	
					<!-- widget div-->
					<div>
	
						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
	
						</div>
						<!-- end widget edit box -->
	
						<!-- widget content -->
						<div class="widget-body">
	
						<form class="form-horizontal" method="post" action="" enctype="multipart/form-data">

								<fieldset>
									<legend>Category Details <?php echo $message; ?></legend>

								<div class="form-group">
									
									<div class="col-xs-9 col-sm-6 col-lg-3">
										<label class="control-label">Name</label>
										<div class="">
											<input class="form-control input-xs" type="text" name="name" required>
										</div>
									</div>
									
									
									
									<div class="col-xs-9 col-sm-6 col-lg-2">
										<label class="control-label">Parent</label>
										<div class="">
											<select id="category_id-" name="category_id" class="form-control itemsel input-xs wei-add-field category" required><option value="">Select Option</option><?php echo product_category_id();?></select>
										</div>
									</div>									

									<div class="col-xs-9 col-sm-6 col-lg-2">
										<label class="control-label">Description</label>
										<div class="">
										<input class="form-control input-xs" type="text" name="business_unit" required>
										</div>
									</div>


									<div class="col-xs-12 col-sm-6 col-lg-2">
										<label class="control-label">Active</label>
										<div class="">

										<select class="form-control input-xs" name="active">
											
											<option selected value="1">Yes</option>
											<option value="0">No</option>
										
										</select> 
											
									</div>
										
									</div><!-- END OF select active -->

									

								</div>


								</fieldset>

								<fieldset>
								<legend>More Details</legend>
									<div class="form-group">
								
									
									<div class="col-xs-9 col-sm-6 col-lg-7">
										<label class="control-label">Description</label>
										<div class="">
											<textarea class="form-control" name="description"></textarea>
										</div>
									</div>


								</div>
								</fieldset>


								<fieldset>
									<legend>Other</legend>
								<div class="form-group">

										<div class="col-xs-9 col-sm-6 col-lg-4">
										<label class="control-label">Assigned to</label>
										<div class="">

										

									<select class="form-control" id="assigned_to" name="assigned_to">
											
										<option selected value="<?php echo $session->user_id; ?>"><?php echo $session->username; ?></option>
										<?php 
											$users = User::find_all(); 
												
										foreach($users as $user){
											echo "<option value='$user->id'>$user->username</option>";
										}	
											
										?>
									</select> 
												
										
											<p class="note"><strong>Note:</strong> Default is logged in user.</p>
										</div>
										
									</div>


								</div>
								</fieldset>
								

								<div class="form-actions">
									<div class="row">
										<div class="col-md-12">
										 <a href="products.php" class="btn btn-default" role="button">Cancel</a>
											
								<button class="btn btn-primary" type="submit" name="add_coa">
												<i class="fa fa-save"></i>
												Submit
											</button>
										</div>
									</div>
								</div>
	
							</form>
	
						</div>
						<!-- end widget content -->
	
					</div>
					<!-- end widget div -->
	
				</div>
				<!-- end widget -->
	
				<!-- Widget ID (each widget will need unique ID)-->

				<!-- end widget -->
	

	
			</article>

	
		</div>
	
		<!-- end row -->
	
		<!-- row -->
		<div class="row">
	
			<!-- NEW WIDGET START -->
			
			<!-- WIDGET END -->
	
		</div>
		<!-- end row -->
	
		<!-- row -->
		<div class="row">
	
			
					
		</div>
		<!-- end row -->		
	
	
	</section>
	<!-- end widget grid -->

</div>