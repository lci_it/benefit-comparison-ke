<section id="widget-grid" class="">

					<!-- row -->
					<div class="row">

						<!-- NEW WIDGET START -->
						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							
							<!-- Widget ID (each widget will need unique ID)-->
							<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
								<!-- widget options:
								usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

								data-widget-colorbutton="false"
								data-widget-editbutton="false"
								data-widget-togglebutton="false"
								data-widget-deletebutton="false"
								data-widget-fullscreenbutton="false"
								data-widget-custombutton="false"
								data-widget-collapsed="true"
								data-widget-sortable="false"

								-->

								<header>
									<span class="widget-icon"> <i class="fa fa-table"></i> </span>
									<h2>Product Category List </h2>

								</header>

								<!-- widget div-->
								<div>

									<!-- widget edit box -->
									<div class="jarviswidget-editbox">
										<!-- This area used as dropdown edit box -->

									</div>
									<!-- end widget edit box -->

									<!-- widget content -->
									<div class="widget-body no-padding">
									
										<table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
					
									        <thead>
												
									            <tr>
		            	<th data-hide="phone,tablet"><input type="checkbox" name="select_all"></th>

								                    <th data-hide="phone,tablet">ID</th>
								                    <th data-class="expand">Name</th>
								                    <th data-hide="phone,tablet">Parent</th>
								                    
								                    <th data-hide="phone,tablet">image</th>
								                    <th data-hide="phone,tablet">Revenue In</th>
								                    <th data-hide="phone,tablet">Sales In</th>
								                    <th data-hide="phone,tablet">Active</th>
								                    <th data-hide="phone,tablet">Created By</th>
								                    
								                    <th data-hide="phone,tablet">Actions</th>
								                  
     
									            </tr>
									        </thead>

											<tbody>
											
								<?php $categories = Product_Category::find_all(); 
								
									foreach ($categories as $category) : ?>

									<tr>

	<td><span style='padding-left: 8px;'><input type='checkbox' name='current_row_id'></span></td>

		<td><?php echo $category->id; ?></td>
		<td><?php echo $category->name; ?></td>
		<td><?php echo $category->find_parent_name($category->parent_category_id); ?></td>
		
		<td><?php echo $category->image; ?></td>

		<td><?php echo $category->sales_revenue_in; ?></td>
		<td><?php echo $category->cost_of_sales_in; ?></td>
		<td><?php $category->active(); ?></td>
		
		
		<!-- <td><img class="profile_image" src="<?php echo $category->image_path_placeholder(); ?>" alt=""></td> -->

		<?php $find_name = User::find_by_id($category->created_by); ?>
		<td><?php echo $find_name->username; ?></td>
		

		


		<td>
			<a href="coa.php?action=edit&id=<?php echo $category->id; ?>"><i class="fa fa-lg fa-fw fa-pencil-square-o"></i></a><a href="coa.php?action=delete&id=<?php echo $category->id; ?>"><i class="fa fa-lg fa-fw fa-trash-o"></i></a>
		</td>


							</tr>

												
			
												<!--  <td><span class="center-block padding-5 label label-success">Delivered</span></td>
												 -->
											
											
												<?php endforeach; ?>
											</tbody>
																
										</table>

									</div>
									<!-- end widget content -->

								</div>
								<!-- end widget div -->

							</div>
							<!-- end widget -->

						</article>
						<!-- WIDGET END -->
						
					</div>

					<!-- end row -->

					<!-- row -->

					<div class="row">

						<!-- a blank row to get started -->
						<div class="col-sm-12">
							<!-- your contents here -->
						</div>
							
					</div>

					<!-- end row -->

				</section>