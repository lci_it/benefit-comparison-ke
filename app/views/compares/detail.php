<?php include (APPROOT . "/views/inc/admin_header.php"); ?>
<div class="module">

	<?php include (APPROOT . "/views/inc/admin_detail_toolbar.php"); ?>
</div>

	<!-- widget grid -->
<section id="widget-grid" class="">
				
					<!-- row -->
					<div class="row">
				
						<!-- NEW WIDGET START -->
						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				
							<!-- Widget ID (each widget will need unique ID)-->
							<div class="jarviswidget well jarviswidget-color-darken" id="wid-id-0" data-widget-sortable="false" data-widget-deletebutton="false" data-widget-editbutton="false" data-widget-colorbutton="false">
								<!-- widget options:
								usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
				
								data-widget-colorbutton="false"
								data-widget-editbutton="false"
								data-widget-togglebutton="false"
								data-widget-deletebutton="false"
								data-widget-fullscreenbutton="false"
								data-widget-custombutton="false"
								data-widget-collapsed="true"
								data-widget-sortable="false"
				
								-->
								<header>
									<span class="widget-icon"> <i class="fa fa-barcode"></i> </span>
									<h2>Item #44761 </h2>
				
								</header>
				
								<!-- widget div-->
								<div>
				
									<!-- widget edit box -->
									<div class="jarviswidget-editbox">
										<!-- This area used as dropdown edit box -->
				
									</div>
									<!-- end widget edit box -->
				
									<!-- widget content -->
									<div class="widget-body no-padding">
				
										<div class="widget-body-toolbar">
				
											<div class="row">
				
												<div class="col-sm-4">
				
													<div class="input-group">
														<input class="form-control" type="text" placeholder="Type invoice number or date...">
														<div class="input-group-btn">
															<button class="btn btn-default" type="button">
																<i class="fa fa-search"></i> Search
															</button>
														</div>
													</div>
												</div>
				
												<div class="col-sm-8 text-align-right">
				
													<!-- <div class="btn-group">
														<a href="" class="btn btn-sm btn-primary"> <i class="fa fa-edit"></i> Edit </a>
													</div>
				
													<div class="btn-group">
														<a href="" class="btn btn-sm btn-success"> <i class="fa fa-plus"></i> Create New </a>
													</div> -->
				
												</div>
				
											</div>
				
										</div>
				
										<div class="padding-10">
											<br>
											<div class="pull-left">
												<img src="<?php echo URLROOT; ?>img/logo.png" width="150" height="32" alt="invoice icon">	
												<address>
													<!-- <br> -->
													<strong>Lifecare International Insurance Brokers LLC</strong>
													<br>
													TRN#: 45454545454
													<!-- <br>
													Detroit MI - 48212, USA
													<br>
													<abbr title="Phone">P:</abbr> (123) 456-7890 -->
												</address>
											</div>
											<div class="pull-right">
												<h1 class="font-300">Quotation</h1>
											</div>
											<div class="clearfix"></div>
											<!-- <br> -->
											<br>
											<div class="row">
												<div class="col-sm-9">
													<strong><?php echo $data['detail']->customer_name; ?></strong>
													<address>
														<?php echo $data['detail']->street . ' ' . $data['detail']->city; ?>
														<br>
														<?php echo $data['detail']->state . ' ' . $data['detail']->country; ?>
														<!-- <br> -->
														<!-- 342 Mirlington Road, -->
														<br>
														<abbr title="Phone">Phone:</abbr> <?php echo $data['detail']->phone; ?>
														<br>
														PO BOx: <?php echo $data['detail']->phone; ?>
														<br>
														TRN: <?php echo $data['detail']->salesperson; ?>
													</address>
													
												</div>
												<div class="col-sm-3">
													<div>
														<div>
															<span>Quote No :</span>
															<span class="pull-right"> <strong><?php echo $data['detail']->id; ?></strong> </span>
														</div>				
													</div>
													<div>
														<div>
															<span>Invoice No :</span>
															<span class="pull-right"><strong><a href="<?php echo URLROOT; ?>saleinvoices/detail/<?php echo $data['detail']->saleinvoice_id; ?>" > <?php echo $data['detail']->saleinvoice_id; ?></a></strong> </span>
														</div>				
													</div>

													<div>
														<div>
															<span>Proforma :</span>
															<span class="pull-right"><strong><a href="<?php echo URLROOT; ?>proformainvoices/detail/<?php echo $data['detail']->proformainvoice_id; ?>"> <?php echo $data['detail']->proformainvoice_id; ?></a></strong> </span>
														</div>				
													</div>
													<div>
														<div class="">
															<span>Quote Date :</span>
															<span class="pull-right"> <i class="fa fa-calendar"></i> <?php echo $data['detail']->date; ?> </span>
														</div>
				
													</div>

													<div>
														<div>
															<span>LPO Ref :</span>
															<span class="pull-right"> <?php echo $data['detail']->quote_lpo_ref; ?> </span>
														</div>
				
													</div>

													<div>
														<div>
															<span>Deliver To :</span>
															<span class="pull-right"> <?php echo $data['detail']->delivery_to; ?> </span>
														</div>
				
													</div>

													<div>
														<div>
															<span>Currency :</span>
															<span class="pull-right"> <?php echo dropdownValue($data['detail']->currency, 'currency'); ?> </span>
														</div>
													</div>

													<br>
													<!-- <div class="well well-sm bg-color-darken txt-color-white no-border">
														<div class="fa-lg">
															Total Due :
															<span class="pull-right">  AED** </span>
														</div>				
													</div> -->
													<br>
													<br>
												</div>
											</div>

											<table class="table table-hover">
												<thead>
													<tr>
														<th class="text-center">QTY</th>
														<th>ITEM</th>
														<th>DESCRIPTION</th>
														<th>PRICE</th>
														<th>LINETOTAL</th>
													</tr>
												</thead>
												<tbody>
													<?php 
													$product_detail = $data['products'];
													foreach($product_detail as $product) : ?>
													<tr>
														<td class="text-center"><strong><?php echo $product->quantity; ?></strong></td>
														<td><a href="<?php echo URLROOT; ?>products/detail/<?php echo $product->product_id; ?>"><?php echo $product->product_name; ?></a></td>
														<!-- <td><a href=""><?php echo $product->product_name; ?></a></td> -->
														<td><?php echo $product->product_description; ?></td>
														<td><?php echo $product->unit_price; ?></td>
				
														<td><?php echo $product->line_total; ?></td>
													</tr>

													<?php endforeach; ?>


													<tr>
														<td colspan="4" class="text-align-right">SubTotal</td>
														<td><strong><?php echo $data['detail']->subtotal; ?></strong></td>
													</tr>
													<tr>
														<td colspan="4" class="text-align-right">Dsicount</td>
														<td><strong><?php echo $data['detail']->discount; ?></strong></td>
													</tr>
													<tr>
														<td colspan="4" class="text-align-right">VAT</td>
														<td><strong><?php echo $data['detail']->vat_amount; ?></strong></td>
													</tr>

													<tr>
														<td colspan="4" class="text-align-right font-md"><strong>Total AED</strong></td>
														<td class="font-md"><strong><?php echo $data['detail']->total; ?></strong></td>
													</tr>
													
												</tbody>
											</table>
				
											<div class="invoice-footer">
												<br>

												<fieldset>
													<legend>Attached Docuemnts</legend>
													<div class="col-xs-8 col-sm-6 col-lg-8">
														
														<table id='attachments' class='table-hover table-responsive'>
														<?php
														$file_directory= ATTACHMENT_DIR . DS;
															foreach ($data['files'] as $file){
																echo "<tr>
																	<td>
																	<a href='/{$file_directory}{$file->file_name}' target='_blank'>$file->file_name</a>
																	</td>
																</tr>";
															}
														?>
														</table>
														
													</div>

													<div>
														<input type="hidden" name="filerows" id="filerows" value="">
													</div>

												</fieldset>
				
												<div class="row">
				
													<div class="col-sm-7">
														<div class="payment-methods">
															<h5>Payment Methods</h5>
															<img src="<?php echo URLROOT; ?>img/paypal.png" width="64" height="64" alt="paypal">
															<img src="<?php echo URLROOT; ?>img/americanexpress.png" width="64" height="64" alt="american express">
															<img src="<?php echo URLROOT; ?>img/mastercard.png" width="64" height="64" alt="mastercard">
															<img src="<?php echo URLROOT; ?>img/visa.png" width="64" height="64" alt="visa">
														</div>
													</div>
													<!-- <div class="col-sm-4">
														<div class="invoice-sum-total pull-right">
															<h3><strong>Total: <span class="text-success"><?php ?> AED</span></strong></h3>
														</div>
													</div> -->
				
												</div>
												
												<div class="row">
													<div class="col-sm-12">
														<p class="note">**To avoid any excess penalty charges, please make payments within 30 days of the due date. There will be a 2% interest charge per month on all late invoices.</p>
													</div>
												</div>
				
											</div>
										</div>
				
									</div>
									<!-- end widget content -->
				
								</div>
								<!-- end widget div -->
				
							</div>
							<!-- end widget -->
				
						</article>
						<!-- WIDGET END -->
				
					</div>
				
					<!-- end row -->
				
				</section>
	<!-- end widget grid -->



	<!-- row -->
	<div class="row">

		<!-- <h2 class="row-seperator-header"><i class="fa fa-plus"></i> Customized Tabs </h2> -->

		<!-- NEW WIDGET START -->
		<article class="col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget" id="wid-id-3" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-custombutton="false" data-widget-sortable="false">
				<!-- widget options:
				usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

				data-widget-colorbutton="false"
				data-widget-editbutton="false"
				data-widget-togglebutton="false"
				data-widget-deletebutton="false"
				data-widget-fullscreenbutton="false"
				data-widget-custombutton="false"
				data-widget-collapsed="true"
				data-widget-sortable="false"

				-->
				<!-- <header>
					<span class="widget-icon"> <i class="fa fa-comments"></i> </span>
					<h2>Default Tabs with border </h2>

				</header> -->

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->

					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body">
						<!-- 				
						<p>
							Tabs inside
							<code>
								.jarviswidget .well
							</code>
							(Bordered Tabs)
						</p> -->
						<!-- <hr class="simple"> -->
						<ul id="myTab1" class="nav nav-tabs bordered tabs-noborder">
							<li class="active">
								<a href="#s1" data-toggle="tab"> Proforma Invoices </a>
							</li>

							<li class="">
								<a href="#s2" data-toggle="tab"> Invoices </a>
							</li>

							<li>
								<a href="#s3" data-toggle="tab"> Receipts </a>
							</li>

							<li>
								<a href="#s4" data-toggle="tab"> History </a>
							</li>
							
							<li>
								<a href="#s5" data-toggle="tab"> More Details </a>
							</li>

							<!-- <li>
								<a href="#s3" data-toggle="tab"> History </a>
							</li> -->

						
							<li class="pull-right">
								<a href="javascript:void(0);">
								<div class="sparkline txt-color-pinkDark text-align-right" data-sparkline-height="18px" data-sparkline-width="90px" data-sparkline-barwidth="7">
									5,10,6,7,4,3
								</div> </a>
							</li>
						</ul>

						<div id="myTabContent1" class="tab-content padding-10">

							<div class="tab-pane fade in active" id="s1">

								<?php echo "include('supplier_purchases.php')"; ?>

							</div>

							<div class="tab-pane fade" id="s2">
								

							</div>

							<div class="tab-pane fade" id="s3">

								<!-- widget grid -->
								<section id="widget-grid-1" class="">



									<!-- row -->
									<div class="row">
										
										<!-- NEW WIDGET START -->
										<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

											<!-- Widget ID (each widget will need unique ID)-->
											<div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

												<!-- widget options:
													usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
													
													data-widget-colorbutton="false"	
													data-widget-editbutton="false"
													data-widget-togglebutton="false"
													data-widget-deletebutton="false"
													data-widget-fullscreenbutton="false"
													data-widget-custombutton="false"
													data-widget-collapsed="true" 
													data-widget-sortable="false"
													
												-->
												<header>
													<span class="widget-icon"> <i class="fa"></i> </span>
													<h2 class="custom-h2">Address Details </h2>				
													
												</header>

												<!-- widget div-->
												<div>
													
													<!-- widget edit box -->
													<div class="jarviswidget-editbox">
														<!-- This area used as dropdown edit box -->
														<input class="form-control" type="text">	
													</div>
													<!-- end widget edit box -->
													
													<!-- widget content -->
													<div class="widget-body no-padding detail508">

														<table cellpadding="5" cellspacing="0" border="0" class="table table-condensed">
													       <!--  <tr>
													            <td scope="col" style="width:12.5%">Area</td>
													            <td style="width: 35.5%">value</td>
													            <td scope="col" style="width:12.5%">Address</td>
													            <td style="width: 35.5%">value</td>

													        </tr>
													         
													        <tr>
													            <td scope="col" style="width:12.5%">City</td>
													            <td style="width: 35.5%">value</td>
													            <td scope="col" style="width:12.5%">State</td>
													            <td style="width: 35.5%">value</td>

													        </tr>

													        <tr>
													            <td scope="col" style="width:12.5%">PO Box</td>
													            <td style="width: 35.5%">value</td>
													            <td scope="col" style="width:12.5%">Country</td>
													            <td style="width: 35.5%">value</td>

													        </tr> -->

													       
												    </table>
							
														
													

													</div>
													<!-- end widget content -->
													
												</div>
												<!-- end widget div -->
												
											</div>
											<!-- end widget -->

										</article>
										<!-- WIDGET END -->
										
									</div>

									<!-- end row -->

									<!-- row -->

									<div class="row">

										<!-- a blank row to get started -->
										<div class="col-sm-12">
											<!-- your contents here -->
										</div>
											
									</div>

									<!-- end row -->

								</section>
								<!-- end widget grid -->
								
								<!-- widget grid -->
								<section id="widget-grid-1" class="">



									<!-- row -->
									<div class="row">
										
										<!-- NEW WIDGET START -->
										<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

											<!-- Widget ID (each widget will need unique ID)-->
											<div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

												<!-- widget options:
													usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
													
													data-widget-colorbutton="false"	
													data-widget-editbutton="false"
													data-widget-togglebutton="false"
													data-widget-deletebutton="false"
													data-widget-fullscreenbutton="false"
													data-widget-custombutton="false"
													data-widget-collapsed="true" 
													data-widget-sortable="false"
													
												-->
												<header>
													<span class="widget-icon"> <i class="fa"></i> </span>
													<h2 class="custom-h2">More Details </h2>				
													
												</header>

												<!-- widget div-->
												<div>
													
													<!-- widget edit box -->
													<div class="jarviswidget-editbox">
														<!-- This area used as dropdown edit box -->
														<input class="form-control" type="text">	
													</div>
													<!-- end widget edit box -->
													
													<!-- widget content -->
													<div class="widget-body no-padding detail508">

														<table cellpadding="5" cellspacing="0" border="0" class="table table-condensed">
													        <!-- <tr>
													            <td scope="col" style="width:12.5%">Description</td>
													            <td style="width: 35.5%">value</td>
													            <td scope="col" style="width:12.5%">Assigned To</td>
													            <td style="width: 35.5%">value</td>

													        </tr>
													         
													        <tr>
													            <td scope="col" style="width:12.5%">Date Entered</td>
													            <td style="width: 35.5%">value</td>
													            <td scope="col" style="width:12.5%">Created By</td>
													            <td style="width: 35.5%">value</td>

													        </tr>

													        <tr>
													            <td scope="col" style="width:12.5%">Modified By</td>
													            <td style="width: 35.5%">value</td>
													            <td scope="col" style="width:12.5%">Date Modified</td>
													            <td style="width: 35.5%">value</td>

													        </tr>

													        <tr>
													            <td scope="col" style="width:12.5%">Image</td>
													            <td style="width: 35.5%">value</td>
													            <td scope="col" style="width:12.5%"></td>
													            <td style="width: 35.5%">value</td>

													        </tr> -->

													       
												    </table>
							
														
													

													</div>
													<!-- end widget content -->
													
												</div>
												<!-- end widget div -->
												
											</div>
											<!-- end widget -->

										</article>
										<!-- WIDGET END -->
										
									</div>

									<!-- end row -->

									<!-- row -->

									<div class="row">

										<!-- a blank row to get started -->
										<div class="col-sm-12">
											<!-- your contents here -->
										</div>
											
									</div>

									<!-- end row -->

								</section>
								<!-- end widget grid -->

							</div>
							
						</div>

					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->

			</div>
			<!-- end widget -->


		</article>
		<!-- WIDGET END -->



	</div>
	<!-- end row -->


<!-- PAGE FOOTER -->
<?php include (APPROOT . "/views/inc/admin_footer.php"); ?>
<!-- END PAGE FOOTER -->
