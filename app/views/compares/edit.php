<?php include (APPROOT . "/views/inc/admin_header.php"); ?>

<!-- widget grid -->
	<section id="widget-grid" class="col-">
	
		<!-- <div class="well">
			
		</div> -->
	
		<!-- row -->
		<div class="row">
	
			<!-- NEW WIDGET START -->
			<article class="col-md-12">
	
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false">
					<!-- widget options:
					usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
	
					data-widget-colorbutton="false"
					data-widget-editbutton="false"
					data-widget-togglebutton="false"
					data-widget-deletebutton="false"
					data-widget-fullscreenbutton="false"
					data-widget-custombutton="false"
					data-widget-collapsed="true"
					data-widget-sortable="false"
	
					-->
					<header>
						<span class="widget-icon"> <i class="fa fa-eye"></i> </span>
						<h2>Edit <?php echo $this->module; ?></h2>	
					</header>
	
					<!-- widget div-->
					<div>
						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->	
						</div>
						<!-- end widget edit box -->
	
						<!-- widget content -->
						<div class="widget-body">
							<form class="form-horizontal" method="post" action="<?php echo htmlspecialchars( URLROOT . current_class($this) . '/edit/' . $data['record']->id); ?>" enctype="multipart/form-data">

								<fieldset>
									<legend>Quote Details <span class="label bg-color-purple pull-right">ID <?php echo $data['record']->id; ?></span></legend><br>
									<!-- <legend>Customer Details <?php; ?></legend> -->
									<!-- START OF pull-left -->
										<div class="pull-left">
											<div class="form-group">
												<label class="col-md-5 control-label text-left">Title</label>
												<div class="col-md-7">
													<input class="form-control input-xs" type="text" name="name" id="name" value="<?php echo $data['record']->name; ?>">
												</div>
											</div>

											<div class="form-group hidden">
												<label class="col-md-5 control-label text-left">Customer Code <sup>*</sup></label>
												<div class="col-md-7">
													<input class="form-control input-xs ui-widget" type="text" name="customer_id" id="customer_id" value="<?php echo $data['record']->customer_id; ?>" required>
												</div>
											</div>

											<div class="form-group">
												<label class="col-md-5 control-label text-left">Customer <sup>*</sup></label>
												<div class="col-md-7">
													<input class="form-control input-xs txt-auto ui-widget" name="customer_name" id="customer_name" value="<?php echo $data['record']->customer_name; ?>" required>
												</div>
											</div>

											<div class="form-group">
												<label class="col-md-5 control-label text-left">Contact</label>
												<div class="col-md-7">
													<input class="form-control input-xs txt-auto ui-widget" name="contact_name" id="contact_name" value="<?php echo $data['record']->contact_name; ?>" required>
												</div>
											</div>

											<br><br>

											<div class="form-group">
												<label class="col-md-5 control-label text-left">street</label>
												<div class="col-md-7">
													<input class="form-control input-xs" type="text" name="street" id="street" value="<?php echo $data['record']->street; ?>">
												</div>
											</div>

											<div class="form-group">
												<label class="col-md-5 control-label text-left">City</label>
												<div class="col-md-7">
													<input class="form-control input-xs" type="text" name="city" id="city" value="<?php echo $data['record']->city; ?>">
												</div>
											</div>

											<div class="form-group">
												<label class="col-md-5 control-label text-left">State</label>
												<div class="col-md-7">
													<input class="form-control input-xs" type="text" name="state" id="state" value="<?php echo $data['record']->state; ?>">
												</div>
											</div>

											<div class="form-group">
												<label class="col-md-5 control-label text-left">Country</label>
												<div class="col-md-7">
													<input class="form-control input-xs" type="text" name="country" id="country" value="<?php echo $data['record']->country; ?>">
												</div>
											</div>

											

											<div class="form-group">
												<label class="col-md-5 control-label text-left">Customer TRN</label>
												<div class="col-md-7">
													<input class="form-control input-xs" type="text" name="customer_trn" id="customer_trn" value="<?php echo $data['record']->customer_trn; ?>">
												</div>
											</div>

										</div>
										<!-- END OF pull-left -->

										<!-- START OF pull-right -->
											<div class="pull-right">

												<div class="form-group">
													<label class="col-md-5 control-label text-left">Opportunity</label>
													<div class="col-md-7">
														<input class="form-control input-xs txt-auto ui-widget" name="opportunity" id="opportunity" value="<?php echo $data['record']->opportunity_id; ?>">
													</div>
												</div>

												<div class="form-group">
													<label class="col-md-5 control-label text-left">Quote Date</label>
													<div class="col-md-7">
														<input class="form-control input-xs" type="date" name="date" value="<?php echo $data['record']->date; ?>">
													</div>
												</div>

												<!-- <div class="form-group">
													<label class="col-md-5 control-label text-left">Invoice No.</label>
													<div class="col-md-7">
														<input class="form-control input-xs" type="text" name="invoice_no">
													</div>
												</div> -->

												

												<div class="form-group">
													<label class="col-md-5 control-label text-left">Valid Until</label>
													<div class="col-md-7">
														<input class="form-control input-xs" type="date" name="valid_until" value="<?php echo $data['record']->valid_until; ?>">
													</div>
												</div>

												<!-- <div class="form-group">
													<label class="col-md-5 control-label text-left">Quote/LPO Ref</label>
													<div class="col-md-7">
														<input class="form-control input-xs" type="text" name="quote_lpo_ref">
													</div>
												</div> -->

												<!-- <div class="form-group">
													<label class="col-md-5 control-label text-left">Deliver To</label>
													<div class="col-md-7">
														<textarea class="form-control input-xs" rows="1" name="delivery_to"></textarea>
													</div>
												</div> -->

												<br><br>

												<div class="form-group">
													<label class="col-md-5 control-label text-left">Quote Stage</label>
													<div class="col-md-7">
														<select class="form-control input-xs" name="quote_stage" id="quote_stage">
															<option value="<?php echo $data['record']->quote_stage ?>"><?php echo dropDownValue($data['record']->quote_stage, 'quote_stage'); ?></option>
															<?php echo dropDownList("quote_stage"); ?>
														</select>
													</div>
												</div>

												<div class="form-group">
													<label class="col-md-5 control-label text-left">Currency</label>
													<div class="col-md-7">
														<select class="form-control input-xs" name="currency" id="currency">
															<option value="<?php echo $data['record']->currency ?>"><?php echo dropDownValue($data['record']->currency, 'currency'); ?></option>
															<?php echo dropDownList("currency"); ?>
														</select>
													</div>
												</div>

												<div class="form-group">
													<label class="col-md-5 control-label text-left">Phone</label>
													<div class="col-md-7">
														<input class="form-control input-xs" type="text" name="phone" id="phone" value="<?php echo $data['record']->phone; ?>">
													</div>
												</div>

												<div class="form-group">
													<label class="col-md-5 control-label text-left">Postal Code</label>
													<div class="col-md-7">
														<input class="form-control input-xs" type="text" name="postal_code" id="postal_code" value="<?php echo $data['record']->postal_code; ?>">
													</div>
												</div>

											</div>
											<!-- END OF pull-right -->				
								</fieldset>

								<fieldset>
									<div class="pull-left">
										
									</div>

									<div class="pull-right">
										
										
									</div>
									
								</fieldset>

								<div class="text-center">
									<button class="btn btn-success wei-add-service-button" type="button">
										<i class="fa fa-plus-square"></i>
										Add Row
									</button>

									<button class="btn btn-success" type="button" >
										<i class="fa fa-codepen"></i>
										Add Product
									</button>
								</div>
								<br>

								<fieldset>
									<div class="table-responsive">
										<table class="order-details table-hover" id="items_table">
											<tbody>
												<?php
													$find_products = $data['product'];
													if(!(bool)$find_products){
														$i = 1;
														$html = "";
														echo $html;

													} else {
														$html = '';

														$i = 1;

														// echo "rows present";
														foreach ($find_products as $product){
															$html .= '<tr id="row-'.$i.'">

															<td class="col-md-1 col-xs-9 hidden"><input type="text" id="j_table_id-'.$i.'" name="j_table_id-[]" placeholder="id" class="form-control input-xs wei-add-field id '.$i.'" value="'.$product->id.'"></td>

															<td class="col-md-1 col-xs-9 hidden"><input type="text" id="product_id-'.$i.'" name="product_id-[]" placeholder="Product ID" class="form-control input-xs wei-add-field id '.$i.'" value="'.$product->product_id.'"></td>

															<td class="col-md-2 col-xs-9"><input type="text" id="product_name-'.$i.'" name="product_name-[]" placeholder="Name" class="form-control input-xs wei-add-field name '.$i.'" value="'.$product->product_name.'" required></td>

															<td class="col-md-2 col-xs-9"><input type="text" id="part_number-'.$i.'" name="part_number-[]" class="form-control input-xs wei-add-field part '.$i.'" value="'.$product->part_number.'"></td>

															<td class="col-md-2 col-xs-9"><textarea class="form-control input-xs wei-add-field description '.$i.'" rows="1" id="product_desc-'.$i.'" name="product_desc-[]" placeholder="Description">'.$product->product_description.'</textarea></td>

															<td class="col-md-1 col-xs-9 hidden"><input type="text" id="project_id-'.$i.'" name="project_id-[]" placeholder="Project" class="form-control input-xs wei-add-field project '.$i.'" value="'.$product->project_id.'"></td>

															<td class="col-md-1 col-xs-9"><input type="currency" id="unit_price-'.$i.'" name="unit_price-[]" placeholder="Price" class="form-control input-xs wei-add-field unit-price '.$i.'" value="'.$product->unit_price.'" required></td>

															<td class="col-md-1 col-xs-9"><input type="number" id="product_qty-'.$i.'" name="product_qty-[]" placeholder="Qty" class="form-control input-xs wei-add-field quantity '.$i.'" value="'.$product->quantity.'" required></td>

															<td class="col-md-2 col-xs-9"><input type="currency" id="price_total-'.$i.'" name="line_total-[]" class="form-control input-xs wei-add-field price-total '.$i.'" value="'.$product->line_total.'"></td>

															<td class="col-md-2 col-xs-9"><input type="hidden" id="delete-'.$i.'" name="delete-[]" value="0" class="input-xs wei-add-field delete '.$i.'"><a class="fa fa-trash-o btn btn-danger input-xs" href="#"> x</a></td>

															</tr>';

															$i = $i + 1;
														}

														echo $html;
													}
												?>
													
											</tbody>
										</table>
										<br>
										<div class="text-left">
											<button class="btn btn-success wei-add-service-button" id="addbutton" type="button">
												<i class="fa fa-plus-square"></i>Add Row
											</button>
											<button class="btn btn-success" type="button">
												<i class="fa fa-codepen"></i>Add Product
											</button>
										</div>
										<!-- <div class="wei-add-service"><a href="#" class="button-secondary wei-add-service-button">Add Item</a></div> -->
										<br>

										<table class="wei-add-totals items_total" id="items_total">
											<tr>
												<td class="col-md-1 col-xs-9"></td>
												<td class="col-md-1 col-xs-9 col-md-offset-6"><strong>Sub Total</strong></td>
												<td class="col-md-2 col-xs-9 col-md-offset-6"><input class="form-control input-xs wie-add-subtotal currency" type="currency" name="subtotal" id="subtotal" value="<?php echo $data['record']->subtotal; ?>" readonly></td>
												<td class="col-md-1 col-xs-9"></td>
											</tr>

											<tr>
												<td class="col-md-8 col-xs-9"></td>
												<td class="col-md-1 col-xs-9 col-md"><strong>Discount</strong></td>
												<td class="col-md-2 col-xs-9 col-md-offset-6"><input class="form-control input-xs wie-add-discount" type="currency" name="discount" id="discount" value="<?php echo $data['record']->discount; ?>"></td>
												<td class="col-md-1 col-xs-9"></td>
											</tr>

											<tr>
												<td class="col-md-8 col-xs-9"></td>
												<td class="col-md-1 col-xs-9 col-md-offset-6"><strong>
													<select name="vat_rate" id="vat_rate" class="form-control input-xs">
														<option value="<?php echo $data['record']->vat_rate; ?>"><?php echo $data['record']->vat_rate; ?></option>
														<option value="0">VAT 0%</option>
														<option value="5" selected="selected">VAT 5%</option>
														<option value="10">VAT 10%</option>

													</select></strong>

												</td>
												<td class="col-md-1 col-xs-9 col-md-offset-6"><input class="form-control input-xs wie-add-vat-amount" type="currency" name="vat_amount" id="vat_amount" value="<?php echo $data['record']->vat_amount; ?>"></td>
												<td class="col-md-1 col-xs-9"></td>
											</tr>

											<tr>
												<td class="col-md-8 col-xs-9"></td>
												<td class="col-md-1 col-xs-9 col-md"><strong>Total</strong></td>
												<td class="col-md-2 col-xs-9 col-md-offset-6"><input class="form-control input-xs wie-add-totals" type="currency" name="total" id="total" value="<?php echo $data['record']->total; ?>" readonly></td>
												<td class="col-md-1 col-xs-9"></td>
											</tr>

										</table>

										<div><input type="hidden" name="totalrows" id="totalrows" value=""></div>

										<!-- <div><button type="button" name="add" id="add" class="btn btn-success">Add More</button></div> -->

										<!-- <div style="display: none;">Total Row: <input type="number" name="totalrow" id="totalrow"></div> -->
									</div>
								</fieldset>
								<br>

								<fieldset>
									<div class="form-group col-md-6">
										<label class="col-md-4 control-label text-left">Payment Terms</label>
										<div class="col-md-7">
										<textarea class="form-control input-xs" rows="1" name="payment_terms"><?php echo $data['record']->payment_terms; ?></textarea>
										</div>
									</div>

									<div class="form-group col-md-6">											
										<label class="control-label text-left col-md-4 col-md-offset-1">Sales Person</label>
										<div class="col-md-7">
											<select class="form-control input-xs" id="salesperson" name="salesperson">
													<option selected value="<?php echo $data['record']->salesperson; ?>"><?php echo findName('users', $data['record']->salesperson); ?>
													</option>
													<?php echo getListFromTable('users'); ?>
												</select>
											<p class="note"><strong>Note:</strong> Default is logged in user.</p>
										</div>
									</div>

									<div class="form-group col-md-6">
										<label class="col-md-4 control-label text-left">Comments</label>
										<div class="col-md-7">
											<textarea class="form-control input-xs" rows="2" name="comments"><?php echo $data['record']->notes; ?></textarea>
										</div>
									</div>

									<div class="form-group col-md-6">
										<label class="control-label text-left col-md-4 col-md-offset-1">Assigned to</label>
										<div class="col-md-7">
											<select class="form-control input-xs" id="assigned_to" name="assigned_to">
												<option selected value="<?php echo $data['record']->assigned_to; ?>; ?>"><?php echo findName('users', $data['record']->assigned_to); ?>
												</option>
												<?php echo getListFromTable('users'); ?>
											</select>
										</div>
									</div>

								</fieldset>
									
								<br>

								<fieldset>
									<legend>Attached Docuemnts</legend>
									<div class="col-xs-8 col-sm-6 col-lg-8">														
										<table id='attachments' class='table-hover table-responsive'>
										<?php
											foreach ($data['files'] as $file){
												$file_directory= ATTACHMENT_DIR;	
												echo "<tr>
													<td>
													<a href='/{$file_directory}/{$file->file_name}' target='_blank'>$file->file_name</a>
													</td>
													<td style='padding: 3px;'>
														<input type='hidden' name='file_id-[]' value='{$file->id}'><input type='hidden' id='delete_file-' name='delete_file-[]' value='0' class='input-xs delete_file '>
														&nbsp &nbsp
														<a class='fa fa-trash-o btn btn-warning delete-icon btn-sm' href='#'> x</a>
													</td>
												</tr>";
											}
										?>
										</table>
										
									</div>

									<div><input type="hidden" name="filerows" id="filerows" value=""></div>
								</fieldset>

								<fieldset>									
								<legend>Upload Docuemnts</legend>
									<div class="form-group">
										<div class="col-md-7">
											<input class="" type="file" name="files[]" multiple="multiple">
										</div>
									</div>
								</fieldset>

								<div class="form-actions">
									<div class="row">
										<div class="col-md-12">
											<a href="<?php echo htmlspecialchars( URLROOT . current_class($this) ); ?>" class="btn btn-default" role="button">Cancel</a>
											<button class="btn btn-primary" id="submit" type="submit" name="submit">
												<i class="fa fa-save"></i>Submit
											</button>
										</div>
									</div>
								</div>
		
							</form>
						</div>
						<!-- end widget content -->
	
					</div>
					<!-- end widget div -->
	
				</div>
				<!-- end widget -->
	
				<!-- Widget ID (each widget will need unique ID)-->

				<!-- end widget -->
			</article>
		</div>
	
		<!-- end row -->
	
		<!-- row -->
		<div class="row">
	
			<!-- NEW WIDGET START -->
			
			<!-- WIDGET END -->
	
		</div>
		<!-- end row -->
	
		<!-- row -->
		<div class="row">
			
		</div>
		<!-- end row -->
	</section>
	<!-- end widget grid -->

<!-- PAGE FOOTER -->
<?php include (APPROOT . "/views/inc/admin_footer.php"); ?>
<!-- END PAGE FOOTER -->