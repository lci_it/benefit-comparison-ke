<?php 

$aMan  = array();
$aPCat = array();
$aWorldwide = array();
$aWorldwideExUSA = array();
$aUae = array();

/// Manufacturers Code Starts ///

if(isset($_REQUEST['man'])&&is_array($_REQUEST['man'])){
	foreach($_REQUEST['man'] as $sKey=>$sVal){
		if((int)$sVal!=0){
			$aMan[(int)$sVal] = (int)$sVal;
		}
	}
}
/// Manufacturers Code Ends ///

/// Dental Code Starts ///
if(isset($_REQUEST['p_cat'])&&is_array($_REQUEST['p_cat'])){
	foreach($_REQUEST['p_cat'] as $sKey=>$sVal){
		if($sVal!=''){
			$aPCat[$sVal] = $sVal;
		}
	}
}
/// Dental Code Ends ///

// Worldwide Code Starts ///
if(isset($_REQUEST['worldwide'])&&is_array($_REQUEST['worldwide'])){
	foreach($_REQUEST['worldwide'] as $sKey=>$sVal){
		if($sVal!=''){
			$aWorldwide[$sVal] = $sVal;
		}
	}
}
/// Worldwide Code Ends ///

// Worldwide Code Starts ///
if(isset($_REQUEST['WorldwideExUSA'])&&is_array($_REQUEST['WorldwideExUSA'])){
	foreach($_REQUEST['WorldwideExUSA'] as $sKey=>$sVal){
		if($sVal!=''){
			$aWorldwideExUSA[$sVal] = $sVal;
		}
	}
}
/// Worldwide Code Ends ///

// Worldwide Code Starts ///
if(isset($_REQUEST['uae'])&&is_array($_REQUEST['uae'])){
	foreach($_REQUEST['uae'] as $sKey=>$sVal){
		if($sVal!=''){
			$aUae[$sVal] = $sVal;
		}
	}
}
/// Worldwide Code Ends ///

?>

<?php include (APPROOT . "/views/inc/admin_header.php"); ?>
	
	<!-- widget grid -->
	<section id="widget-grid" class="col-">
	
		<!-- <div class="well">
			
		</div> -->
	
		<!-- row -->
		<div class="row">
	
			<!-- NEW WIDGET START -->
			<article class="col-md-12">
	
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false">
					<!-- widget options:
					usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
	
					data-widget-colorbutton="false"
					data-widget-editbutton="false"
					data-widget-togglebutton="false"
					data-widget-deletebutton="false"
					data-widget-fullscreenbutton="false"
					data-widget-custombutton="false"
					data-widget-collapsed="true"
					data-widget-sortable="false"
	
					-->
					<header>
						<span class="widget-icon"> <i class="fa fa-eye"></i> </span>
						<h2>Add <?php echo $this->module; ?></h2>	
					</header>
	
					<!-- widget div-->
					<div>
						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->	
						</div>
						<!-- end widget edit box -->
	
						<!-- widget content -->
						<div class="widget-body">
							<form class="form-horizontal" method="post" action="<?php echo htmlspecialchars( URLROOT . current_class($this) . '/add'); ?>" enctype="multipart/form-data">

								<fieldset>
									<!-- <legend>Customer Details <?php; ?></legend> -->
									<!-- START OF pull-left -->
										<div class="pull-left">
											<div class="form-group">
												<label class="col-md-5 control-label text-left">Title</label>
												<div class="col-md-7">
													<input class="form-control input-xs" type="text" name="name" id="name">
												</div>
											</div>

											<div class="form-group hidden">
												<label class="col-md-5 control-label text-left">Customer Code <sup>*</sup></label>
												<div class="col-md-7">
													<input class="form-control input-xs ui-widget" type="text" name="customer_id" id="customer_id" required>
												</div>
											</div>

											<div class="form-group">
												<label class="col-md-5 control-label text-left">Customer <sup>*</sup></label>
												<div class="col-md-7">
													<input class="form-control input-xs txt-auto ui-widget" name="customer_name" id="customer_name" required>
												</div>
											</div>

											<div class="form-group">
												<label class="col-md-5 control-label text-left">Contact</label>
												<div class="col-md-7">
													<input class="form-control input-xs txt-auto ui-widget" name="contact_name" id="contact_name">
												</div>
											</div>

											<!-- <br><br> -->

											<!-- <div class="form-group">
												<label class="col-md-5 control-label text-left">street</label>
												<div class="col-md-7">
													<input class="form-control input-xs" type="text" name="street" id="street">
												</div>
											</div>

											<div class="form-group">
												<label class="col-md-5 control-label text-left">City</label>
												<div class="col-md-7">
													<input class="form-control input-xs" type="text" name="city" id="city">
												</div>
											</div>

											<div class="form-group">
												<label class="col-md-5 control-label text-left">State</label>
												<div class="col-md-7">
													<input class="form-control input-xs" type="text" name="state" id="state">
												</div>
											</div>

											<div class="form-group">
												<label class="col-md-5 control-label text-left">Country</label>
												<div class="col-md-7">
													<input class="form-control input-xs" type="text" name="country" id="country">
												</div>
											</div> -->

											

											<!-- <div class="form-group">
												<label class="col-md-5 control-label text-left">Customer TRN</label>
												<div class="col-md-7">
													<input class="form-control input-xs" type="text" name="customer_trn" id="customer_trn">
												</div>
											</div> -->

										</div>
										<!-- END OF pull-left -->

										<!-- START OF pull-right -->
											<div class="pull-right">

												<div class="form-group">
													<label class="col-md-5 control-label text-left">Opportunity</label>
													<div class="col-md-7">
														<input class="form-control input-xs txt-auto ui-widget" name="opportunity" id="opportunity">
													</div>
												</div>

												<div class="form-group">
													<label class="col-md-5 control-label text-left">Quote Date</label>
													<div class="col-md-7">
														<input class="form-control input-xs" type="date" name="date" value="<?php echo date('Y-m-d'); ?>">
													</div>
												</div>

												<!-- <div class="form-group">
													<label class="col-md-5 control-label text-left">Invoice No.</label>
													<div class="col-md-7">
														<input class="form-control input-xs" type="text" name="invoice_no">
													</div>
												</div> -->

												

												<div class="form-group">
													<label class="col-md-5 control-label text-left">Valid Until</label>
													<div class="col-md-7">
														<input class="form-control input-xs" type="date" name="valid_until" value="<?php echo validityDate('7'); ?>">
													</div>
												</div>

												<!-- <div class="form-group">
													<label class="col-md-5 control-label text-left">Quote/LPO Ref</label>
													<div class="col-md-7">
														<input class="form-control input-xs" type="text" name="quote_lpo_ref">
													</div>
												</div> -->

												<!-- <div class="form-group">
													<label class="col-md-5 control-label text-left">Deliver To</label>
													<div class="col-md-7">
														<textarea class="form-control input-xs" rows="1" name="delivery_to"></textarea>
													</div>
												</div> -->

												<!-- <br><br> -->

												<!-- <div class="form-group">
													<label class="col-md-5 control-label text-left">Currency</label>
													<div class="col-md-7">
														<select class="form-control input-xs" name="currency" id="currency">
															<?php echo dropDownList("currency"); ?>
														</select>
													</div>
												</div>

												<div class="form-group">
													<label class="col-md-5 control-label text-left">Phone</label>
													<div class="col-md-7">
														<input class="form-control input-xs" type="text" name="phone" id="phone" >
													</div>
												</div>

												<div class="form-group">
													<label class="col-md-5 control-label text-left">Postal Code</label>
													<div class="col-md-7">
														<input class="form-control input-xs" type="text" name="postal_code" id="postal_code" >
													</div>
												</div> -->

											</div>
											<!-- END OF pull-right -->				
								</fieldset>

								<fieldset>
									<div class="pull-left">
										
									</div>

									<div class="pull-right">
										
										
									</div>
									
								</fieldset>

								<fieldset>
									

									<!-- New Item manufacturers Start -->									
									<div class="col-md-12" style="border-right: 1px solid #ccc;">
										<h6> Insurance Providers</h6>
										
										<div class="panel-collapse collapse-data">

											<div class="panel-body scroll-menu">
												<ul class="nav nav-pills nav-stacked category-menu" id="dev-manufacturer">

													<?php foreach($data['manufacturer'] as $record) : ?>

														<li style='display: inline-block;' class='checkbox checkbox-primary'>
															<a>
																<label>
																	<input <?php if(isset($aMan[$record->id])){ echo "checked='checked'";} ?>
																	 type='checkbox' value='<?php echo $record->id; ?>' name='manufacturer' class='get_manufacturer'>
																		<span>							
																			<?php echo $record->name; ?>
																		</span>

																</label>
															</a>
														</li>

													<?php endforeach; ?>
													
												</ul>				
											</div>
											
										</div>	
									</div>
									<!-- New Item manufacturers End -->

									<!-- New Item Benefits Start -->									
									
									<!-- New Item Benefits End -->

									
								</fieldset>

								<button style="color: #fff;" class="btn btn-info compare" rel="<?php ; ?>">Compare Plans</button><br><br>

								

								<fieldset>

								<div class="col-md-12" style="overflow-x: auto;">
									<div class="row" id="Products">
										<div class="">
											
											
										</div>
										
									</div>
									
								</div>								

									
								</fieldset>

								<!-- <div class="text-center">
									<button class="btn btn-success wei-add-service-button" type="button">
										<i class="fa fa-plus-square"></i>
										Add Row
									</button>

									<button class="btn btn-success wei-add-compare-button" type="button">
										<i class="fa fa-plus-square"></i>
										Get Comparision
									</button>

									<button class="btn btn-success" type="button" >
										<i class="fa fa-codepen"></i>
										Add Product
									</button>
								</div> -->
								<br>

								<!-- Old working Products filtering -->

								<!-- div class="row" id="Productsold">
									
								</div> -->

								<fieldset>
									<div class="table-responsive">
										<table class="order-details table-hover" id="items_table">
											<tbody>
												
											</tbody>
										</table>
										<br>

										<!-- <div class="text-left">
											<button class="btn btn-success wei-add-service-button" id="addbutton" type="button">
												<i class="fa fa-plus-square"></i>Add Row
											</button>
											<button class="btn btn-success" type="button">
												<i class="fa fa-codepen"></i>Add Product
											</button>
										</div> -->
										<!-- <div class="wei-add-service"><a href="#" class="button-secondary wei-add-service-button">Add Item</a></div> -->
										<br>

										

										<div><input type="hidden" name="totalrows" id="totalrows" value=""></div>

										<!-- <div><button type="button" name="add" id="add" class="btn btn-success">Add More</button></div> -->

										<!-- <div style="display: none;">Total Row: <input type="number" name="totalrow" id="totalrow"></div> -->
									</div>
								</fieldset>
								<br>

								<fieldset>

									
									
								</fieldset>

								<fieldset>
									<div class="form-group col-md-6">
										<label class="col-md-4 control-label text-left">Payment Terms</label>
										<div class="col-md-7">
										<textarea class="form-control input-xs" rows="1" name="payment_terms"></textarea>
										</div>
									</div>

									<div class="form-group col-md-6">											
										<label class="control-label text-left col-md-4 col-md-offset-1">Sales Person</label>
										<div class="col-md-7">
											<select class="form-control input-xs" id="salesperson" name="salesperson">
													<option selected value="<?php echo $_SESSION['user_id']; ?>"><?php echo findName('users', $_SESSION['user_id']); ?>
													</option>
													<?php echo getListFromTable('users'); ?>
												</select>
											<p class="note"><strong>Note:</strong> Default is logged in user.</p>
										</div>
									</div>

									<div class="form-group col-md-6">
										<label class="col-md-4 control-label text-left">Comments</label>
										<div class="col-md-7">
											<textarea class="form-control input-xs" rows="2" name="comments"></textarea>
										</div>
									</div>

									<div class="form-group col-md-6">
										<label class="control-label text-left col-md-4 col-md-offset-1">Assigned to</label>
										<div class="col-md-7">
											<select class="form-control input-xs" id="assigned_to" name="assigned_to">
												<option selected value="<?php echo $_SESSION['user_id']; ?>"><?php echo findName('users', $_SESSION['user_id']); ?>
												</option>
												<?php echo getListFromTable('users'); ?>
											</select>
										</div>
									</div>

								</fieldset>
									
								<br>

								<fieldset>									
								<legend>Upload Docuemnts</legend>
									<div class="form-group">
										<div class="col-md-7">
											<input class="" type="file" name="files[]" multiple="multiple">
										</div>
									</div>
								</fieldset>

								<div class="form-actions">
									<div class="row">
										<div class="col-md-12">
											<a href="<?php echo htmlspecialchars( URLROOT . current_class($this) ); ?>" class="btn btn-default" role="button">Cancel</a>
											<button class="btn btn-primary" id="submit" type="submit" name="submit">
												<i class="fa fa-save"></i> Save & Send
											</button>
										</div>
									</div>
								</div>
		
							</form>
						</div>
						<!-- end widget content -->
	
					</div>
					<!-- end widget div -->
	
				</div>
				<!-- end widget -->
	
				<!-- Widget ID (each widget will need unique ID)-->

				<!-- end widget -->
			</article>
		</div>
	
		<!-- end row -->
	
		<!-- row -->
		<div class="row">
	
			<!-- NEW WIDGET START -->
			
			<!-- WIDGET END -->
	
		</div>
		<!-- end row -->
	
		<!-- row -->
		<div class="row">
			
		</div>
		<!-- end row -->
	</section>
	<!-- end widget grid -->

<!-- PAGE FOOTER -->
<?php include (APPROOT . "/views/inc/admin_footer.php"); ?>
<!-- END PAGE FOOTER -->