<?php include (APPROOT . "/views/inc/admin_header.php"); ?>

	<!-- START CONTENT  -->
	<section id="widget-grid" class="">

		<!-- row -->
		<div class="row">

			<!-- NEW WIDGET START -->
			<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
					<!-- widget options:
					usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

					data-widget-colorbutton="false"
					data-widget-editbutton="false"
					data-widget-togglebutton="false"
					data-widget-deletebutton="false"
					data-widget-fullscreenbutton="false"
					data-widget-custombutton="false"
					data-widget-collapsed="true"
					data-widget-sortable="false"

					-->

					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2>List <?php echo $this->module; ?></h2>

					</header>

					<!-- widget div-->
					<div>

						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
						</div>
						<!-- end widget edit box -->

						<!-- widget content -->
						<div class="widget-body no-padding">
							<form method="post" action='<?php echo htmlspecialchars( URLROOT . current_class($this) . "/action"); ?>'>
						
							<table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
		
					        <thead>
					            <tr>
					            	<th data-hide="phone,tablet"><input type="checkbox" id="selectAllboxes"></th>
					            	<th data-hide="phone,tablet">ID</th>
					            	<!-- <th data-hide="phone,tablet">Quote#</th> -->
				                  <th data-class="expand">Title</th>
				                  <th data-class="expand">Customer</th>
				                  <!-- <th data-hide="phone,tablet">Address</th> -->
				                  <!-- <th data-hide="phone,tablet">TRN</th> -->
				                  <th data-hide="phone,tablet">Contact</th>
				                  <th data-hide="phone,tablet">Total</th>
				                  <th data-hide="phone,tablet">Stage</th>
				                  <th data-hide="phone,tablet">Invoice Status</th>				                  
				                  <th data-hide="phone,tablet">Phone</th>
				                  <th data-hide="phone,tablet">Created By</th>
				                  <th data-hide="phone,tablet">Date Entered</th>
				                  <th data-hide="phone,tablet">SelectAction</th>				                  
					            </tr>						         
					        </thead>

					        <tbody>
					        	<?php foreach($data['list'] as $record) : ?>
									<tr>
										<td><span><input type='checkbox' class='checkBoxes' name='checkBoxArray[]' value="<?php echo $record->id; ?>"></span></td>
										<td><?php echo $record->id; ?></td>
										<!-- <td><?php 'Quote#'; ?></td> -->
										<td><a href="<?php echo URLROOT . current_class($this); ?>/detail/<?php echo $record->id; ?>"><?php echo $record->name; ?></a></td>
										<td><a href="<?php echo URLROOT; ?>customers/detail/<?php echo $record->id; ?>"><?php echo findName('customers', $record->customer_id); ?></a></td>
										<!-- <td></td> -->
										<!-- <td><?php 'trn'; ?></td> -->
										<td><?php echo $record->contact_id; ?></td>
										<td><?php echo $record->total; ?></td>
										<td><?php echo dropdownValue($record->quote_stage, 'quote_stage'); ?></td>
										<td><?php echo show_yes_no($record->invoice_status); ?></td>
										<td><?php echo $record->phone; ?></td>
										<td><?php echo findName('users', $record->created_by); ?></td>
										<td><?php echo UserTimeZone($record->date_entered); ?></td>
										<td>
											<a href="<?php echo URLROOT . current_class($this); ?>/edit/<?php echo $record->id; ?>"><i class="fa fa-lg fa-fw fa-pencil-square-o"></i></a>

											<a href="<?php echo URLROOT . current_class($this); ?>/delete/<?php echo $record->id; ?>" onClick="return confirm('Are you sure you want to delete <?php echo $record->id; ?>?');"><i class="fa fa-lg fa-trash-o"></i></a>

											
										</td>
									</tr>
									<?php endforeach; ?>								
									
								</tbody>
													
							</table>
							</form>

						</div>
						<!-- end widget content -->

					</div>
					<!-- end widget div -->

				</div>
				<!-- end widget -->

			</article>
			<!-- WIDGET END -->
			
		</div>

		<!-- end row -->

		<!-- row -->

		<div class="row">

			<!-- a blank row to get started -->
			<div class="col-sm-12">
				<!-- your contents here -->
			</div>
				
		</div>

		<!-- end row -->

	</section>
	<!-- END CONTENT -->	


<!-- PAGE FOOTER -->
<?php include (APPROOT . "/views/inc/admin_footer.php"); ?>
<!-- END PAGE FOOTER -->