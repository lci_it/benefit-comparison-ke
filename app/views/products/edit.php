<?php include (APPROOT . "/views/inc/admin_header.php"); ?>

<div id="content">

	<div class="row">
		
		
	</div>
	
	<!-- widget grid -->
	<section id="widget-grid" class="col-">
	
		<!-- <div class="well">
			
		</div> -->
	
		<!-- row -->
		<div class="row">
	
			<!-- NEW WIDGET START -->
			<article class="col-md-12">
	
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false">
					<!-- widget options:
					usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
	
					data-widget-colorbutton="false"
					data-widget-editbutton="false"
					data-widget-togglebutton="false"
					data-widget-deletebutton="false"
					data-widget-fullscreenbutton="false"
					data-widget-custombutton="false"
					data-widget-collapsed="true"
					data-widget-sortable="false"
	
					-->
					<header>
						<span class="widget-icon"> <i class="fa fa-eye"></i> </span>
						<h2>Edit Plan</h2>
	
					</header>
	
					<!-- widget div-->
					<div>
	
						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
	
						</div>
						<!-- end widget edit box -->
	
						<!-- widget content -->
						<div class="widget-body">
	
						<form class="form-horizontal" method="post" action='<?php echo htmlspecialchars( URLROOT . current_class($this) . "/edit/" . $data['product']->id); ?>' enctype="multipart/form-data">

								<fieldset>
									<!-- <legend>Product Details <?php ?></legend> -->

									<div class="form-group">

										<!-- <div class="col-xs-9 col-sm-6 col-lg-2">
											<label class="control-label">Type</label>
											<div class="">
												<select class="form-control input-xs" name="product_type" id="product_type" required>
													<option value="" selected>Select</option>
													<?php echo "dropDown"; ?>
												</select>
											</div>
										</div> -->

										
										<div class="col-xs-9 col-sm-6 col-lg-2">
											<label class="control-label">Name</label>
											<div class="">
												<input class="form-control input-xs" type="text" name="name" value="<?php echo $data['product']->name; ?>" required>
											</div>
										</div>

										<div class="col-xs-9 col-sm-6 col-lg-2">
											<label class="control-label">Plan Type</label>
											<div class="">
												<select class="form-control input-xs" name="product_type" id="product_type" required>
													<option value="<?php echo $data['product']->plan_type; ?>" selected><?php echo dropDownValue($data['product']->plan_type, 'product_type'); ?></option>
													
													<?php echo dropDownList("product_type"); ?>
												</select>
											</div>
										</div>

										<div class="col-xs-9 col-sm-6 col-lg-2">
											<label class="control-label">Category</label>
											<div class="">
												<select id="category_id-" name="category_id" class="form-control itemsel input-xs wei-add-field category" required>
													<option value="<?php echo $data['product']->category_id; ?>" selected><?php echo findName('product_categories', $data['product']->category_id); ?></option>
													
													<?php echo getCategoryDropdown();?>
												</select>
											</div>
										</div>

										<div class="col-xs-9 col-sm-6 col-lg-2">
											<label class="control-label">Provider</label>
											<div class="">										
												<select name="manufacturer_id" class="form-control input-xs" required>
													<option value="<?php echo $data['product']->manufacturer_id; ?>" selected><?php echo findName('manufacturer', $data['product']->manufacturer_id); ?></option>
													<?php echo getListFromTable('manufacturer'); ?>
												</select>
											</div>
										</div>
										
										<div class="col-xs-9 col-sm-6 col-lg-2">
											<label class="control-label">Currency</label>
											<div class="">
												<select class="form-control input-xs" name="currency_id" id="currency_id" required>
													<option value="<?php echo $data['product']->currency; ?>" selected><?php echo dropDownValue($data['product']->currency_id, 'currency'); ?></option>
													
													<?php echo dropDownList("currency"); ?>
												</select>
											</div>
										</div>
																			
										

									</div>
								</fieldset>

								<fieldset>
									<table class='table table-hover table-striped table-bordered custom-height' id='comparison_table'>
										<thead style='background: #496949 !important; color: #fff;'>
											
											<tr style='background: #496949 !important; color: #fff; text-transform: uppercase;'>
												<th style='text-align: left; font-weight: bold; vertical-align: middle; width:250px; height:50px;'>Plan Details</th>

												<th style='font-weight: bold; vertical-align: middle; text-align: center;'><p></p>
													<div class='alert alert-success alert-dismissible success' id='success' style='display:none;'>
													  <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
													</div>
											</tr>

										</thead>
										<tbody style="font-size: 11px;">
											<tr>
												<td style='text-align: left; font-weight: bold;'>Annual limit</td>

												<td style='text-align: center;'><input class="form-control input-xs" type="text" name="annual_cover" value="<?php echo $data['product']->annual_cover; ?>"></td></tr>

											<tr>
												<td style='text-align: left; font-weight: bold;'>Area of Cover</td>
												<td style='text-align: center;'><input class="form-control input-xs" type="text" name="area_of_cover" value="<?php echo $data['product']->area_of_cover; ?>"></td>
											</tr>

											<tr>
												<td style='text-align: left; font-weight: bold;'>Emergency Treatment Outside Area of Cover</td>
												<td style='text-align: center;'><input class="form-control input-xs" type="text" name="emergency_treatment_outside_area_of_cover" value="<?php echo $data['product']->emergency_treatment_outside_area_of_cover; ?>"></td>
											</tr>

											<tr>
												<td style='text-align: left; font-weight: bold;'>Pre-existing Conditions(2) including Pre-existing Chronic Conditions</td>
												<td style='text-align: center;'><input class="form-control input-xs" type="text" name="preexisting_conditions" value="<?php echo $data['product']->preexisting_conditions; ?>"> </td>
											</tr>

											<tr>
												<td style='text-align: left; font-weight: bold;'>Congenital Conditions</td>
												<td style='text-align: center;'><input class="form-control input-xs" type="text" name="congenital_conditions" value="<?php echo $data['product']->congenital_conditions; ?>"> </td>
											</tr>

												<tr>
												<td style='text-align: left; font-weight: bold;'>Direct Settlement</td>
												<td style='text-align: center;'><input class="form-control input-xs" type="text" name="network" value="<?php echo $data['product']->network; ?>"> </td>
												</tr>

											<tr style="display:none">
												<td style='text-align: left; font-weight: bold;'>Plan Compliance (DHA/ HAAD/ CCHI)</td>
												<td style='text-align: center;'><input class="form-control input-xs" type="text" name="plan_compliance" value="<?php echo $data['product']->plan_compliance; ?>"> </td>
											</tr>

											<tr>
												<td style='text-align: left; font-weight: bold;'>Underwriting Criteria (FMU / MHD)</td>
												<td style='text-align: center;'><input class="form-control input-xs" type="text" name="underwriting_critera" value="<?php echo $data['product']->underwriting_critera; ?>"> </td>
											</tr>

											<tr>
												<td style='text-align: left; font-weight: bold;'>Pricing Model (Community Rated / Individual Rated)</td>
												<td style='text-align: center;'><input class="form-control input-xs" type="text" name="pricing_model" value="<?php echo $data['product']->pricing_model; ?>"> </td>
											</tr>

												<tr>
												<td style='text-align: left; font-weight: bold; color: #fff; background:#8c6a03;' colspan='100%'>IN PATIENT TREATMENT</td>
												</tr>

											<tr>
												<td style='text-align: left; font-weight: bold;'>Deductible / Co-insurance / Excess</td>
												<td style='text-align: center;'><input class="form-control input-xs" type="text" name="ip_deductible" value="<?php echo $data['product']->ip_deductible; ?>"> </td>
											</tr>

											<tr>
												<td style='text-align: left; font-weight: bold;'>Room & Board</td>
												<td style='text-align: center;'><input class="form-control input-xs" type="text" name="ip_room_board" value="<?php echo $data['product']->ip_room_board; ?>"> </td>
											</tr>

											<tr>
												<td style='text-align: left; font-weight: bold;'>Specialist Fees</td>
												<td style='text-align: center;'><input class="form-control input-xs" type="text" name="ip_specialist_fees" value="<?php echo $data['product']->ip_specialist_fees; ?>"> </td>
											</tr>

											<tr>
												<td style='text-align: left; font-weight: bold;'>Surgery and Anesthesia</td>
												<td style='text-align: center;'><input class="form-control input-xs" type="text" name="ip_surgery_anesthesia" value="<?php echo $data['product']->ip_surgery_anesthesia; ?>"> </td>
											</tr>

											<tr>
												<td style='text-align: left; font-weight: bold;'>Diagnostic Tests</td>
												<td style='text-align: center;'><input class="form-control input-xs" type="text" name="ip_diagnostic_tests" value="<?php echo $data['product']->ip_diagnostic_tests; ?>"> </td>
											</tr>

												<tr>
												<td style='text-align: left; font-weight: bold;'>Prescribed Medication</td>
												<td style='text-align: center;'><input class="form-control input-xs" type="text" name="ip_prescribed_medication" value="<?php echo $data['product']->ip_prescribed_medication; ?>"> </td>
											</tr>

											<tr>
												<td style='text-align: left; font-weight: bold;'>Cancer Treatment </td>
												<td style='text-align: center;'><input class="form-control input-xs" type="text" name="ip_cancer_treatment" value="<?php echo $data['product']->ip_cancer_treatment; ?>"> </td>
											</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Organ Transplant</td>
													<td style='text-align: center;'><input class="form-control input-xs" type="text" name="ip_organ_transplant" value="<?php echo $data['product']->ip_organ_transplant; ?>"> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Prosthetic Device</td>
													<td style='text-align: center;'><input class="form-control input-xs" type="text" name="ip_prosthetic_device" value="<?php echo $data['product']->ip_prosthetic_device; ?>"> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Rehabilitation</td>
													<td style='text-align: center;'><input class="form-control input-xs" type="text" name="ip_rehabilitation" value="<?php echo $data['product']->ip_rehabilitation; ?>"> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Home Nursing (immediately after or instead of hospitalization)</td>
													<td style='text-align: center;'><input class="form-control input-xs" type="text" name="ip_home_nursing" value="<?php echo $data['product']->ip_home_nursing; ?>"> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Parent / Companion Accomodation</td>
													<td style='text-align: center;'><input class="form-control input-xs" type="text" name="ip_companian_accomodation" value="<?php echo $data['product']->ip_companian_accomodation; ?>"> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>In-Patient Cash Benefit</td>
													<td style='text-align: center;'><input class="form-control input-xs" type="text" name="ip_cashbenenit" value="<?php echo $data['product']->ip_cashbenenit; ?>"> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold; color: #fff; background:#8c6a03;' colspan='100%'>OUT PATIENT TREATMENT</td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Annual Limit</td>
													<td style='text-align: center;'><input class="form-control input-xs" type="text" name="op_annual_limit" value="<?php echo $data['product']->op_annual_limit; ?>"> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Deductible / Co-insurance / Excess</td>
													<td style='text-align: center;'><input class="form-control input-xs" type="text" name="op_deductible" value="<?php echo $data['product']->op_deductible; ?>"> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>General Practioner Fees</td>
													<td style='text-align: center;'><input class="form-control input-xs" type="text" name="op_gp_fees" value="<?php echo $data['product']->op_gp_fees; ?>"> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Specialist Fees</td>
													<td style='text-align: center;'><input class="form-control input-xs" type="text" name="op_specialist_fees" value="<?php echo $data['product']->op_specialist_fees; ?>"> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Diagnostic Tests</td>
													<td style='text-align: center;'><input class="form-control input-xs" type="text" name="op_diagnostic_tests" value="<?php echo $data['product']->op_diagnostic_tests; ?>"> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Prescribed Medication</td>
													<td style='text-align: center;'><input class="form-control input-xs" type="text" name="op_prescribed_medication" value="<?php echo $data['product']->op_prescribed_medication; ?>"> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Physiotherapy</td>
													<td style='text-align: center;'><input class="form-control input-xs" type="text" name="op_physiotherapy" value="<?php echo $data['product']->op_physiotherapy; ?>"> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Alternative Treatment</td>
													<td style='text-align: center;'><input class="form-control input-xs" type="text" name="op_alternative_treatment" value="<?php echo $data['product']->op_alternative_treatment; ?>"> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Cancer Treatment </td>
													<td style='text-align: center;'><input class="form-control input-xs" type="text" name="op_cancer_treatment" value="<?php echo $data['product']->op_cancer_treatment; ?>"> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold; color: #fff; background:#8c6a03;' colspan='100%'>PSYCHIATRIC COVER</td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Inpatient</td>
													<td style='text-align: center;'><input class="form-control input-xs" type="text" name="psychiatric_inpatient" value="<?php echo $data['product']->psychiatric_inpatient; ?>"> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Outpatient</td>
													<td style='text-align: center;'><input class="form-control input-xs" type="text" name="psychiatric_outpatient" value="<?php echo $data['product']->psychiatric_outpatient; ?>"> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold; color: #fff; background:#8c6a03;' colspan='100%'>MATERNITY COVER</td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Outpatient</td>
													<td style='text-align: center;'><input class="form-control input-xs" type="text" name="maternity_outpatient" value="<?php echo $data['product']->maternity_outpatient; ?>"> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Inpatient</td>
													<td style='text-align: center;'><input class="form-control input-xs" type="text" name="maternity_inpatient" value="<?php echo $data['product']->maternity_inpatient; ?>"> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>C-Section</td>
													<td style='text-align: center;'><input class="form-control input-xs" type="text" name="maternity_c_section" value="<?php echo $data['product']->maternity_c_section; ?>"> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Complication</td>
													<td style='text-align: center;'><input class="form-control input-xs" type="text" name="maternity_complication" value="<?php echo $data['product']->maternity_complication; ?>"> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>New Born Cover</td>
													<td style='text-align: center;'><input class="form-control input-xs" type="text" name="maternity_new_born" value="<?php echo $data['product']->ip_surgery_anesthesia; ?>"> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold; color: #fff; background:#8c6a03;' colspan='100%'>ADDITIONAL BENEFITS</td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Dental</td>
													<td style='text-align: center;'><input class="form-control input-xs" type="text" name="dental" value="<?php echo $data['product']->dental; ?>"> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Optical</td>
													<td style='text-align: center;'><input class="form-control input-xs" type="text" name="optical" value="<?php echo $data['product']->optical; ?>"> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Annual Health Check-up / Wellness Screening</td>
													<td style='text-align: center;'><input class="form-control input-xs" type="text" name="wellness" value="<?php echo $data['product']->wellness; ?>"> </td>
												</tr>

												<tr class="hidden">
													<td style='text-align: left; font-weight: bold;'>Vaccinations (DHA / MOH approved)</td>
													<td style='text-align: center;'><input class="form-control input-xs" type="text" name="vaccinations_dha_moh_approved" value="<?php echo $data['product']->vaccinations_dha_moh_approved; ?>"> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Vaccinations (other including Travel Vaccinations)</td>
													<td style='text-align: center;'><input class="form-control input-xs" type="text" name="vaccinations_others_travel" value="<?php echo $data['product']->vaccinations_others_travel; ?>"> </td>
												</tr>
												
												<tr>
													<td style='text-align: left; font-weight: bold; color: #fff; background:#8c6a03;' colspan='100%'>EVACUATION  & REPATRIATION</td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Evacuation</td>
													<td style='text-align: center;'><input class="form-control input-xs" type="text" name="evacuation" value="<?php echo $data['product']->evacuation; ?>"> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Repatriation</td>
													<td style='text-align: center;'><input class="form-control input-xs" type="text" name="repatriation" value="<?php echo $data['product']->repatriation; ?>"> </td>
												</tr>

												<tr class="hidden">
													<td style='text-align: left; font-weight: bold; color: #fff; background:#8c6a03;' colspan='100%'>CO-INSURANCE (Inside UAE)</td>
												</tr>

												<tr class="hidden">
													<td style='text-align: left; font-weight: bold;'>Inside the Network</td>
													<td style='text-align: center;'><input class="form-control input-xs" type="text" name="co_insur_uae_inside_network" value="<?php echo $data['product']->co_insur_uae_inside_network; ?>"> </td>
												</tr>

												<tr class="hidden">
													<td style='text-align: left; font-weight: bold;'>Outside the Network</td>
													<td style='text-align: center;'><input class="form-control input-xs" type="text" name="co_insur_uae_outside_network" value="<?php echo $data['product']->co_insur_uae_outside_network; ?>"> </td>
												</tr>

												<tr class="hidden">
													<td style='text-align: left; font-weight: bold; color: #fff; background:#8c6a03;' colspan='100%'>CO-INSURANCE (Outside UAE)</td>
												</tr>

												<tr class="hidden">
													<td style='text-align: left; font-weight: bold;'>Inside the Network</td>
													<td style='text-align: center;'><input class="form-control input-xs" type="text" name="co_insur_outsideuae_inside_network" value="<?php echo $data['product']->co_insur_outsideuae_inside_network; ?>"> </td>
												</tr>

												<tr class="hidden">
													<td style='text-align: left; font-weight: bold;'>Outside the Network</td>
													<td style='text-align: center;'><input class="form-control input-xs" type="text" name="co_insur_outsideuae_outside_network" value="<?php echo $data['product']->co_insur_outsideuae_outside_network; ?>"> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold; color: #fff; background:#8c6a03;' colspan='100%'>PREMIUM SUMMARY</td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Total No of Members (Emp + Dependents)</td>
													<td style='text-align: center;'><input class="form-control input-xs" type="text" name="total_members" value="<?php echo $data['product']->total_members; ?>"> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Premium Payment Mode</td>
													<td style='text-align: center;'><input class="form-control input-xs" type="text" name="payment_mode" value="<?php echo $data['product']->payment_mode; ?>"></td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Expiring Premium</td>
													<td style='text-align: center;'><input class="form-control input-xs" type="text" name="expiring_premium" value="<?php echo $data['product']->expiring_premium; ?>"> </td>
												</tr>

												<tr class='' style="background: #d2ebf4 !important;">
													<td style='text-align: left; font-weight: bold;'>Proposed Annual / Renewal Premium (excluding VAT) <span style="color: purple;">AED</span></td>
													<td style='text-align: center;'><input class="form-control input-xs" type="text" name="proposed_annual_renewal_premium" value="<?php echo $data['product']->proposed_annual_renewal_premium; ?>"></td>
												</tr>

											
											<tr style="background: #d2ebf4 !important;">
												<td style='text-align: left; font-weight: bold;'>% Increase</td>
												<td style='text-align: center;'><input class="form-control input-xs" type="text" name="increase" value="<?php echo $data['product']->increase; ?>"></td>
											</tr>

										</tbody>
									</table>
								</fieldset>

								<fieldset>
								<legend>More Details</legend>
									<div class="form-group">								
									
									<div class="col-xs-9 col-sm-6 col-lg-7">
										<label class="control-label">Comment</label>
										<div class="">
											<textarea class="form-control input-xs" name="comment" required><?php echo $data['product']->comment; ?></textarea>
										</div>
									</div>


									<!-- <div class="col-xs-9 col-sm-6 col-lg-3">
										<label class="control-label">notes</label>
										<div class="">
										<textarea class="form-control" name="notes" rows="1"><?php ?></textarea>
										</div>
									</div>
 -->

								</div>
								</fieldset>


								<fieldset>
									<legend>Other</legend>
								<div class="form-group">

										<div class="col-xs-9 col-sm-6 col-lg-4">
										<label class="control-label">Assigned to</label>
										<div class="">

										

									<select class="form-control" id="assigned_to" name="assigned_to">
											
										<option selected value="<?php echo $data['product']->assigned_to; ?>"><?php echo findName('users', $data['product']->assigned_to); ?></option>
										
										<?php 
												echo getListFromTable('users');
											
										?>
									</select> 
												
										
											<p class="note"><strong>Note:</strong> Default is logged in user.</p>
										</div>


									</div>

									<div class="col-xs-8 col-sm-6 col-lg-2 col-lg-offset-1">
									
									<a href=""><img class="img-responsive edit_profile_image" src="<?php echo Upload::showImage($data['product']->image); ?>"></a>
									</div>


									<div class="col-xs-9 col-sm-6 col-lg-3 col-lg-offset-1 hidden">
										<label class="control-label">Upload Image</label>
										<div class="">

										<input type="file" name="image">
										</div>
									</div>

								</div>
								</fieldset>
								

								<div class="form-actions">
									<div class="row">
										<div class="col-md-12">
										 <a href="<?php echo htmlspecialchars(URLROOT . current_class($this) ); ?>" class="btn btn-default" role="button">Cancel</a>
											
								<button class="btn btn-primary" type="submit" name="update_product">
												<i class="fa fa-save"></i>
												Submit
											</button>
										</div>
									</div>
								</div>
	
							</form>
	
						</div>
						<!-- end widget content -->
	
					</div>
					<!-- end widget div -->
	
				</div>
				<!-- end widget -->
	
				<!-- Widget ID (each widget will need unique ID)-->

				<!-- end widget -->
	

	
			</article>

	
		</div>
	
		<!-- end row -->
	
		<!-- row -->
		<div class="row">
	
			<!-- NEW WIDGET START -->
			
			<!-- WIDGET END -->
	
		</div>
		<!-- end row -->
	
		<!-- row -->
		<div class="row">
	
			
					
		</div>
		<!-- end row -->		
	
	
	</section>
	<!-- end widget grid -->

</div>

<!-- PAGE FOOTER -->
<?php include (APPROOT . "/views/inc/admin_footer.php"); ?>
<!-- END PAGE FOOTER -->