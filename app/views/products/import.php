<?php include (APPROOT . "/views/inc/admin_header.php"); ?>
	
	<!-- widget grid -->
	<section id="widget-grid" class="col-">
	
		<!-- <div class="well">
			
		</div> -->
	
		<!-- row -->
		<div class="row">
	
			<!-- NEW WIDGET START -->
			<article class="col-md-12">
	
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false">
					<!-- widget options:
					usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
	
					data-widget-colorbutton="false"
					data-widget-editbutton="false"
					data-widget-togglebutton="false"
					data-widget-deletebutton="false"
					data-widget-fullscreenbutton="false"
					data-widget-custombutton="false"
					data-widget-collapsed="true"
					data-widget-sortable="false"
	
					-->
					<header>
						<span class="widget-icon"> <i class="fa fa-eye"></i> </span>
						<h2>Import</h2>
	
					</header>
	
					<!-- widget div-->
					<div>
	
						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
	
						</div>
						<!-- end widget edit box -->
	
						<!-- widget content -->
						<div class="widget-body">

							<form class="form-horizontal" method="post" action="<?php echo htmlspecialchars( URLROOT . current_class($this) . '/import'); ?>" enctype="multipart/form-data">

								<fieldset>
									<!-- <legend>Product Details <?php ?></legend> -->

									<div class="form-group">
										<label class="col-md-3 control-label text-left">Select New Plans to Import <sup>*</sup></label>
										<div class="col-md-7">
											<input type="file" name="file" id="file" class="input-large" required>
										</div>
									</div>

									<div class="form-group">
										<label class="col-md-3 control-label text-left">Comments</label>
										<div class="col-md-7">
											<textarea class="form-control input-xs" rows="2" name="comment" required></textarea>
										</div>
									</div>

					            </fieldset>

						</div>

							<fieldset class="hidden">
								<legend>Other</legend>
								<div class="form-group">
										<div class="col-xs-9 col-sm-6 col-lg-4">
										<label class="control-label">Assigned to</label>
										<div class="">											
											<select class="form-control" id="assigned_to" name="assigned_to">
												<option selected value="<?php echo $_SESSION['user_id']; ?>"><?php echo findName('users', $_SESSION['user_id']); ?>
												</option>
												<?php echo getListFromTable('users'); ?>
											</select>										
											<p class="note"><strong>Note:</strong> Default is logged in user.</p>
										</div>
										
									</div>

								</div>
							</fieldset>
								

								<div class="form-actions">
									<div class="row">
										<div class="col-md-12">
										 <a href="<?php echo htmlspecialchars( URLROOT . current_class($this) ); ?>" class="btn btn-default" role="button">Cancel</a>
											
								<button class="btn btn-primary" type="submit" name="import">
												<i class="fa fa-save"></i>
												Submit
											</button>
										</div>
									</div>
								</div>
	
							</form>
	
						</div>
						<!-- end widget content -->
	
					</div>
					<!-- end widget div -->
	
				</div>
				<!-- end widget -->
	
				<!-- Widget ID (each widget will need unique ID)-->

				<!-- end widget -->
	

	
			</article>

	
		</div>
	
		<!-- end row -->
	
		<!-- row -->
		<div class="row">
	
			<!-- NEW WIDGET START -->
			
			<!-- WIDGET END -->
	
		</div>
		<!-- end row -->
	
		<!-- row -->
		<div class="row">
	
			
					
		</div>
		<!-- end row -->		
	
	
	</section>
	<!-- end widget grid -->

<!-- PAGE FOOTER -->
<?php include (APPROOT . "/views/inc/admin_footer.php"); ?>
<!-- END PAGE FOOTER -->