<?php include (APPROOT . "/views/inc/admin_header.php"); ?>
<div class="module">
	<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
		<h4><?php echo $data['detail']->name; ?>  - ID <?php echo $data['detail']->id; ?></h4>
	</div>

	<?php include (APPROOT . "/views/inc/admin_detail_toolbar.php"); ?>
</div>

	<!-- widget grid -->
	<section id="widget-grid-1" class="">



		<!-- row -->
		<div class="row">
			
			<!-- NEW WIDGET START -->
			<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

					<!-- widget options:
						usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
						
						data-widget-colorbutton="false"	
						data-widget-editbutton="false"
						data-widget-togglebutton="false"
						data-widget-deletebutton="false"
						data-widget-fullscreenbutton="false"
						data-widget-custombutton="false"
						data-widget-collapsed="true" 
						data-widget-sortable="false"
						
					-->
					<header>
						<span class="widget-icon"> <i class="fa"></i> </span>
						<h2 class="custom-h2">Plan Details </h2>				
						
					</header>

					<!-- widget div-->
					<div>
						
						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
							<input class="form-control" type="text">	
						</div>
						<!-- end widget edit box -->
						
						<!-- widget content -->
						<div class="widget-body no-padding detail508">



							<fieldset>
								<div class="table-responsive">

									<table class='table table-hover table-striped table-bordered custom-height' id='comparison_table'>
										<thead style='background: #496949 !important; color: #fff;'>
											<tr style='background: #496949 !important; color: #fff; text-transform: uppercase;'>
												<th style='text-align: left; font-weight: bold; vertical-align: middle; min-width:175px !important; height:45px;'>Service Provider</th>

												<?php
													
													$manufacturer = findName('manufacturer', $data['detail']->manufacturer_id);
													echo "<th style='background: none; font-weight: bold; vertical-align: middle; text-align: center; width: 200px;'>$manufacturer</th>";													
												?>

											</tr>

											<tr style='background: #496949 !important; color: #fff; text-transform: uppercase;'>
												<th style='text-align: left; font-weight: bold; vertical-align: middle; width:200px; height:50px;'>Plan</th>
												
												<th style='font-weight: bold; vertical-align: middle; text-align: center;'><p><?php echo $data['detail']->name; ?></p>
												<div class='alert alert-success alert-dismissible success' id='success' style='display:none;'>
												  <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
												</div>															
												</th>
											</tr>
										</thead>
											<tbody>
											
												<tr>
													<td style='text-align: left; font-weight: bold;'>Currency</td>				
													<td style='text-align: center;'><?php echo dropdownValue($data['detail']->currency_id, 'currency'); ?></td>
												</tr>
											
												<tr>
													<td style='text-align: left; font-weight: bold;'>Annual limit</td>				
													<td style='text-align: center;'><?php echo $data['detail']->annual_cover; ?></td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Area of Cover</td>
													<td style='text-align: center;'><?php echo $data['detail']->area_of_cover; ?></td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Emergency Treatment Outside Area of Cover</td>
													<td style='text-align: center;'><?php echo $data['detail']->emergency_treatment_outside_area_of_cover; ?></td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Pre-existing Conditions(2) including Pre-existing Chronic Conditions</td>
													<td style='text-align: center;'><?php echo $data['detail']->preexisting_conditions; ?> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Congenital Conditions</td>
													<td style='text-align: center;'><?php echo $data['detail']->congenital_conditions; ?> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Direct Settlement</td>
													<td style='text-align: center;'><?php echo $data['detail']->network; ?> </td>
												</tr>

												<tr style="display:none">
													<td style='text-align: left; font-weight: bold;'>Plan Compliance (DHA/ HAAD/ CCHI)</td>
													<td style='text-align: center;'><?php echo $data['detail']->plan_compliance; ?> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Underwriting Criteria (FMU / MHD)</td>
													<td style='text-align: center;'><?php echo $data['detail']->underwriting_critera; ?> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Pricing Model (Experience Rated / Community Rated / Individual Rated)</td>
													<td style='text-align: center;'><?php echo $data['detail']->pricing_model; ?> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold; color: #fff; background:#8c6a03;' colspan='100%'>IN PATIENT TREATMENT</td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Deductible / Co-insurance / Excess</td>
													<td style='text-align: center;'><?php echo $data['detail']->ip_deductible; ?> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Room & Board</td>
													<td style='text-align: center;'><?php echo $data['detail']->ip_room_board; ?> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Specialist Fees</td>
													<td style='text-align: center;'><?php echo $data['detail']->ip_specialist_fees; ?> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Specialist Fees</td>
													<td style='text-align: center;'><?php echo $data['detail']->ip_specialist_fees; ?> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Surgery and Anesthesia</td>
													<td style='text-align: center;'><?php echo $data['detail']->ip_surgery_anesthesia; ?> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Diagnostic Tests</td>
													<td style='text-align: center;'><?php echo $data['detail']->ip_diagnostic_tests; ?> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Prescribed Medication</td>
													<td style='text-align: center;'><?php echo $data['detail']->ip_prescribed_medication; ?> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Cancer Treatment </td>
													<td style='text-align: center;'><?php echo $data['detail']->ip_cancer_treatment; ?> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Organ Transplant</td>
													<td style='text-align: center;'><?php echo $data['detail']->ip_organ_transplant; ?> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Prosthetic Device</td>
													<td style='text-align: center;'><?php echo $data['detail']->ip_prosthetic_device; ?> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Rehabilitation</td>
													<td style='text-align: center;'><?php echo $data['detail']->ip_rehabilitation; ?> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Home Nursing (immediately after or instead of hospitalization)</td>
													<td style='text-align: center;'><?php echo $data['detail']->ip_home_nursing; ?> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Parent / Companion Accomodation</td>
													<td style='text-align: center;'><?php echo $data['detail']->ip_companian_accomodation; ?> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>In-Patient Cash Benefit</td>
													<td style='text-align: center;'><?php echo $data['detail']->ip_cashbenenit; ?> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold; color: #fff; background:#8c6a03;' colspan='100%'>OUT PATIENT TREATMENT</td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Annual Limit</td>
													<td style='text-align: center;'><?php echo $data['detail']->op_annual_limit; ?> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Deductible / Co-insurance / Excess</td>
													<td style='text-align: center;'><?php echo $data['detail']->op_deductible; ?> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>General Practioner Fees</td>
													<td style='text-align: center;'><?php echo $data['detail']->op_gp_fees; ?> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Specialist Fees</td>
													<td style='text-align: center;'><?php echo $data['detail']->op_specialist_fees; ?> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Diagnostic Tests</td>
													<td style='text-align: center;'><?php echo $data['detail']->op_diagnostic_tests; ?> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Prescribed Medication</td>
													<td style='text-align: center;'><?php echo $data['detail']->op_prescribed_medication; ?> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Physiotherapy</td>
													<td style='text-align: center;'><?php echo $data['detail']->op_physiotherapy; ?> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Alternative Treatment</td>
													<td style='text-align: center;'><?php echo $data['detail']->op_alternative_treatment; ?> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Cancer Treatment </td>
													<td style='text-align: center;'><?php echo $data['detail']->op_cancer_treatment; ?> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Prosthetic Device & Consumed Medical Equipment</td>
													<td style='text-align: center;'><?php echo $data['detail']->op_prosthetic_device; ?> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold; color: #fff; background:#8c6a03;' colspan='100%'>PSYCHIATRIC COVER</td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Inpatient</td>
													<td style='text-align: center;'><?php echo $data['detail']->psychiatric_inpatient; ?> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Outpatient</td>
													<td style='text-align: center;'><?php echo $data['detail']->psychiatric_outpatient; ?> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold; color: #fff; background:#8c6a03;' colspan='100%'>MATERNITY COVER</td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Outpatient</td>
													<td style='text-align: center;'><?php echo $data['detail']->maternity_outpatient; ?> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Inpatient</td>
													<td style='text-align: center;'><?php echo $data['detail']->maternity_inpatient; ?> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>C-Section</td>
													<td style='text-align: center;'><?php echo $data['detail']->maternity_c_section; ?> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Complication</td>
													<td style='text-align: center;'><?php echo $data['detail']->maternity_complication; ?> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>New Born Cover</td>
													<td style='text-align: center;'><?php echo $data['detail']->maternity_new_born; ?> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold; color: #fff; background:#8c6a03;' colspan='100%'>ADDITIONAL BENEFITS</td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Dental</td>
													<td style='text-align: center;'><?php echo $data['detail']->dental; ?> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Optical</td>
													<td style='text-align: center;'><?php echo $data['detail']->optical; ?> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Annual Health Check-up / Wellness Screening</td>
													<td style='text-align: center;'><?php echo $data['detail']->wellness; ?> </td>
												</tr>

												<tr class="hidden">
													<td style='text-align: left; font-weight: bold;'>Vaccinations (DHA / MOH approved)</td>
													<td style='text-align: center;'><?php echo $data['detail']->vaccinations_dha_moh_approved; ?> </td>
												</tr>

												<tr>
													<td style='text-align: left; font-weight: bold;'>Vaccinations (other including Travel Vaccinations)</td>
													<td style='text-align: center;'><?php echo $data['detail']->vaccinations_others_travel; ?> </td>
												</tr>

													<tr>
													<td style='text-align: left; font-weight: bold; color: #fff; background:#8c6a03;' colspan='100%'>EVACUATION  & REPATRIATION</td>
													</tr>

													<tr>
														<td style='text-align: left; font-weight: bold;'>Evacuation</td>
														<td style='text-align: center;'><?php echo $data['detail']->evacuation; ?> </td>
													</tr>

													<tr>
														<td style='text-align: left; font-weight: bold;'>Repatriation</td>
														<td style='text-align: center;'><?php echo $data['detail']->repatriation; ?> </td>
													</tr>

													<tr class="hidden">
														<td style='text-align: left; font-weight: bold; color: #fff; background:#8c6a03;' colspan='100%'>CO-INSURANCE (Inside UAE)</td>
													</tr>

													<tr class="hidden">
														<td style='text-align: left; font-weight: bold;'>Inside the Network</td>
														<td style='text-align: center;'><?php echo $data['detail']->co_insur_uae_inside_network; ?> </td>
													</tr class="hidden">

													<tr>
														<td style='text-align: left; font-weight: bold;'>Outside the Network</td>
														<td style='text-align: center;'><?php echo $data['detail']->co_insur_uae_outside_network; ?> </td>
													</tr>

													<tr class="hidden">
													<td style='text-align: left; font-weight: bold; color: #fff; background:#8c6a03;' colspan='100%'>CO-INSURANCE (Outside UAE)</td>
													</tr>

													<tr class="hidden">
														<td style='text-align: left; font-weight: bold;'>Inside the Network</td>
														<td style='text-align: center;'><?php echo $data['detail']->co_insur_outsideuae_inside_network; ?> </td>
													</tr>

													<tr class="hidden">
														<td style='text-align: left; font-weight: bold;'>Outside the Network</td>
														<td style='text-align: center;'><?php echo $data['detail']->co_insur_outsideuae_outside_network; ?> </td>
													</tr>

													<tr>
													<td style='text-align: left; font-weight: bold; color: #fff; background:#8c6a03;' colspan='100%'>PREMIUM SUMMARY</td>
													</tr>

													<tr>
														<td style='text-align: left; font-weight: bold;'>Total No of Members (Emp + Dependents)</td>
														<td style='text-align: center;'><?php echo $data['detail']->total_members; ?> </td>
													</tr>

													<tr>
														<td style='text-align: left; font-weight: bold;'>Premium Payment Mode</td>
														<td style='text-align: center;'><?php echo $data['detail']->payment_mode; ?></td>
													</tr>

													<tr>
														<td style='text-align: left; font-weight: bold;'>Expiring Premium</td>
														<td style='text-align: center;'><?php echo $data['detail']->expiring_premium; ?> </td>
													</tr>

													<tr class='' style="background: #d2ebf4 !important;">
														<td style='text-align: left; font-weight: bold;'>Proposed Annual / Renewal Premium (excluding VAT) <span style="color: purple;">AED</span></td>
														<td style='text-align: center;'><?php echo $data['detail']->proposed_annual_renewal_premium; ?></td>
													</tr>

												<tr class=''>
													<td style='text-align: left; font-weight: bold;'>Proposed Annual / Renewal Premium (excluding VAT) <span style="color: red;">USD</span> </td>
													
														<?php $usd = $data['detail']->proposed_annual_renewal_premium*3.67; ?>
														<td style='text-align: center;'><?php echo $usd; ?></td>
												</tr>

												<tr style="background: #d2ebf4 !important;">
													<td style='text-align: left; font-weight: bold;'>% Increase</td>
													<td style='text-align: center;'><?php echo $data['detail']->increase; ?></td>
												</tr>

											</tbody>
									</table>
								</div>

							</fieldset>

							

							
						

						</div>
						<!-- end widget content -->
						
					</div>
					<!-- end widget div -->
					
				</div>
				<!-- end widget -->

			</article>
			<!-- WIDGET END -->
			
		</div>

		<!-- end row -->

		<!-- row -->

		<div class="row">

			<!-- a blank row to get started -->
			<div class="col-sm-12">
				<!-- your contents here -->
			</div>
				
		</div>

		<!-- end row -->

	</section>
	<!-- end widget grid -->



	<!-- row -->
	<div class="row">

		<!-- <h2 class="row-seperator-header"><i class="fa fa-plus"></i> Customized Tabs </h2> -->

		<!-- NEW WIDGET START -->
		<article class="col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget" id="wid-id-3" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-custombutton="false" data-widget-sortable="false">
				<!-- widget options:
				usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

				data-widget-colorbutton="false"
				data-widget-editbutton="false"
				data-widget-togglebutton="false"
				data-widget-deletebutton="false"
				data-widget-fullscreenbutton="false"
				data-widget-custombutton="false"
				data-widget-collapsed="true"
				data-widget-sortable="false"

				-->
				<!-- <header>
					<span class="widget-icon"> <i class="fa fa-comments"></i> </span>
					<h2>Default Tabs with border </h2>

				</header> -->

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->

					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body">
						<!-- 				
						<p>
							Tabs inside
							<code>
								.jarviswidget .well
							</code>
							(Bordered Tabs)
						</p> -->
						<!-- <hr class="simple"> -->
						<ul id="myTab1" class="nav nav-tabs bordered tabs-noborder">
							
							<li>
								<a href="#s2" data-toggle="tab"> More Details </a>
							</li>

							<!-- <li>
								<a href="#s3" data-toggle="tab"> History </a>
							</li> -->

						
							<li class="pull-right">
								<a href="javascript:void(0);">
								<div class="sparkline txt-color-pinkDark text-align-right" data-sparkline-height="18px" data-sparkline-width="90px" data-sparkline-barwidth="7">
									5,10,6,7,4,3
								</div> </a>
							</li>
						</ul>

						<div id="myTabContent1" class="tab-content padding-10">

							<div class="tab-pane fade in active" id="s1">

								
							</div>

							<div class="tab-pane fade" id="s2">
								

							</div>

							<div class="tab-pane fade" id="s3">

								<!-- widget grid -->
								<section id="widget-grid-1" class="">



									<!-- row -->
									<div class="row">
										
										<!-- NEW WIDGET START -->
										<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

											<!-- Widget ID (each widget will need unique ID)-->
											<div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

												<!-- widget options:
													usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
													
													data-widget-colorbutton="false"	
													data-widget-editbutton="false"
													data-widget-togglebutton="false"
													data-widget-deletebutton="false"
													data-widget-fullscreenbutton="false"
													data-widget-custombutton="false"
													data-widget-collapsed="true" 
													data-widget-sortable="false"
													
												-->
												<header>
													<span class="widget-icon"> <i class="fa"></i> </span>
													<h2 class="custom-h2">Address Details </h2>				
													
												</header>

												<!-- widget div-->
												<div>
													
													<!-- widget edit box -->
													<div class="jarviswidget-editbox">
														<!-- This area used as dropdown edit box -->
														<input class="form-control" type="text">	
													</div>
													<!-- end widget edit box -->
													
													<!-- widget content -->
													<div class="widget-body no-padding detail508">

														<table cellpadding="5" cellspacing="0" border="0" class="table table-condensed">
													       <!--  <tr>
													            <td scope="col" style="width:12.5%">Area</td>
													            <td style="width: 35.5%">value</td>
													            <td scope="col" style="width:12.5%">Address</td>
													            <td style="width: 35.5%">value</td>

													        </tr>
													         
													        <tr>
													            <td scope="col" style="width:12.5%">City</td>
													            <td style="width: 35.5%">value</td>
													            <td scope="col" style="width:12.5%">State</td>
													            <td style="width: 35.5%">value</td>

													        </tr>

													        <tr>
													            <td scope="col" style="width:12.5%">PO Box</td>
													            <td style="width: 35.5%">value</td>
													            <td scope="col" style="width:12.5%">Country</td>
													            <td style="width: 35.5%">value</td>

													        </tr> -->

													       
												    </table>
							
														
													

													</div>
													<!-- end widget content -->
													
												</div>
												<!-- end widget div -->
												
											</div>
											<!-- end widget -->

										</article>
										<!-- WIDGET END -->
										
									</div>

									<!-- end row -->

									<!-- row -->

									<div class="row">

										<!-- a blank row to get started -->
										<div class="col-sm-12">
											<!-- your contents here -->
										</div>
											
									</div>

									<!-- end row -->

								</section>
								<!-- end widget grid -->
								
								<!-- widget grid -->
								<section id="widget-grid-1" class="">



									<!-- row -->
									<div class="row">
										
										<!-- NEW WIDGET START -->
										<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

											<!-- Widget ID (each widget will need unique ID)-->
											<div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

												<!-- widget options:
													usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
													
													data-widget-colorbutton="false"	
													data-widget-editbutton="false"
													data-widget-togglebutton="false"
													data-widget-deletebutton="false"
													data-widget-fullscreenbutton="false"
													data-widget-custombutton="false"
													data-widget-collapsed="true" 
													data-widget-sortable="false"
													
												-->
												<header>
													<span class="widget-icon"> <i class="fa"></i> </span>
													<h2 class="custom-h2">More Details </h2>				
													
												</header>

												<!-- widget div-->
												<div>
													
													<!-- widget edit box -->
													<div class="jarviswidget-editbox">
														<!-- This area used as dropdown edit box -->
														<input class="form-control" type="text">	
													</div>
													<!-- end widget edit box -->
													
													<!-- widget content -->
													<div class="widget-body no-padding detail508">

														<table cellpadding="5" cellspacing="0" border="0" class="table table-condensed">
													        <!-- <tr>
													            <td scope="col" style="width:12.5%">Description</td>
													            <td style="width: 35.5%">value</td>
													            <td scope="col" style="width:12.5%">Assigned To</td>
													            <td style="width: 35.5%">value</td>

													        </tr>
													         
													        <tr>
													            <td scope="col" style="width:12.5%">Date Entered</td>
													            <td style="width: 35.5%">value</td>
													            <td scope="col" style="width:12.5%">Created By</td>
													            <td style="width: 35.5%">value</td>

													        </tr>

													        <tr>
													            <td scope="col" style="width:12.5%">Modified By</td>
													            <td style="width: 35.5%">value</td>
													            <td scope="col" style="width:12.5%">Date Modified</td>
													            <td style="width: 35.5%">value</td>

													        </tr>

													        <tr>
													            <td scope="col" style="width:12.5%">Image</td>
													            <td style="width: 35.5%">value</td>
													            <td scope="col" style="width:12.5%"></td>
													            <td style="width: 35.5%">value</td>

													        </tr> -->

													       
												    </table>
							
														
													

													</div>
													<!-- end widget content -->
													
												</div>
												<!-- end widget div -->
												
											</div>
											<!-- end widget -->

										</article>
										<!-- WIDGET END -->
										
									</div>

									<!-- end row -->

									<!-- row -->

									<div class="row">

										<!-- a blank row to get started -->
										<div class="col-sm-12">
											<!-- your contents here -->
										</div>
											
									</div>

									<!-- end row -->

								</section>
								<!-- end widget grid -->

							</div>
							
						</div>

					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->

			</div>
			<!-- end widget -->


		</article>
		<!-- WIDGET END -->



	</div>
	<!-- end row -->


<!-- PAGE FOOTER -->
<?php include (APPROOT . "/views/inc/admin_footer.php"); ?>
<!-- END PAGE FOOTER -->
