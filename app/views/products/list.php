<?php include (APPROOT . "/views/inc/admin_header.php"); ?>

	<!-- START CONTENT  -->
	<section id="widget-grid" class="">

		<!-- row -->
		<div class="row">

			<!-- NEW WIDGET START -->
			<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
					<!-- widget options:
					usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

					data-widget-colorbutton="false"
					data-widget-editbutton="false"
					data-widget-togglebutton="false"
					data-widget-deletebutton="false"
					data-widget-fullscreenbutton="false"
					data-widget-custombutton="false"
					data-widget-collapsed="true"
					data-widget-sortable="false"

					-->

					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2>List Plans</h2>

					</header>

					<!-- widget div-->
					<div>

						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
						</div>
						<!-- end widget edit box -->

						<!-- widget content -->
						<div class="widget-body no-padding">
							<form method="post" action='<?php echo htmlspecialchars( URLROOT . current_class($this) . "/action"); ?>'>
						
							<table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
		
					        <thead>
					            <tr>
					            	<th data-hide="phone,tablet"><input type="checkbox" id="selectAllboxes"></th>
			                    	<th data-hide="phone,tablet">ID</th>
				                  <th data-class="expand" class="col-md-2">Name</th>
				                  <th data-class="expand">Provider</th>
				                  <th data-hide="phone,tablet">Type</th>						                    
				                  <th data-hide="phone,tablet">Category</th>
				                  <th data-hide="phone,tablet">Coverage</th>
								  <th data-hide="phone,tablet">Currency</th>
				                  <th data-hide="phone,tablet">Annaul Premium</th>
				                  <th data-hide="phone,tablet">Image</th>
				                  <th data-hide="phone,tablet">Date Entered</th>
				                  <th data-hide="phone,tablet">Created By</th>						                    
				                  <th data-hide="phone,tablet">SelectAction</th>
					            </tr>						         
					        </thead>

					        <tbody>								
						
									<?php foreach($data['list'] as $record) : ?>
									<tr>
										<td><span><input type='checkbox' class='checkBoxes' name='checkBoxArray[]' value="<?php echo $record->id; ?>"></span></td>

										<td><?php echo $record->id; ?></td>
										<td><a href="<?php echo URLROOT . current_class($this); ?>/detail/<?php echo $record->id; ?>"><?php echo $record->name; ?></a></td>
										<td><?php echo findName('manufacturer', $record->manufacturer_id); ?></td>
										<td><?php echo dropdownValue($record->plan_type, 'product_type'); ?></td>
										<td><?php echo findName('product_categories', $record->category_id); ?></td>
										<td><?php echo $record->area_of_cover; ?></td>
										<td><?php if($record->currency_id){
											echo dropdownValue($record->currency_id, 'currency');
										} ?></td>
										
										<td><?php echo $record->proposed_annual_renewal_premium; ?></td>
										<td><img class="profile_image" src="<?php echo Upload::showImage($record->image); ?>" alt=""></td>
										<td><?php echo UserTimeZone($record->date_entered); ?></td>
										<td><?php echo findName('users', $record->created_by); ?></td>
										<td>
											<a href="<?php echo URLROOT . current_class($this); ?>/edit/<?php echo $record->id; ?>"><i class="fa fa-lg fa-fw fa-pencil-square-o"></i></a>

											<form style="display: inline;" action="<?php echo URLROOT . current_class($this); ?>/delete/<?php echo $record->id; ?>" method="post">
												<!-- <input type="submit" value="DELETE" class="btn btn-danger"> -->
												<button onClick="return confirm('Are you sure you want to delete <?php echo $record->id; ?>?');" style="background: none; border: none;" class="fa fa-lg fa-trash-o"></button>
											</form>
										</td>
									</tr>
									<?php endforeach; ?>
									
								</tbody>
													
							</table>
							</form>

						</div>
						<!-- end widget content -->

					</div>
					<!-- end widget div -->

				</div>
				<!-- end widget -->

			</article>
			<!-- WIDGET END -->
			
		</div>

		<!-- end row -->

		<!-- row -->

		<div class="row">

			<!-- a blank row to get started -->
			<div class="col-sm-12">
				<!-- your contents here -->
			</div>
				
		</div>

		<!-- end row -->

	</section>
	<!-- END CONTENT -->	


<!-- PAGE FOOTER -->
<?php include (APPROOT . "/views/inc/admin_footer.php"); ?>
<!-- END PAGE FOOTER -->