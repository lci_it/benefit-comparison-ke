<?php

	class QuoteDetail extends Crud {

		protected static $table = 'quotes_products';

		public function checkQuoteProduct($quote_id, $product_id){
			$this->db->query("SELECT * FROM  " . static::$table . " WHERE quote_id=:quote_id AND product_id=:product_id");
			$this->db->bind(':quote_id', $quote_id);
			$this->db->bind(':product_id', $product_id);
			return $this->db->resultSet();
		}

		public function removeQuoteProduct($quote_id, $product_id){
			$this->db->query("DELETE FROM  " . static::$table . " WHERE quote_id=:quote_id AND product_id=:product_id LIMIT 1");
			$this->db->bind(':quote_id', $quote_id);
			// $this->db->bind(':created_by', $created_by);
			$this->db->bind(':product_id', $product_id);
			if($this->db->execute()){
				return true;
			} else {
				return false;
			}
		}

		// On saving a quote or comparison, delete records from comparedetail table
		public function deleteFromCompare($created_by, $product_id){
			$this->db->query("DELETE FROM  " . static::$table . " WHERE created_by=:created_by AND product_id=:product_id LIMIT 1");
			$this->db->bind(':created_by', $created_by);
			$this->db->bind(':product_id', $product_id);
			if($this->db->execute()){
				return true;
			} else {
				return false;
			}
		}

		// count products in quotes_products table
		public function countQuoteProducts($quote_id, $created_by, $product_id){
			$result_set = $this->db->query("SELECT COUNT(*) FROM " . static::$table . " WHERE quote_id=$quote_id AND created_by=$created_by AND product_id=$product_id");
			return $result_set;

		}

		// count total products related to quote
		public function countTotalProducts($quote_id){
			$this->db->query("SELECT COUNT(*) FROM " . static::$table . " WHERE quote_id=$quote_id");
			$this->db->bind(':quote_id', $quote_id);
			return $this->db->resultSet();

		}






	}