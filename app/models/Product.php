<?php 
	class Product extends Crud {

		protected static $table = 'products';

		/*--------------------------------------------------------------------*/		

		// find product by username
		public function find_name($product){
			$this->db->query('SELECT * FROM products WHERE name = :product');
			$this->db->bind(':product', $product);

			$row = $this->db->singleRow();

			// check row 
			if($this->db->rowCount() > 0 ){
				return true;
			} else {
				return false;
			}
		}
		/*--------------------------------------------------------------------*/

		// Add User
		// public function create($data){
		// 	return $this->db->insertBind($data);
		// }
		/*--------------------------------------------------------------------*/
		// public function update($data){
		// 	return $this->db->updateBind($data);
		// }
		/*--------------------------------------------------------------------*/
		// public function delete($id){
		// 	return $this->db->deleteBind($id);
		// }

		/*--------------------------------------------------------------------*/

		// public function getList(){
		// 	// $this->db->query("SELECT * FROM $table");
		// 	// return $this->db->resultSet();

		// 	return $this->db->selectAll();
		// }
		/*--------------------------------------------------------------------*/

		// public function getById($id){
		// 	// $this->db->query("SELECT * FROM $table");
		// 	// return $this->db->resultSet();

		// 	$results = $this->db->SelectByID($id);
		// 	return $results;
		// }
		/*--------------------------------------------------------------------*/

		public function count($id){
			return $this->db->countByID($id);

		}
		/*--------------------------------------------------------------------*/

		public function countByName($name){
			return $this->db->countByColumnName($name);
		}
		/*--------------------------------------------------------------------*/

		// public function lastInsertId(){
		// 	return $this->db->lastInsertId();
		// }
		/*--------------------------------------------------------------------*/
		public function getProductDetails($name, $type){
			$this->db->query("SELECT id, name, part_number, description, selling_price FROM products where UPPER($type) LIKE '%".strtoupper($name)."%'");
			$row = $this->db->resultSet();
			return $row;
		}







	} // end class
