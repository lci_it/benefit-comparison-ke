<?php

	class CompareDetail extends Crud {

		protected static $table = 'comparedetail';

		public function checkSessionProduct($created_by, $product_id){
			$this->db->query("SELECT * FROM  " . static::$table . " WHERE created_by=:created_by AND product_id=:product_id");
			$this->db->bind(':created_by', $created_by);
			$this->db->bind(':product_id', $product_id);
			return $this->db->resultSet();
		}

		public function removeProduct($created_by, $product_id){
			$this->db->query("DELETE FROM  " . static::$table . " WHERE created_by=:created_by AND product_id=:product_id LIMIT 1");
			$this->db->bind(':created_by', $created_by);
			$this->db->bind(':product_id', $product_id);
			if($this->db->execute()){
				return true;
			} else {
				return false;
			}
		}

		// On saving a quote or comparison, delete records from comparedetail table
		public function deleteFromCompare($created_by, $product_id){
			$this->db->query("DELETE FROM  " . static::$table . " WHERE created_by=:created_by AND product_id=:product_id LIMIT 1");
			$this->db->bind(':created_by', $created_by);
			$this->db->bind(':product_id', $product_id);
			if($this->db->execute()){
				return true;
			} else {
				return false;
			}

		}





	} 
	// end of calls