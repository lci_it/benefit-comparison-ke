<?php 
	class User extends Crud {

		protected static $table = 'users';

		/*--------------------------------------------------------------------*/		

		// find user by username
		public function find_user($username){
			$this->db->query("SELECT * FROM " . static::$table . " WHERE email = :user");
			$this->db->bind(':user', $username);

			$row = $this->db->singleRow();

			// check row 
			if($this->db->rowCount() > 0 ){
				return true;
			} else {
				return false;
			}
		}
		/*--------------------------------------------------------------------*/

		// Verify user login details
		public function verify_login($name, $password){
			$this->db->query("SELECT * FROM users WHERE email = :name");
			$this->db->bind(':name', $name);

			$row = $this->db->singleRow();

			//get hashed password from database
			$hashed_password = $row->password;

			if(password_verify($password, $hashed_password)){
				return $row;
			} else {
				return false;
			}
		}
		/*--------------------------------------------------------------------*/

		// public function getById($id){
			// $this->db->query("SELECT * FROM $table");
			// return $this->db->resultSet();

		// 	$results = $this->db->SelectByID($id);
		// 	return $results;
		// }
		/*--------------------------------------------------------------------*/

		public function count($id){
			return $this->db->countByID($id);

		}
		/*--------------------------------------------------------------------*/

		// public function countByName($name){
		// 	return $this->countByColumnName($name);
		// }
		/*--------------------------------------------------------------------*/

		public function lastInsertId(){
			return $this->db->lastInsertId();
		}


	// public function countByColumnName($name){
	// 			$row = $this->db->query("SELECT * FROM " . static::$table . " WHERE name = ?");
	// 			$this->db->execute([$name]);
	// 			return $count = $row->rowCount();

	// 		// $this->bind(':id', $id);
	// 		// $this->resultSet();
	// 		// return $this->rowCount();
	// 	}




	} // end class
