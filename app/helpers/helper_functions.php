<?php

	// redirect page
	function redirect($page){
		header('location: ' . URLROOT . $page);
	}
	/*--------------------------------------------------------------------*/

	function dropDownList($column_name){
		$database = new Crud();
		$output = "";
		$values = $database->getDropdownList($column_name);
		foreach($values as $list){
			$output .= "<option value='$list->id'>" . $list->$column_name . "</option>";											
			}
		return $output;
	}
	/*--------------------------------------------------------------------*/

	function dropdownValue($id, $column_name){
		if($id !=''){
			$database = new Crud();
			$row = $database->getDropdownValue($id, $column_name);
			return $row->$column_name;
		}
		
	}
	/*--------------------------------------------------------------------*/

	// function timeZone(){
	// 	$output = "";
	// 	$database = new Database();		
	// 	$timezones = $database->selectAllFromTable('timezones');
	// 	foreach($timezones as $time){
	// 		$output .= "<option value='$time->id'>" . $time->name . "</option>";
	// 	}
	// 	return $output;
	// }
	/*--------------------------------------------------------------------*/

	function getListFromTable($table){
		$output = '';
		$database = new Crud();
		$list = $database->selectAllFromTable($table);
		foreach($list as $record){
			$output .= "<option value='$record->id'>" . $record->name . "</option>";											
		}
		return $output;
	}
	/*--------------------------------------------------------------------*/
	
	function findByColumnName($table, $name){
		$database = new Database();
		$row = $database->SelectByColumnName($table, $name);
		return $row->name;
	}
	/*--------------------------------------------------------------------*/

	function findName($table, $id){
		$database = new Crud();
		$row = $database->SelectByTableID($table, $id);
		if($row) {
			return $row->name;
		}
	}
	/*--------------------------------------------------------------------*/

	// function showImage($image){
	// 	$url = URLROOT . 'img/';
	// 	$image_placeholder = 'dummy-img.png';
	// 	return empty($image) || $image == NULL ? $url . $image_placeholder : $url . $image;
	// 	// return $url . $image;
	// }
	/*--------------------------------------------------------------------*/

	function get_url(){
		return Core::getUrl();
	}
	/*--------------------------------------------------------------------*/

	function current_path($class_name){
		return URLROOT . current_class($class_name);
	}
	/*--------------------------------------------------------------------*/
	function current_method(){
		return Controller::getUrlMethod();
	}

	/*--------------------------------------------------------------------*/

	function current_class($class_name){
		return strtolower(get_class($class_name));
	}
	/*--------------------------------------------------------------------*/

	function TimeZoneList(){
		$output = '';
		$database = new Crud();
		$list = $database->selectAllFromTable('timezones');
		foreach($list as $record){
			$output .= "<option value='$record->id'>" . $record->name . "</option>";
		}
		return $output;
	}
	/*--------------------------------------------------------------------*/
	
	function timeZoneDesc($table, $id){
		$database = new Crud();
		$row = $database->SelectByTableID($table, $id);
		return $row->timezone;
	}	
	/*--------------------------------------------------------------------*/
		
	function UserTimeZone($date){
	 	$date_entered = new DateTime($date, new DateTimeZone('UTC'));
	 	$date_entered->setTimezone(new DateTimeZone($_SESSION['user_time_zone']));
	 	$date_entered = $date_entered->format('d-m-Y h:i:s A');

	 	if(!empty($date) && isset($date)){
	 		return  $date_entered;
	 	} else {
	 		return null;
	 	}
	}
	/*--------------------------------------------------------------------*/

	function show_yes_no($field_value){
		if ($field_value == 1){
			echo "Yes";
		} else{
			echo "No";
		}
	}

	/*--------------------------------------------------------------------*/

	function fetchCategoryTree($parent = 0, $spacing = '', $user_tree_array = '') {
	 	$database = new Database;

		  if (!is_array($user_tree_array))
		    $user_tree_array = array();
		  $sql = "SELECT `id`, `name`, `parent_category_id` FROM `product_categories` WHERE 1 AND `parent_category_id` = $parent AND deleted = 0 ORDER BY id ASC";

		  $database->query($sql);
		  $database->execute();

		  $count = $database->rowCount();
		  $result = $database->resultSet();


		  // var_dump($count);

		  // if($count > 0){
		  // 	$row = $database->resultSet();
		  // 		echo $row[0]->name;

		  	

		  // }

		  if ($count > 0) {
		  	
		    foreach ($result as $row ) {
		      $user_tree_array[] = array("id" => $row->id, "name" => $spacing . $row->name);
		      $user_tree_array = fetchCategoryTree($row->id, $spacing . '&nbsp;&nbsp;', $user_tree_array);
		    // echo "hello";  
		    }
		  }
		  return $user_tree_array;
	}

	/*--------------------------------------------------------------------*/

	function getCategoryDropdown(){
		$categories = fetchCategoryTree();

		$output = '';
		foreach($categories as $category) {
			$output .= '<option value="'.$category["id"].'">'.$category["name"].'</option>';
		}
		return $output;
	}
	/*--------------------------------------------------------------------*/

	function validityDate($day){
		return date('Y-m-d', strtotime(" + $day days"));
	}
	/*--------------------------------------------------------------------*/

	function mark_invoice_status($status){
		switch ($status) {
			case '1':
				echo "green";
				break;

			case '2':
				echo "red";
				break;
			
			case '3':
				echo "purple";
				break;
			
			default:
				echo "red";
				break;
		}

	}
	/*--------------------------------------------------------------------*/

	function dateDiff($due_on){
		$due_date = (date("d-m-Y",strtotime(UserTimeZone($due_on))) );
		$date1=date_create($due_date);
		$date2=date_create(date('d-m-Y'));
		if(!empty($due_on)){
			$diff=date_diff($date1,$date2);
		return $diff->format("%R%a days");
		}
	}

   	/*--------------------------------------------------------------------*/

	function fetchExpenseCategories($parent = 0, $spacing = '', $user_tree_array = '') {
	 	$database = new Database;

		  if (!is_array($user_tree_array))
		    $user_tree_array = array();
		  $sql = "SELECT `id`, `name`, `parent_category_id` FROM `expensecategory` WHERE 1 AND `parent_category_id` = $parent AND deleted = 0 ORDER BY id ASC";

		  $database->query($sql);
		  $database->execute();

		  $count = $database->rowCount();
		  $result = $database->resultSet();


		  // var_dump($count);

		  // if($count > 0){
		  // 	$row = $database->resultSet();
		  // 		echo $row[0]->name;

		  	

		  // }

		  if ($count > 0) {
		  	
		    foreach ($result as $row ) {
		      $user_tree_array[] = array("id" => $row->id, "name" => $spacing . $row->name);
		      $user_tree_array = fetchExpenseCategories($row->id, $spacing . '&nbsp;&nbsp;', $user_tree_array);
		    // echo "hello";  
		    }
		  }
		  return $user_tree_array;
	}

	/*--------------------------------------------------------------------*/

	function getExpenseCategories(){
		$categories = fetchExpenseCategories();

		$output = '';
		foreach($categories as $category) {
			$output .= '<option value="'.$category["id"].'">'.$category["name"].'</option>';
		}
		return $output;
	}
	/*--------------------------------------------------------------------*/

	function nullEmptyInt($inputVal){
	    if($inputVal == '' || empty($inputVal)){
	        return null;
	    }else{
	        return (int)($inputVal);
	    }
	}
	/*--------------------------------------------------------------------*/

	function getReceiptInvoices($id){
		$database = new Crud();
		return $result = $database->SelectAllByTableColumn('receiptdetail', $id, 'receipt_id');
		
	}
	/*--------------------------------------------------------------------*/

	// getProducts function Starts

	function getProducts(){

		/// getProducts function Code Starts ///

		$database = new Database;

		$aWhere = array();

		/// Manufacturers Code Starts ///

		if(isset($_REQUEST['man'])&&is_array($_REQUEST['man'])){

			foreach($_REQUEST['man'] as $sKey=>$sVal){

				if((int)$sVal!=0){
					$aWhere[] = 'manufacturer_id='.(int)$sVal;
				}
			}
		}

		/// Manufacturers Code Ends ///

		$per_page=50;

		if(isset($_GET['page'])){

		$page = $_GET['page'];

		}else {

		$page=1;

		}

		$start_from = ($page-1) * $per_page ;

		$sLimit = " order by 1 DESC LIMIT $start_from,$per_page";

		$sWhere = (count($aWhere)>0?' WHERE '.implode(' or ',$aWhere):'').$sLimit;

		$get_products = "select * from products  ".$sWhere;

		$database->query($get_products);

		$database->execute();

		$result = $database->resultSet();

		foreach($result as $row_products){

		$pro_id = $row_products ->id;

		$pro_title = $row_products->name;

		$pro_price = $row_products->selling_price;

		$pro_img1 = $row_products->image;

		echo "

		<div class='col-md-4 col-sm-6 center-responsive' >

		<div class='product' >

		<a href='details.php?pro_id=$pro_id' >

		<img src='admin_area/product_images/$pro_img1' class='img-responsive' >

		</a>

		<div class='text' >

		<h3>$pro_title</h3>

		<p class='price' > $pro_price </p>

		<p class='buttons' >

		<a href='details.php?pro_id=$pro_id' class='btn btn-default' >View details</a>

		<a href='details.php?pro_id=$pro_id' class='btn btn-primary'>

		<i class='fa fa-shopping-cart' ></i> Add To Cart

		</a>

		</p>

		</div>

		</div>

		</div>

		";

		}

		/// getProducts function Code Ends ///

	} 

// getProducts function Ends

	function getProductsTable(){

		/// getProducts function Code Starts ///

		$database = new Database;
		$aWhere = array();
		$bWhere = array();
		$cWhere = array();
		$dWhere = array();
		$sql = '';

		/// Manufacturers Code Starts ///

		if(isset($_REQUEST['man'])&&is_array($_REQUEST['man'])){

			foreach($_REQUEST['man'] as $sKey=>$sVal){

				if((int)$sVal!=0){
					$aWhere[] = 'manufacturer_id='.(int)$sVal;
				}
			}
		}
		/// Manufacturers Code Ends ///

		/// Maternity Code Starts ///
		if(isset($_REQUEST['p_cat'])&&is_array($_REQUEST['p_cat'])){

			foreach($_REQUEST['p_cat'] as $sKey=>$sVal){
				if($sVal!=''){
					$sVal2 = ' Like "Covered"';
					$bWhere[] = $sVal.$sVal2;
				}
			}
		}
		/// Maternity Code Ends ///

		/// Category Individual Code Starts ///
		if(isset($_REQUEST['plantypeInd'])&&is_array($_REQUEST['plantypeInd'])){

			foreach($_REQUEST['plantypeInd'] as $sKey=>$sVal){
				if($sVal!=''){
					$sVal2 = ' Like "2"';
					$cWhere[] = $sVal.$sVal2;
				}
			}
		}
		/// Category Individual Code Ends ///


		/// Category Group Code Starts ///
		if(isset($_REQUEST['plantypeGrp'])&&is_array($_REQUEST['plantypeGrp'])){

			foreach($_REQUEST['plantypeGrp'] as $sKey=>$sVal){
				if($sVal!=''){
					$sVal2 = ' Like "3"';
					$cWhere[] = $sVal.$sVal2;
				}
			}
		}
		/// Category Group Code Ends ///

		/// dha Code Starts ///
	/*	if(isset($_REQUEST['dha'])&&is_array($_REQUEST['dha'])){

			foreach($_REQUEST['dha'] as $sKey=>$sVal){
				if($sVal!=''){
					$sVal2 = ' Like "DHA"';
					$cWhere[] = $sVal.$sVal2;
				}
			}
		}
		/// dha Code Ends ///


		/// haad Code Starts ///
		if(isset($_REQUEST['haad'])&&is_array($_REQUEST['haad'])){

			foreach($_REQUEST['haad'] as $sKey=>$sVal){
				if($sVal!=''){
					$sVal2 = ' Like "HAAD"';
					$cWhere[] = $sVal.$sVal2;
				}
			}
		}
		/// haad Code Ends ///
		*/
		
		
		//print_r($_REQUEST);
		

		/// WWExUSA Code Starts ///
		if(isset($_REQUEST['uae'])&&is_array($_REQUEST['uae'])){

			foreach($_REQUEST['uae'] as $sKey=>$sVal){
				if($sVal!=''){
					$sVal2 = ' Like "UAE"';
					$cWhere[] = $sVal.$sVal2;
				}
			}
		}
		/// WWExUSA Code Ends ///

		/// Plantype Code Starts ///
		// if(isset($_REQUEST['plantype'])&&is_array($_REQUEST['plantype'])){

		// 	foreach($_REQUEST['plantype'] as $sKey=>$sVal){
		// 		if($sVal!=''){
		// 			$sVal2 = ' Like "1"';
		// 			$dWhere[] = $sVal.$sVal2;
		// 		}
		// 	}
		// }
		/// Plantype Code Ends ///

		/// Euro Code Starts ///
		if(isset($_REQUEST['euro'])&&is_array($_REQUEST['euro'])){

			foreach($_REQUEST['euro'] as $sKey=>$sVal){
				if($sVal!=''){
					$sVal2 = ' Like "3"';
					$cWhere[] = $sVal.$sVal2;
				}
			}
		}
		/// Euro Code Ends ///


		/// usd Code Starts ///
		if(isset($_REQUEST['usd'])&&is_array($_REQUEST['usd'])){

			foreach($_REQUEST['usd'] as $sKey=>$sVal){
				if($sVal!=''){
					$sVal2 = ' Like "1"';
					$cWhere[] = $sVal.$sVal2;
				}
			}
		}
		/// usd Code Ends ///

		/// gbp Code Starts ///
		if(isset($_REQUEST['gbp'])&&is_array($_REQUEST['gbp'])){

			foreach($_REQUEST['gbp'] as $sKey=>$sVal){
				if($sVal!=''){
					$sVal2 = ' Like "2"';
					$cWhere[] = $sVal.$sVal2;
				}
			}
		}
		/// gbp Code Ends ///

		/// gbp Code Starts ///
		if(isset($_REQUEST['kes'])&&is_array($_REQUEST['kes'])){

			foreach($_REQUEST['kes'] as $sKey=>$sVal){
				if($sVal!=''){
					$sVal2 = ' Like "4"';
					$cWhere[] = $sVal.$sVal2;
				}
			}
		}
		/// gbp Code Ends ///





		$per_page=14;

		if(isset($_GET['page'])){
			$page = $_GET['page'];
		}else {
			$page=1;
		}

		$start_from = ($page-1) * $per_page ;

		$sLimit = " order by 1 DESC LIMIT $start_from,$per_page";

		$sWhere = (count($aWhere)>0?' WHERE ' . '(' .implode(' or ',$aWhere) . ')':'');


		if($sWhere ){
			$tWhere = (count($bWhere)>0?' AND ' . implode(' AND ',$bWhere):'');
		} else {
			$tWhere = (count($bWhere)>0?' WHERE ' . implode(' or ',$bWhere):'');
		}

		if($sWhere || $tWhere){
			$uWhere = (count($cWhere)>0?' AND ' . implode(' AND ',$cWhere):'');
		} else {
			$uWhere = (count($cWhere)>0?' WHERE ' . implode(' AND ',$cWhere):'');
		}

		$sql .= $sWhere;
		$sql .= $tWhere;
		$sql .= $uWhere;

		// print_r($sql);
		// print_r($sWhere);

		$get_products = "select * from products ".$sql;

		 // print_r($get_products);

		$database->query($get_products);

		$database->execute();

		$result = $database->resultSet();

		echo "
		<table class='table table-hover table-striped table-bordered custom-height' id='comparison_table'>
			<thead style='background: #542989 !important; color: #fff;'>
				<tr style='background: #542989 !important; color: #fff; text-transform: uppercase;'>
					<th style='text-align: left; font-weight: bold; vertical-align: middle; min-width:175px !important; height:45px;'>Service Provider</th>";

					foreach($result as $record) {
						$manufacturer = findName('manufacturer', $record->manufacturer_id);
						echo "<th style='background: none; font-weight: bold; vertical-align: middle; text-align: center; width: 200px;'>$manufacturer</th>";
					}
				echo "</tr>
				<tr style='background: #542989 !important; color: #fff; text-transform: uppercase;'>
					<th style='text-align: left; font-weight: bold; vertical-align: middle; width:200px; height:50px;'>Plan</th>";

					foreach($result as $record) {

						echo "<th style='font-weight: bold; vertical-align: middle; text-align: center;'><p>$record->name</p>
						<div class='alert alert-success alert-dismissible success' id='success' style='display:none;''>
						  <a href='#'' class='close' data-dismiss='alert' aria-label='close'>×</a>
						</div>
						
						<form id='fupForm' name='form1' method='post'>
							<input type='hidden' name='product_id' value='$record->id' id='product_id' class='product_id'/>";
							if($_SESSION['plan'] == 'addplan'){
								$quote = $_SESSION['quote_id'];
								echo "<input type='hidden' name='quote_id' value='$quote' id='quote_id' class='quote_id'/>";
								echo "<input type='button' class='btn btn-warning btn-xs addplan' name='addplan' value='Add To Ref# $quote' id='addplan'/>
								</form>
								</th>";
							
							} else {
								echo "<input type='button' class='btn btn-warning btn-xs butsave' name='save' value='Add' id='butsave'/>
								</form>
								</th>";
							}

							

					}
				echo "</tr>
			</thead>
				<tbody>
					<tr>
						<td style='text-align: left; font-weight: bold;'>Currency</td>";

						foreach($result as $record) {
							echo "<td style='text-align: center;'>" . dropdownValue($record->currency_id, 'currency') . "</td>";      
						}						

					echo "</tr>
				<tbody>
					<tr>
						<td style='text-align: left; font-weight: bold;'>Annual limit</td>";

						foreach($result as $record) {
							echo "<td style='text-align: center;'>$record->annual_cover</td>";      
						}						

					echo "</tr>

					<tr>
						<td style='text-align: left; font-weight: bold;'>Area of Cover</td>";

						foreach($result as $record) {
							echo "<td style='text-align: center;'>$record->area_of_cover</td>";       
						}
						
					echo "</tr><tr>
						<td style='text-align: left; font-weight: bold;'>Emergency Treatment Outside Area of Cover</td>";

						foreach($result as $record) {
							echo "<td style='text-align: center;'>$record->emergency_treatment_outside_area_of_cover</td>";
						}

						echo "</tr>

						<tr>
						<td style='text-align: left; font-weight: bold;'>Congenital Conditions</td>";

						foreach($result as $record) {
							echo "<td style='text-align: center;'>$record->congenital_conditions </td>";
						}

						echo "</tr>

						<tr>
						<td style='text-align: left; font-weight: bold;'>Direct Settlement</td>";

						foreach($result as $record) {
							echo "<td style='text-align: center;'>$record->network </td>";
						}

						echo "</tr>


						<tr>
						<td style='text-align: left; font-weight: bold;'>Underwriting Criteria (FMU / MHD)</td>";

						foreach($result as $record) {
							echo "<td style='text-align: center;'>$record->underwriting_critera </td>";
						}

						echo "</tr>

						<tr>
						<td style='text-align: left; font-weight: bold;'>Pricing Model (Experience Rated / Pool Rated / Blended)</td>";

						foreach($result as $record) {
							echo "<td style='text-align: center;'>$record->pricing_model </td>";
						}

						echo "</tr>

						<tr>
						<td style='text-align: left; font-weight: bold; color: #fff; background:#00aedb;' colspan='100%'>IN PATIENT TREATMENT</td>
						</tr>
						<tr>
						<td style='text-align: left; font-weight: bold;'>Deductible / Co-insurance / Excess</td>";

						foreach($result as $record) {
							echo "<td style='text-align: center;'>$record->ip_deductible </td>";
						}

						echo "</tr>

						<tr>
						<td style='text-align: left; font-weight: bold;'>Room & Board</td>";

						foreach($result as $record) {
							echo "<td style='text-align: center;'>$record->ip_room_board </td>";
						}

						echo "</tr>

						<tr>
						<td style='text-align: left; font-weight: bold;'>Specialist Fees</td>";

						foreach($result as $record) {
							echo "<td style='text-align: center;'>$record->ip_specialist_fees </td>";
						}

						echo "</tr>

						<tr>
						<td style='text-align: left; font-weight: bold;'>Specialist Fees</td>";

						foreach($result as $record) {
							echo "<td style='text-align: center;'>$record->ip_specialist_fees </td>";
						}

						echo "</tr>

						<tr>
						<td style='text-align: left; font-weight: bold;'>Surgery and Anesthesia</td>";

						foreach($result as $record) {
							echo "<td style='text-align: center;'>$record->ip_surgery_anesthesia </td>";
						}

						echo "</tr>

						<tr>
						<td style='text-align: left; font-weight: bold;'>Diagnostic Tests</td>";

						foreach($result as $record) {
							echo "<td style='text-align: center;'>$record->ip_diagnostic_tests </td>";
						}

						echo "</tr>

						<tr>
						<td style='text-align: left; font-weight: bold;'>Prescribed Medication</td>";

						foreach($result as $record) {
							echo "<td style='text-align: center;'>$record->ip_prescribed_medication </td>";
						}

						echo "</tr>

						<tr>
						<td style='text-align: left; font-weight: bold;'>Cancer Treatment </td>";

						foreach($result as $record) {
							echo "<td style='text-align: center;'>$record->ip_cancer_treatment </td>";
						}

						echo "</tr>

						<tr>
						<td style='text-align: left; font-weight: bold;'>Organ Transplant</td>";

						foreach($result as $record) {
							echo "<td style='text-align: center;'>$record->ip_organ_transplant </td>";
						}

						echo "</tr>

						<tr>
						<td style='text-align: left; font-weight: bold;'>Prosthetic Device</td>";

						foreach($result as $record) {
							echo "<td style='text-align: center;'>$record->ip_prosthetic_device </td>";
						}

						echo "</tr>

						<tr>
						<td style='text-align: left; font-weight: bold;'>Rehabilitation</td>";

						foreach($result as $record) {
							echo "<td style='text-align: center;'>$record->ip_rehabilitation </td>";
						}

						echo "</tr>

						<tr>
						<td style='text-align: left; font-weight: bold;'>Home Nursing (immediately after or instead of hospitalization)</td>";

						foreach($result as $record) {
							echo "<td style='text-align: center;'>$record->ip_home_nursing </td>";
						}

						echo "</tr>

						<tr>
						<td style='text-align: left; font-weight: bold;'>Parent / Companion Accomodation</td>";

						foreach($result as $record) {
							echo "<td style='text-align: center;'>$record->ip_companian_accomodation </td>";
						}

						echo "</tr>

						<tr>
						<td style='text-align: left; font-weight: bold;'>In-Patient Cash Benefit</td>";

						foreach($result as $record) {
							echo "<td style='text-align: center;'>$record->ip_cashbenenit </td>";
						}

						echo "</tr>

						<tr>
						<td style='text-align: left; font-weight: bold; color: #fff; background:#00aedb;' colspan='100%'>OUT PATIENT TREATMENT</td>
						</tr>

						<tr>
						<td style='text-align: left; font-weight: bold;'>Annual Limit</td>";
						foreach($result as $record) {
							echo "<td style='text-align: center;'>$record->op_annual_limit </td>";
						}
						echo "</tr>

						<tr>
						<td style='text-align: left; font-weight: bold;'>Deductible / Co-insurance / Excess</td>";
						foreach($result as $record) {
							echo "<td style='text-align: center;'>$record->op_deductible </td>";
						}
						echo "</tr>

						<tr>
						<td style='text-align: left; font-weight: bold;'>General Practioner Fees</td>";
						foreach($result as $record) {
							echo "<td style='text-align: center;'>$record->op_gp_fees </td>";
						}
						echo "</tr>

						<tr>
						<td style='text-align: left; font-weight: bold;'>Specialist Fees</td>";
						foreach($result as $record) {
							echo "<td style='text-align: center;'>$record->op_specialist_fees </td>";
						}
						echo "</tr>

						<tr>
						<td style='text-align: left; font-weight: bold;'>Diagnostic Tests</td>";
						foreach($result as $record) {
							echo "<td style='text-align: center;'>$record->op_diagnostic_tests </td>";
						}
						echo "</tr>

						<tr>
						<td style='text-align: left; font-weight: bold;'>Prescribed Medication</td>";
						foreach($result as $record) {
							echo "<td style='text-align: center;'>$record->op_prescribed_medication </td>";
						}
						echo "</tr>

						<tr>
						<td style='text-align: left; font-weight: bold;'>Physiotherapy</td>";
						foreach($result as $record) {
							echo "<td style='text-align: center;'>$record->op_physiotherapy </td>";
						}
						echo "</tr>

						<tr>
						<td style='text-align: left; font-weight: bold;'>Alternative Treatment</td>";
						foreach($result as $record) {
							echo "<td style='text-align: center;'>$record->op_alternative_treatment </td>";
						}
						echo "</tr>

						<tr>
						<td style='text-align: left; font-weight: bold;'>Cancer Treatment </td>";
						foreach($result as $record) {
							echo "<td style='text-align: center;'>$record->op_cancer_treatment </td>";
						}
						echo "</tr>
						

						<tr>
						<td style='text-align: left; font-weight: bold; color: #fff; background:#00aedb;' colspan='100%'>MATERNITY COVER</td>
						</tr>


						<tr>
						<td style='text-align: left; font-weight: bold;'>C-Section</td>";
						foreach($result as $record) {
							echo "<td style='text-align: center;'>$record->maternity_c_section </td>";
						}
						echo "</tr>

						<tr>
						<td style='text-align: left; font-weight: bold;'>Complication</td>";
						foreach($result as $record) {
							echo "<td style='text-align: center;'>$record->maternity_complication </td>";
						}
						echo "</tr>

						<tr>
						<td style='text-align: left; font-weight: bold;'>New Born Cover</td>";
						foreach($result as $record) {
							echo "<td style='text-align: center;'>$record->maternity_new_born </td>";
						}
						echo "</tr>

						<tr>
						<td style='text-align: left; font-weight: bold; color: #fff; background:#00aedb;' colspan='100%'>ADDITIONAL BENEFITS</td>
						</tr>

						<tr>
						<td style='text-align: left; font-weight: bold;'>Dental</td>";
						foreach($result as $record) {
							echo "<td style='text-align: center;'>$record->dental </td>";
						}
						echo "</tr>

						<tr>
						<td style='text-align: left; font-weight: bold;'>Optical</td>";
						foreach($result as $record) {
							echo "<td style='text-align: center;'>$record->optical </td>";
						}
						echo "</tr>

						<tr>
						<td style='text-align: left; font-weight: bold;'>Annual Health Check-up / Wellness Screening</td>";
						foreach($result as $record) {
							echo "<td style='text-align: center;'>$record->wellness </td>";
						}
						echo "</tr>

						

						<tr>
						<td style='text-align: left; font-weight: bold;'>Vaccinations (other including Travel Vaccinations)</td>";
						foreach($result as $record) {
							echo "<td style='text-align: center;'>$record->vaccinations_others_travel </td>";
						}
						echo "</tr>

						<tr>
						<td style='text-align: left; font-weight: bold; color: #fff; background:#00aedb;' colspan='100%'>EVACUATION  & REPATRIATION</td>
						</tr>

						<tr>
						<td style='text-align: left; font-weight: bold;'>Evacuation</td>";
						foreach($result as $record) {
							echo "<td style='text-align: center;'>$record->evacuation </td>";
						}
						echo "</tr>

						<tr>
						<td style='text-align: left; font-weight: bold;'>Repatriation</td>";
						foreach($result as $record) {
							echo "<td style='text-align: center;'>$record->repatriation </td>";
						}
						echo "</tr>

						
						<tr>
						<td style='text-align: left; font-weight: bold; color: #fff; background:#00aedb;' colspan='100%'>PREMIUM SUMMARY</td>
						</tr>

						<tr>
						<td style='text-align: left; font-weight: bold;'>Total No of Members (Emp + Dependents)</td>";
						foreach($result as $record) {
							echo "<td style='text-align: center;'>$record->total_members </td>";
						}
						echo "</tr>

						<tr>
						<td style='text-align: left; font-weight: bold;'>Premium Payment Mode</td>";
						foreach($result as $record) {							
							echo "<td style='text-align: center;'>$record->payment_mode</td>";
						}
						echo "</tr>

						<tr>
						<td style='text-align: left; font-weight: bold;'>Expiring Premium</td>";
						foreach($result as $record) {
							echo "<td style='text-align: center;'>$record->expiring_premium </td>";
						}
						
						echo "</tr>
						<tr class='bg-color-blueLight'>

						<td style='text-align: left; font-weight: bold;'>Proposed Annual / Renewal Premium (excluding VAT)</td>";
						foreach($result as $record) {
							echo "<td style='text-align: center;'><input style='width: 90px !important; text-align: center;' type='text' name='premium' value='$record->proposed_annual_renewal_premium'></td>";
						}
					echo "</tr>

					<tr>
						<td style='text-align: left; font-weight: bold;'>% Increase</td>";
						foreach($result as $record) {							
							echo "<td style='text-align: center;'>$record->increase</td>";
						}
						echo "</tr>

				</tbody>
		</table>";	

	}
	//  end

	// checkCovered Start
	function checkCovered ($value){
		switch ($value) {
			case 'Covered':
				echo "<i class='fa-lg fa fa-check txt-color-green'>";
				break;

			case 'Not Covered':
				echo "<i class='fa fa-lg fa-times txt-color-red'></i>";
				break;
			
			default:
				echo "$value";
				break;
		}
	}
	// checkCovered End

	function getProduct($id){

		$database = new Crud();
		return $result = $database->SelectAllByTableColumn('products', $id, 'id');
	}

	// --------------------------------------------------------//

	function currency($number, $string){		
	    $formatter = new NumberFormatter('en_US', NumberFormatter::CURRENCY);
	    return $formatter->formatCurrency($number, $string);
	}
	// --------------------------------------------------------//

	function findUsersReportsTo($session_reports_to, $user_tree_array='0') {
	 	$database = new Database;

		  if (!is_array($user_tree_array))
		    $user_tree_array = array();
		  $sql = "SELECT id FROM `users` WHERE `reports_to` = '$session_reports_to' AND deleted = 0 ORDER BY id ASC";

		  $database->query($sql);
		  $database->execute();

		  $count = $database->rowCount();
		  $result = $database->resultSet();

		  // print_r($result);

		  if($count > 0){
		  	foreach ($result as $row) {
		  		$user_tree_array[]=array("id" => $row->id);				
				$user_tree_array = findUsersReportsTo($row->id,$user_tree_array);
				// $row->id . "<br>";
		  	}

		  }

		  return $user_tree_array;
	}

	// --------------------------------------------------------//

	function findReportsTo($cid){
		$database = new Database;

		$cids=findUsersReportsTo($cid);

		$cids = array_map(function($cids){ return $cids['id']; }, $cids);
		
		$str = implode(',', $cids);

		if(empty($str)){
			$sql = "SELECT * FROM `quotes` WHERE `created_by` IN ($cid) AND deleted = 0 ORDER BY id ASC";
			print_r($sql);

		} else {
			$sql = "SELECT * FROM `quotes` WHERE `created_by` IN ($cid, $str) AND deleted = 0 ORDER BY id ASC";
			print_r($sql);

		}


		// print_r($str);

		// $cids[]=$str;

		// echo "<pre>";

		// print_r($cids);
	
		// $cids_str=implode(",",$cids);

		// var_dump($cids_str);


		  // if (!is_array($user_tree_array))
	    	// $user_tree_array = array();
		  

		  

		  $database->query($sql);
		  $database->execute();

		  $count = $database->rowCount();
		  return $result = $database->resultSet();


	}


