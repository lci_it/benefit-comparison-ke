<?php
require_once(APPROOT . DS . 'libraries' .DS . 'Database.php');

class Session {

  protected $db;

  public function __construct(){
   $this->db = new Database;
    // $this->db->table = strtolower('quotes');

  } // end of __construct

  public function _open(){  
    // If successful  
    if($this->db){  
    // Return True  
    return true;  
    }  
    // Return False  
    return false;  
  }


  public function _close(){  
  // Close the database connection  
  // If successful  
  if($this->db = null){  
  // Return True  
  return true;  
  }  
  // Return False  
  return false;  
  } 


 //  public function _read($id){

 //    if (!$this->db) {
 //        $this->db = new Database;
 //      }

 //     $sql = "SELECT data from sessions where id=:id";
 //     $this->db->query($sql);
 //      $this->db->bind(':id', $id);
 //      $row = $this->db->singleRow();

 //      if ($row) {

 //        $result = (array)$row;

 //        print_r($result);
       
 //        $data = $result;
 //        return $data;
 //      }
 //      else {
 //        return '';  //  return empty string if no data returned
 //      }
 // }

 public function _read($id){

   if (!$this->db) {
        $this->db = new Database;
      }

    // Set query
    $this->db->query("SELECT data FROM sessions WHERE id = :id");
    // Bind the Id
    $this->db->bind(':id', $id);
    // Attempt execution
    // If successful
    if($this->db->execute()){
      // Save returned row
      $row = $this->db->singleRow();

      $row = (array)$row;

      // Return the data
      if (is_null($row['data'])) {
        return '';
      }
      return $row['data'];
    }
  }


 public function _write($id, $data){  
    // Create time stamp  
    $access = time();

    // Set query  
    $this->db->query("REPLACE INTO sessions VALUES (:id, :access, :data)");

    // Bind data  
    $this->db->bind(":id", $id);  
    $this->db->bind(":access", $access);  
    $this->db->bind(":data", $data);

    // Attempt Execution  
    // If successful  
    if($this->db->execute()){  
    // Return True  
    return true;  
    }

    // Return False  
    return false;  
}


public function _destroy($id){  
// Set query  
$this->db->query("DELETE FROM sessions WHERE id = :id");

// Bind data  
$this->db->bind(":id", $id);

// Attempt execution  
// If successful  
if($this->db->execute()){  
// Return True  
return true;  
}

// Return False  
return false;  
}



public function _gc($max){  
 // Calculate what is to be deemed old  

 $old = time()-$max;

 // Set query  
 $this->db->query("DELETE * FROM sessions WHERE access < :old");

 // Bind data  
 $this->db->bind(":old", $old);

 // Attempt execution  
 if($this->db->execute()){  
 // Return True  
 return true;  
 }

 // Return False  
 return false;  
 }  





} // end of class

$sess = new Session();
  session_set_save_handler(
    array($sess,'_open'),
    array($sess,'_close'),
    array($sess,'_read'),
    array($sess,'_write'),
    array($sess,'_destroy'),
    array($sess,'_gc')
    );
  register_shutdown_function('session_write_close');
  session_start();

/*-----------------------------------------------------------------*/

// session_start();

$message;

  function isLoggedIn(){
    if(isset($_SESSION['user_id'])){
      return true;
    } else {
      return false;
    }
  }
  /*-----------------------------------------------------------------*/

  function checkLogin(){
    if(!isLoggedIn()){
      redirect ('login');
      exit;
    }
  }

  /*-----------------------------------------------------------------*/

  function print_messages(){
    $types = array("success", "danger", "warning", "info", "tip");

    foreach($types as $type){
      if(isset($_SESSION[$type]) && !empty($_SESSION[$type]) && is_array($_SESSION[$type])){
        foreach($_SESSION[$type] as $msg) {
          $shout = strtoupper($type);
          echo "<div id='print_messages' class='alert alert-$type' style='margin-left: 7px; margin-right: 7px; margin-bottom: 2px;'><div style='font-weight: bold; float: left;'> $msg </div><div class='alert-close text-right'><button style='background: none;'> x </button></div></div>";
        }
        unset($_SESSION[$type]);
      }
    }
  }

  function print_upload_msgs($msg){
    if(isset($_SESSION[$msg]) && !empty($_SESSION[$msg]) && is_array($_SESSION[$msg])){
      foreach ($_SESSION[$msg] as $key=>$value) {
        foreach($value as $upload_msg){
          echo "<div id='print_messages' class='alert alert-info' style='margin-left: 7px; margin-right: 7px; margin-bottom: 2px;'><div style='font-weight: bold; float: left;'> $upload_msg </div><div class='alert-close text-right'><button style='background: none;'> x </button></div></div>";
        }
      }
      unset($_SESSION[$msg]);
    }
  }


  /*-----------------------------------------------------------------*/
  // Flash message helper
  // function flash($name = '', $message = '', $class = 'alert alert-success'){
  //   if(!empty($name)){
  //     //No message, create it
  //     if(!empty($message) && empty($_SESSION[$name])){
  //       if(!empty( $_SESSION[$name])){
  //           unset( $_SESSION[$name]);
  //       }
  //       if(!empty( $_SESSION[$name.'_class'])){
  //           unset( $_SESSION[$name.'_class']);
  //       }
  //       $_SESSION[$name] = $message;
  //       $_SESSION[$name.'_class'] = $class;
  //     }
  //     //Message exists, display it
  //     elseif(!empty($_SESSION[$name]) && empty($message)){
  //       $class = !empty($_SESSION[$name.'_class']) ? $_SESSION[$name.'_class'] : 'success';
  //       echo '<div class="'.$class.'" id="msg-flash">'.$_SESSION[$name].'</div>';
  //       unset($_SESSION[$name]);
  //       unset($_SESSION[$name.'_class']);
  //     }
  //   }
  // }