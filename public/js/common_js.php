<!-- COMMON JAVA SCRIPTS IN ALL PAGES -->

<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
		<!-- <script data-pace-options='{ "restartOnRequestAfter": true }' src="js/plugin/pace/pace.min.js"></script> -->

		<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo ASSETURL; ?>js/libs/jquery-2.1.1.min.js"><\/script>');
			}
		</script>

		<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo ASSETURL; ?>js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>

		<!-- PPTXGEN Export HTML to PowerPoint -->
		<script src="https://cdn.jsdelivr.net/npm/pptxgenjs@3.3.1/dist/pptxgen.bundle.js"></script>

		<!-- JSPDF -->
		<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.debug.js"></script> -->
		<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.min.js"></script> -->
		
		<!-- HTML2PDF -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.9.2/html2pdf.bundle.js"></script>

		<!-- JSPDF -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.debug.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>

		<script type="text/javascript">
			function genPDF() {
				var quotes = document.getElementById('invoice');
				 var pdf = new jsPDF('p', 'pt', 'letter');
				html2canvas(quotes, {
					onrendered: function (canvas) {

						for (var i = 0; i <= quotes.clientHeight / 980; i++) {
		                    //! This is all just html2canvas stuff
		                    var srcImg = canvas;
		                    var sX = 0;
		                    var sY = 1100 * i; // start 1100 pixels down for every new page
		                    var sWidth = 1200;
		                    var sHeight = 1100;
		                    var dX = 0;
		                    var dY = 0;
		                    var dWidth = 1000;
		                    var dHeight = 1100;

		                    window.onePageCanvas = document.createElement("canvas");
		                    onePageCanvas.setAttribute('width', 900);
		                    onePageCanvas.setAttribute('height', 1100);
		                    var ctx = onePageCanvas.getContext('2d');
		                    // details on this usage of this function: 
		                    // https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Using_images#Slicing
		                    ctx.drawImage(srcImg, sX, sY, sWidth, sHeight, dX, dY, dWidth, dHeight);

		                    // document.body.appendChild(canvas);
		                    var canvasDataURL = onePageCanvas.toDataURL("image/png", 1.0);

		                    var width = onePageCanvas.width;
		                    var height = onePageCanvas.clientHeight;

		                    //! If we're on anything other than the first page,
		                    // add another page
		                    if (i > 0) {
		                        pdf.addPage(612, 791); //8.5" x 11" in pts (in*72)
		                    }
		                    //! now we declare that we're working on that page
		                    pdf.setPage(i + 1);
		                    //! now we add content to that page!
		                    pdf.addImage(canvasDataURL, 'JPEG', 20, 20, (width * .62), (height * .62));


		                }
		                pdf.save('test.pdf');




						// var img = canvas.toDataURL("image/png");
						// var doc = new jsPDF('p', 'pt', 'letter');
						// doc.addImage(img, 'JPEG',20,20);
						// doc.save('test.pdf');

					}
				});
			};

			
		</script>





		<script type="text/javascript" src="<?php echo ASSETURL; ?>js/jquery.price_format.2.0.js"></script>

		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo ASSETURL; ?>js/app.config.js"></script>

		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo ASSETURL; ?>js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 

		<!-- BOOTSTRAP JS -->
		<script src="<?php echo ASSETURL; ?>js/bootstrap/bootstrap.min.js"></script>

		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo ASSETURL; ?>js/notification/SmartNotification.min.js"></script>

		<!-- JARVIS WIDGETS -->
		<script src="<?php echo ASSETURL; ?>js/smartwidgets/jarvis.widget.min.js"></script>

		<!-- EASY PIE CHARTS -->
		<script src="<?php echo ASSETURL; ?>js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

		<!-- SPARKLINES -->
		<script src="<?php echo ASSETURL; ?>js/plugin/sparkline/jquery.sparkline.min.js"></script>

		<!-- JQUERY VALIDATE -->
		<script src="<?php echo ASSETURL; ?>js/plugin/jquery-validate/jquery.validate.min.js"></script>

		  <!-- include the Tools -->
		<!--  <script src="js/jquery.tools.min.js"></script> -->

		<script src="<?php echo ASSETURL; ?>js/plugin/modernizr/modernizr-custom.js"></script>

		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo ASSETURL; ?>js/plugin/masked-input/jquery.maskedinput.min.js"></script>

		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo ASSETURL; ?>js/plugin/select2/select2.min.js"></script>

		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo ASSETURL; ?>js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>

		<!-- browser msie issue fix -->
		<script src="<?php echo ASSETURL; ?>js/plugin/msie-fix/jquery.mb.browser.min.js"></script>

		<!-- FastClick: For mobile devices -->
		<script src="<?php echo ASSETURL; ?>js/plugin/fastclick/fastclick.min.js"></script>

		<!--[if IE 8]>

		<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

		<![endif]-->

		<!-- Demo purpose only -->
		<!-- <script src="js/demo.min.js"></script> -->

		<!-- MAIN APP JS FILE -->
		<script src="<?php echo ASSETURL; ?>js/app.min.js"></script>

		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->
		<!-- <script src="js/speech/voicecommand.min.js"></script> -->

		<!-- SmartChat UI : plugin -->
		<!-- <script src="js/smart-chat-ui/smart.chat.ui.min.js"></script> -->
		<!-- <script src="js/smart-chat-ui/smart.chat.manager.min.js"></script> -->

		<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo ASSETURL; ?>js/libs/jquery-2.1.1.min.js"><\/script>');
			}
		</script>

		<!-- ##################################################### -->

		<script type="text/javascript" src="<?php echo ASSETURL; ?>js/script.js"></script>

		<!-- <script type="text/javascript" src="js/jquery.maskMoney.min.js"></script> -->

					<!-- INVOICE PAGE RELATED PLUGIN(S) <script src="..."></script>-->

		<script type="text/javascript">
		
		// DO NOT REMOVE : GLOBAL FUNCTIONS!
		
		$(document).ready(function() {
			
			pageSetUp();

		})

		</script>

		<!-- END INVOICE PAGE RELATED PLUGIN(S) <script src="..."></script>-->



		<!-- PAGE RELATED PLUGIN(S) 
		<script src="..."></script>-->
		<script src="<?php echo ASSETURL; ?>js/plugin/datatables/jquery.dataTables.min.js"></script>
		<script src="<?php echo ASSETURL; ?>js/plugin/datatables/dataTables.colVis.min.js"></script>
		<script src="<?php echo ASSETURL; ?>js/plugin/datatables/dataTables.tableTools.min.js"></script>
		<script src="<?php echo ASSETURL; ?>js/plugin/datatables/dataTables.bootstrap.min.js"></script>
		<script src="<?php echo ASSETURL; ?>js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
		<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script> -->

		<!-- <script src="js/plugin/bootstrapvalidator/bootstrapValidator.min.js"></script> -->
		
		<script type="text/javascript">

			$(document).ready(function() {
			 	
				/* DO NOT REMOVE : GLOBAL FUNCTIONS!
				 *
				 * pageSetUp(); WILL CALL THE FOLLOWING FUNCTIONS
				 *
				 * // activate tooltips
				 * $("[rel=tooltip]").tooltip();
				 *
				 * // activate popovers
				 * $("[rel=popover]").popover();
				 *
				 * // activate popovers with hover states
				 * $("[rel=popover-hover]").popover({ trigger: "hover" });
				 *
				 * // activate inline charts
				 * runAllCharts();
				 *
				 * // setup widgets
				 * setup_widgets_desktop();
				 *
				 * // run form elements
				 * runAllForms();
				 *
				 ********************************
				 *
				 * pageSetUp() is needed whenever you load a page.
				 * It initializes and checks for all basic elements of the page
				 * and makes rendering easier.
				 *
				 */
				
				 pageSetUp();
				 
				/*
				 * ALL PAGE RELATED SCRIPTS CAN GO BELOW HERE
				 * eg alert("my home function");
				 * 
				 * var pagefunction = function() {
				 *   ...
				 * }
				 * loadScript("js/plugin/_PLUGIN_NAME_.js", pagefunction);
				 * 
				 * TO LOAD A SCRIPT:
				 * var pagefunction = function (){ 
				 *  loadScript(".../plugin.js", run_after_loaded);	
				 * }
				 * 
				 * OR
				 * 
				 * loadScript(".../plugin.js", run_after_loaded);
				 */

				/* // DOM Position key index //
				
					l - Length changing (dropdown)
					f - Filtering input (search)
					t - The Table! (datatable)
					i - Information (records)
					p - Pagination (paging)
					r - pRocessing 
					< and > - div elements
					<"#id" and > - div with an id
					<"class" and > - div with a class
					<"#id.class" and > - div with an id and class
					
					Also see: http://legacy.datatables.net/usage/features
				*/	

				/* BASIC ;*/
					var responsiveHelper_dt_basic = undefined;
					var responsiveHelper_datatable_fixed_column = undefined;
					var responsiveHelper_datatable_col_reorder = undefined;
					var responsiveHelper_datatable_tabletools = undefined;
					
					var breakpointDefinition = {
						tablet : 1024,
						phone : 480
					};

				/* END BASIC */
				
				/* COLUMN FILTER  */
			    var otable = $('#datatable_fixed_column').DataTable({
			    	//"bFilter": false,
			    	//"bInfo": false,
			    	//"bLengthChange": false
			    	//"bAutoWidth": false,
			    	//"bPaginate": false,
			    	//"bStateSave": true // saves sort state using localStorage
					"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
							"t"+
							"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
					"autoWidth" : true,
					"oLanguage": {
						"sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
					},
					"preDrawCallback" : function() {
						// Initialize the responsive datatables helper once.
						if (!responsiveHelper_datatable_fixed_column) {
							responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper($('#datatable_fixed_column'), breakpointDefinition);
						}
					},
					"rowCallback" : function(nRow) {
						responsiveHelper_datatable_fixed_column.createExpandIcon(nRow);
					},
					"drawCallback" : function(oSettings) {
						responsiveHelper_datatable_fixed_column.respond();
					}		
				
			    });
			    
			    // custom toolbar
			    $("div.toolbar").html('<div class="text-right"><img src="img/logo.png" alt="SmartAdmin" style="width: 111px; margin-top: 3px; margin-right: 10px;"></div>');
			    	   
			    // Apply the filter
			    $("#datatable_fixed_column thead th input[type=text]").on( 'keyup change', function () {
			    	
			        otable
			            .column( $(this).parent().index()+':visible' )
			            .search( this.value )
			            .draw();
			            
			    });
			    /* END COLUMN FILTER */   

				/* TABLETOOLS */
			$('#datatable_tabletools').dataTable({
				"pageLength": 20,
				
				// Tabletools options: 
				//   https://datatables.net/extensions/tabletools/button_options
				"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 col-lg-8'f><'col-sm-6 col-xs-6 col-lg-4 hidden-xs'T>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
				"oLanguage": {
					"sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
				},
		        "oTableTools": {
		        	 "aButtons": [
		        	  {
                    "sExtends":    "text",
                    "sButtonText": "Add New",
                    "fnClick": function ( nButton, oConfig, oFlash ) {
                    	window.location = "<?php echo URLROOT . current_class($this) . '/' . 'add'; ?>"
                    }
                },
		             "copy",
		             "csv",
		             "xls",
		                {
		                    "sExtends": "pdf",
		                    "sTitle": "LinkAdmin_PDF",
		                    "sPdfMessage": "LinkAdmin PDF Export",
		                    "sPdfSize": "letter"
		                },
		             	{
	                    	"sExtends": "print",
	                    	"sMessage": "Generated by Link CRM Admin <i>(press Esc to close)</i>"
	                	}
		             ],
		            "sSwfPath": "<?php echo ASSETURL; ?>js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
		        },
				"autoWidth" : true,
				"preDrawCallback" : function() {
					// Initialize the responsive datatables helper once.
					if (!responsiveHelper_datatable_tabletools) {
						responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#datatable_tabletools'), breakpointDefinition);
					}
				},
				"rowCallback" : function(nRow) {
					responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
				},
				"drawCallback" : function(oSettings) {
					responsiveHelper_datatable_tabletools.respond();
				}
			});

			 $('<div id="bulkOptionsContainer" class="pull-left col-xs-8 col-sm-8 col-lg-4">' +
			 	'<select class="form-control" name="bulk_options" id="bulkoptionsid">'+
			 	'<option value="">Select Optoin</option>'+
			 	'<option value="deleteall">Delete</option>'+
			 	'<option value="clone">Duplicate</option>'+
			 	'</select>' + 
			 	 
			 	'</div><div><input type="submit" name="submit" value="Apply" class="btn btn-info pull-left" style="width: 5em;"></div>').appendTo("#datatable_tabletools_wrapper .dataTables_filter");
			 $(".dataTables_filter label").addClass("pull-left");
			
			/* END TABLETOOLS */
			


			// $('#togglingForm').bootstrapValidator({
			// 		feedbackIcons : {
			// 			valid : 'glyphicon glyphicon-ok',
			// 			invalid : 'glyphicon glyphicon-remove',
			// 			validating : 'glyphicon glyphicon-refresh'
			// 		},

			// 		fields : {
			// 			supplier_code : {
			// 				validators : {
			// 					notEmpty : {
			// 						message : 'supplier code is required'
			// 					}
			// 				}
			// 			},

			// 			supplier_name : {
			// 				validators : {
			// 					notEmpty : {
			// 						message : 'supplier name is required'
			// 					}
			// 				}
			// 			},

			// 			payment_mode : {
			// 				validators : {
			// 					notEmpty : {
			// 						message : 'payment mode is required'
			// 					}
			// 				}
			// 			}

			// 			// 'product_name-[]': {
   //    //               validators: {
   //    //                   notEmpty: {
   //    //                       message: 'product name required'
   //    //                   },
                        
   //    //               }
   //    //           }
						
	

			// 		} // end of fields


			// 	}).find('button[data-toggle]').on('click', function() {
			// 		var $target = $($(this).attr('data-toggle'));
			// 		// Show or hide the additional fields
			// 		// They will or will not be validated based on their visibilities
			// 		$target.toggle();
			// 		if (!$target.is(':visible')) {
			// 			// Enable the submit buttons in case additional fields are not valid
			// 			$('#togglingForm').data('bootstrapValidator').disableSubmitButtons(false);
			// 		}

			// });

			// 	// end toggle form

			})

			
		
		</script>

		<script type="text/javascript">
		  $(document).ready(function(){
		    $(document).on('click', '.wei-add-expense-button', function(){
		      
		      var countrows = $('#items_table tr').length;
		      counter = countrows + 1;

		      var url_path = window.location.href.split("=")[1];

		       if($("#bill_ref").attr("name") === 'bill_ref' ) {
		        var newRow = $('<tr id="'+counter+'"><td class="col-md-1 col-xs-9 hidden"><input type="text" id="j_table_id-'+ counter+'" name="j_table_id-[]" placeholder="id" class="form-control input-xs wei-add-field id ' + counter + '" value=""></td><td class="col-md-2 col-xs-9"><select id="category_id-'+ counter+'" name="category_id-[]" class="form-control itemsel input-xs wei-add-field category ' + counter + '" required><option value="">Select Option</option><?php echo getExpenseCategories();?></select></td><td class="col-md-3 col-xs-9"><textarea class="form-control input-xs wei-add-field description '+ counter+'" rows="2" id="description-'+ counter+'" name="description-[]" placeholder="Description"></textarea></td><td class="col-md-1 col-xs-9 hidden"><input type="text" id="property_id-'+ counter+'" name="property_id-[]" placeholder="Property ID" class="form-control itemsel input-xs wei-add-field property_id ' + counter + '" value="0" required></td><td class="col-md-1 col-xs-9"><input type="currency" id="unit_price-'+ counter+'" name="unit_price-[]" placeholder="Price" class="form-control input-xs wei-add-field unit-price ' + counter + '" required></td><td class="col-md-1 col-xs-9"><input type="number" id="product_qty-'+ counter+'" name="product_qty-[]" placeholder="Qty" class="form-control itemqty input-xs wei-add-field quantity ' + counter + '" required></td><td class="col-md-2 col-xs-9"><input type="currency" id="line_total-'+ counter+'" name="line_total-[]" placeholder="0.00" class="form-control input-xs wei-add-field price-total ' + counter + '"></td><td class="col-md-2 col-xs-9"><input type="hidden" id="delete-'+counter+'" name="delete-[]" value="0" class="input-xs wei-add-field delete '+counter+'"><a class="fa fa-trash-o btn btn-danger" href="#"> x</a></td></tr>');

		      }

		      $('table.order-details').append(newRow);

		      $('#unit_price-'+counter).priceFormat({prefix: '', thousandsSeparator: ''});

		      //================== start autocomplete project_id  ====================//

		      $("#project_id-"+counter).autocomplete({

		        source: function( request, response ) {
		          $.ajax({
		            url : './js/ajax.php',
		            dataType: "json",
		            method: 'post',
		            data: {
		              name_startsWith: request.term,
		              type: 'project_t.id'
		            },
		            success: function( data ) {
		              response( $.map( data, function( item ) {
		                var code = item.split("|");
		                  return {
		                    label: code[0],
		                    value: code[0],
		                    data : item
		                  }
		                }));
		              }
		            });
		        },

		        autoFocus: true,          
		        minLength: 1,
		        select: function( event, ui ) {
		            //$('#myButton').show();
		          var names = ui.item.data.split("|");
		          id_arr = $(this).attr('id');
		          id = id_arr.split("-");
		          $(this).val(names[0]);
		          $("#project_id-"+id[1]).val(names[1]);
		         
		        },


		      }); //end of autocomplete

		      //================== start autocomplete property_name- in purchase module  ====================//

		      $("#property_name-"+counter).autocomplete({

		        source: function( request, response ) {
		          $.ajax({
		            url : './js/ajax.php',
		            dataType: "json",
		            method: 'post',
		            data: {
		              name_startsWith: request.term,
		              type: 'property_t.property_name'
		            },
		            success: function( data ) {
		              response( $.map( data, function( item ) {
		                var code = item.split("|");
		                  return {
		                    label: code[1],
		                    value: code[1],
		                    data : item
		                  }
		                }));
		              }
		            });
		        },

		        autoFocus: true,          
		        minLength: 1,
		        select: function( event, ui ) {
		            //$('#myButton').show();
		          var names = ui.item.data.split("|");
		          id_arr = $(this).attr('id');
		          id = id_arr.split("-");
		          $(this).val(names[1]);
		          $("#property_id-"+id[1]).val(names[0]);
		         
		        },


		      }); //end of autocomplete property_name- in purchase module

		      // ==========================  start auto complete flat details in purchase module ==================//

		      $(".pull_flats-"+counter).on( "autocompletechange", function(event,ui) {

		        var prop_id = $("#property_id-"+counter).val();
		        $("#flat_id-"+counter).empty();
		        $("#flat_id-"+counter).append("<option value=''>Select</option>");

		        $.ajax({
		          url: '.js/ajax.php',
		          type: 'post',
		          data: {depart:prop_id},
		          dataType: 'json',
		          
		          success:function(response){
		            var len = response.length;

		            for( var i = 0; i<len; i++){
		              var id = response[i]['id'];

		              var name = response[i]['name'];
		              var flat_status = response[i]['flat_status'];
		              $("#flat_id-"+counter).append("<option value='"+id+"'>"+name+"</option>");
		            }

		          } // end success function

		        });
		   
		      });

		      // ==============================  end flat details in purchase module =============================

		      //================== END autocomplete   ====================//

		    });

			
			//================== add to compare script start   ====================//
			$(document).on('click', '.butsave', function() {
			  // $(this).css( "background-color", "green");							    
			    var product_id = $(this).parent().parent().find("input[name=product_id]").val();
				// var email = $('#email').val();
				// var phone = $('#phone').val();
				// var city = $('#city').val();
				// alert(product_id);
				if(product_id!=""){
					$.ajax({
						url: "ajax/addtocompare",
						type: "POST",
						data: {
							product_id: product_id,
							// email: email,
							// phone: phone,
							// city: city				
						},
						cache: false,
						success: function(dataResult){
							var dataResult = JSON.parse(dataResult);
							if(dataResult.statusCode==200){
								// $("#butsave").removeAttr("disabled");
								// $('#fupForm').find('input:text').val('');
								// $(".success").show();
								// $('.success').html('Added to Compare !'); 						
							}
							else if(dataResult.statusCode==201){
							   alert("Already Added !");
							} else {
								alert("Maximum Plans Added !");
							}
							
						}
					});
				}
				else{
					alert('Please fill all the field !');
				}
			});
			//================== add to compare script end   ====================//

			//================== addplan to compare script end   ====================//

			$(document).on('click', '.addplan', function() {
			  // $(this).css( "background-color", "green");							    
			    var product_id = $(this).parent().parent().find("input[name=product_id]").val();
				var quote_id = $(this).parent().parent().find("input[name=quote_id]").val();
				// var phone = $('#phone').val();
				// var city = $('#city').val();
				// alert(product_id);
				if(product_id!=""){
					$.ajax({
						url: "ajax/addtoexisting",
						type: "POST",
						data: {
							product_id: product_id,
							quote_id: quote_id,
							// phone: phone,
							// city: city				
						},
						cache: false,
						success: function(dataResult){
							var dataResult = JSON.parse(dataResult);
							if(dataResult.statusCode==200){
								// $("#butsave").removeAttr("disabled");
								// $('#fupForm').find('input:text').val('');
								// $(".success").show();
								// $('.success').html('Added to Compare !'); 						
							}
							else if(dataResult.statusCode==201){
							   alert("Already Added !");
							} else {
								alert("Maximum Plans Added !");
							}
							
						}
					});
				}
				else{
					alert('Please fill all the field !');
				}
			});
			//================== addplan to compare script end   ====================//

			// Remove from compare start
			$(document).on('click', '.remove', function() {
			  // $(this).css( "background-color", "green");							    
			    var product_id = $(this).parent().parent().find("input[name=product_id]").val();
				// var email = $('#email').val();
				// var phone = $('#phone').val();
				// var city = $('#city').val();
				// alert(product_id);
				if(product_id!=""){
					$.ajax({
						url: "ajax/removeFromCompare",
						type: "POST",
						data: {
							product_id: product_id,
							// email: email,
							// phone: phone,
							// city: city				
						},
						cache: false,
						success: function(dataResult){
							var dataResult = JSON.parse(dataResult);
							if(dataResult.statusCode==200){
								// $("#butsave").removeAttr("disabled");
								// $('#fupForm').find('input:text').val('');
								// $(".success").show();
								// $('.success').html('Added to Compare !');
								window.location.reload();
							}
							else if(dataResult.statusCode==201){
							   alert("Not Deleted !");
							}
							
						}
					});
				}
				else{
					alert('Please fill all the field !');
				}
			});
			// Remove from compare end

			//================== Remove from Edit screen start   ====================//

			$(document).on('click', '.deleteFromQuote', function() {
			  // $(this).css( "background-color", "green");							    
			    var product_id = $(this).parent().parent().find("input[name=product_id]").val();
				var quote_id = $(this).parent().parent().find("input[name=quote_id]").val();
				// var phone = $('#phone').val();
				// var city = $('#city').val();
				// alert(product_id);
				if(product_id!=""){
					$.ajax({
						url: "ajax/deleteFromQuote",
						type: "POST",
						data: {
							product_id: product_id,
							quote_id: quote_id,
							// email: email,
							// phone: phone,
							// city: city				
						},
						cache: false,
						success: function(dataResult){
							var dataResult = JSON.parse(dataResult);
							if(dataResult.statusCode==200){
								// $("#butsave").removeAttr("disabled");
								// $('#fupForm').find('input:text').val('');
								// $(".success").show();
								// $('.success').html('Added to Compare !');
								window.location.reload();
							}
							else if(dataResult.statusCode==201){
							   alert("Not Deleted !");
							}
							
						}
					});
				}
				else{
					alert('Please fill all the field !');
				}
			});
			//================== Remove from Edit screen end   ====================//



		  });
		// end of Document.REady

 


		</script>